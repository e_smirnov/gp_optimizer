#include <windows.h>
#include <d3d9.h>
#include <d3dx9.h>

// ���������� ������ ������ ... 
#define MY_VERTEX_FORMAT (D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1)

struct SMyVertex
{
  D3DXVECTOR3 pos;		// ��������� ������� � ������������
  D3DXVECTOR3 norm;		// �������
  FLOAT tu, tv;			// ���������� ���������
};

// ���������� ���������� ...
HWND				g_hWnd;						// ���������� ����
LPDIRECT3D9			g_pd3d			= NULL;		// ��������� �� ��������� Direct3D
LPDIRECT3DDEVICE9	g_pd3dDevice	= NULL;		// ���������� ������ Direct3D -)
LPDIRECT3DTEXTURE9	g_pTexture		= NULL;		// ��������� �� ��������.
LPDIRECT3DVERTEXBUFFER9 g_pVB		= NULL;		// �������� �����.
LPDIRECT3DPIXELSHADER9       pixelShader = NULL; //PS (NEW)

// ������������� ...
bool Init()
{
	HRESULT hr;

	// ������� Direct3D ...
	g_pd3d = Direct3DCreate9(D3D_SDK_VERSION);
	if (!g_pd3d) return false;

	// ��� �������� ������� ��������� ���������� ����� �������� � ���� ...
	// ������� �������� �������� ������ ������ �������� ...
	D3DDISPLAYMODE d3ddm;
	if (FAILED (g_pd3d->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &d3ddm)))
		return false;

	D3DPRESENT_PARAMETERS d3dpp;
	ZeroMemory(&d3dpp, sizeof(d3dpp));
	d3dpp.Windowed = true;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.BackBufferFormat = d3ddm.Format;
	
	// ������� ���������� Direct3DDevice
	if (FAILED(g_pd3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, g_hWnd, 
		D3DCREATE_HARDWARE_VERTEXPROCESSING, &d3dpp, &g_pd3dDevice)))
		return false;

	// ����� ���� ��� �� ��������� ������������� Direct3D ��������� � �������� ������� ...
	
	// ��������� �������� �� ����� tex1.bmp, �������������� � ������� ��������
	if (FAILED(D3DXCreateTextureFromFile(g_pd3dDevice, "Tex1.bmp", &g_pTexture)))
		return false;

    // ������� ��������� �����.
    if( FAILED( g_pd3dDevice->CreateVertexBuffer( 6*sizeof(SMyVertex),
                                                  0, MY_VERTEX_FORMAT,
                                                  D3DPOOL_DEFAULT, &g_pVB, NULL ) ) )
        return false;

  // ��������� ��������� �����. ��� ������ ������� ������������� ����������
	// ���������� (tu, tv), ������� ����� � ��������� �� 0.0 �� 1.0
	SMyVertex* vertices;
    if( FAILED( g_pVB->Lock( 0, 0, (void**)&vertices, 0 ) ) )
        return false;

	vertices[0].pos = D3DXVECTOR3(-1.0, 1.0, 0.0);
	vertices[0].norm = D3DXVECTOR3(0.0, 0.0, -1.0);
	vertices[0].tu = 0.0f;
	vertices[0].tv = 0.0f;
	
	vertices[1].pos = D3DXVECTOR3(1.0, 1.0, 0.0);
	vertices[1].norm = D3DXVECTOR3(0.0, 0.0, -1.0);	
	vertices[1].tu = 1.0f;
	vertices[1].tv = 0.0f;
	
	vertices[2].pos = D3DXVECTOR3(-1.0, -1.0, 0.0);
	vertices[2].norm = D3DXVECTOR3(0.0, 0.0, -1.0);	
	vertices[2].tu = 0.0f;
	vertices[2].tv = 1.0f;

	vertices[3].pos = D3DXVECTOR3(-1.0, -1.0, 0.0);
	vertices[3].norm = D3DXVECTOR3(0.0, 0.0, -1.0);	
	vertices[3].tu = 0.0f;
	vertices[3].tv = 1.0f;	
	
	vertices[4].pos = D3DXVECTOR3(1.0, 1.0, 0.0);
	vertices[4].norm = D3DXVECTOR3(0.0, 0.0, -1.0);	
	vertices[4].tu = 1.0f;
	vertices[4].tv = 0.0f;

	vertices[5].pos = D3DXVECTOR3(1, -1.0, 0.0);
	vertices[5].norm = D3DXVECTOR3(0.0, 0.0, -1.0);	
	vertices[5].tu = 1.0f;
	vertices[5].tv = 1.0f;

	g_pVB->Unlock();


  // ��������� ����
  g_pd3dDevice->SetRenderState( D3DRS_LIGHTING, FALSE );

	// ������������� ������� ������������� ...
	D3DXMATRIX matWorld;
	D3DXMatrixRotationY(&matWorld, 0);
	hr = g_pd3dDevice->SetTransform(D3DTS_WORLD, &matWorld);
 	if (FAILED(hr))
		return false;
 	
	D3DXMATRIX matView;
	D3DXMatrixLookAtLH(&matView, &D3DXVECTOR3(0.0f, 0.0f, -5.0f),
								 &D3DXVECTOR3(0.0f, 0.0f, 0.0f),
								 &D3DXVECTOR3(0.0f, 1.0f, 0.0f));
	hr = g_pd3dDevice->SetTransform(D3DTS_VIEW, &matView);
 	if (FAILED(hr))
 		return false;

	D3DXMATRIX matProj;
	D3DXMatrixPerspectiveFovLH(&matProj, D3DX_PI/4, 1.0f, 1.0f, 100.0f );
	hr = g_pd3dDevice->SetTransform(D3DTS_PROJECTION, &matProj);
 	if (FAILED(hr))
 		return false;

	LPD3DXBUFFER                 code = NULL;
	LPD3DXBUFFER                 errors = NULL;
	// set up Pixel Shader (NEW)
	hr = D3DXCompileShaderFromFile("test.hlsl",   //filepath
                                   NULL,          //macro's            
                                   NULL,          //includes           
                                   "main",     //main function      
                                   "ps_2_0",      //shader profile     
                                   0,             //flags              
                                   &code,         //compiled operations
                                   &errors,          //errors
                                   NULL);         //constants
	if(FAILED(hr)) {

		MessageBox(g_hWnd, (LPCSTR)errors->GetBufferPointer(), "Error", MB_TASKMODAL|MB_ICONSTOP);		
	}

	g_pd3dDevice->CreatePixelShader((DWORD*)code->GetBufferPointer(),
                                   &pixelShader);
	code->Release();

	return true;
}

HRESULT RenderFrame()
{
	HRESULT hr;

	// ������� ����� ...
	g_pd3dDevice->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 0, 255), 1.0f, 0);	

	hr = g_pd3dDevice->BeginScene();
	if (FAILED(hr)) return hr;

	// ���������, ����� ����� ������ ���� ������������ ��� ����������.
	//g_pd3dDevice->SetVertexShader(MY_VERTEX_FORMAT);
	g_pd3dDevice->SetFVF(MY_VERTEX_FORMAT);

	// ������������� � ������ ���������� ������� (� �������� stage) ���� ��������.
	g_pd3dDevice->SetTexture(0, g_pTexture);
	
	// ������������� ���������, ������� ���������, ����� ������� ����� ����������������� ��������
	g_pd3dDevice->SetTextureStageState( 0, D3DTSS_COLOROP,   D3DTOP_MODULATE );
	g_pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE );
	g_pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG2, D3DTA_DIFFUSE );
	g_pd3dDevice->SetTextureStageState( 0, D3DTSS_ALPHAOP,   D3DTOP_DISABLE );

	// ������������� ��������� �����, ������� ���� ���������� ...
	g_pd3dDevice->SetStreamSource(0, g_pVB, 0, sizeof(SMyVertex));
	
	//g_pd3dDevice->SetVertexShader(MY_VERTEX_FORMAT);
	g_pd3dDevice->SetFVF(MY_VERTEX_FORMAT);
	
    // ������ ��� �������� =)
	g_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 2);
	g_pd3dDevice->SetPixelShader(pixelShader);

	g_pd3dDevice->EndScene();

	// ������� ��������� ������ �� ����� ...
	g_pd3dDevice->Present(NULL, NULL, NULL, NULL);

	return D3D_OK;
}

// ����������� ������� ...
void Release()
{
	if (g_pTexture) 
		g_pTexture->Release();
	if (pixelShader) {
		pixelShader->Release();
	}
	if (g_pVB)
		g_pVB->Release();
	if (g_pd3dDevice)
		g_pd3dDevice->Release();
	if (g_pd3d)
		g_pd3d->Release();
}

LRESULT WINAPI WindowMsgProc(HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
	switch(msg) 
	{
		case WM_DESTROY:
			Release();
			PostQuitMessage(0);
			break;
	}
  return CallWindowProc( (WNDPROC)DefWindowProc, hWnd, msg, wParam, lParam );
}



// An entry point for Win32 app
int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
	
  const char * szAppName = "Demo";
	WNDCLASS wndclass;
  wndclass.style         = 0;
  wndclass.lpfnWndProc   = (WNDPROC)WindowMsgProc;
  wndclass.cbClsExtra    = 0;
  wndclass.cbWndExtra    = 0;
  wndclass.hInstance     = hInstance;
  wndclass.hIcon         = LoadIcon (hInstance, szAppName);
  wndclass.hCursor       = LoadCursor (NULL,IDC_ARROW);
  wndclass.hbrBackground = (HBRUSH)(COLOR_WINDOW);
  wndclass.lpszMenuName  = szAppName;
  wndclass.lpszClassName = szAppName;


	// Registring class ...
  RegisterClass(&wndclass);

	// Creating window ...
  int nWidth = 640;
  int nHeight = 480;

  g_hWnd = CreateWindow(szAppName, "Texture mapping sample", WS_OVERLAPPEDWINDOW,
                      (GetSystemMetrics(SM_CXSCREEN)-nWidth)/2,
                      (GetSystemMetrics(SM_CYSCREEN)-nHeight)/2,
                      nWidth, nHeight,
                      0, 0, hInstance, NULL);
	if(g_hWnd)
	{
		ShowWindow(g_hWnd,SW_SHOWNORMAL);
		UpdateWindow(g_hWnd);
	}

	if (!Init())
	{
		MessageBox(g_hWnd, "Cannot initialize =(", "Error", MB_TASKMODAL|MB_ICONSTOP);		
		Release();
		PostQuitMessage(0);
	}
	
	MSG messages;
	HRESULT hr;
    
	while(1)
  if(PeekMessage(&messages,NULL,0,0,PM_REMOVE))
  {
    if(messages.message==WM_QUIT) break;
    TranslateMessage(&messages);
    DispatchMessage(&messages);
	}
	else
	{
    // ���������� ���� � "���������" ����� =) ...
    hr = RenderFrame();
		if (FAILED(hr))
		{
			MessageBox(g_hWnd, "An error occured during Rendering scene", "Error", MB_TASKMODAL|MB_ICONSTOP);
			Release();
			PostQuitMessage(0);
			break;
		}
	}

	return 0;
}