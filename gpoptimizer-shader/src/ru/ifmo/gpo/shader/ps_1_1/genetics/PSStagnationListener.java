package ru.ifmo.gpo.shader.ps_1_1.genetics;

import org.jgap.InvalidConfigurationException;
import org.jgap.Population;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ifmo.gpo.core.GPConfiguration;
import ru.ifmo.gpo.core.StagnationMonitor;

/**
 * In case of stagnation in a population, enables additional genetic operators
 */
public class PSStagnationListener implements StagnationMonitor.StagnationListener {

    private ConstantValueMutationOperator mutationOp;

    private static final Logger logger = LoggerFactory.getLogger(PSStagnationListener.class);

    public PSStagnationListener(GPConfiguration conf) throws InvalidConfigurationException {
        mutationOp = new ConstantValueMutationOperator(conf, 25);
        mutationOp.setEnabled(false);
        conf.addGeneticOperator(mutationOp);
    }

    @Override
    public void stagnationDetected(GPConfiguration conf, Population population) {
        logger.info("Stagnation detected, enabling special genetic operators");
        mutationOp.setEnabled(true);
    }

    @Override
    public void stagnationFixed(GPConfiguration conf, Population population) {
        logger.info("Stagnation fixed, disabling special operators");
        mutationOp.setEnabled(false);
    }
}
