package ru.ifmo.gpo.shader.ps_1_1.genetics;

import org.jgap.Gene;
import org.jgap.IChromosome;
import org.jgap.InvalidConfigurationException;
import org.jgap.RandomGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ifmo.gpo.core.CodeGene;
import ru.ifmo.gpo.core.GPConfiguration;
import ru.ifmo.gpo.core.instructions.generic.IGenericInstruction;
import ru.ifmo.gpo.core.instructions.generic.NumericConstant;
import ru.ifmo.gpo.core.operators.BaseSingleChromosomeOperator;
import ru.ifmo.gpo.shader.ps_1_1.instructions.InstructionType;
import ru.ifmo.gpo.shader.ps_1_1.instructions.PSOpcodes;
import ru.ifmo.gpo.shader.ps_1_1.instructions.PixelShaderInstruction;
import ru.ifmo.gpo.shader.ps_1_1.state.PSStateSchema;

import java.util.List;

/**
 * Special mutation operator for pixel shaders. Searches for DEF instructions and changes all constant values.
 */
public class ConstantValueMutationOperator extends BaseSingleChromosomeOperator {

    private static final long serialVersionUID = 2412139101140272425L;

    private boolean isEnabled = false;

    public ConstantValueMutationOperator(GPConfiguration conf, int mutationRate) throws InvalidConfigurationException {
        super(conf, mutationRate);
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    @Override
    public void operate(List population, IChromosome candidate, RandomGenerator generator) {
        if (!isEnabled) {
            return;
        }
        final GPConfiguration conf = (GPConfiguration) getConfiguration();

        for (Gene g : candidate.getGenes()) {
            if (! (g instanceof CodeGene)) {
                throw new IllegalArgumentException("ConstantValueMutationOperator can only operate on chromosomes consisting of CodeGene");
            }
            CodeGene cg = (CodeGene)g;
            IGenericInstruction insn = cg.getInstruction();
            if (! (insn instanceof PixelShaderInstruction)) {
                throw new IllegalArgumentException("ConstantValueMutationOperator can only operate on CodeGene that contain PixelShaderInstruction");
            }
            PixelShaderInstruction pixelShaderInstruction = (PixelShaderInstruction) insn;
            if (pixelShaderInstruction.getOpcodeInternal() != PSOpcodes.DEF) {
                if (pixelShaderInstruction.getOpcodeInternal().getType() == InstructionType.ARITHMETIC) {
                    // all def instructions are placed before any of arithmetic instructions
                    break;
                }
                continue;
            }

            for (int i = 0; i < generator.nextInt(4); ++i) {
                pixelShaderInstruction.getParameters().set(1 + i, new NumericConstant(((PSStateSchema)conf.getStateSchema()).getConstants().getFloatConstant()));
            }
            if (generator.nextBoolean()) {
                break;
            }
        }
        population.add(candidate);
    }
}
