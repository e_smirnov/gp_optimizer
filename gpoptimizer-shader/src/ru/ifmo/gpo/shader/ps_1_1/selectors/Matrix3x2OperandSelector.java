package ru.ifmo.gpo.shader.ps_1_1.selectors;

import com.google.common.collect.Lists;
import ru.ifmo.gpo.core.instructions.IOperandSelector;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.vmstate.NoSuitableOperandFoundException;
import ru.ifmo.gpo.shader.ps_1_1.instructions.PSOpcodes;
import ru.ifmo.gpo.shader.ps_1_1.state.PSRegister;
import ru.ifmo.gpo.shader.ps_1_1.state.PSState;
import ru.ifmo.gpo.shader.ps_1_1.state.PSStateDelta;
import ru.ifmo.gpo.shader.ps_1_1.state.RegisterType;
import ru.ifmo.gpo.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Operand selector for texm3x2tex and texm3x2depth instructions.
 * Selects 3 texture registers in order
 * tn, tm, tm+1, n < m, n for read, m and m+1 for write
 */
public class Matrix3x2OperandSelector implements IOperandSelector<PSOpcodes, PSState, PSStateDelta> {

    @Override
    public List<GenericOperand> selectOperandsRandom(PSOpcodes opcode, PSState state) throws NoSuitableOperandFoundException {

        List<List<GenericOperand>> candidates = new ArrayList<List<GenericOperand>>();
        List<PSRegister> sortedRegisters = new ArrayList<PSRegister>(state.getTextureRegisters().values());

        Collections.sort(sortedRegisters, new Comparator<PSRegister>() {
            @Override
            public int compare(PSRegister o1, PSRegister o2) {
                return o1.getIndex() - o2.getIndex();
            }
        });

        for (int i = 2; i < sortedRegisters.size(); ++i) {
            if (sortedRegisters.get(i).getIndex() - sortedRegisters.get(i - 1).getIndex() != 1) {
                // need 2 registers with consecutive ids
                continue;
            }
            if (sortedRegisters.get(i).isWriteable() && sortedRegisters.get(i - 1).isWriteable()) {
                for (int j = 0; j < i - 1; ++j) {
                    if (sortedRegisters.get(j).isReadable()) {
                        candidates.add(Lists.<GenericOperand>newArrayList(sortedRegisters.get(j), sortedRegisters.get(i - 1), sortedRegisters.get(i)));
                    }
                }
            }
        }
        if (candidates.isEmpty()) {
            throw new NoSuitableOperandFoundException(opcode.getOpcode());
        }
        return CollectionUtils.selectRandomElement(candidates);

    }

    @Override
    public List<GenericOperand> selectOparandsSatisfyRestrictions(PSOpcodes opcode, PSState stateBefore, PSState desiredState, PSStateDelta diffWithDesiredState) throws NoSuitableOperandFoundException {
        List<List<GenericOperand>> candidates = new ArrayList<List<GenericOperand>>();
        List<PSRegister> sortedRegisters = new ArrayList<PSRegister>();

        for (PSRegister reg : diffWithDesiredState.getFullRegisters()) {
            if (reg.getRegisterType() == RegisterType.TEXTURE) {
                sortedRegisters.add(reg);
            }
        }


        Collections.sort(sortedRegisters, new Comparator<PSRegister>() {
            @Override
            public int compare(PSRegister o1, PSRegister o2) {
                return o1.getIndex() - o2.getIndex();
            }
        });

        for (int i = 2; i < sortedRegisters.size(); ++i) {
            if (sortedRegisters.get(i).getIndex() - sortedRegisters.get(i - 1).getIndex() != 1) {
                // need 2 registers with consecutive ids
                continue;
            }
            if (sortedRegisters.get(i).isWriteable() && sortedRegisters.get(i - 1).isWriteable()) {
                for (PSRegister reg : stateBefore.getTextureRegisters().values()) {
                    if (reg.isReadable() && reg.getIndex() < i - 1) {
                        candidates.add(Lists.<GenericOperand>newArrayList(reg, sortedRegisters.get(i - 1), sortedRegisters.get(i)));
                    }
                }
            }
        }
        if (candidates.isEmpty()) {
            throw new NoSuitableOperandFoundException(opcode.getOpcode());
        }
        return CollectionUtils.selectRandomElement(candidates);
    }

    @Override
    public List<List<GenericOperand>> getAllPossibleOperandSets(PSOpcodes opcode, PSState state) throws NoSuitableOperandFoundException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean equals(Object o) {
        return this == o || !(o == null || getClass() != o.getClass());

    }

    @Override
    public int hashCode() {
        return Matrix3x2OperandSelector.class.hashCode();
    }
}
