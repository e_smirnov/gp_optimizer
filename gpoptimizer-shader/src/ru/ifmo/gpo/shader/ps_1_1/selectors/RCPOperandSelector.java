/**
 * Created with IntelliJ IDEA.
 * User: Egor.Smirnov
 * Date: 07.08.12
 * Time: 13:19
 */

package ru.ifmo.gpo.shader.ps_1_1.selectors;

import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.vmstate.NoSuitableOperandFoundException;
import ru.ifmo.gpo.shader.ps_1_1.instructions.PSOpcodes;
import ru.ifmo.gpo.shader.ps_1_1.state.PSRegister;
import ru.ifmo.gpo.shader.ps_1_1.state.PSState;
import ru.ifmo.gpo.shader.ps_1_1.state.PSStateDelta;
import ru.ifmo.gpo.shader.ps_1_1.state.SourceRegisterModifier;
import ru.ifmo.gpo.util.CollectionUtils;

import java.util.List;

/**
 * Selector for RCP and RSQ opcodes. They require one-component swizzle on source reg
 */
public class RCPOperandSelector extends PSRegisterOperandSelector {

    private static final SourceRegisterModifier[] SOURCE_MASKS = {SourceRegisterModifier.SWIZZLE_XXXX, SourceRegisterModifier.SWIZZLE_YYYY, SourceRegisterModifier.SWIZZLE_ZZZZ, SourceRegisterModifier.SWIZZLE_ZZZZ};

    public RCPOperandSelector() {
        super(1);
    }

    private List<GenericOperand> resetSourceMask(List<GenericOperand> rz) {
        PSRegister source = (PSRegister) rz.get(1);
        if (source.getModifiers() != null && source.getModifiers().length == 1 && CollectionUtils.contains(source.getModifiers()[0], SOURCE_MASKS)) {
            // already ok
            return rz;
        }
        rz.set(1, new PSRegister(source, CollectionUtils.selectRandomElement(SOURCE_MASKS)));
        return rz;
    }

    @Override
    public List<GenericOperand> selectOperandsRandom(PSOpcodes opcode, PSState state) throws NoSuitableOperandFoundException {
        return resetSourceMask(super.selectOperandsRandom(opcode, state));
    }

    @Override
    public List<GenericOperand> selectOparandsSatisfyRestrictions(PSOpcodes opcode, PSState stateBefore, PSState desiredState, PSStateDelta diffWithDesiredState) throws NoSuitableOperandFoundException {
        return resetSourceMask(super.selectOparandsSatisfyRestrictions(opcode, stateBefore, desiredState, diffWithDesiredState));
    }
}
