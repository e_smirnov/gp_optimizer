/**
 * User: jedi-philosopher
 * Date: 24.01.12
 * Time: 20:29
 */
package ru.ifmo.gpo.shader.ps_1_1.selectors;

import ru.ifmo.gpo.core.instructions.IOperandSelector;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.instructions.generic.NumericConstant;
import ru.ifmo.gpo.core.vmstate.ConstantStorage;
import ru.ifmo.gpo.core.vmstate.NoSuitableOperandFoundException;
import ru.ifmo.gpo.shader.ps_1_1.instructions.PSOpcodes;
import ru.ifmo.gpo.shader.ps_1_1.state.PSRegister;
import ru.ifmo.gpo.shader.ps_1_1.state.PSState;
import ru.ifmo.gpo.shader.ps_1_1.state.PSStateDelta;
import ru.ifmo.gpo.shader.ps_1_1.state.RegisterType;
import ru.ifmo.gpo.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Selects operands for a DEF instruction that assign constant 4-component vector value to a constant register
 */
public class DefOperandSelector implements IOperandSelector<PSOpcodes, PSState, PSStateDelta> {
    @Override
    public List<GenericOperand> selectOperandsRandom(PSOpcodes opcode, PSState state) throws NoSuitableOperandFoundException {
        List<PSRegister> candidates = new ArrayList<PSRegister>();
        for (PSRegister reg : state.getConstantRegisters().values()) {
            // we should have write access to this reg, but it must contain no value, as def'ing same reg for second time is forbidden
            if (reg.isWriteable() && !reg.isReadable()) {
                candidates.add(reg);
            }
        }
        if (candidates.isEmpty()) {
            throw new NoSuitableOperandFoundException(opcode.getOpcode());
        }
        List<GenericOperand> rz = new ArrayList<GenericOperand>(2);
        rz.add(CollectionUtils.selectRandomElement(candidates));
        addConstantArguments(rz, state);
        return rz;
    }

    @Override
    public List<GenericOperand> selectOparandsSatisfyRestrictions(PSOpcodes opcode, PSState stateBefore, PSState desiredState, PSStateDelta diffWithDesiredState) throws NoSuitableOperandFoundException {
        List<PSRegister> candidates = new ArrayList<PSRegister>();
        for (PSRegister reg : diffWithDesiredState.getFullRegisters()) {
            if (reg.getRegisterType() == RegisterType.CONSTANT) {
                candidates.add(reg);
            }
        }
        if (candidates.isEmpty()) {
            throw new NoSuitableOperandFoundException(opcode.getOpcode());
        }
        List<GenericOperand> rz = new ArrayList<GenericOperand>(2);
        rz.add(CollectionUtils.selectRandomElement(candidates));
        addConstantArguments(rz, stateBefore);
        return rz;
    }

    private void addConstantArguments(List<GenericOperand> rz, PSState state) {
        final ConstantStorage cs = state.getConstants();
        for (int i = 0; i < 4; ++i) {
            rz.add(new NumericConstant(cs.getFloatConstant()));
        }
    }

    @Override
    public List<List<GenericOperand>> getAllPossibleOperandSets(PSOpcodes opcode, PSState state) throws NoSuitableOperandFoundException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean equals(Object o) {
        return this == o || !(o == null || getClass() != o.getClass());
    }

    @Override
    public int hashCode() {
        return DefOperandSelector.class.hashCode();
    }

}
