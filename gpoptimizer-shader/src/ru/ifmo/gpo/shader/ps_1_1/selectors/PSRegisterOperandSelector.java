package ru.ifmo.gpo.shader.ps_1_1.selectors;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import ru.ifmo.gpo.core.instructions.IOperandSelector;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.vmstate.NoSuitableOperandFoundException;
import ru.ifmo.gpo.shader.ps_1_1.instructions.PSOpcodes;
import ru.ifmo.gpo.shader.ps_1_1.state.*;
import ru.ifmo.gpo.util.CollectionUtils;
import ru.ifmo.gpo.util.ProbabilitySet;

import java.util.*;

/**
 * User: jedi-philosopher
 * Date: 19.09.11
 * Time: 18:24
 * <p/>
 * Operand selector for simple pixel shader arithmetic instructions. Such instructions take X full registers as source
 * and store output in a single out registers, writing all its components.
 */
public class PSRegisterOperandSelector implements IOperandSelector<PSOpcodes, PSState, PSStateDelta> {

    private int sourceCount;

    public PSRegisterOperandSelector(int sourceCount) {
        this.sourceCount = sourceCount;
    }

    private static ProbabilitySet<Integer> sourceRegModifierCountChances = new ProbabilitySet<Integer>();

    static {
        sourceRegModifierCountChances.put(0, 1.5);
        sourceRegModifierCountChances.put(1, 0.3);
        sourceRegModifierCountChances.put(2, 0.2);
    }

    public boolean isSuitableAsNextSource(PSOpcodes opcode, PSRegister dest, List<GenericOperand> source, PSRegister sourceCandidate) {
        return true;
    }

    @Override
    public List<GenericOperand> selectOperandsRandom(PSOpcodes opcode, PSState state) throws NoSuitableOperandFoundException {
        List<PSRegister> sourceCandidates = new ArrayList<PSRegister>();
        List<PSRegister> destCandidates = new ArrayList<PSRegister>();

        final List<PSRegister> allRegisters = state.getAllRegisters();
        for (PSRegister psReg : allRegisters) {
            if (psReg.isWriteable()) {
                if (psReg.getRegisterType() == RegisterType.OUTPUT_COLOR) {
                    if (psReg.isReadable()) {
                        // output color registers can be written only once
                        continue;
                    }
                    if (opcode != PSOpcodes.MOV) {
                        // output color registers can only be written with MOV instruction
                        continue;
                    }
                }
                destCandidates.add(psReg);
            }
            if (psReg.isReadable()) {
                sourceCandidates.add(psReg);
            }
        }

        filterRegistersByType(destCandidates, RegisterType.CONSTANT);
        filterRegistersByType(destCandidates, RegisterType.SAMPLER);
        filterRegistersByType(sourceCandidates, RegisterType.SAMPLER); // sampler registers available only for texture operations, that are processed by TextureOperandSelector

        if (destCandidates.isEmpty()) {
            throw new NoSuitableOperandFoundException(opcode.getOpcode());
        }

        List<GenericOperand> result = new ArrayList<GenericOperand>();
        PSRegister dest = CollectionUtils.selectRandomElement(destCandidates);
        if (opcode.getSupportedDestWriteMasks() != null && dest.getRegisterType() != RegisterType.OUTPUT_COLOR) { // output color registers can not have write mask
            // this instruction supports destination register write masks (e.g. add r1.x r2 r3 will only write x component of dest register).
            // Randomly select one of supported masks
            DestinationWriteMask mask = CollectionUtils.selectRandomElement(opcode.getSupportedDestWriteMasks());
            if (mask != DestinationWriteMask.XYZW) {
                result.add(new PSRegister(dest, mask));
            } else {
                // xyzw write mask is the same as no write mask at all
                result.add(dest);
            }
        } else {
            result.add(dest);
        }

        // now must select sourceCount source registers without breaking 'read port limit' attribute
        Multiset<RegisterType> readPortCount = HashMultiset.create();
        if (sourceCandidates.size() < sourceCount) {
            throw new NoSuitableOperandFoundException(opcode.getOpcode());
        }
        Collections.shuffle(sourceCandidates);

        final SourceRegisterModifier[] modifiers = SourceRegisterModifier.values();

        for (PSRegister reg : sourceCandidates) {
            if (!isSuitableAsNextSource(opcode, dest, result, reg)) {
                continue;
            }
            if (readPortCount.count(reg.getRegisterType()) < reg.getRegisterType().getReadPortLimit()) {

                int sourceRegCount = sourceRegModifierCountChances.getRandom();
                boolean success = sourceRegCount > 0;
                if (success) {
                    // add source register modifier in 1/3 cases
                    SourceRegisterModifier mod = CollectionUtils.selectRandomElement(modifiers);
                    if (mod.isCompatible(opcode)) {

                        if (sourceRegCount == 2) {
                            SourceRegisterModifier mod2 = CollectionUtils.selectRandomElement(modifiers);
                            if (mod != mod2 && mod.isCompatible(mod2) && mod2.isCompatible(mod) && mod2.isCompatible(opcode)) {
                                result.add(new PSRegister(reg, mod, mod2));
                            } else {
                                success = false;
                            }
                        } else {
                            result.add(new PSRegister(reg, mod));
                        }
                    } else {
                        success = false;
                    }
                }

                if (!success) {
                    result.add(reg);
                }


                readPortCount.add(reg.getRegisterType());
            }
            if (result.size() == 1 + sourceCount) {
                break;
            }
        }
        if (result.size() != 1 + sourceCount) {
            // failed to find all source registers due to restrictions
            throw new NoSuitableOperandFoundException(opcode.getOpcode());
        }
        return result;
    }

    @Override
    public List<GenericOperand> selectOparandsSatisfyRestrictions(PSOpcodes opcode, PSState stateBefore, PSState desiredState, PSStateDelta diffWithDesiredState) throws NoSuitableOperandFoundException {
        // the only diff ith selectOperandsRandom() is dest register, which is first element in list
        List<GenericOperand> operands = selectOperandsRandom(opcode, stateBefore);

        if (opcode.getSupportedDestWriteMasks() == null) {
            Set<PSRegister> fullRegisters = diffWithDesiredState.getFullRegisters();
            // constant and sampler registers are not writable
            filterRegistersByType(fullRegisters, RegisterType.CONSTANT);
            filterRegistersByType(fullRegisters, RegisterType.SAMPLER);
            if (opcode != PSOpcodes.MOV) {
                filterRegistersByType(fullRegisters, RegisterType.OUTPUT_COLOR);
            }
            PSRegister dst = CollectionUtils.selectRandomElement(fullRegisters);
            if (dst == null) {
                // this opcode does not support dest write masks (always writes to all 4 channels), and there is no whole register that needs to be written
                throw new NoSuitableOperandFoundException(opcode.getOpcode());
            }
            operands.set(0, dst);
            return operands;
        }
        List<PSRegister> possibleVariants = new LinkedList<PSRegister>();
        for (DestinationWriteMask mask : opcode.getSupportedDestWriteMasks()) {
            for (PSRegister cg : diffWithDesiredState.getPSRegisters()) {
                if (cg.getRegisterType() == RegisterType.OUTPUT_COLOR) {
                    if (mask != DestinationWriteMask.XYZW) {
                        // writes to dest color register can only be done without a mask
                        continue;
                    }
                    if (cg.isReadable()) {
                        // output color register was already written to, second write is not allowed
                        continue;
                    }
                    if (opcode != PSOpcodes.MOV) {
                        // output color registers can only be written with MOV instruction
                        continue;
                    }
                }
                if (cg.getRegisterType() == RegisterType.CONSTANT && opcode != PSOpcodes.DEF) {
                    // only def instruction can write to const regs
                    continue;
                }
                if (cg.satisfiesMask(mask)) {
                    if (mask == DestinationWriteMask.XYZW) {
                        possibleVariants.add(cg);
                    } else {
                        possibleVariants.add(new PSRegister(cg, mask));
                    }
                }
            }
        }
        if (possibleVariants.isEmpty()) {
            // no registers that have enough available channels to satisfy any of write masks, supported by this opcode
            throw new NoSuitableOperandFoundException(opcode.getOpcode());
        }
        operands.set(0, CollectionUtils.selectRandomElement(possibleVariants));
        return operands;
    }

    private <T extends PSRegister> void filterRegistersByType(Collection<T> regs, RegisterType type) {
        for (Iterator<T> iter = regs.iterator(); iter.hasNext(); ) {
            T cg = iter.next();
            if (cg.getRegisterType() == type) {
                iter.remove();
            }
        }
    }

    @Override
    public List<List<GenericOperand>> getAllPossibleOperandSets(PSOpcodes opcode, PSState state) throws NoSuitableOperandFoundException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PSRegisterOperandSelector selector = (PSRegisterOperandSelector) o;

        return sourceCount == selector.sourceCount;
    }

    @Override
    public int hashCode() {
        return 31 * sourceCount + PSRegisterOperandSelector.class.hashCode();
    }
}
