package ru.ifmo.gpo.shader.ps_1_1.selectors;

import ru.ifmo.gpo.core.instructions.IOperandSelector;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.vmstate.NoSuitableOperandFoundException;
import ru.ifmo.gpo.shader.ps_1_1.instructions.PSOpcodes;
import ru.ifmo.gpo.shader.ps_1_1.state.PSRegister;
import ru.ifmo.gpo.shader.ps_1_1.state.PSState;
import ru.ifmo.gpo.shader.ps_1_1.state.PSStateDelta;
import ru.ifmo.gpo.shader.ps_1_1.state.RegisterType;
import ru.ifmo.gpo.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Selects single texture register for texture sampling
 */
public class TextureOperandSelector implements IOperandSelector<PSOpcodes, PSState, PSStateDelta> {
    private void selectSourceOperands(List<GenericOperand> dst, PSOpcodes opcode, PSState stateBefore) throws NoSuitableOperandFoundException {
        List<PSRegister> firstSrcList = stateBefore.getByTypeAndAccess(RegisterType.TEXTURE, GenericOperand.AccessType.READ);
        firstSrcList.addAll(filterDependentRegisters(stateBefore, stateBefore.getByTypeAndAccess(RegisterType.TEMP, GenericOperand.AccessType.READ)));
        // temp or texture
        PSRegister firstArg = CollectionUtils.selectRandomElement(firstSrcList);
        if (firstArg == null) {
            throw new NoSuitableOperandFoundException(opcode.getOpcode());
        }
        dst.add(firstArg);

        PSRegister secondArg = CollectionUtils.selectRandomElement(stateBefore.getByTypeAndAccess(RegisterType.SAMPLER, GenericOperand.AccessType.READ));
        if (secondArg == null) {
            throw new NoSuitableOperandFoundException(opcode.getOpcode());
        }
        dst.add(secondArg);
    }

    /**
     * Removes from list temp registers that are already a part of dependent texture manipulations of maximum length
     */
    private List<PSRegister> filterDependentRegisters(PSState state, List<PSRegister> regs) {
        for (Iterator<PSRegister> iter = regs.iterator(); iter.hasNext(); ) {
            PSRegister reg = iter.next();
            if (reg.getRegisterType() != RegisterType.TEMP) {
                throw new IllegalArgumentException("Dependent tex op sequences are valid only for temp registers");
            }
            if (state.getValue(reg).getDependentTexOpRead() >= RegisterType.DEPENDENT_TEX_OP_LIMIT) {
                iter.remove();
            }
        }
        return regs;
    }

    @Override
    public List<GenericOperand> selectOperandsRandom(PSOpcodes opcode, PSState state) throws NoSuitableOperandFoundException {
        List<GenericOperand> results = new ArrayList<GenericOperand>();

        PSRegister dst = CollectionUtils.selectRandomElement(filterDependentRegisters(state, state.getByTypeAndAccess(RegisterType.TEMP, GenericOperand.AccessType.WRITE)));
        if (dst == null) {
            throw new NoSuitableOperandFoundException(opcode.getOpcode());
        }

        results.add(dst);
        selectSourceOperands(results, opcode, state);
        return results;
    }

    @Override
    public List<GenericOperand> selectOparandsSatisfyRestrictions(PSOpcodes opcode, PSState stateBefore, PSState desiredState, PSStateDelta diffWithDesiredState) throws NoSuitableOperandFoundException {
        List<GenericOperand> destCandidates = new ArrayList<GenericOperand>();
        for (PSRegister reg : diffWithDesiredState.getFullRegisters()) {
            PSRegister actualValue = stateBefore.getValue(reg);
            if (actualValue.getRegisterType() == RegisterType.TEMP && actualValue.isWriteable() && actualValue.getDependentTexOpRead() < RegisterType.DEPENDENT_TEX_OP_LIMIT) {
                destCandidates.add(actualValue);
            }
        }
        if (destCandidates.isEmpty()) {
            throw new NoSuitableOperandFoundException(opcode.getOpcode());
        }
        List<GenericOperand> result = new ArrayList<GenericOperand>();
        result.add(CollectionUtils.selectRandomElement(destCandidates));
        selectSourceOperands(result, opcode, stateBefore);
        return result;
    }

    @Override
    public List<List<GenericOperand>> getAllPossibleOperandSets(PSOpcodes opcode, PSState state) throws NoSuitableOperandFoundException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean equals(Object o) {
        return this == o || !(o == null || getClass() != o.getClass());
    }

    @Override
    public int hashCode() {
        return TextureOperandSelector.class.hashCode();
    }
}
