/**
 * Created with IntelliJ IDEA.
 * User: Egor.Smirnov
 * Date: 26.12.12
 * Time: 16:01
 */
package ru.ifmo.gpo.shader.ps_1_1.selectors;

import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.shader.ps_1_1.instructions.PSOpcodes;
import ru.ifmo.gpo.shader.ps_1_1.state.PSRegister;

import java.util.List;

/**
 * Special check for LRP instruction to avoid error X5765: Dest register for LRP cannot be the same as first or third source register
 */
public class LRPOperandSelector extends PSRegisterOperandSelector {
    public LRPOperandSelector() {
        super(3);
    }

    @Override
    public boolean isSuitableAsNextSource(PSOpcodes opcode, PSRegister dest, List<GenericOperand> source, PSRegister sourceCandidate) {
        return !sourceCandidate.getId().equals(dest.getId()) || source.size() == 1;
    }
}
