package ru.ifmo.gpo.shader.ps_1_1;

import ru.ifmo.gpo.core.ICodeExecutionEnvironment;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.vmstate.IStateSchema;
import ru.ifmo.gpo.shader.ps_1_1.emulator.Texture;
import ru.ifmo.gpo.shader.ps_1_1.state.Channel;
import ru.ifmo.gpo.shader.ps_1_1.state.PSRegister;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * Set of parameters required to emulate a pixel shader.
 * Contains values for registers and textures assigned to texture stages
 */
public class PixelShaderCodeExecutionEnvironment implements ICodeExecutionEnvironment {

    private Set<GenericOperand> values = new HashSet<GenericOperand>();

    private Texture[] textureStages = new Texture[Texture.MAX_STAGES];

    private boolean inited = false;

    public PixelShaderCodeExecutionEnvironment(IStateSchema stateSchema) {
        for (GenericOperand op : stateSchema.getSlots()) {
            if (op instanceof PSRegister) {
                PSRegister reg = (PSRegister) op;
                values.add(new PSRegister(reg));
            } else if (op instanceof Channel) {
                Channel c = (Channel) op;
                values.add(new Channel(c.getOwner(), c.getName(), c.read(), c.getExpression()));
            } else {
                throw new IllegalArgumentException("Unknown operand type " + op.getClass());
            }
        }

        for (int i = 0; i < Texture.MAX_STAGES; ++i) {
            textureStages[i] = new Texture();
        }
    }

    public Set<GenericOperand> getValues() {
        if (!inited) {
            throw new IllegalStateException("PixelShaderCodeExecutionEnvironment not initialized. Call one of setToXXX() methods first");
        }
        return values;
    }

    public Texture[] getTextureStages() {
        return textureStages;
    }

    @Override
    public void setToZero() throws IllegalStateException {
        for (GenericOperand op : values) {
            PSRegister reg = (PSRegister) op;
            // can safely modify this without removing from set, as only id and type are counted in hasCode() for psregister
            for (Channel c : reg) {
                c.write(0.0);
            }

        }
        for (Texture ts : textureStages) {
            ts.setToZero();
        }
        inited = true;
    }

    @Override
    public void setToFF() throws IllegalStateException {
        for (GenericOperand op : values) {
            PSRegister reg = (PSRegister) op;
            // can safely modify this without removing from set, as only id and type are counted in hasCode() for psregister
            for (Channel c : reg) {
                c.write(1.0);
            }

        }
        for (Texture ts : textureStages) {
            ts.setToFF();
        }
        inited = true;
    }

    @Override
    public void setRandomValues() throws IllegalStateException {
        Random rnd = new Random();
        for (GenericOperand op : values) {
            PSRegister reg = (PSRegister) op;
            // can safely modify this without removing from set, as only id and type are counted in hasCode() for psregister
            double maxVal = reg.getRegisterType().getRangeMax();
            for (Channel c : reg) {
                c.write(rnd.nextDouble() * maxVal);
            }
        }
        for (Texture ts : textureStages) {
            ts.setRandomValues();
        }
        inited = true;
    }
}
