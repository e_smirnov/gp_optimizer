package ru.ifmo.gpo.shader.ps_1_1.parser;

import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.instructions.generic.NumericConstant;
import ru.ifmo.gpo.shader.ps_1_1.instructions.InstructionModifier;
import ru.ifmo.gpo.shader.ps_1_1.instructions.PSOpcodes;
import ru.ifmo.gpo.shader.ps_1_1.instructions.PixelShaderInstruction;
import ru.ifmo.gpo.shader.ps_1_1.state.DestinationWriteMask;
import ru.ifmo.gpo.shader.ps_1_1.state.PSRegister;
import ru.ifmo.gpo.shader.ps_1_1.state.SourceRegisterModifier;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parses dx pixel shader and returns it as InstructionSequence
 */
public class ShaderParser {

    public InstructionSequence parse(String shaderString) throws ShaderParseException {
        InstructionSequence result = new InstructionSequence();
        String[] lines = shaderString.split("\n");
        int lineCount = 0;
        for (String line : lines) {
            try {
                PixelShaderInstruction insn = line(line);
                if (insn != null) {
                    result.add(insn);
                }
                lineCount++;
            } catch (Exception ex) {
                throw new ShaderParseException("Error parsing shader on line " + lineCount, ex);
            }
        }
        return result;
    }

    /*
    Simplified grammar
    line -> insn argsList
            | insn
    insn -> opcode
        | opcode _ modifierList
    modifierList -> modifier
                  | modifier _ modifierList

    argsList -> arg
            | arg, argsList
    arg -> regName
        | regName . writeMask
        | regName . swizzle (currently only .wwww)
        | - arg
        | 1 - arg
        | arg _ source register modifier name
        | immediate

     */

    private PixelShaderInstruction line(String l) throws ShaderParseException {

        // strip single-line comments
        String[] codeAndComment = l.split("//");
        l = codeAndComment[0];
        l = l.trim();
        if (l.isEmpty()) {
            // line contains only a comment
            return null;
        }
        String[] parts = l.split("\\s", 2);


        if (parts.length == 1) {
            return insn(parts[0], Collections.<GenericOperand>emptyList());
        }
        Collection<GenericOperand> operands = argsList(parts[1]);
        return insn(parts[0], operands);
    }

    private PixelShaderInstruction insn(String l, Collection<GenericOperand> args) throws ShaderParseException {
        l = l.trim();
        // check that it is a full instruction that contains underscore like DCL_2D (not to mix it with case when underscore separates instruction modifier)
        for (PSOpcodes op : PSOpcodes.values()) {
            if (l.equalsIgnoreCase(op.name())) {
                return new PixelShaderInstruction(op, args);
            }
        }

        String[] parts = l.split("_", 2);
        PSOpcodes opcode = null;
        if (parts[0].equalsIgnoreCase("ps")) {
            // special case where _xxx is not a modifier, but a part of instruction itself
            return new PixelShaderInstruction(PSOpcodes.PS_2_0);
        } else {
            for (PSOpcodes op : PSOpcodes.values()) {
                if (parts[0].equalsIgnoreCase(op.name())) {
                    opcode = op;
                }
            }
        }
        if (opcode == null) {
            throw new ShaderParseException("Unknown instruction contained in line '" + l + "'");
        }
        if (parts.length == 1) {
            return new PixelShaderInstruction(opcode, args);
        }
        List<InstructionModifier> modifiers = modifierList(parts[1]);
        return new PixelShaderInstruction(opcode, modifiers, args);
    }

    private List<InstructionModifier> modifierList(String l) {
        l = l.trim();
        String[] parts = l.split("_");
        List<InstructionModifier> modifiers = new ArrayList<InstructionModifier>(2);
        modifiers.add(InstructionModifier.valueOf(parts[0].toUpperCase()));
        if (parts.length > 1) {
            modifiers.addAll(modifierList(parts[1]));
        }
        return modifiers;
    }

    private Collection<GenericOperand> argsList(String l) throws ShaderParseException {
        String[] parts = l.split(",", 2);
        List<GenericOperand> rz = new ArrayList<GenericOperand>(parts.length);
        rz.add(arg(parts[0]));
        if (parts.length > 1) {
            rz.addAll(argsList(parts[1]));
        }
        return rz;
    }

    private static final Pattern sourceRegModifierPattern = Pattern.compile("([\\w]+)_([a-zA-Z0-9]+)");
    private static final Pattern maskPattern = Pattern.compile("(\\w+)\\.([xyzw]+)");
    private static final Pattern negatePattern = Pattern.compile("-\\s*([\\w_\\.]+)");

    private GenericOperand arg(String s) throws ShaderParseException {
        s = s.trim();
        try {
            Double constant = Double.parseDouble(s);
            return new NumericConstant(constant);
        } catch (NumberFormatException ex) {
            // this is not a number, than this is a register
        }
        return regArg(s);
    }


    private PSRegister regArg(String s) {
        Matcher m = sourceRegModifierPattern.matcher(s);
        if (m.matches()) {
            SourceRegisterModifier modifier = SourceRegisterModifier.valueOf(m.group(2).toUpperCase());
            PSRegister reg = regArg(m.group(1));
            if (reg instanceof PSRegister) {
                return new PSRegister(reg, modifier);
            }
            SourceRegisterModifier[] newModifiers = new SourceRegisterModifier[reg.getModifiers().length + 1];
            System.arraycopy(reg.getModifiers(), 0, newModifiers, 0, reg.getModifiers().length);
            newModifiers[newModifiers.length - 1] = modifier;
            return new PSRegister(reg, newModifiers);
        }
        m = maskPattern.matcher(s);
        if (m.matches()) {
            PSRegister reg = new PSRegister(m.group(1));
            final String modifierIdString = m.group(2).toUpperCase();
            try {
                DestinationWriteMask mask = DestinationWriteMask.valueOf(modifierIdString);
                return new PSRegister(reg, mask);
            } catch (IllegalArgumentException ex) {
                // not a write mask, may be a swizzle
                SourceRegisterModifier modifier = SourceRegisterModifier.valueOf("SWIZZLE_" + modifierIdString);
                return new PSRegister(reg, modifier);
            }
        }
        m = negatePattern.matcher(s);
        if (m.matches()) {
            PSRegister reg = regArg(m.group(1));
            if (reg instanceof PSRegister) {
                return new PSRegister(reg, SourceRegisterModifier.NEGATE);
            }
            if (reg.getModifiers() != null) {
                SourceRegisterModifier[] newModifiers = new SourceRegisterModifier[reg.getModifiers().length + 1];
                System.arraycopy(reg.getModifiers(), 0, newModifiers, 0, reg.getModifiers().length);
                newModifiers[newModifiers.length - 1] = SourceRegisterModifier.NEGATE;
                return new PSRegister(reg, newModifiers);
            } else {
                return new PSRegister(reg, new SourceRegisterModifier[]{SourceRegisterModifier.NEGATE});
            }

        }
        // just plain simple register
        return new PSRegister(s);
    }
}
