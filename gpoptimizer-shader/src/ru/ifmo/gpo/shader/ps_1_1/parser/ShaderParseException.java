package ru.ifmo.gpo.shader.ps_1_1.parser;

/**
 * Thrown by ShaderParser in case of error while trying to parse pixel shader string
 */
public class ShaderParseException extends Exception {
    public ShaderParseException() {
    }

    public ShaderParseException(String message) {
        super(message);
    }

    public ShaderParseException(String message, Throwable cause) {
        super(message, cause);
    }

    public ShaderParseException(Throwable cause) {
        super(cause);
    }
}
