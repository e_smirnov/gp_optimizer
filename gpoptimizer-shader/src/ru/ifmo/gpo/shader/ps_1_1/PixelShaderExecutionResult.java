/**
 * User: e_smirnov
 * Date: 23.06.2011
 * Time: 17:41:24
 */

package ru.ifmo.gpo.shader.ps_1_1;

import ru.ifmo.gpo.core.IExecutionResult;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.shader.ps_1_1.state.Channel;
import ru.ifmo.gpo.shader.ps_1_1.state.PSState;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Results of a pixel shader code emulation run. Contains values of all channels from all registers
 */
public class PixelShaderExecutionResult implements IExecutionResult {

    private Map<String, Double> values = new HashMap<String, Double>();

    public PixelShaderExecutionResult() {
    }

    public PixelShaderExecutionResult(PSState state) {
        for (GenericOperand op : state.getChannels().values()) {
            Channel c = (Channel) op;
            values.put(c.getId(), c.read());
        }
    }

    @Override
    public BigDecimal computeDifference(IExecutionResult reference) {
        if (!(reference instanceof PixelShaderExecutionResult)) {
            throw new IllegalArgumentException("Can not compare execution results of type " + getClass() + " and " + reference.getClass());
        }
        PixelShaderExecutionResult refResult = (PixelShaderExecutionResult) reference;

        if (values.size() != refResult.values.size()) {
            throw new IllegalArgumentException("Execution results have different sets of results");
        }

        BigDecimal rz = BigDecimal.valueOf(0);

        for (String opId : values.keySet()) {
            Double first = values.get(opId);
            Double second = refResult.getValues().get(opId);
            if (second == null) {
                throw new IllegalArgumentException("Execution results have different sets of arguments. Value for key " + opId + " not found in second result set");
            }

            // if only one element is NaN or infinity - set difference to max value
            if ((first.isInfinite() ^ second.isInfinite()) || (first.isNaN() ^ second.isNaN())) {
                rz = rz.add(BigDecimal.valueOf(Double.MAX_VALUE));
                break;
            }
            if (first.isInfinite() || first.isNaN()) {
                first = Double.MAX_VALUE;
            }
            if (second.isInfinite() || second.isNaN()) {
                second = Double.MAX_VALUE;
            }
            rz = rz.add((BigDecimal.valueOf(first - second)).pow(2));
        }

        return rz;
    }

    @Override
    public BigDecimal maxPossibleDifference() {
        return BigDecimal.valueOf(Double.MAX_VALUE).pow(2).multiply(BigDecimal.valueOf(values.size()));
    }

    @Override
    public Exception getException() {
        return null;
    }

    @Override
    public void dispose() {
        // nothing to dispose
    }

    public Map<String, Double> getValues() {
        return values;
    }
}
