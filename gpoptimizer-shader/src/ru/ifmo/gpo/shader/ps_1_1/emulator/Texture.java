package ru.ifmo.gpo.shader.ps_1_1.emulator;

import ru.ifmo.gpo.core.ICodeExecutionEnvironment;

import java.io.Serializable;
import java.util.Random;

/**
 * Serves as input data for texture sampling operations
 */
public class Texture implements ICodeExecutionEnvironment, Serializable {

    private static final long serialVersionUID = 1L;

    public static final int MAX_STAGES = 8; // actually it is 16, but let's save some mem

    private float[][][] rgba;
    private int width;
    private int height;

    public static final int DEFAULT_TEX_WIDTH = 4;
    public static final int DEFAULT_TEX_HEIGHT = 4;

    public Texture() {
        this(DEFAULT_TEX_WIDTH, DEFAULT_TEX_HEIGHT);
    }

    public Texture(int width, int height) {
        this.width = width;
        this.height = height;
        this.rgba = new float[width][height][4];
    }

    public Texture(Texture proto) {
        this(proto.width, proto.height);
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                System.arraycopy(proto.rgba[x][y], 0, this.rgba[x][y], 0, 4);
            }
        }
    }

    /**
     * Returns color of a pixel at desired coordinates
     *
     * @param u Pixel x coordinate
     * @param v Pixel y coordinate
     * @return RGB color array
     */
    public float[] sample(int u, int v) {
        return rgba[u][v];
    }

    public float[] sample(float u, float v) {
        if (u < 0) {
            u = 0;
        } else if (u > 1) {
            u = 1;
        }
        if (v < 0) {
            v = 0;
        } else if (v > 1) {
            v = 1;
        }

        return rgba[getX(u)][getY(v)];
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getX(float u) {
        return Math.round(u * (getWidth() - 1));
    }

    public int getY(float v) {
        return Math.round(v * (getHeight() - 1));
    }

    @Override
    public void setToZero() throws IllegalStateException {
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                for (int i = 0; i < 4; ++i) {
                    rgba[x][y][i] = 0.0f;
                }
            }
        }
    }

    @Override
    public void setToFF() throws IllegalStateException {
        // actually impossible as texture values are limited. set to 1 instead
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                for (int i = 0; i < 4; ++i) {
                    rgba[x][y][i] = 1.0f;
                }
            }
        }
    }

    @Override
    public void setRandomValues() throws IllegalStateException {
        Random r = new Random();
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                for (int i = 0; i < 4; ++i) {
                    rgba[x][y][i] = r.nextFloat() * 100 - 50.0f;
                }
            }
        }
    }
}
