/**
 * User: e_smirnov
 * Date: 27.07.2011
 * Time: 15:04:50
 */

package ru.ifmo.gpo.shader.ps_1_1.emulator;

import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.UnsupportedOpcodeException;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.instructions.generic.IGenericInstruction;
import ru.ifmo.gpo.core.instructions.generic.NumericConstant;
import ru.ifmo.gpo.shader.ps_1_1.instructions.InstructionModifier;
import ru.ifmo.gpo.shader.ps_1_1.instructions.InstructionType;
import ru.ifmo.gpo.shader.ps_1_1.instructions.PixelShaderInstruction;
import ru.ifmo.gpo.shader.ps_1_1.state.*;
import ru.ifmo.gpo.verify.*;

import java.util.List;


/**
 * Emulates DirectX pixel shader execution, as there seem to be no simple way to get whole processor state back from GPU after
 * shader evalutaion.
 */
public abstract class ShaderEmulator {

    /**
     * Creates name of an input variable that will be used in later verification of results produced by code
     *
     * @param stageIdx number of texture stage
     * @param name     component name
     * @return variable name
     */
    private static String buildTextureSampleVariableName(int stageIdx, IArithmeticTreeNode u, IArithmeticTreeNode v, Channel.Name name) {
        return String.format("texture_%d_%s_%s_%s", stageIdx, u.toString(), v.toString(), name);
    }

    private static String buildTextureSampleVariableName(int stageIdx, IArithmeticTreeNode u, IArithmeticTreeNode v) {
        return String.format("texture_%d_%s_%s", stageIdx, u.toString(), v.toString());
    }

    /**
     * After computing value in a register, additional modifiers can be applied to it
     * E.g. add_x2 instruction will not only add, but also multiply result by 2
     *
     * @param state     Target state
     * @param dst       Register containing result of arithmetic instruction
     * @param modifiers List of modifiers for that operation
     */
    private static void applyInstructionModifiers(PSState state, PSRegister dst, List<InstructionModifier> modifiers) {
        if (modifiers == null) {
            return;
        }
        for (InstructionModifier modifier : modifiers) {
            for (int i = 0; i < 4; ++i) {
                if (!dst.hasChannel(i)) {
                    // dst write mask is applied, no need to write to that channel
                    continue;
                }
                Channel dstChannel = state.getValue(dst.getChannel(i).getOwner()).getChannel(i);
                Channel part = new Channel(dstChannel);
                state.setValue(part);
            }

        }
    }

    /**
     * Determines what registers are used by seq
     *
     * @param seq Shader code
     * @return Structure representing all necessary resources for shader execution
     */
    public static PSStateSchema buildSchema(InstructionSequence seq) {
        PSStateSchema result = new PSStateSchema();
        for (IGenericInstruction genericInsn : seq) {
            PixelShaderInstruction insn = (PixelShaderInstruction) genericInsn;
            // always check first source operands!
            switch (insn.getOpcodeInternal()) {
                case DCL:
                case DCL_2D:
                case DCL_CUBE:
                case DCL_VOLUME:
                    result.addOrUpdateSlot(insn.getParameters().get(0), Type.DOUBLE_TYPE, GenericOperand.AccessType.READ);
                    break;
                case ADD:
                case SUB:
                case MUL:
                case DP3:
                    //todo: actually, DP3 does not require .w component
                    result.addOrUpdateSlot(insn.getParameters().get(1), Type.DOUBLE_TYPE, GenericOperand.AccessType.READ);
                    result.addOrUpdateSlot(insn.getParameters().get(2), Type.DOUBLE_TYPE, GenericOperand.AccessType.READ);
                    result.addOrUpdateSlot(insn.getParameters().get(0), Type.DOUBLE_TYPE, GenericOperand.AccessType.WRITE);
                    break;
                case MAD:
                case LRP:
                    result.addOrUpdateSlot(insn.getParameters().get(1), Type.DOUBLE_TYPE, GenericOperand.AccessType.READ);
                    result.addOrUpdateSlot(insn.getParameters().get(2), Type.DOUBLE_TYPE, GenericOperand.AccessType.READ);
                    result.addOrUpdateSlot(insn.getParameters().get(3), Type.DOUBLE_TYPE, GenericOperand.AccessType.READ);
                    result.addOrUpdateSlot(insn.getParameters().get(0), Type.DOUBLE_TYPE, GenericOperand.AccessType.WRITE);
                    break;
                case DEF:
                    result.addOrUpdateSlot(insn.getParameters().get(0), Type.DOUBLE_TYPE, GenericOperand.AccessType.READ);
                    for (int i = 0; i < 4; ++i) {
                        NumericConstant constant = (NumericConstant) insn.getParameters().get(1 + i);
                        result.getConstants().addFloatConstant((float) constant.doubleValue());
                    }
                    break;
                case MOV:
                case RCP:
                    result.addOrUpdateSlot(insn.getParameters().get(1), Type.DOUBLE_TYPE, GenericOperand.AccessType.READ);
                    result.addOrUpdateSlot(insn.getParameters().get(0), Type.DOUBLE_TYPE, GenericOperand.AccessType.WRITE);
                    break;
                case TEXLD:
                    result.addOrUpdateSlot(insn.getParameters().get(1), Type.DOUBLE_TYPE, GenericOperand.AccessType.READ);
                    result.addOrUpdateSlot(insn.getParameters().get(2), Type.DOUBLE_TYPE, GenericOperand.AccessType.READ);
                    result.addOrUpdateSlot(insn.getParameters().get(0), Type.DOUBLE_TYPE, GenericOperand.AccessType.WRITE);
                    break;
                case NOP:
                case PS_2_0:
                    break;
                default:
                    throw new UnsupportedOpcodeException(insn.getOpcodeInternal().toString());
            }
            if (insn.getOpcodeInternal().getType() == InstructionType.SETUP) {
                result.addSetupInstruction(insn);
            }
        }
        return result;
    }


    /**
     * Emulates single PS instruction
     *
     * @param state Initial state of a processor
     * @param instr Instruction to emulate
     * @return State of a processor after instruction execution
     */
    public static PSState emulateInstruction(PSState state, PixelShaderInstruction instr) {
        PSState result = new PSState(state);

        // check that instruction dest register is also a source for it
        int maxTexOpSeq = -1;
        if (!instr.getParameters().isEmpty()) {
            for (int i = 0; i < instr.getParameters().size(); ++i) {
                GenericOperand op = instr.getParameters().get(i);
                if (!PSRegister.class.isAssignableFrom(op.getClass())) {
                    continue;
                }
                maxTexOpSeq = Math.max(maxTexOpSeq, state.getValue(op).getDependentTexOpRead());
            }
        }

        switch (instr.getOpcodeInternal()) {
            case ADD: {
                PSRegister dst = (PSRegister) instr.getParameters().get(0);
                PSRegister src0 = new PSRegister(state.getValue(instr.getParameters().get(1)), ((PSRegister) instr.getParameters().get(1)).getModifiers());
                PSRegister src1 = new PSRegister(state.getValue(instr.getParameters().get(2)), ((PSRegister) instr.getParameters().get(2)).getModifiers());

                for (int i = 0; i < 4; ++i) {
                    if (!dst.hasChannel(i)) {
                        // dst write mask is applied, no need to write to that channel
                        continue;
                    }
                    Channel dstChannel = dst.getChannel(i);
                    Channel src0Channel = src0.getChannel(i);
                    Channel src1Channel = src1.getChannel(i);


                    Channel part = new Channel(dstChannel.getOwner(), Channel.Name.getName(i), src0Channel.read() + src1Channel.read(), new BinaryOperatorNode(src0Channel.getExpression(), src1Channel.getExpression(), GenericOperator.PLUS));
                    part.setAccessType(GenericOperand.AccessType.WRITE);
                    part.setDependentTexOpReads(maxTexOpSeq);
                    part.setWritable();
                    part.setHasValue(true);
                    result.setValue(part);
                }

                applyInstructionModifiers(result, dst, instr.getModifiers());

                break;
            }
            case DEF: {
                PSRegister dst = (PSRegister) instr.getParameters().get(0);
                dst.setExpression(new NumericConstantTreeNode(Type.DOUBLE_TYPE, 1.0));
                NumericConstant x = (NumericConstant) instr.getParameters().get(1);
                NumericConstant y = (NumericConstant) instr.getParameters().get(2);
                NumericConstant z = (NumericConstant) instr.getParameters().get(3);
                NumericConstant w = (NumericConstant) instr.getParameters().get(4);

                dst.setChannel(new Channel(dst, Channel.Name.x, x.doubleValue(), new NumericConstantTreeNode(Type.DOUBLE_TYPE, x.getValue())));
                dst.setChannel(new Channel(dst, Channel.Name.y, y.doubleValue(), new NumericConstantTreeNode(Type.DOUBLE_TYPE, y.getValue())));
                dst.setChannel(new Channel(dst, Channel.Name.z, z.doubleValue(), new NumericConstantTreeNode(Type.DOUBLE_TYPE, z.getValue())));
                dst.setChannel(new Channel(dst, Channel.Name.w, w.doubleValue(), new NumericConstantTreeNode(Type.DOUBLE_TYPE, w.getValue())));
                dst.setAccessType(GenericOperand.AccessType.READ);
                dst.setDependentTexOpRead(0);
                result.setValue(dst);
                break;
            }
            case SUB: {
                PSRegister dst = (PSRegister) instr.getParameters().get(0);
                PSRegister src0 = new PSRegister(state.getValue(instr.getParameters().get(1)), ((PSRegister) instr.getParameters().get(1)).getModifiers());
                PSRegister src1 = new PSRegister(state.getValue(instr.getParameters().get(2)), ((PSRegister) instr.getParameters().get(2)).getModifiers());

                for (int i = 0; i < 4; ++i) {
                    if (!dst.hasChannel(i)) {
                        // dst write mask is applied, no need to write to that channel
                        continue;
                    }
                    Channel dstChannel = dst.getChannel(i);
                    Channel src0Channel = src0.getChannel(i);
                    Channel src1Channel = src1.getChannel(i);


                    Channel part = new Channel(dstChannel.getOwner(), Channel.Name.getName(i), src0Channel.read() - src1Channel.read(), new BinaryOperatorNode(src0Channel.getExpression(), src1Channel.getExpression(), GenericOperator.MINUS));
                    part.setAccessType(GenericOperand.AccessType.WRITE);
                    part.setDependentTexOpReads(maxTexOpSeq);
                    part.setHasValue(true);
                    part.setWritable();
                    result.setValue(part);
                }

                applyInstructionModifiers(result, dst, instr.getModifiers());

                break;

            }

            case MUL: {
                PSRegister dst = (PSRegister) instr.getParameters().get(0);
                PSRegister src0 = new PSRegister(state.getValue(instr.getParameters().get(1)), ((PSRegister) instr.getParameters().get(1)).getModifiers());
                PSRegister src1 = new PSRegister(state.getValue(instr.getParameters().get(2)), ((PSRegister) instr.getParameters().get(2)).getModifiers());

                for (int i = 0; i < 4; ++i) {
                    if (!dst.hasChannel(i)) {
                        // dst write mask is applied, no need to write to that channel
                        continue;
                    }
                    Channel dstChannel = dst.getChannel(i);
                    Channel src0Channel = src0.getChannel(i);
                    Channel src1Channel = src1.getChannel(i);


                    Channel part = new Channel(dstChannel.getOwner(), Channel.Name.getName(i), src0Channel.read() * src1Channel.read(), new BinaryOperatorNode(src0Channel.getExpression(), src1Channel.getExpression(), GenericOperator.MUL));
                    part.setAccessType(GenericOperand.AccessType.WRITE);
                    part.setHasValue(true);
                    part.setWritable();
                    part.setDependentTexOpReads(maxTexOpSeq);
                    result.setValue(part);
                }

                applyInstructionModifiers(result, dst, instr.getModifiers());
                break;

            }
            case MOV: {
                PSRegister dst = (PSRegister) instr.getParameters().get(0);
                PSRegister src0 = new PSRegister(state.getValue(instr.getParameters().get(1)), ((PSRegister) instr.getParameters().get(1)).getModifiers());
                for (int i = 0; i < 4; ++i) {
                    if (!dst.hasChannel(i)) {
                        // dst write mask is applied, no need to write to that channel
                        continue;
                    }
                    Channel dstChannel = dst.getChannel(i);
                    Channel src0Channel = src0.getChannel(i);
                    Channel part = new Channel(dstChannel.getOwner(), Channel.Name.getName(i), src0Channel.read(), src0Channel.getExpression());
                    part.setAccessType(GenericOperand.AccessType.WRITE);
                    part.setHasValue(true);
                    part.setWritable();
                    part.setDependentTexOpReads(maxTexOpSeq);
                    result.setValue(part);
                }
                applyInstructionModifiers(result, dst, instr.getModifiers());
                break;
            }
            case RCP: {
                PSRegister dst = (PSRegister) instr.getParameters().get(0);
                PSRegister src0 = new PSRegister(state.getValue(instr.getParameters().get(1)), ((PSRegister) instr.getParameters().get(1)).getModifiers());
                for (int i = 0; i < 4; ++i) {
                    if (!dst.hasChannel(i)) {
                        // dst write mask is applied, no need to write to that channel
                        continue;
                    }
                    Channel dstChannel = dst.getChannel(i);
                    Channel src0Channel = src0.getChannel(i);
                    Channel part = new Channel(dstChannel.getOwner(), Channel.Name.getName(i), 1.0 / src0Channel.read(), TreeNodeFactory.createBinaryWithConstant(1.0, GenericOperator.FDIV, src0Channel.getExpression()));
                    part.setAccessType(GenericOperand.AccessType.WRITE);
                    part.setHasValue(true);
                    part.setDependentTexOpReads(maxTexOpSeq);
                    result.setValue(part);
                }
                applyInstructionModifiers(result, dst, instr.getModifiers());
                break;
            }

            case DP3: {
                PSRegister dst = (PSRegister) instr.getParameters().get(0);
                PSRegister src0 = new PSRegister(state.getValue(instr.getParameters().get(1)), ((PSRegister) instr.getParameters().get(1)).getModifiers());
                Channel src0_x = src0.getChannel(0);
                Channel src0_y = src0.getChannel(1);
                Channel src0_z = src0.getChannel(2);

                PSRegister src1 = new PSRegister(state.getValue(instr.getParameters().get(2)), ((PSRegister) instr.getParameters().get(2)).getModifiers());
                Channel src1_x = src1.getChannel(0);
                Channel src1_y = src1.getChannel(1);
                Channel src1_z = src1.getChannel(2);

                final double value = src0_x.read() * src1_x.read() + src0_y.read() * src1_y.read() + src0_y.read() * src1_y.read();
                final IArithmeticTreeNode node = new BinaryOperatorNode(
                        new BinaryOperatorNode(
                                new BinaryOperatorNode(
                                        src0_x.getExpression(),
                                        src1_x.getExpression(),
                                        GenericOperator.MUL
                                ),
                                new BinaryOperatorNode(
                                        src0_y.getExpression(),
                                        src1_y.getExpression(),
                                        GenericOperator.MUL
                                ),
                                GenericOperator.PLUS
                        ),
                        new BinaryOperatorNode(
                                src0_z.getExpression(),
                                src1_z.getExpression(),
                                GenericOperator.MUL
                        ),
                        GenericOperator.PLUS
                );

                for (int i = 0; i < 4; ++i) {
                    if (!dst.hasChannel(i)) {
                        continue;
                    }
                    Channel c = new Channel(dst, Channel.Name.getName(i), value, node);
                    c.setAccessType(GenericOperand.AccessType.WRITE);
                    c.setDependentTexOpReads(maxTexOpSeq);
                    c.setHasValue(true);
                    c.setWritable();
                    result.setValue(c);
                }
                applyInstructionModifiers(result, dst, instr.getModifiers());
                break;
            }
            case LRP: {
                // dest = src2 + src0 * (src1 - src2)
                PSRegister dst = (PSRegister) instr.getParameters().get(0);
                PSRegister src0 = new PSRegister(state.getValue(instr.getParameters().get(1)), ((PSRegister) instr.getParameters().get(1)).getModifiers());
                PSRegister src1 = new PSRegister(state.getValue(instr.getParameters().get(2)), ((PSRegister) instr.getParameters().get(2)).getModifiers());
                PSRegister src2 = new PSRegister(state.getValue(instr.getParameters().get(3)), ((PSRegister) instr.getParameters().get(3)).getModifiers());

                for (int i = 0; i < 4; ++i) {
                    if (!dst.hasChannel(i)) {
                        continue;
                    }
                    Channel src0Channel = src0.getChannel(i);
                    Channel src1Channel = src1.getChannel(i);
                    Channel src2Channel = src2.getChannel(i);

                    Channel rzChannel = new Channel(dst, Channel.Name.getName(i), src2Channel.read() + src0Channel.read() * (src1Channel.read() - src2Channel.read()), new BinaryOperatorNode(
                            src2Channel.getExpression()
                            , new BinaryOperatorNode(
                            src0Channel.getExpression()
                            , new BinaryOperatorNode(
                            src1Channel.getExpression()
                            , src2Channel.getExpression()
                            , GenericOperator.MINUS)
                            , GenericOperator.MUL)
                            , GenericOperator.PLUS));
                    rzChannel.setAccessType(GenericOperand.AccessType.WRITE);
                    rzChannel.setDependentTexOpReads(maxTexOpSeq);
                    rzChannel.setHasValue(true);
                    rzChannel.setWritable();
                    result.setValue(rzChannel);
                }
                applyInstructionModifiers(result, dst, instr.getModifiers());
                break;
            }
            case MAD: {
                // x * y + z
                PSRegister dst = (PSRegister) instr.getParameters().get(0);
                PSRegister src0 = new PSRegister(state.getValue(instr.getParameters().get(1)), ((PSRegister) instr.getParameters().get(1)).getModifiers());
                PSRegister src1 = new PSRegister(state.getValue(instr.getParameters().get(2)), ((PSRegister) instr.getParameters().get(2)).getModifiers());
                PSRegister src2 = new PSRegister(state.getValue(instr.getParameters().get(3)), ((PSRegister) instr.getParameters().get(3)).getModifiers());
                for (int i = 0; i < 4; ++i) {
                    if (!dst.hasChannel(i)) {
                        continue;
                    }
                    Channel src0Channel = src0.getChannel(i);
                    Channel src1Channel = src1.getChannel(i);
                    Channel src2Channel = src2.getChannel(i);

                    Channel dstChannel = new Channel(dst, Channel.Name.getName(i), src0Channel.read() * src1Channel.read() + src2Channel.read(), new BinaryOperatorNode(new BinaryOperatorNode(src0Channel.getExpression(), src1Channel.getExpression(), GenericOperator.MUL), src2Channel.getExpression(), GenericOperator.PLUS));
                    dstChannel.setAccessType(GenericOperand.AccessType.WRITE);
                    dstChannel.setDependentTexOpReads(maxTexOpSeq);
                    dstChannel.setHasValue(true);
                    dstChannel.setWritable();
                    result.setValue(dstChannel);
                }
                applyInstructionModifiers(result, dst, instr.getModifiers());
                break;
            }
            case TEXLD: {
                PSRegister dst = state.getValue(instr.getParameters().get(0));
                PSRegister coordinatesSrc = state.getValue(instr.getParameters().get(1));
                PSRegister samplerSrc = (PSRegister) instr.getParameters().get(2);
                final float u = (float) coordinatesSrc.getChannel(0).read();
                final IArithmeticTreeNode uExpression = coordinatesSrc.getChannel(0).getExpression();
                final float v = (float) coordinatesSrc.getChannel(1).read();
                final IArithmeticTreeNode vExpression = coordinatesSrc.getChannel(1).getExpression();

                PSRegister resultValue = new PSRegister(dst.getRegisterType(), dst.getIndex());
                for (Channel c : resultValue) {
                    c.setExpression(new VariableNode(buildTextureSampleVariableName(samplerSrc.getIndex(), uExpression, vExpression, c.getName()), Type.DOUBLE_TYPE));
                }

                float[] texData = state.getTextureStages()[samplerSrc.getIndex()].sample(u, v);

                resultValue.setChannel(new Channel(resultValue, Channel.Name.x, texData[0], new VariableNode(buildTextureSampleVariableName(samplerSrc.getIndex(), uExpression, vExpression, Channel.Name.x), Type.DOUBLE_TYPE)));
                resultValue.setChannel(new Channel(resultValue, Channel.Name.y, texData[1], new VariableNode(buildTextureSampleVariableName(samplerSrc.getIndex(), uExpression, vExpression, Channel.Name.y), Type.DOUBLE_TYPE)));
                resultValue.setChannel(new Channel(resultValue, Channel.Name.z, texData[2], new VariableNode(buildTextureSampleVariableName(samplerSrc.getIndex(), uExpression, vExpression, Channel.Name.z), Type.DOUBLE_TYPE)));
                resultValue.setChannel(new Channel(resultValue, Channel.Name.w, texData[3], new VariableNode(buildTextureSampleVariableName(samplerSrc.getIndex(), uExpression, vExpression, Channel.Name.w), Type.DOUBLE_TYPE)));
                for (int i = 0; i < 4; ++i) {
                    resultValue.getChannel(i).setAccessType(GenericOperand.AccessType.WRITE);
                }
                /* if (resultValue.getRegisterType() == RegisterType.TEMP) {
                    // updating dependent tex-op chains, see comment in PSRegister to dependentTexOpReads member
                    if (dst.getDependentTexOpRead() == -1) {
                        resultValue.setDependentTexOpRead(1);
                    } else {
                        resultValue.setDependentTexOpRead(dst.getDependentTexOpRead() + 1);
                    }
                }*/
                // A 1st order dependent tex-op is a tex[ld*|kill] instruction in which either: (1) an r# reg is input (NOT t# reg), or (2) output r# reg was previously written, now being written AGAIN

                if (coordinatesSrc.getRegisterType() == RegisterType.TEMP || (dst.getRegisterType() == RegisterType.TEMP && dst.hasValue())) {
                    resultValue.setDependentTexOpRead(1);
                }


                // A 2nd order dependent tex-op occurs if: a tex-op reads OR WRITES to an r# reg whose contents, BEFORE executing the tex-op, depend (perhaps indirectly) on the outcome of a 1st order dependent tex-op
                if (coordinatesSrc.getRegisterType() == RegisterType.TEMP && coordinatesSrc.getDependentTexOpRead() > 0) {
                    resultValue.setDependentTexOpRead(coordinatesSrc.getDependentTexOpRead() + 1);
                }
                if (dst.getRegisterType() == RegisterType.TEMP && dst.getDependentTexOpRead() > 0) {
                    resultValue.setDependentTexOpRead(Math.max(dst.getDependentTexOpRead() + 1, resultValue.getDependentTexOpRead()));
                }
                resultValue.setHasValue(true);
                resultValue.setWritable();

                result.setValue(resultValue);
                break;
            }

            case PS_2_0:
            case NOP:
            case DCL:
            case DCL_2D:
            case DCL_CUBE:
            case DCL_VOLUME:
                break;
            default:
                throw new UnsupportedOpcodeException(instr.getOpcodeInternal().toString());

        }
        return result;
    }
}
