package ru.ifmo.gpo.shader.ps_1_1;

import org.jgap.InvalidConfigurationException;
import ru.ifmo.gpo.core.*;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.IGenericInstruction;
import ru.ifmo.gpo.core.vmstate.IInstructionSequenceAnalyzer;
import ru.ifmo.gpo.core.vmstate.IMachineState;
import ru.ifmo.gpo.core.vmstate.IStateDelta;
import ru.ifmo.gpo.core.vmstate.IStateSchema;
import ru.ifmo.gpo.shader.ps_1_1.emulator.ShaderEmulator;
import ru.ifmo.gpo.shader.ps_1_1.instructions.InstructionType;
import ru.ifmo.gpo.shader.ps_1_1.instructions.PSOpcodes;
import ru.ifmo.gpo.shader.ps_1_1.instructions.PixelShaderInstruction;
import ru.ifmo.gpo.shader.ps_1_1.state.*;

/**
 * User: e_smirnov
 * Date: 23.06.2011
 * Time: 17:38:02
 */
public class PixelShaderTargetArchitecture implements ITargetArchitecture {

    private static final long serialVersionUID = 1L;

    public final static String id = "ps_1_1";

    private PixelShaderCodeGenerator generator = new PixelShaderCodeGenerator();

    private PixelShaderCodeExecutor executor = new PixelShaderCodeExecutor();

    private PixelShaderInstructionSequenceGenerator sequenceGenerator = new PixelShaderInstructionSequenceGenerator(this);

    @Override
    public InstructionSequence preprocessSource(InstructionSequence source) {
        InstructionSequence result = new InstructionSequence();
        for (IGenericInstruction insn : source) {
            if (((PixelShaderInstruction) insn).getOpcodeInternal().getType() != InstructionType.SETUP) {
                result.add(insn);
            }
        }
        return result;
    }

    @Override
    public IInstructionSequenceAnalyzer analyzeCode(InstructionSequence code, IStateSchema stateSchema) {
        return new PSInstructionSequenceAnalyzer(code, (PSStateSchema) stateSchema);
    }

    @Override
    public IMachineState cloneState(IMachineState state) {
        return new PSState(state);
    }

    @Override
    public IStateDelta getStateDelta(IMachineState first, IMachineState second) {
        return new PSStateDelta((PSState) first, (PSState) second);
    }

    @Override
    public boolean isDeltaZero(IMachineState first, IMachineState second) {
        return getStateDelta(first, second).getDelta() == 0;
    }

    @Override
    public IStateSchema buildStateSchema(InstructionSequence code) {
        return ShaderEmulator.buildSchema(code);
    }

    @Override
    public IMachineState createInitialState(IStateSchema schema) {
        return new PSState((PSStateSchema) schema);
    }

    @Override
    public IMachineState evaluateInstruction(IMachineState sourceState, IGenericInstruction insn) {
        return ShaderEmulator.emulateInstruction((PSState) sourceState, (PixelShaderInstruction) insn);
    }

    @Override
    public ICodeGenerator getCodeGenerator() {
        return generator;
    }

    @Override
    public ICodeExecutor getCodeExecutor() {
        return executor;
    }

    @Override
    public IInstructionSequenceGenerator getInstructionSequenceGenerator() {
        return sequenceGenerator;
    }

    @Override
    public IGenericInstruction cloneInstruction(IGenericInstruction instruction) {
        if (!(instruction instanceof PixelShaderInstruction)) {
            throw new IllegalArgumentException("PSInstruction can only be created from another PSInstruction, not from " + instruction.getClass());
        }
        return new PixelShaderInstruction((PixelShaderInstruction) instruction);
    }

    @Override
    public IGenericInstruction createNOPInstruction() {
        return new PixelShaderInstruction();
    }

    @Override
    public boolean isInstructionSequenceValid(InstructionSequence seq) {
        int arithCount = 0;
        int texCount = 0;

        PSStateSchema ss = ShaderEmulator.buildSchema(seq);
        PSState state = new PSState(ss);
        for (IGenericInstruction insn : seq) {
            PixelShaderInstruction psInsn = (PixelShaderInstruction) insn;
            if (psInsn.getOpcodeInternal() == PSOpcodes.NOP) {
                continue;
            }
            InstructionType type = psInsn.getOpcodeInternal().getType();
            if (type == InstructionType.ARITHMETIC) {
                arithCount++;
            } else if (type == InstructionType.TEXTURE) {
                texCount++;
            }
            // validate dependent tex op sequences
            state = ShaderEmulator.emulateInstruction(state, (PixelShaderInstruction) insn);
            for (PSRegister reg : state.getTempRegisters().values()) {
                if (reg.getDependentTexOpRead() > RegisterType.DEPENDENT_TEX_OP_LIMIT) {
                    return false;
                }
            }
        }

        return arithCount <= 64 && texCount <= 32;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void configurationCreated(GPConfiguration gpConfiguration) throws InvalidConfigurationException {
        //gpConfiguration.getStagnationMonitor().addStagnationListener(new PSStagnationListener(gpConfiguration));
    }
}
