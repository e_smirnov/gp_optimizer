package ru.ifmo.gpo.shader.ps_1_1;

import ru.ifmo.gpo.core.BaseCodeGenerationResult;
import ru.ifmo.gpo.core.ICodeExecutionEnvironment;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.vmstate.IStateSchema;

/**
 * User: e_smirnov
 * Date: 23.06.2011
 * Time: 17:40:41
 */
public class PixelShaderCodeGenerationResult extends BaseCodeGenerationResult {

    private String generatedCode;

    private IStateSchema stateSchema;

    private InstructionSequence generated;

    public PixelShaderCodeGenerationResult(String name, InstructionSequence source, InstructionSequence generated, IStateSchema stateSchema) {
        super(name, source);
        this.generated = generated;
        this.stateSchema = stateSchema;
    }

    public PixelShaderCodeGenerationResult(String name, InstructionSequence source, String generatedCode, IStateSchema stateSchema) {
        super(name, source);
        this.generatedCode = generatedCode;
        this.stateSchema = stateSchema;
    }

    @Override
    public ICodeExecutionEnvironment createExecutionEnvironment() {
        return new PixelShaderCodeExecutionEnvironment(stateSchema);
    }

    @Override
    public void dumpOnDisc() {
    }

    public String getGeneratedCode() {
        return generatedCode;
    }

    public InstructionSequence getGenerated() {
        return generated;
    }

    public IStateSchema getStateSchema() {
        return stateSchema;
    }
}
