package ru.ifmo.gpo.shader.ps_1_1.state;

/**
 * Enum for write masks supported in ps 1.1
 * See http://msdn.microsoft.com/en-us/library/windows/desktop/bb219870(v=VS.85).aspx
 */
public enum DestinationWriteMask {
    XYZW(0, 1, 2, 3), // default write mask
    XYZ(0, 1, 2),
    X(0),
    Y(1),
    Z(2),
    XY(0, 1),
    W(3);

    private int[] channelIndices;

    public int getChannelCount() {
        return channelIndices.length;
    }

    public int[] getChannelIndices() {
        return channelIndices;
    }

    DestinationWriteMask(int... channelIndices) {
        this.channelIndices = channelIndices;
    }

    public boolean hasChannel(int index) {
        for (int i : channelIndices) {
            if (i == index) {
                return true;
            }
        }
        return false;
    }
}
