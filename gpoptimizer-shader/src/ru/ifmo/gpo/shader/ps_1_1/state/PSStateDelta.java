package ru.ifmo.gpo.shader.ps_1_1.state;

import ru.ifmo.gpo.core.vmstate.BaseStateDelta;

import java.util.*;

/**
 * User: e_smirnov
 * Date: 17.08.2011
 * Time: 15:21:20
 */
public class PSStateDelta extends BaseStateDelta<PSState> {

    private Set<Channel> channels = new HashSet<Channel>();

    private Map<String, PSRegister> PSRegisters = new HashMap<String, PSRegister>();

    private Set<PSRegister> fullRegisters = new HashSet<PSRegister>();

    public PSStateDelta(PSState source, PSState target) {
        super(source, target);
        processArea(source.getChannels(), target.getChannels(), channels);
        findFullRegisters();
    }

    private void findFullRegisters() {
        for (Channel channel : channels) {
            if (channel.getOwner().satisfiesMaskStrict(DestinationWriteMask.XYZW)) {
                fullRegisters.add(target.getValue(channel.getOwner()));
            }
        }
    }

    /**
     * Counts how many channels of this register must be written to.
     *
     * @param reg Register to count channel writes
     * @return Number of channels that must be written for this register. Should be in range [0,4]
     */
    public int countDesiredChannelsToWrite(PSRegister reg) {
        int count = 0;
        for (Channel c : channels) {
            if (c.getOwner().getId().equals(reg.getId())) {
                count++;
            }
        }
        if (count > 4) {
            throw new IllegalStateException("Register '" + reg.getId() + "' has " + count + " desired channel writes, while registers have only 4 channels");
        }
        return count;
    }

    public Set<PSRegister> getFullRegisters() {
        return fullRegisters;
    }

    public Collection<PSRegister> getPSRegisters() {
        return PSRegisters.values();
    }

    public Set<Channel> getChannels() {
        return channels;
    }

    @Override
    public int getDelta() {
        return channels.size();
    }
}
