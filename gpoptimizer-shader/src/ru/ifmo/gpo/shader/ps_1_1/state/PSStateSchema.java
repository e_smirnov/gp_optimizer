package ru.ifmo.gpo.shader.ps_1_1.state;

import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.vmstate.ConstantStorage;
import ru.ifmo.gpo.core.vmstate.IStateSchema;
import ru.ifmo.gpo.shader.ps_1_1.instructions.PixelShaderInstruction;

import java.util.*;

/**
 * User: e_smirnov
 * Date: 27.07.2011
 * Time: 15:07:03
 */
public class PSStateSchema implements IStateSchema {

    private ConstantStorage constants = new ConstantStorage();

    /**
     * All slots that are used by instruction sequence
     */
    private Map<Object, GenericOperand> operands = new HashMap<Object, GenericOperand>();

    /**
     * Slots that are read before being written, must have values set before code execution
     */
    private Set<Object> requiredContents = new HashSet<Object>();

    /**
     * Setup instructions, like 'ps_2_0', 'dcl' etc that should always be present at the beginning of the shader,
     * and should always be the same
     */
    private List<PixelShaderInstruction> setupInstructions = new ArrayList<PixelShaderInstruction>();

    public void addSetupInstruction(PixelShaderInstruction insn) {
        setupInstructions.add(new PixelShaderInstruction(insn));
    }

    public List<PixelShaderInstruction> getSetupInstructions() {
        return setupInstructions;
    }

    @Override
    public GenericOperand addOrUpdateSlot(GenericOperand slot, Type initialType, GenericOperand.AccessType accessType) {
        if (!(slot instanceof PSRegister)) {
            throw new IllegalArgumentException("Can not add slot of type " + slot.getClass() + " as it is not a subtype of a PSRegister");
        }

        PSRegister alreadyExistingSlot = (PSRegister) operands.get(slot.getId());
        if (alreadyExistingSlot == null) {
            alreadyExistingSlot = (PSRegister) slot;
            operands.put(slot.getId(), slot);
        } else {
            alreadyExistingSlot.mergeChannels((PSRegister) slot);
        }

        if (accessType == GenericOperand.AccessType.READ || accessType == GenericOperand.AccessType.ANY) {
            // noone has written anything here
            if (!alreadyExistingSlot.isWriteable()) {
                requiredContents.add(slot.getId());
            }
        }


        if (accessType == GenericOperand.AccessType.WRITE || accessType == GenericOperand.AccessType.ANY) {
            alreadyExistingSlot.setWritable();
        }

        return alreadyExistingSlot;
    }

    public ConstantStorage getConstants() {
        return constants;
    }

    @Override
    public Iterable<? extends GenericOperand> getSlots() {
        return operands.values();
    }

    public Set<Object> getRequiredContents() {
        return requiredContents;
    }

    public Map<Object, GenericOperand> getOperands() {
        return operands;
    }
}
