package ru.ifmo.gpo.shader.ps_1_1.state;

import ru.ifmo.gpo.shader.ps_1_1.instructions.InstructionType;
import ru.ifmo.gpo.shader.ps_1_1.instructions.PSOpcodes;
import ru.ifmo.gpo.verify.GenericOperator;
import ru.ifmo.gpo.verify.IArithmeticTreeNode;
import ru.ifmo.gpo.verify.UnaryOperatorNode;

/**
 * Possible source register modifiers. They do not influence value in register, instead modify value after it is read and before it is used
 * in operation.
 * see http://msdn.microsoft.com/en-us/library/windows/desktop/bb219863(v=vs.85).aspx
 * <p/>
 * There were quite many of them in 1.x versions, but only 'negate' was left in 2.0
 */
public enum SourceRegisterModifier {

    // - value
    NEGATE(0) {
        @Override
        public String getOutputString() {
            return "-%s";
        }

        @Override
        public double transformValue(PSRegister cg, int channelIdx, double value) {
            return -value;
        }

        @Override
        public IArithmeticTreeNode transformExpression(PSRegister cg, int channelIdx, IArithmeticTreeNode exprToTransform) {
            return new UnaryOperatorNode(exprToTransform, GenericOperator.MINUS, UnaryOperatorNode.OperatorType.TYPE_PREFIX);
        }

        @Override
        public boolean isCompatible(SourceRegisterModifier other) {
            return true;
        }

        @Override
        public boolean isCompatible(PSOpcodes instruction) {
            return instruction.getType() == InstructionType.ARITHMETIC;
        }
    },

    // all channels are read from W channel
    SWIZZLE_XXXX(10) {
        @Override
        public double transformValue(PSRegister cg, int channelIdx, double value) {
            return cg.getChannelDirect(Channel.Name.x.getChannelIdx()).read();
        }

        @Override
        public IArithmeticTreeNode transformExpression(PSRegister cg, int channelIdx, IArithmeticTreeNode exprToTransform) {
            // hack. swizzle is alwayst applied first, so it can read value directly from cg, as it is not yet modified by any other modifier
            return cg.getChannelDirect(Channel.Name.x.getChannelIdx()).getExpression();
        }

        @Override
        public boolean isCompatible(SourceRegisterModifier other) {
            return /*other != BIAS && other != BX2 &&*/ other != SWIZZLE_WWWW && other != SWIZZLE_YYYY && other != SWIZZLE_ZZZZ;
        }

        @Override
        public boolean isCompatible(PSOpcodes instruction) {
            return true; //todo: some individual instructions are actually incompatible
        }

        @Override
        public String getOutputString() {
            return "%s.xxxx";
        }
    },
    // all channels are read from W channel
    SWIZZLE_YYYY(10) {
        @Override
        public double transformValue(PSRegister cg, int channelIdx, double value) {
            return cg.getChannelDirect(Channel.Name.y.getChannelIdx()).read();
        }

        @Override
        public IArithmeticTreeNode transformExpression(PSRegister cg, int channelIdx, IArithmeticTreeNode exprToTransform) {
            // hack. swizzle is alwayst applied first, so it can read value directly from cg, as it is not yet modified by any other modifier
            return cg.getChannelDirect(Channel.Name.y.getChannelIdx()).getExpression();
        }

        @Override
        public boolean isCompatible(SourceRegisterModifier other) {
            return /*other != BIAS && other != BX2&&*/ other != SWIZZLE_WWWW && other != SWIZZLE_XXXX && other != SWIZZLE_ZZZZ;
        }

        @Override
        public boolean isCompatible(PSOpcodes instruction) {
            return true; //todo: some individual instructions are actually incompatible
        }

        @Override
        public String getOutputString() {
            return "%s.yyyy";
        }
    },
    // all channels are read from W channel
    SWIZZLE_ZZZZ(10) {
        @Override
        public double transformValue(PSRegister cg, int channelIdx, double value) {
            return cg.getChannelDirect(Channel.Name.z.getChannelIdx()).read();
        }

        @Override
        public IArithmeticTreeNode transformExpression(PSRegister cg, int channelIdx, IArithmeticTreeNode exprToTransform) {
            // hack. swizzle is alwayst applied first, so it can read value directly from cg, as it is not yet modified by any other modifier
            return cg.getChannelDirect(Channel.Name.z.getChannelIdx()).getExpression();
        }

        @Override
        public boolean isCompatible(SourceRegisterModifier other) {
            return /*other != BIAS && other != BX2 &&*/ other != SWIZZLE_WWWW && other != SWIZZLE_YYYY && other != SWIZZLE_XXXX;
        }

        @Override
        public boolean isCompatible(PSOpcodes instruction) {
            return true; //todo: some individual instructions are actually incompatible
        }

        @Override
        public String getOutputString() {
            return "%s.zzzz";
        }
    },

    // all channels are read from W channel
    SWIZZLE_WWWW(10) {
        @Override
        public double transformValue(PSRegister cg, int channelIdx, double value) {
            return cg.getChannelDirect(Channel.Name.w.getChannelIdx()).read();
        }

        @Override
        public IArithmeticTreeNode transformExpression(PSRegister cg, int channelIdx, IArithmeticTreeNode exprToTransform) {
            // hack. swizzle is alwayst applied first, so it can read value directly from cg, as it is not yet modified by any other modifier
            return cg.getChannelDirect(Channel.Name.w.getChannelIdx()).getExpression();
        }

        @Override
        public boolean isCompatible(SourceRegisterModifier other) {
            //return other != BIAS && other != BX2;
            return true;
        }

        @Override
        public boolean isCompatible(PSOpcodes instruction) {
            return true; //todo: some individual instructions are actually incompatible
        }

        @Override
        public String getOutputString() {
            return "%s.wwww";
        }
    },
    SWIZZLE_YZXW(10) {
        private final int[] indexMap = {2, 0, 1, 3};

        @Override
        public double transformValue(PSRegister cg, int channelIdx, double value) {
            return cg.getChannelDirect(indexMap[channelIdx]).read();
        }

        @Override
        public IArithmeticTreeNode transformExpression(PSRegister cg, int channelIdx, IArithmeticTreeNode exprToTransform) {
            return cg.getChannelDirect(indexMap[channelIdx]).getExpression();
        }

        @Override
        public boolean isCompatible(SourceRegisterModifier other) {
            return other == NEGATE; // swizzles are incompatible with each other and only compatible with negate
        }

        @Override
        public boolean isCompatible(PSOpcodes instruction) {
            return true;
        }

        @Override
        public String getOutputString() {
            return "%s.yzxw";
        }
    },
    SWIZZLE_ZXYW(10) {
        private final int[] indexMap = {1, 2, 3, 0};

        @Override
        public double transformValue(PSRegister cg, int channelIdx, double value) {
            return cg.getChannelDirect(indexMap[channelIdx]).read();
        }

        @Override
        public IArithmeticTreeNode transformExpression(PSRegister cg, int channelIdx, IArithmeticTreeNode exprToTransform) {
            return cg.getChannelDirect(indexMap[channelIdx]).getExpression();
        }

        @Override
        public boolean isCompatible(SourceRegisterModifier other) {
            return other == NEGATE;
        }

        @Override
        public boolean isCompatible(PSOpcodes instruction) {
            return true;
        }

        @Override
        public String getOutputString() {
            return "%s.zxyw";
        }
    },
    SWIZZLE_WZYX(10) {
        private final int[] indexMap = {3, 2, 1, 0};

        @Override
        public double transformValue(PSRegister cg, int channelIdx, double value) {
            return cg.getChannelDirect(indexMap[channelIdx]).read();
        }

        @Override
        public IArithmeticTreeNode transformExpression(PSRegister cg, int channelIdx, IArithmeticTreeNode exprToTransform) {
            return cg.getChannelDirect(indexMap[channelIdx]).getExpression();
        }

        @Override
        public boolean isCompatible(SourceRegisterModifier other) {
            return other == NEGATE;
        }

        @Override
        public boolean isCompatible(PSOpcodes instruction) {
            return true;
        }

        @Override
        public String getOutputString() {
            return "%s.wzyx";
        }
    },;

    /**
     * If there are many modifiers for same arg, they are used based on priority
     */
    private int priority;

    SourceRegisterModifier(int priority) {
        this.priority = priority;
    }

    public int getPriority() {
        return priority;
    }

    /**
     * Applies value transformation of a given channel
     *
     * @param cg         Channel group containing desired channel
     * @param channelIdx Channel index
     * @param value      Value to transform. In case of multiple modifiers for same reg will not be the same as in cg[channelIdx]!
     * @return Value taken from that channel and modified by this modifier
     */
    public abstract double transformValue(PSRegister cg, int channelIdx, double value);

    /**
     * Applies symbolic expression transformation of a given channel
     *
     * @param cg              Channel group containing desired channel
     * @param channelIdx      Channel index
     * @param exprToTransform Expression to transform. In case of multiple modifiers for same reg will not be the same as in cg[channelIdx]!
     * @return Expression for new value modified by this modifier
     */
    public abstract IArithmeticTreeNode transformExpression(PSRegister cg, int channelIdx, IArithmeticTreeNode exprToTransform);

    /**
     * Checks if this modifier can be used for same register in same instruction with other modifier
     *
     * @param other Modifier
     * @return true if this and other can be used together
     */
    public abstract boolean isCompatible(SourceRegisterModifier other);

    /**
     * Checks that this modifier can be used in this instruction
     *
     * @param instruction Instruction to be checked for compatibility with modifier
     * @return true if this modifier can be used in this instruction
     */
    public abstract boolean isCompatible(PSOpcodes instruction);

    /**
     * Returns string in format usable by String.format() that can be used to print expression representing register modified by this modifier
     * Must contain %s, that will be replaced by reg name
     * E.g. for 'invert' operation will be "1 - %s", after replacement will look like "1 - r0"
     *
     * @return String representing this modification of a source reg
     */
    public abstract String getOutputString();

}
