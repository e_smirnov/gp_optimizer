package ru.ifmo.gpo.shader.ps_1_1.state;

import ru.ifmo.gpo.core.instructions.generic.GenericOperand;

/**
 * User: jedi-philosopher
 * Date: 12.07.11
 * Time: 16:03
 */
public enum RegisterType {
    CONSTANT(GenericOperand.AccessType.ANY, 1, -1.0, 1.0) {
        @Override
        public String getNamePrefix() {
            return "c";
        }
    },
    TEMP(GenericOperand.AccessType.ANY, 3, -100.0, 100.0) {
        @Override
        public String getNamePrefix() {
            return "r";
        }
    }, //todo: somehow get real values
    TEXTURE(GenericOperand.AccessType.ANY, 1, 0.0, 1.0) {
        @Override
        public String getNamePrefix() {
            return "t";
        }
    },
    COLOR(GenericOperand.AccessType.READ, 1, 0.0, 1.0) {
        @Override
        public String getNamePrefix() {
            return "v";
        }
    },

    SAMPLER(GenericOperand.AccessType.READ, 1, 0.0, 1.0) {
        @Override
        public String getNamePrefix() {
            return "s";
        }
    },

    OUTPUT_COLOR(GenericOperand.AccessType.WRITE, 1, -100, 100) {
        @Override
        public String getNamePrefix() {
            return "oC";
        }
    };

    /**
     * Maximum number of dependent texture operations sequences
     * See comment in Channel
     */
    public static final int DEPENDENT_TEX_OP_LIMIT = 3;

    public static RegisterType getByPrefix(String prefix) {
        // not iteration over values() as it copies memory
        if (prefix.startsWith(CONSTANT.getNamePrefix())) {
            return CONSTANT;
        }

        if (prefix.startsWith(TEMP.getNamePrefix())) {
            return TEMP;
        }

        if (prefix.startsWith(TEXTURE.getNamePrefix())) {
            return TEXTURE;
        }

        if (prefix.startsWith(COLOR.getNamePrefix())) {
            return COLOR;
        }

        if (prefix.startsWith(SAMPLER.getNamePrefix())) {
            return SAMPLER;
        }

        if (prefix.startsWith(OUTPUT_COLOR.getNamePrefix())) {
            return OUTPUT_COLOR;
        }

        throw new IllegalArgumentException("Invalid prefix " + prefix + " for register type");
    }

    /**
     * Specifies access type of register class. For example, constant registers are readonly, while texture registers
     * are awailable for both reading and writing.
     */
    private GenericOperand.AccessType possibleAccess;

    /**
     * Specifies read port limit of a register type. Registers with limit of N can appear at most N times as
     * source registers in single instruction. E.g. mad r0, v1, v2, v0 is incorrect, as color registers have limit of 2,
     * and in this expression there are 3 color source registers.
     */
    private int readPortLimit;

    private double rangeMin;

    private double rangeMax;

    public abstract String getNamePrefix();

    RegisterType(GenericOperand.AccessType possibleAccess, int readPortLimit, double rangeMin, double rangeMax) {
        this.possibleAccess = possibleAccess;
        this.readPortLimit = readPortLimit;
        this.rangeMin = rangeMin;
        this.rangeMax = rangeMax;
    }

    public GenericOperand.AccessType getPossibleAccess() {
        return possibleAccess;
    }

    public int getReadPortLimit() {
        return readPortLimit;
    }

    public double getRangeMin() {
        return rangeMin;
    }

    public double getRangeMax() {
        return rangeMax;
    }
}
