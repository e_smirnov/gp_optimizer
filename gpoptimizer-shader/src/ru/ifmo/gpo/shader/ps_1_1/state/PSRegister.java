/**
 * User: jedi-philosopher
 * Date: 12.07.11
 * Time: 16:15
 */
package ru.ifmo.gpo.shader.ps_1_1.state;

import ru.ifmo.gpo.core.instructions.generic.GenericRegister;
import ru.ifmo.gpo.verify.IArithmeticTreeNode;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;

/**
 * Class for a single register on a GPU.
 * Register is a complex structure and contains one or more channels (one channel for x, y, z and w component)
 * Channel actually contains values and expression trees, and register only wraps them
 * Full register contains all 4 channels, but under some condition register can have less of them.
 * <p/>
 * In pixel shaders, there are several types of registers available: texture, temp, color etc. All of them have different
 * purposes and access restrictions. Register is referenced by its type and number, like 'temp register #0'. Each type
 * has a one-letter name, e.g. temp registers are 'r', so 'temp reg #1' name will be 'r1'
 */
public class PSRegister extends GenericRegister implements Iterable<Channel> {

    private static final long serialVersionUID = 1L;

    /**
     * Type of this register (temp, color etc)
     */
    private RegisterType type;

    /**
     * Index of this register, together with type forms unique register key
     */
    private int index;

    /**
     * Unique register id string, received from type and index. Stored for performance.
     */
    private String id;

    /**
     * Channels that are available in this register. Array size is up to MAX_CHANNELS, some elements may be null (if respective
     * channel is not available for this register)
     */
    private Channel[] channels;

    /**
     * Source Register Modifiers
     *
     * @link http://msdn.microsoft.com/en-us/library/windows/desktop/bb219868(v=vs.85).aspx
     * Optional list of modifiers for this register. When register is queried for values or expressions, raw values taken from channels
     * are modified using these modifiers.
     */
    private SourceRegisterModifier[] modifiers;

    public static final int MAX_CHANNELS = 4;

    ////////////////////////////////// constructors //////////////////////////////////////////////

    /**
     * Creates register with given string id and full set of channels
     */
    public PSRegister(String id) {
        this.type = RegisterType.getByPrefix(id);
        this.index = Integer.parseInt(id.substring(this.type.getNamePrefix().length()));
        this.id = id;

        fillParts();
    }

    /**
     * Creates register with given type and index and full set of channels
     */
    public PSRegister(RegisterType type, int index) {
        this.type = type;
        this.index = index;
        this.id = getRegisterId(type, index);

        fillParts();
    }

    /**
     * Creates register with given type and index and given set of channels
     */
    public PSRegister(RegisterType type, int index, Channel... channels) {
        this.type = type;
        this.index = index;
        this.id = getRegisterId(type, index);

        this.channels = new Channel[MAX_CHANNELS];
        for (Channel c : channels) {
            if (c == null) {
                continue;
            }
            if (this.channels[c.getName().getChannelIdx()] != null) {
                throw new IllegalArgumentException("Two instances of same channel passed to PSRegister constructor, channel " + c.getName());
            }
            this.channels[c.getName().getChannelIdx()] = new Channel(this, c);
        }
    }

    /**
     * Creates deep copy
     */
    public PSRegister(PSRegister source) {
        this(source.type, source.index, source.channels);
    }

    public PSRegister(PSRegister source, DestinationWriteMask mask) {
        this.type = source.type;
        this.index = source.index;
        this.id = getRegisterId(type, index);

        this.channels = new Channel[MAX_CHANNELS];
        for (Channel c : source.channels) {
            if (c == null) {
                continue;
            }
            if (!mask.hasChannel(c.getName().getChannelIdx())) {
                continue;
            }
            if (this.channels[c.getName().getChannelIdx()] != null) {
                throw new IllegalArgumentException("Two instances of same channel passed to PSRegister constructor, channel " + c.getName());
            }
            this.channels[c.getName().getChannelIdx()] = new Channel(this, c);
        }
    }

    public PSRegister(PSRegister source, SourceRegisterModifier... modifiers) {
        this(source);
        if (source.modifiers == null) {
            this.modifiers = modifiers;
        } else {
            this.modifiers = new SourceRegisterModifier[source.modifiers.length + modifiers.length];
            System.arraycopy(source.modifiers, 0, this.modifiers, 0, source.modifiers.length);
            System.arraycopy(modifiers, 0, this.modifiers, source.modifiers.length, modifiers.length);
        }
        if (modifiers != null && this.modifiers.length > 1) {
            Arrays.sort(this.modifiers, new Comparator<SourceRegisterModifier>() {
                @Override
                public int compare(SourceRegisterModifier o1, SourceRegisterModifier o2) {
                    return o2.getPriority() - o1.getPriority(); // by priority in descending order
                }
            });
        }
    }


    //////////////////////////////////// public methods ////////////////////////////////////////////

    /**
     * Register itself does not contain any expression, call for this method is invalid
     * Throws UnsupportedOperationException
     */
    @Override
    public IArithmeticTreeNode[] getExpressions() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void addAccessType(AccessType type) {
        for (Channel c : channels) {
            if (c != null) {
                c.addAccessType(type);
            }
        }
    }

    @Override
    public boolean hasAccessType(AccessType type) {
        for (Channel c : channels) {
            if (c == null || !c.hasAccessType(type)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void setAccessType(AccessType accessType) {
        for (Channel c : channels) {
            if (c != null) {
                c.setAccessType(accessType);
            }
        }
    }

    @Override
    public AccessType getAccessType() {
        // todo: what if different channels have different access types
        for (Channel c : channels) {
            if (c != null) {
                return c.getAccessType();
            }
        }
        return AccessType.NONE;
    }

    public void setHasValue(boolean value) {
        for (Channel c : channels) {
            if (c != null) {
                c.setHasValue(value);
            }
        }
    }

    @Override
    public boolean isWriteable() {
        for (Channel c : channels) {
            if (c == null) {
                continue;
            }
            if (!c.isWriteable()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean isReadable() {
        for (Channel c : channels) {
            if (c == null) {
                continue;
            }
            if (!c.isReadable()) {
                return false;
            }
        }
        return true;
    }


    public void setWritable() {
        for (Channel c : channels) {
            if (c != null) {
                c.setWritable();
            }
        }
    }

    /**
     * Builds string register id representation from reg type and index
     */
    public static String getRegisterId(RegisterType type, int idx) {
        return type.getNamePrefix() + idx;
    }

    /**
     * Copies channel state from given register into this instance
     */
    public void setValues(PSRegister other) {
        if (other == this) {
            return;
        }
        this.channels = new Channel[MAX_CHANNELS];
        for (Channel c : other.channels) {
            if (c == null) {
                continue;
            }
            this.channels[c.getName().getChannelIdx()] = new Channel(this, c);
        }
    }

    /**
     * Adds channels from other into this, does not modify channels that exist in this but do not exist in other
     */
    public void mergeChannels(PSRegister other) {
        if (other == this) {
            return;
        }

        for (Channel c : other.channels) {
            if (c == null) {
                continue;
            }
            final Channel myChannel = this.channels[c.getName().getChannelIdx()];
            if (myChannel != null) {
                myChannel.addAccessType(c.getAccessType());
                myChannel.setDependentTexOpReads(c.getDependentTexOpReads());
                myChannel.setExpression(c.getExpression());
                myChannel.write(c.read());
            } else {
                this.channels[c.getName().getChannelIdx()] = new Channel(this, c);
            }
        }

    }

    /**
     * Checks if this channel group contains all channels that are specified in provided mask, and nothing more
     *
     * @param mask Mask to check
     * @return True if channel group has only channels from this mask
     */
    public boolean satisfiesMaskStrict(DestinationWriteMask mask) {

        int count = 0;
        for (Channel c : channels) {
            if (c == null) {
                continue;
            }

            if (!mask.hasChannel(c.getName().getChannelIdx())) {
                return false;
            }
            count++;
        }

        return count == mask.getChannelCount();
    }

    public boolean satisfiesMask(DestinationWriteMask mask) {
        int count = mask.getChannelCount();
        for (Channel c : channels) {
            if (c == null) {
                continue;
            }

            if (mask.hasChannel(c.getName().getChannelIdx())) {
                count--;
            }
        }
        return count == 0;
    }

    /**
     * Get channel after applying all modifiers
     */
    public Channel getChannel(int idx) {

        if (modifiers == null) {
            return channels[idx];
        }

        if (channels[idx] == null) {
            return null;
        }
        // apply modifier
        double value = channels[idx].read();
        IArithmeticTreeNode expr = channels[idx].getExpression();
        for (SourceRegisterModifier mod : modifiers) {
            value = mod.transformValue(this, idx, value);
            expr = mod.transformExpression(this, idx, expr);
        }
        return new Channel(this, Channel.Name.getName(idx), value, expr);
    }

    /**
     * Get channel without applying modifiers
     */
    public Channel getChannelDirect(int idx) {
        return channels[idx];
    }

    public Channel getChannel(Channel.Name channelName) {
        return channels[channelName.getChannelIdx()];
    }

    public void setChannel(Channel c) {
        channels[c.getName().getChannelIdx()] = c;
    }

    public boolean hasChannel(int idx) {
        return channels[idx] != null;
    }

    public int getChannelCount() {
        int count = 0;
        for (Channel c : channels) {
            if (c != null) {
                count++;
            }
        }
        return count;
    }

    public int getDependentTexOpRead() {
        int maxVal = -1;
        for (Channel c : channels) {
            if (c == null) {
                continue;
            }
            maxVal = Math.max(maxVal, c.getDependentTexOpReads());
        }
        return maxVal;
    }

    public void setDependentTexOpRead(int newValue) {
        for (Channel c : channels) {
            c.setDependentTexOpReads(newValue);
        }
    }

    /**
     * Returns iterator that iterates only over channels that exist in this register instance
     */
    @Override
    public Iterator<Channel> iterator() {
        return new Iterator<Channel>() {

            private int i = 0;

            private Channel next = null;

            @Override
            public boolean hasNext() {
                if (next() != null) {
                    return true;
                }

                while (i < MAX_CHANNELS) {
                    next = channels[i++];
                    if (next != null) {
                        return true;
                    }
                }
                next = null;
                return false;
            }

            @Override
            public Channel next() {
                Channel r = next;
                next = null;
                return r;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    public RegisterType getRegisterType() {
        return type;
    }

    public int getIndex() {
        return index;
    }

    @Override
    public String getId() {
        return id;
    }

    public Channel[] getChannels() {
        return channels;
    }

    public SourceRegisterModifier[] getModifiers() {
        return modifiers;
    }

    public Channel x() {
        return getChannel(Channel.Name.x.getChannelIdx());
    }

    public Channel y() {
        return getChannel(Channel.Name.y.getChannelIdx());
    }

    public Channel z() {
        return getChannel(Channel.Name.z.getChannelIdx());
    }

    public Channel w() {
        return getChannel(Channel.Name.w.getChannelIdx());
    }

    public void setX(double val) {
        channels[0].write(val);
    }

    public void setY(double val) {
        channels[1].write(val);
    }

    public void setZ(double val) {
        channels[2].write(val);
    }

    public void setW(double val) {
        channels[3].write(val);
    }

    @Override
    public boolean hasValue() {
        for (Channel c : channels) {
            if (c != null && c.hasValue()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("PSRegister ").append(id).append('[');
        for (Channel c : channels) {
            if (c == null) {
                sb.append("<null>");
            } else {
                sb.append(c.getName()).append("=").append(c.read()).append("<").append(c.getAccessType()).append(">");
            }
            sb.append(", ");
        }
        sb.append(']');
        return sb.toString();
    }

    /**
     * Returns string representation of this register that can be used later in code generation, like "-r1.xxxx"
     */
    public String getStringRepresentation() {
        StringBuilder builder = new StringBuilder(id);
        if (getChannelCount() < 4) {
            builder.append(".");
            for (Channel c : channels) {
                if (c == null) {
                    continue;
                }
                builder.append(c.getName().toString());
            }
        }

        String s = builder.toString();
        if (modifiers != null) {
            for (SourceRegisterModifier mod : modifiers) {
                s = String.format(mod.getOutputString(), s);
            }
        }

        return s;
    }

    //////////////////////////////////////// private methods /////////////////////////////////////////
    private void fillParts() {
        channels = new Channel[MAX_CHANNELS];
        channels[0] = new Channel(this, Channel.Name.x);
        channels[1] = new Channel(this, Channel.Name.y);
        channels[2] = new Channel(this, Channel.Name.z);
        channels[3] = new Channel(this, Channel.Name.w);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        PSRegister channels1 = (PSRegister) o;

        if (index != channels1.index) return false;
        if (!Arrays.equals(channels, channels1.channels)) return false;
        if (id != null ? !id.equals(channels1.id) : channels1.id != null) return false;
        if (!Arrays.equals(modifiers, channels1.modifiers)) return false;
        if (type != channels1.type) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + index;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (channels != null ? Arrays.hashCode(channels) : 0);
        result = 31 * result + (modifiers != null ? Arrays.hashCode(modifiers) : 0);
        return result;
    }
}
