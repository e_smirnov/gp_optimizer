/**
 * User: e_smirnov
 * Date: 03.08.2011
 * Time: 15:52:26
 */
package ru.ifmo.gpo.shader.ps_1_1.state;

import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.verify.IArithmeticTreeNode;
import ru.ifmo.gpo.verify.VariableNode;

/**
 * Single channel of a register in a shader. Contains one value - either r, g, b or a
 * Register owns its channels
 */
public class Channel extends GenericOperand {

    private static final long serialVersionUID = 1L;

    public static enum Name {
        x(0),
        y(1),
        z(2),
        w(3);

        private int channelIdx;

        public int getChannelIdx() {
            return channelIdx;
        }

        Name(int channelIdx) {
            this.channelIdx = channelIdx;
        }

        public static Name getName(int idx) {
            for (Name n : values()) {
                if (n.getChannelIdx() == idx) {
                    return n;
                }
            }
            return null;
        }
    }

    /**
     * Comment from compiler error message:
     * <p/>
     * Dependent tex-op sequence too long (4th order). A 1st order dependent tex-op is a tex[ld*|kill] instruction in which either:
     * (1) an r# reg is input (NOT t# reg), or (2) output r# reg was previously written, now being written AGAIN.
     * A 2nd order dependent tex-op occurs if: a tex-op reads OR WRITES to an r# reg whose contents, BEFORE executing the tex-op,
     * depend (perhaps indirectly) on the outcome of a 1st order dependent tex-op. An (n)th order dependent tex-op derives from an (n-1)th order tex-op.
     * A given tex-op may be dependent to at most 3rd order (ps_2_0/x only).
     * <p/>
     * Contains length of tex-op sequence for this register
     * -1 - register was not yet written at all
     * 0 - register was written by some arithmetic instruction
     * >0 - register was written by tex instructions n times dependently
     */
    private int dependentTexOpReads = -1;

    /**
     * Value of a channel (modified by shader emulator)
     */
    private double value;

    private Name name;

    private PSRegister owner;

    private final String id;

    /**
     * true if this channel contains value and can be read
     */
    private boolean hasValue;

    private boolean isWritable;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return owner.getId() + "." + name;
    }

    @Override
    public boolean isReadable() {
        return hasValue;
    }

    @Override
    public AccessType getAccessType() {
        if (isReadable()) {
            if (isWriteable()) {
                return AccessType.ANY;
            }
            return AccessType.READ;
        }
        if (isWritable) {
            return AccessType.WRITE;
        }
        return AccessType.NONE;
    }

    @Override
    public boolean isWriteable() {
        return isWritable;
    }

    public PSRegister getOwner() {
        return owner;
    }

    public Name getName() {
        return name;
    }

    public Channel(PSRegister owner, Name n) {
        super(new VariableNode(owner.getId() + "." + n.name(), Type.DOUBLE_TYPE), Type.DOUBLE_TYPE);
        this.owner = owner;
        this.name = n;
        this.hasValue = false;
        this.id = owner.getId() + "." + n;
    }

    public Channel(PSRegister owner, Name n, double value, IArithmeticTreeNode expr) {
        super(expr, Type.DOUBLE_TYPE);
        this.owner = owner;
        this.name = n;
        this.value = value;
        this.hasValue = true;
        this.id = owner.getId() + "." + n;
    }

    public Channel(Channel proto) {
        this(proto.getOwner(), proto);
    }

    public Channel(PSRegister newOwner, Channel proto) {
        super(proto.getExpression(), Type.DOUBLE_TYPE);
        // do not call other ctor to avoid id string recreation (leads to many unnecessary string concatenations)
        this.owner = newOwner;
        this.name = proto.getName();
        this.value = proto.read();
        this.hasValue = proto.hasValue();
        this.id = proto.getId();
        this.isWritable = proto.isWritable;
        this.accessType = proto.accessType;
        this.dependentTexOpReads = proto.getDependentTexOpReads();
    }

    @Override
    public void setExpression(IArithmeticTreeNode expression) {
        super.setExpression(expression);
        this.hasValue = true;
    }

    @Override
    public void addAccessType(AccessType type) {
        super.addAccessType(type);
        if (type == AccessType.READ || type == AccessType.ANY) {
            // for channel to be readable it must have value explicitly set
            hasValue = true;
        }
    }

    public void setHasValue(boolean value) {
        hasValue = value;
    }

    public void setWritable() {
        isWritable = true;
    }

    @Override
    public boolean hasValue() {
        return hasValue;
    }

    public double read() {
        return value;
    }

    public void write(double value) {
        this.value = value;
    }


    public int getDependentTexOpReads() {
        return dependentTexOpReads;
    }

    public void setDependentTexOpReads(int dependentTexOpReads) {
        this.dependentTexOpReads = dependentTexOpReads;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Channel channel = (Channel) o;

        return name == channel.name && !(owner != null ? !owner.getId().equals(channel.owner.getId()) : channel.owner != null);

    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (owner != null ? owner.getId().hashCode() : 0);
        return result;
    }
}
