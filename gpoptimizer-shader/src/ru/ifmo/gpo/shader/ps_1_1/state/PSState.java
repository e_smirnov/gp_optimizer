package ru.ifmo.gpo.shader.ps_1_1.state;

import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.instructions.generic.GenericStackItem;
import ru.ifmo.gpo.core.vmstate.ConstantStorage;
import ru.ifmo.gpo.core.vmstate.IMachineState;
import ru.ifmo.gpo.shader.ps_1_1.PixelShaderCodeExecutionEnvironment;
import ru.ifmo.gpo.shader.ps_1_1.emulator.Texture;

import java.util.*;

/**
 * User: jedi-philosopher
 * Date: 12.07.11
 * Time: 15:59
 */
public class PSState implements IMachineState {

    private Map<String, PSRegister> constantRegisters = new HashMap<String, PSRegister>();
    private Map<String, PSRegister> tempRegisters = new HashMap<String, PSRegister>();
    private Map<String, PSRegister> colorRegisters = new HashMap<String, PSRegister>();
    private Map<String, PSRegister> textureRegisters = new HashMap<String, PSRegister>();
    private Map<String, PSRegister> samplerRegisters = new HashMap<String, PSRegister>();
    private Map<String, PSRegister> outputColorRegisters = new HashMap<String, PSRegister>();
    private ConstantStorage constants;
    private Texture[] textureStages = new Texture[Texture.MAX_STAGES];

    public PSState() {
        for (int i = 0; i < Texture.MAX_STAGES; ++i) {
            textureStages[i] = new Texture();
        }
        constants = new ConstantStorage();
    }

    private void copyMap(Map<String, PSRegister> dst, Map<String, PSRegister> src) {
        for (Map.Entry<String, PSRegister> entry : src.entrySet()) {
            dst.put(entry.getKey(), new PSRegister(entry.getValue()));
        }
    }

    public PSState(IMachineState s) {
        if (!(s instanceof PSState)) {
            throw new IllegalArgumentException("PSState can only be constructed from another PSState, not from " + s.getClass());
        }
        PSState state = (PSState) s;
        this.constantRegisters = new HashMap<String, PSRegister>();
        copyMap(this.constantRegisters, state.constantRegisters);
        // deep copy for temp and texture registers, as their contained channels can be modified in emulation
        this.tempRegisters = new HashMap<String, PSRegister>();
        copyMap(this.tempRegisters, state.tempRegisters);

        this.outputColorRegisters = new HashMap<String, PSRegister>();
        copyMap(this.outputColorRegisters, state.outputColorRegisters);

        this.colorRegisters = new HashMap<String, PSRegister>(state.colorRegisters);
        this.samplerRegisters = new HashMap<String, PSRegister>(state.samplerRegisters);

        this.textureRegisters = new HashMap<String, PSRegister>();
        copyMap(this.textureRegisters, state.textureRegisters);

        this.constants = new ConstantStorage(state.constants);
        // textures themselves are not copied, as they are only modified when part of execution environment
        System.arraycopy(state.textureStages, 0, textureStages, 0, state.textureStages.length);
    }

    public PSState(PSStateSchema schema) {
        for (GenericOperand op : schema.getSlots()) {
            PSRegister psReg = (PSRegister) op;

            PSRegister myCopy = new PSRegister(psReg);
            getMapByType(psReg.getRegisterType()).put(psReg.getId(), myCopy);
            myCopy.setHasValue(schema.getRequiredContents().contains(myCopy.getId()));
        }
        for (int i = 0; i < Texture.MAX_STAGES; ++i) {
            textureStages[i] = new Texture();
        }
        this.constants = schema.getConstants();
    }

    public PSState(PixelShaderCodeExecutionEnvironment env) {
        for (GenericOperand value : env.getValues()) {
            PSRegister psReg = (PSRegister) value;
            getMapByType(psReg.getRegisterType()).put(psReg.getId(), psReg);
            setValue(psReg);
        }
        constants = new ConstantStorage();
        // no need to copy texture stages themselves as they are not modified by any operation
        System.arraycopy(env.getTextureStages(), 0, textureStages, 0, env.getTextureStages().length);
    }

    public List<PSRegister> getAllRegisters() {
        List<PSRegister> rz = new ArrayList<PSRegister>(8);
        rz.addAll(constantRegisters.values());
        rz.addAll(tempRegisters.values());
        rz.addAll(colorRegisters.values());
        rz.addAll(textureRegisters.values());
        rz.addAll(samplerRegisters.values());
        rz.addAll(outputColorRegisters.values());
        return rz;
    }

    public List<PSRegister> getByTypeAndAccess(RegisterType type, GenericOperand.AccessType t) {
        Map<String, PSRegister> map = getMapByType(type);
        List<PSRegister> rz = new ArrayList<PSRegister>();
        for (PSRegister reg : map.values()) {
            if (reg.hasAccessType(t)) {
                rz.add(reg);
            }
        }
        return rz;
    }

    @Override
    public Map<String, ? extends GenericOperand> getRegisters() {
        Map<String, PSRegister> rz = new HashMap<String, PSRegister>();

        // only temp, output color, contstant and texture here, as only they are mutable
        for (PSRegister reg : tempRegisters.values()) {
            rz.put(reg.getId(), reg);
        }
        for (PSRegister reg : outputColorRegisters.values()) {
            rz.put(reg.getId(), reg);
        }
        for (PSRegister reg : constantRegisters.values()) {
            rz.put(reg.getId(), reg);
        }
        for (PSRegister reg : textureRegisters.values()) {
            rz.put(reg.getId(), reg);
        }

        return rz;
    }

    private Map<String, PSRegister> getMapByType(RegisterType rt) {
        switch (rt) {
            case COLOR:
                return colorRegisters;
            case CONSTANT:
                return constantRegisters;
            case TEMP:
                return tempRegisters;
            case TEXTURE:
                return textureRegisters;
            case SAMPLER:
                return samplerRegisters;
            case OUTPUT_COLOR:
                return outputColorRegisters;
            default:
                throw new IllegalStateException("Unsupported register type");
        }
    }

    public PSRegister getValue(GenericOperand key) {
        if (key instanceof PSRegister) {
            return getMapByType(((PSRegister) key).getRegisterType()).get(key.getId());
        }
        if (key instanceof Channel) {
            return getMapByType(((Channel) key).getOwner().getRegisterType()).get(key.getId());
        }
        throw new IllegalArgumentException("Unsupported operand type " + key.getClass() + ", only PSRegister operands are allowed");
    }

    public Map<String, Channel> getChannels() {
        Map<String, Channel> rz = new HashMap<String, Channel>();
        // only temp texture and const here, as only they are mutable
        for (PSRegister reg : tempRegisters.values()) {
            for (Channel part : reg) {
                rz.put(part.getId(), part);
            }
        }
        for (PSRegister reg : constantRegisters.values()) {
            for (Channel part : reg) {
                rz.put(part.getId(), part);
            }
        }
        for (PSRegister reg : textureRegisters.values()) {
            for (Channel part : reg) {
                rz.put(part.getId(), part);
            }
        }

        for (PSRegister reg : outputColorRegisters.values()) {
            for (Channel part : reg) {
                rz.put(part.getId(), part);
            }
        }
        return rz;
    }

    public Texture[] getTextureStages() {
        return textureStages;
    }

    public Texture getTextureStage(int idx) {
        return textureStages[idx];
    }

    public void setValue(PSRegister newValue) {
        PSRegister existingRegister = getValue(newValue);
        if (existingRegister == null) {
            throw new IllegalArgumentException("Register with id " + newValue.getId() + " is not available");
        }
        existingRegister.setValues(newValue);
        existingRegister.setAccessType(newValue.getAccessType());
        existingRegister.setExpression(newValue.getExpression());
    }

    public void setValue(Channel newValue) {
        PSRegister existingRegister = getValue(newValue.getOwner());
        if (existingRegister == null) {
            throw new IllegalArgumentException("Register with id " + newValue.getId() + " is not available");
        }
        existingRegister.setChannel(newValue);
    }

    public Map<String, PSRegister> getConstantRegisters() {
        return constantRegisters;
    }

    public Map<String, PSRegister> getTempRegisters() {
        return tempRegisters;
    }

    public Map<String, PSRegister> getColorRegisters() {
        return colorRegisters;
    }

    public Map<String, PSRegister> getTextureRegisters() {
        return textureRegisters;
    }

    public Map<String, PSRegister> getSamplerRegisters() {
        return samplerRegisters;
    }

    public Map<String, PSRegister> getOutputColorRegisters() {
        return outputColorRegisters;
    }

    @Override
    public List<? extends GenericStackItem> getStack() {
        return Collections.emptyList();
    }

    @Override
    public Map<String, ? extends GenericOperand> getMemory() {
        return Collections.emptyMap();
    }

    @Override
    public ConstantStorage getConstants() {
        return constants;
    }
}
