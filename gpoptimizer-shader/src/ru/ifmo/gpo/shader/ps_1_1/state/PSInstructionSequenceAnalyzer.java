package ru.ifmo.gpo.shader.ps_1_1.state;

import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.vmstate.BaseInstructionSequenceAnalyzer;
import ru.ifmo.gpo.core.vmstate.IInstructionSequenceAnalyzer;
import ru.ifmo.gpo.shader.ps_1_1.instructions.PixelShaderInstruction;
import ru.ifmo.gpo.shader.ps_1_1.emulator.ShaderEmulator;

/**
 * User: e_smirnov
 * Date: 17.08.2011
 * Time: 15:20:55
 */
public class PSInstructionSequenceAnalyzer extends BaseInstructionSequenceAnalyzer<PSState, PSStateDelta> {

    private PSStateSchema schema;

    public PSInstructionSequenceAnalyzer(InstructionSequence sequence, PSStateSchema schema) {
        super(sequence);
        this.schema = schema;
        analyze();
    }

    @Override
    protected void analyze() {
        PSState state = new PSState(schema);
        initialState = state;
        for (int i = from; i < from + rangeLength; ++i) {
            PixelShaderInstruction instr = (PixelShaderInstruction) source.get(i);
            state = ShaderEmulator.emulateInstruction(state, instr);
            states.add(state);
        }
        stateAfter = states.get(states.size() - 1);
        stateDelta = new PSStateDelta(initialState, stateAfter);
    }

    @Override
    public boolean checkStateEqual(IInstructionSequenceAnalyzer second) {
        return (second instanceof PSInstructionSequenceAnalyzer) && (new PSStateDelta(stateAfter, (PSState) second.getStateAfter()).getDelta() == 0);
    }
}
