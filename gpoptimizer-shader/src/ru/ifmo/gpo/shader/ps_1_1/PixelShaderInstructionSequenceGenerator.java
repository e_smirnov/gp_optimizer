package ru.ifmo.gpo.shader.ps_1_1;

import ru.ifmo.gpo.core.BaseInstructionSequenceGenerator;
import ru.ifmo.gpo.core.ITargetArchitecture;
import ru.ifmo.gpo.core.instructions.IOperandSelector;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.instructions.generic.IGenericInstruction;
import ru.ifmo.gpo.core.vmstate.FailedToSatisfyRestrictionsException;
import ru.ifmo.gpo.core.vmstate.IStateDelta;
import ru.ifmo.gpo.core.vmstate.NoSuitableOperandFoundException;
import ru.ifmo.gpo.shader.ps_1_1.instructions.InstructionModifier;
import ru.ifmo.gpo.shader.ps_1_1.instructions.InstructionType;
import ru.ifmo.gpo.shader.ps_1_1.instructions.PSOpcodes;
import ru.ifmo.gpo.shader.ps_1_1.instructions.PixelShaderInstruction;
import ru.ifmo.gpo.shader.ps_1_1.state.PSRegister;
import ru.ifmo.gpo.shader.ps_1_1.state.PSState;
import ru.ifmo.gpo.shader.ps_1_1.state.PSStateDelta;
import ru.ifmo.gpo.shader.ps_1_1.state.RegisterType;
import ru.ifmo.gpo.util.CollectionUtils;

import java.util.*;

/**
 * User: jedi-philosopher
 * Date: 19.09.11
 * Time: 18:13
 */
public class PixelShaderInstructionSequenceGenerator extends BaseInstructionSequenceGenerator<PSState, PSStateDelta> {

    public PixelShaderInstructionSequenceGenerator(ITargetArchitecture target) {
        super(target);
    }

    @Override
    protected double computeBreakProbability(int resultSize, int desiredLength, int stateDelta) {
        if (stateDelta != 0) {
            return -1;
        }
        return resultSize / (double) desiredLength - 0.5;
    }

    private Collection<InstructionModifier> createModifiersForInstruction(PSState state, PSOpcodes opcode, List<GenericOperand> operands) {
        if (!opcode.isSupportsModifiers()) {
            return Collections.emptyList();
        }

        // currently there are 3 different modifiers available. actually, instruction may have > 1 modifiers, but as all
        // supported are x2 x4 d2 there is no use in combining them
        Random r = new Random();
        if (r.nextBoolean()) {
            return Collections.emptyList();
        }

        // to apply modifiers, all channels of a dest register must be available
        PSRegister dest = state.getValue(operands.get(0));
        for (int i = 0; i < 4; ++i) {
            if (!dest.hasChannel(i) || !dest.getChannel(i).hasValue()) {
                return Collections.emptyList();
            }
        }

        return CollectionUtils.selectRandomElementToCollection(InstructionModifier.values());
    }

    @Override
    protected IGenericInstruction selectInstructionSatisfyRestrictions(InstructionSequence alreadyGenerated, PSState state, PSStateDelta delta) throws FailedToSatisfyRestrictionsException {
        List<PixelShaderInstruction> candidates = new LinkedList<PixelShaderInstruction>();
        List<GenericOperand> operands;

        PixelShaderInstruction prevInsn = alreadyGenerated.isEmpty() ? null : (PixelShaderInstruction) alreadyGenerated.get(alreadyGenerated.size() - 1);

        // cache selectors that failed to select anything under current state
        Set<IOperandSelector> failedSelectors = new HashSet<IOperandSelector>();

        final boolean constWritesRemaining = deltaHasWritesToConstRegs(delta);

        for (PSOpcodes op : PSOpcodes.values()) {
            if (constWritesRemaining && op != PSOpcodes.DEF) {
                // all writes to constant registers must be completed before any other operations may happen
                continue;
            }
            if (prevInsn != null && ((op.getType() == InstructionType.TEXTURE && prevInsn.getOpcodeInternal().getType() == InstructionType.ARITHMETIC)
                    || (op.getType() == InstructionType.SETUP && (prevInsn.getOpcodeInternal().getType() != InstructionType.SETUP && prevInsn.getOpcodeInternal().getType() != InstructionType.CONSTANT))
                    || (op.getType() == InstructionType.CONSTANT && (prevInsn.getOpcodeInternal().getType() == InstructionType.ARITHMETIC || prevInsn.getOpcodeInternal().getType() == InstructionType.TEXTURE)))) {
                // texture instructions may not follow arithmetic ones
                // constant instructions may only follow each other and version instruction (must be at the beginning of the sequence
                continue;
            }

            IOperandSelector selector = op.getOperandSelector();
            if (failedSelectors.contains(selector)) {
                continue;
            } else {
                try {
                    operands = selector.selectOparandsSatisfyRestrictions(op, state, delta.getTarget(), delta);
                } catch (NoSuitableOperandFoundException e) {
                    // just continue to next opcode
                    failedSelectors.add(selector);
                    continue;
                }
            }
            candidates.add(new PixelShaderInstruction(op, createModifiersForInstruction(state, op, operands), operands));
        }

        if (candidates.isEmpty()) {
            throw new FailedToSatisfyRestrictionsException();
        }

        return CollectionUtils.selectRandomElement(candidates);
    }

    @Override
    protected IGenericInstruction selectAnyInstruction(InstructionSequence alreadyGenerated, PSState state) throws FailedToSatisfyRestrictionsException {
        List<PixelShaderInstruction> candidates = new LinkedList<PixelShaderInstruction>();
        List<GenericOperand> operands;

        PixelShaderInstruction prevInsn = alreadyGenerated.isEmpty() ? null : (PixelShaderInstruction) alreadyGenerated.get(alreadyGenerated.size() - 1);
        // cache selectors that failed to select anything under current state
        Set<IOperandSelector> failedSelectors = new HashSet<IOperandSelector>();

        for (PSOpcodes op : PSOpcodes.values()) {
            if (prevInsn != null && ((op.getType() == InstructionType.TEXTURE && prevInsn.getOpcodeInternal().getType() == InstructionType.ARITHMETIC)
                    || (op.getType() == InstructionType.CONSTANT && (prevInsn.getOpcodeInternal().getType() == InstructionType.ARITHMETIC || prevInsn.getOpcodeInternal().getType() == InstructionType.TEXTURE)))) {
                // texture instructions may not follow arithmetic ones
                // constant instructions may only follow each other and version instruction (must be at the beginning of the sequence
                continue;
            }

            IOperandSelector selector = op.getOperandSelector();
            if (failedSelectors.contains(selector)) {
                continue;
            } else {
                try {
                    operands = selector.selectOperandsRandom(op, state);
                } catch (NoSuitableOperandFoundException e) {
                    // just continue to next opcode
                    failedSelectors.add(selector);
                    continue;
                }
            }
            candidates.add(new PixelShaderInstruction(op, createModifiersForInstruction(state, op, operands), operands));
        }

        if (candidates.isEmpty()) {
            throw new FailedToSatisfyRestrictionsException();
        }

        return CollectionUtils.selectRandomElement(candidates);
    }

    @Override
    protected double computeRestrictionSatisfactionSelectionProbability(IStateDelta stateDelta, int length, InstructionSequence result) {
        if (deltaHasWritesToConstRegs((PSStateDelta) stateDelta)) {
            return 1.0;
        }
        return super.computeRestrictionSatisfactionSelectionProbability(stateDelta, length, result);    //To change body of overridden methods use File | Settings | File Templates.
    }

    private boolean deltaHasWritesToConstRegs(PSStateDelta delta) {
        for (PSRegister reg : delta.getFullRegisters()) {
            if (reg.getRegisterType() == RegisterType.CONSTANT) {
                // const operation must be completed before any others, as def instructions must always be at the beginning of the file
                return true;
            }
        }
        return false;
    }
}
