package ru.ifmo.gpo.shader.ps_1_1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ifmo.gpo.core.*;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.IGenericInstruction;
import ru.ifmo.gpo.shader.ps_1_1.emulator.ShaderEmulator;
import ru.ifmo.gpo.shader.ps_1_1.instructions.PixelShaderInstruction;
import ru.ifmo.gpo.shader.ps_1_1.state.PSState;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Code executor for pixel shaders.
 * Uses emulation for result evaluation and NvShaderPerf utility for performance checks
 * Requires nvshaderperf v2.0 to be installed (http://www.nvidia.ru/object/nvshaderperf_home.html)
 */
public class PixelShaderCodeExecutor implements ICodeExecutor {

    private static Logger logger = LoggerFactory.getLogger(PixelShaderCodeExecutor.class);

    /**
     * Regexp used to find performance data in nvshaderperf output
     */
    private static Pattern cyclesPattern = Pattern.compile("Results [\\d]+ cycles, [\\d]+ r regs, ([\\d,]+) pixels/s");

    public PixelShaderCodeExecutor() {
        // test that nvshaderperf is available on system
        Runtime run = Runtime.getRuntime();
        try {
            logger.info("Checking for existence of NVShaderPerf application");
            Process pr = run.exec("nvshaderperf -listgpus");

            BufferedReader reader = new BufferedReader(new InputStreamReader(pr.getInputStream()));
            do {
                String ln = reader.readLine();
                if (ln == null) {
                    logger.warn("No output or unrecognized output from NVShaderPerf application (output is '{}'), shader profiling may work wrong", reader.toString());
                    return;
                }

                if (ln.contains("NVShaderPerf : version 2")) {
                    logger.info("NVShaderPerf found");
                    return;
                }
            } while (true);
        } catch (Exception e) {
            logger.error("Failed to find nvshaderperf application, shader profiling can not be done");
            throw new RuntimeException("Failed to find nvshaderperf application", e);
        }
    }

    /**
     * Parses all output from nvshaderperf and returns profile value for shader in ms/gigapixel
     *
     * @param profileOutput All output from tool
     * @return ms/gigapixel
     */
    private long parseProfileOutput(String profileOutput) {
        Matcher matcher = cyclesPattern.matcher(profileOutput);
        if (!matcher.find()) {
            logger.error("Illegal output from nvshaderperf: {}, unable to continue", profileOutput);
            throw new RuntimeException("Failed to parse nvshaderperf output: " + profileOutput);
        }
        final String str = matcher.group(1).replace(",", "");
        long pixelsPerSec = Long.parseLong(str);
        return (long) (1000 / (pixelsPerSec / 10e9)); // ms per gigapixel
    }

    @Override
    public long profileCode(ICodeGenerationResult code, ICodeExecutionEnvironment env) {
        String codeString = ((PixelShaderCodeGenerationResult) code).getGeneratedCode();
        File outputFile = new File(Settings.getOutputDirName(), code.getName() + ".ps");
        try {
            FileWriter writer = new FileWriter(outputFile);
            writer.write(codeString);
            writer.close();

            Runtime run = Runtime.getRuntime();
            try {
                Process pr = run.exec("nvshaderperf.exe -gpu G70 " + outputFile.getAbsolutePath());
                BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
                BufferedReader errorBuf = new BufferedReader(new InputStreamReader(pr.getErrorStream()));
                StringBuilder builder = new StringBuilder();
                do {
                    String errorS = errorBuf.readLine();
                    if (errorS != null) {
                        logger.warn("Output from nvshaderperf to stderr: " + errorS);
                    }

                    String s = buf.readLine();
                    if (s == null) {
                        break;
                    }
                    builder.append(s);
                    Thread.sleep(10);
                } while (true);

                return parseProfileOutput(builder.toString());
            } catch (Exception e) {
                logger.error("Failed to profile pixel shader", e);
                throw new RuntimeException("Failed to profile pixel shader", e);
            }


        } catch (IOException e) {
            logger.error("Failed to profile pixel shader code", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public IExecutionResult executeCode(ICodeGenerationResult code, ICodeExecutionEnvironment env) {
        PSState state = new PSState((PixelShaderCodeExecutionEnvironment) env);
        InstructionSequence seq = ((PixelShaderCodeGenerationResult) code).getGenerated();
        for (IGenericInstruction insn : seq) {
            state = ShaderEmulator.emulateInstruction(state, (PixelShaderInstruction) insn);
        }
        return new PixelShaderExecutionResult(state);
    }
}
