package ru.ifmo.gpo.shader.ps_1_1.instructions;

import ru.ifmo.gpo.core.instructions.generic.BaseGenericInstruction;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.shader.ps_1_1.state.PSRegister;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * User: e_smirnov
 * Date: 23.06.2011
 * Time: 17:39:58
 */
public class PixelShaderInstruction extends BaseGenericInstruction {

    private PSOpcodes opcodeInternal;

    private List<InstructionModifier> modifiers;

    public PixelShaderInstruction() {
        this(PSOpcodes.NOP, Collections.<GenericOperand>emptyList());
    }

    public PixelShaderInstruction(PixelShaderInstruction proto) {
        this(proto.opcodeInternal, proto.getParameters());
        if (proto.modifiers != null) {
            modifiers = new ArrayList<InstructionModifier>(proto.modifiers);
        }
    }

    public PixelShaderInstruction(PSOpcodes opcode, GenericOperand... params) {
        this(opcode, Collections.<InstructionModifier>emptyList(), params);
    }

    public PixelShaderInstruction(PSOpcodes opcode, Collection<InstructionModifier> modifiers, GenericOperand... params) {
        super(opcode.getOpcode(), params);
        this.opcodeInternal = opcode;
        if (!modifiers.isEmpty()) {
            this.modifiers = new ArrayList<InstructionModifier>(modifiers);
        }
    }

    public PixelShaderInstruction(PSOpcodes opcode, Collection<InstructionModifier> modifiers, Collection<GenericOperand> params) {
        super(opcode.getOpcode(), params);
        this.opcodeInternal = opcode;
        if (!modifiers.isEmpty()) {
            this.modifiers = new ArrayList<InstructionModifier>(modifiers);
        }
    }

    public PixelShaderInstruction(PSOpcodes opcode, Collection<GenericOperand> params) {
        super(opcode.getOpcode(), params);
        this.opcodeInternal = opcode;
    }

    public PSOpcodes getOpcodeInternal() {
        return opcodeInternal;
    }

    public List<InstructionModifier> getModifiers() {
        return modifiers;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(opcodeInternal.toString().toLowerCase());
        if (modifiers != null) {
            for (InstructionModifier mod : modifiers) {
                builder.append("_").append(mod.toString().toLowerCase());
            }
        }
        builder.append(' ');
        boolean first = true;
        for (GenericOperand op : getParameters()) {
            if (!first) {
                builder.append(", ");
            } else {
                first = false;
            }
            if (op instanceof PSRegister) {
                builder.append(((PSRegister) op).getStringRepresentation());
            } else {
                builder.append(op.toString());
            }
        }
        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        PixelShaderInstruction that = (PixelShaderInstruction) o;

        if (modifiers != null ? !modifiers.equals(that.modifiers) : that.modifiers != null) return false;
        if (opcodeInternal != that.opcodeInternal) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (opcodeInternal != null ? opcodeInternal.hashCode() : 0);
        result = 31 * result + (modifiers != null ? modifiers.hashCode() : 0);
        return result;
    }
}