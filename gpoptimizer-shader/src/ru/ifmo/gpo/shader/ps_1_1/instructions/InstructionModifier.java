package ru.ifmo.gpo.shader.ps_1_1.instructions;

import org.objectweb.asm.Type;
import ru.ifmo.gpo.verify.IArithmeticTreeNode;
import ru.ifmo.gpo.verify.MinMaxNode;
import ru.ifmo.gpo.verify.NumericConstantTreeNode;

/**
 * Pixel shader instructions may have modifiers
 * Described here http://msdn.microsoft.com/en-us/library/windows/desktop/bb219849(v=vs.85).aspx
 * e.g. add_x2 instruction will multiply result of addition by 2
 */

public enum InstructionModifier {
    SAT {
        @Override
        public IArithmeticTreeNode getExpression(IArithmeticTreeNode origExpression) {
            return new MinMaxNode(new NumericConstantTreeNode(Type.DOUBLE_TYPE, 0.0), new MinMaxNode(origExpression, new NumericConstantTreeNode(Type.DOUBLE_TYPE, 1.0), MinMaxNode.MIN), MinMaxNode.MAX);
        }

        @Override
        public double getValue(double origValue) {
            return Math.max(0, Math.min(origValue, 1.0));
        }
    };

    // Operations that apply this modifier to contents of register
    public abstract IArithmeticTreeNode getExpression(IArithmeticTreeNode origExpression);

    public abstract double getValue(double origValue);
}
