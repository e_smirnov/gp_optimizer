package ru.ifmo.gpo.shader.ps_1_1.instructions;

import ru.ifmo.gpo.core.instructions.IOperandSelector;
import ru.ifmo.gpo.core.instructions.selectors.DummyOperandSelector;
import ru.ifmo.gpo.core.instructions.selectors.OpcodeDisabler;
import ru.ifmo.gpo.shader.ps_1_1.selectors.*;
import ru.ifmo.gpo.shader.ps_1_1.state.DestinationWriteMask;
import ru.ifmo.gpo.shader.ps_1_1.state.PSState;
import ru.ifmo.gpo.shader.ps_1_1.state.PSStateDelta;

/**
 * User: jedi-philosopher
 * Date: 12.07.11
 * Time: 20:38
 */
public enum PSOpcodes {

    // special marker instruction
    PS_2_0(InstructionType.SETUP, OpcodeDisabler.getInstance()),
    DCL(InstructionType.SETUP, OpcodeDisabler.getInstance()),
    DCL_2D(InstructionType.SETUP, OpcodeDisabler.getInstance()),
    DCL_CUBE(InstructionType.SETUP, OpcodeDisabler.getInstance()),
    DCL_VOLUME(InstructionType.SETUP, OpcodeDisabler.getInstance()),

    // constants
    DEF(InstructionType.CONSTANT, new DefOperandSelector()),

    //arithmetic
    ADD(InstructionType.ARITHMETIC, new PSRegisterOperandSelector(2), true, DestinationWriteMask.values()),
    DP3(InstructionType.ARITHMETIC, new PSRegisterOperandSelector(2), true),
    LRP(InstructionType.ARITHMETIC, new LRPOperandSelector(), true),
    MAD(InstructionType.ARITHMETIC, new PSRegisterOperandSelector(3), true, DestinationWriteMask.values()),
    MOV(InstructionType.ARITHMETIC, new PSRegisterOperandSelector(1), true, DestinationWriteMask.values()),
    MUL(InstructionType.ARITHMETIC, new PSRegisterOperandSelector(2), true, DestinationWriteMask.values()),
    NOP(InstructionType.ARITHMETIC, DummyOperandSelector.getInstance()),
    SUB(InstructionType.ARITHMETIC, new PSRegisterOperandSelector(2), true, DestinationWriteMask.values()),

    RCP(InstructionType.ARITHMETIC, new RCPOperandSelector(), false, DestinationWriteMask.values()),

    TEXLD(InstructionType.TEXTURE, new TextureOperandSelector());

    private int opcode;

    private InstructionType type;

    /**
     * If this instruction support instruction modifiers (like add_x2)
     * Arithmetic instructions support them, while texture do not
     */
    private boolean supportsModifiers = false;

    private DestinationWriteMask[] supportedDestWriteMasks;

    private IOperandSelector<PSOpcodes, PSState, PSStateDelta> operandSelector;

    PSOpcodes(InstructionType type, IOperandSelector<PSOpcodes, PSState, PSStateDelta> operandSelector, DestinationWriteMask... masks) {
        this(type, operandSelector);
        this.supportedDestWriteMasks = masks;
    }

    PSOpcodes(InstructionType type, IOperandSelector<PSOpcodes, PSState, PSStateDelta> operandSelector, boolean supportModifiers, DestinationWriteMask... masks) {
        this(type, operandSelector);
        this.supportsModifiers = supportModifiers;
        this.supportedDestWriteMasks = masks;
    }

    PSOpcodes(InstructionType type, IOperandSelector<PSOpcodes, PSState, PSStateDelta> operandSelector, boolean supportsModifiers) {
        this.operandSelector = operandSelector;
        this.opcode = this.ordinal();
        this.supportsModifiers = supportsModifiers;
        this.type = type;
    }

    PSOpcodes(InstructionType type, IOperandSelector<PSOpcodes, PSState, PSStateDelta> operandSelector) {
        this.operandSelector = operandSelector;
        this.opcode = this.ordinal();
        this.type = type;
    }

    public DestinationWriteMask[] getSupportedDestWriteMasks() {
        return supportedDestWriteMasks;
    }

    public int getOpcode() {
        return opcode;
    }

    public InstructionType getType() {
        return type;
    }

    public boolean isSupportsModifiers() {
        return supportsModifiers;
    }

    public IOperandSelector<PSOpcodes, PSState, PSStateDelta> getOperandSelector() {
        return operandSelector;
    }
}
