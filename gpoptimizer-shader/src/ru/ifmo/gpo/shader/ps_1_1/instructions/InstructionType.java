/**
 * User: jedi-philosopher
 * Date: 05.02.12
 * Time: 13:51
 */
package ru.ifmo.gpo.shader.ps_1_1.instructions;

/**
 * Shows to which group instruction belongs
 * Some operations can be performed only for certain groups of instructions, e.g. texture instruction can not follow non-texture one
 */
public enum InstructionType {
    SETUP, // version insn like ps_2_0, should always be the first
    TEXTURE, // texture sampling
    ARITHMETIC,
    CONSTANT // constant declaration, def
}
