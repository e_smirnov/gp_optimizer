import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import java.io.FileOutputStream;
import java.io.IOException;


public class test_example_generator {

    public static void main(String[] args) {
        int iterations = 10000;
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS);
        cw.visit(Opcodes.V1_3, Opcodes.ACC_PUBLIC, "Example", null, "java/lang/Object", null);
        // creates a MethodWriter for the (implicit) constructor
        MethodVisitor mw = cw.visitMethod(Opcodes.ACC_PUBLIC,
                "<init>",
                "()V",
                null,
                null);
        // pushes the 'this' variable
        mw.visitVarInsn(Opcodes.ALOAD, 0);
        // invokes the super class constructor
        mw.visitMethodInsn(Opcodes.INVOKESPECIAL, "java/lang/Object", "<init>", "()V");
        mw.visitInsn(Opcodes.RETURN);
        // this code uses a maximum of one stack element and one local variable
        mw.visitMaxs(1, 1);
        mw.visitEnd();
        // implicit constructor generated

        mw = cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC, "defaultCode", "(I)J", null, null);

        // starting profile here
        //mw.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/System", "currentTimeMillis", "()J");
        mw.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/System", "nanoTime", "()J");
        mw.visitVarInsn(Opcodes.LSTORE, 1);

        mw.visitInsn(Opcodes.ICONST_0);
        mw.visitVarInsn(Opcodes.ISTORE, 3);
        Label l = new Label();
        mw.visitLabel(l);
        mw.visitVarInsn(Opcodes.ILOAD, 3);
        mw.visitLdcInsn(new Integer(iterations));

        Label target = new Label();
        mw.visitJumpInsn(Opcodes.IF_ICMPGE, target);

        mw.visitVarInsn(Opcodes.ILOAD, 0);
        mw.visitVarInsn(Opcodes.ILOAD, 0);
        mw.visitInsn(Opcodes.IMUL);
        mw.visitVarInsn(Opcodes.ILOAD, 0);
        mw.visitInsn(Opcodes.IMUL);
        mw.visitVarInsn(Opcodes.ILOAD, 0);
        mw.visitVarInsn(Opcodes.ILOAD, 0);
        mw.visitInsn(Opcodes.IMUL);
        mw.visitInsn(Opcodes.IADD);
        mw.visitVarInsn(Opcodes.ILOAD, 0);
        mw.visitInsn(Opcodes.IADD);


        mw.visitVarInsn(Opcodes.ISTORE, 0);


        mw.visitIincInsn(3, 1);
        mw.visitJumpInsn(Opcodes.GOTO, l);
        mw.visitLabel(target);

        //mw.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/System", "currentTimeMillis", "()J");
        mw.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/System", "nanoTime", "()J");
        mw.visitVarInsn(Opcodes.LLOAD, 1);
        mw.visitInsn(Opcodes.LSUB);


        mw.visitInsn(Opcodes.LRETURN);
        mw.visitMaxs(0, 0);
        mw.visitEnd();

        mw = cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC, "bestNoJIT", "(I)J", null, null);

        // starting profile here
        mw.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/System", "nanoTime", "()J");
        mw.visitVarInsn(Opcodes.LSTORE, 1);

        mw.visitInsn(Opcodes.ICONST_0);
        mw.visitVarInsn(Opcodes.ISTORE, 3);
        l = new Label();
        mw.visitLabel(l);
        mw.visitVarInsn(Opcodes.ILOAD, 3);
        mw.visitLdcInsn(new Integer(iterations));

        target = new Label();
        mw.visitJumpInsn(Opcodes.IF_ICMPGE, target);

        mw.visitVarInsn(Opcodes.ILOAD, 0);
        mw.visitInsn(Opcodes.DUP);
        mw.visitInsn(Opcodes.IMUL);
        mw.visitVarInsn(Opcodes.ILOAD, 0);
        mw.visitInsn(Opcodes.IADD);
        mw.visitInsn(Opcodes.ICONST_1);
        mw.visitInsn(Opcodes.IOR);
        mw.visitVarInsn(Opcodes.ILOAD, 0);
        mw.visitInsn(Opcodes.IMUL);

        mw.visitVarInsn(Opcodes.ISTORE, 0);


        mw.visitIincInsn(3, 1);
        mw.visitJumpInsn(Opcodes.GOTO, l);
        mw.visitLabel(target);

        mw.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/System", "nanoTime", "()J");
        mw.visitVarInsn(Opcodes.LLOAD, 1);
        mw.visitInsn(Opcodes.LSUB);

        mw.visitInsn(Opcodes.LRETURN);
        mw.visitMaxs(5, 4);
        mw.visitEnd();

        mw = cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC, "bestWithJIT", "(I)J", null, null);

        // starting profile here

        mw.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/System", "nanoTime", "()J");
        mw.visitVarInsn(Opcodes.LSTORE, 1);

        mw.visitInsn(Opcodes.ICONST_0);
        mw.visitVarInsn(Opcodes.ISTORE, 3);
        l = new Label();
        mw.visitLabel(l);
        mw.visitVarInsn(Opcodes.ILOAD, 3);
        mw.visitLdcInsn(new Integer(iterations));

        target = new Label();
        mw.visitJumpInsn(Opcodes.IF_ICMPGE, target);

        mw.visitVarInsn(Opcodes.ILOAD, 0);
        mw.visitInsn(Opcodes.ICONST_1);
        mw.visitInsn(Opcodes.IADD);
        mw.visitVarInsn(Opcodes.ILOAD, 0);
        mw.visitInsn(Opcodes.IMUL);
        mw.visitInsn(Opcodes.ICONST_1);
        mw.visitInsn(Opcodes.IOR);
        mw.visitVarInsn(Opcodes.ILOAD, 0);
        mw.visitInsn(Opcodes.IMUL);
        mw.visitVarInsn(Opcodes.ISTORE, 0);

        mw.visitIincInsn(3, 1);
        mw.visitJumpInsn(Opcodes.GOTO, l);
        mw.visitLabel(target);

        mw.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/System", "nanoTime", "()J");
        mw.visitVarInsn(Opcodes.LLOAD, 1);
        mw.visitInsn(Opcodes.LSUB);

        mw.visitInsn(Opcodes.LRETURN);
        mw.visitMaxs(0, 0);
        mw.visitEnd();


        mw = cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC, "test2Default", "(II)I", null, null);
        mw.visitVarInsn(Opcodes.ILOAD, 0);
        mw.visitInsn(Opcodes.ICONST_M1);
        mw.visitInsn(Opcodes.IXOR);
        mw.visitVarInsn(Opcodes.ILOAD, 1);
        mw.visitInsn(Opcodes.IAND);
        mw.visitInsn(Opcodes.IRETURN);
        mw.visitMaxs(0, 0);
        mw.visitEnd();

        mw = cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC, "test2Optimized", "(II)I", null, null);
        mw.visitVarInsn(Opcodes.ILOAD, 0);
        mw.visitVarInsn(Opcodes.ILOAD, 1);
        mw.visitInsn(Opcodes.IXOR);
        mw.visitVarInsn(Opcodes.ILOAD, 1);
        mw.visitInsn(Opcodes.IAND);
        mw.visitInsn(Opcodes.IRETURN);
        mw.visitMaxs(0, 0);
        mw.visitEnd();


        byte[] compiledCode = cw.toByteArray();
        try {
            FileOutputStream fos = new FileOutputStream("Example.class");
            fos.write(compiledCode);
            fos.close();
        } catch (IOException ignore) {

        }


    }
}