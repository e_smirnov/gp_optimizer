import os
import re
import subprocess

def getRunTime(iterCount, targetName):
	args = r'"D:\Program Files\Java\jdk1.6.0_14\bin\java" -server -Xmx500m ExampleRunner ' + str(iterCount) + " " + targetName
	print args
	process = subprocess.Popen(args = args, stdout = subprocess.PIPE)
	(stdout, stderr) = process.communicate()
	for m in re.finditer("Completed in ([\d]+)", stdout):
		return m.group(1)
	raise Exception("Incorrect output: " + stdout)

records = []
for p in range(3, 8):
	iterCount = pow(10, p)
	print "Processing with " , iterCount , " iterations"
	defaultRunTime = getRunTime(iterCount, "default")
	optimizedRunTime = getRunTime(iterCount, "optimized")
	records.append((str(iterCount), defaultRunTime, optimizedRunTime))
	
result = open("output.csv", "wt")
result.write("iteration count; default code run time; optimized code run time\n")
for record in records:
	result.write(record[0] + ";" + record[1] + ";" + record[2] + "\n")
result.close()
	