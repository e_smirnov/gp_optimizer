public class ExampleRunner {
    public static long processProfileResults(long[] times, int offset) {
        long rz = 0;
        // excludes evidently erroneous results

        double y = 0.0;
        int validTimes = 0;

        for (int i = offset; i < times.length; ++i) {
            if (times[i] != -1) {
                y += times[i];
                validTimes++;
            }
        }
        if (validTimes == 1) {
            return (long) y;
        }
        if (validTimes == 0) {
            return -1;
        }

        y /= validTimes;

        while (true) {
            double S2 = 0;
            double maxDiff = -1;
            int diffIdx = -1;
            validTimes = 0;

            for (int i = offset; i < times.length; ++i) {
                if (times[i] == -1) {
                    continue;
                }
                long l = times[i];
                S2 += Math.pow(l - y, 2);
                validTimes++;

                if (Math.abs(l - y) > maxDiff) {
                    maxDiff = Math.abs(l - y);
                    diffIdx = i;
                }
            }
            if (validTimes <= 2) {
                break;
            }

            S2 *= 1 / (double) (validTimes - 1);

            double U = maxDiff / Math.sqrt(S2);
            if (U > 1.2) {
                times[diffIdx] = -1;

            } else {
                break;
            }
        }

        for (int i = offset; i < times.length; ++i) {
            if (times[i] != -1) {
                rz += times[i];
            }
        }
        return rz / validTimes;
    }

    public static void testDefault(String[] args) {
        final int profile = Integer.parseInt(args[0]);

        int counter = 0;
        java.util.Random r = new java.util.Random();
        System.out.println("Training default");

        int[] x = new int[profile];
        int[] y = new int[profile];

        for (int i = 0; i < profile; ++i) {
            x[i] = r.nextInt();
            y[i] = r.nextInt();
            counter += Example.test2Default(x[i], y[i]);
        }
        System.out.println("Profiling default");
        long start = System.nanoTime();
        for (int i = 0; i < profile; ++i) {
            counter += Example.test2Default(x[i], y[i]);
        }
        long defaultTime = System.nanoTime() - start;

        System.out.println("Completed in " + defaultTime);
        System.out.println(counter);
    }

    public static void testOptimized(String[] args) {
        final int profile = Integer.parseInt(args[0]);

        int counter = 0;
        java.util.Random r = new java.util.Random();
        System.out.println("Training optimized");

        int[] x = new int[profile];
        int[] y = new int[profile];

        for (int i = 0; i < profile; ++i) {
            x[i] = r.nextInt();
            y[i] = r.nextInt();
            counter += Example.test2Optimized(x[i], y[i]);
        }
        System.out.println("Profiling optimized");
        long start = System.nanoTime();
        for (int i = 0; i < profile; ++i) {
            counter += Example.test2Optimized(x[i], y[i]);
        }
        long defaultTime = System.nanoTime() - start;

        System.out.println("Completed in " + defaultTime);
        System.out.println(counter);
    }

    public static void main(String[] args) {
        if (args[1].equals("default")) {
            testDefault(args);
        } else {
            testOptimized(args);
        }
    }


}