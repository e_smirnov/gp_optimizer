//
// Calculates possible number of chromosomes for given length of code sequence
//
public class ChromosomeNumberCalculator
{
    public static void main(String[] args)
    {
        long loadInt = 2;
        long loadInt1 = 3;
        
        long popInt1 = 2;
        long popInt2 = 12;
        
        long nonModify = 1;
        long nonModify1 = 2;
        long nonModify2 = 3;

        int INSTRUCTIONS_NMB = 11;
        long[] S = new long[INSTRUCTIONS_NMB];
        
        S[0] = 1;

        for (int i = 0; i < INSTRUCTIONS_NMB; ++i) {
            long[] Snew = new long[INSTRUCTIONS_NMB];
            Snew[0] = S[0] * nonModify + S[1] * popInt1;
            Snew[1] = S[0] * loadInt + S[1] * nonModify1 + S[2] * popInt2;
            
            for (int j = 2; j < INSTRUCTIONS_NMB - 1; ++j) {
                Snew[j] = S[j-1] * loadInt1 + S[j] * nonModify2 + S[j+1] * popInt2;
            }
            
            Snew[INSTRUCTIONS_NMB - 1] = S[INSTRUCTIONS_NMB - 2] * loadInt1 + S[INSTRUCTIONS_NMB - 1] * nonModify2;
            S = Snew;
        }
        System.out.println("For a sequence of " + INSTRUCTIONS_NMB + " possible number of chromosomes is " + S[1])  ;
    }
}