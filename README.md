# README #

GPOptimizer - a tool that performs peephole code optimizations using genetic algorithms.

Has support for JVM opcodes, x86 instructions and pixel shader 2.0 assembler.

### Requirements ###

* A Z3 SMT solver tool from Microsoft is required to run 
* A NvShaderPerf tool from NVidia is required for running shader optimization

### Usage ###

* As a library. Check the main.java for how to set up and use GPOptimizer classes
* As a standalone Java optimization tool. Check client and server packages. Server uses the JPPF grid computing framework for distributed processing of tasks. Client is capable to parse provided .jar files, extract suitable instruction sequences, send them to the server, apply received optimizations to these jars.