package ru.ifmo.gpo.protocol.conn_events;

import ru.ifmo.gpo.protocol.PatternItem;

import java.io.Serializable;

public class RequestEvent implements Serializable {
    private static final long serialVersionUID = 1;

    private PatternItem item;

    public RequestEvent(PatternItem item) {
        this.item = item;
    }

    public PatternItem getRequestData() {
        return item;
    }
}