package ru.ifmo.gpo.protocol.conn_events;

public class DataResponseEvent extends SimpleResponseEvent {
    private Object data;

    private static final long serialVersionUID = 1;

    public DataResponseEvent(int code, Object _data) {
        super(code);
        this.data = _data;
    }

    public Object getData() {
        return data;
    }
}