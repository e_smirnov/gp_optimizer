package ru.ifmo.gpo.protocol.conn_events;

import java.io.Serializable;

public class SessionEndedEvent implements Serializable {
    private static final long serialVersionUID = 1;

    private String cause;

    public SessionEndedEvent(String cause) {
        this.cause = cause;
    }

    public String getCause() {
        return cause;
    }
}