package ru.ifmo.gpo.protocol.conn_events;

import java.io.Serializable;

public class SessionStartedEvent implements Serializable {
    private static final long serialVersionUID = 1;

    public SessionStartedEvent() {
    }
}