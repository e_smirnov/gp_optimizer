package ru.ifmo.gpo.protocol.conn_events;

import java.io.Serializable;

public class SimpleResponseEvent implements Serializable {
    private static final long serialVersionUID = 1;
    private int code;

    public SimpleResponseEvent(int code) {
        this.code = code;
    }

    public int getResult() {
        return code;
    }

    public static final int OK = 0;
    public static final int SCHEDULED = 1;
    public static final int REJECTED = 2;
    public static final int FAILED = -1;
}