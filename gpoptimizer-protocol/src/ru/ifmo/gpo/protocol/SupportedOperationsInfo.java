package ru.ifmo.gpo.protocol;

import java.io.Serializable;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * User: jedi-philosopher
 * Date: 25.04.2010
 * Time: 20:34:29
 * <p/>
 * Is used by client pattern collector to check what instruction, constant types etc are acceptable by server. Depends
 * on server implementation of genetic process
 */
public class SupportedOperationsInfo implements Serializable {

    private Set<Integer> supportedOpcodes;
    private Set<Class> _supportedConstantTypes;
    private static final long serialVersionUID = 1;

    public SupportedOperationsInfo(Set<Integer> supportedOpcodes, Set<Class> _supportedConstantTypes) {
        this.supportedOpcodes = supportedOpcodes;
        this._supportedConstantTypes = _supportedConstantTypes;
    }

    public boolean isSupportedInstruction(int instrIdx) {
        return supportedOpcodes.contains(instrIdx);
    }

    public boolean isSupportedConstantType(Class c) {
        return _supportedConstantTypes.contains(c);
    }
}
