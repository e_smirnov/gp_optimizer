package ru.ifmo.gpo.protocol;


import ru.ifmo.gpo.core.instructions.InstructionSequence;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: jedi-philosopher
 * Date: 26.03.2010
 * Time: 23:21:24
 * <p/>
 * This class is used to transfer data about code patterns between server and client. Contains source and (optionally) optimized
 * code sequences, and information about source origin.
 */
public class PatternItem implements Serializable {

    private static final long serialVersionUID = 1;

    /**
     * Contains information about origing of PatternItem - its class, method and position in method
     *
     * @author jedi-philosopher
     */
    public static class SourceFileInfo implements Serializable {
        /**
         * Class name. Contains name in JVM internal format (i.e. package/subpackage/OuterClass$InnerClass)
         */
        public String className;

        /**
         * Name of method that contains pattern code
         */
        public String methodName;

        /**
         * Standart JVM descriptor for method, used to resolve method overloading
         */
        public String methodDescriptor;

        /**
         * Index of first instruction of source sequence in original method code
         */
        public int instructionIdx;
        private static final long serialVersionUID = -8703207282015023935L;

        public SourceFileInfo(String name, String methodName, String methodDescr, int idx) {
            this.className = name;
            this.methodName = methodName;
            this.methodDescriptor = methodDescr;
            this.instructionIdx = idx;
        }
    }

    @Override
    public String toString() {
        return "PatternItem{" +
                "_source=" + _source +
                '}';
    }

    /**
     * Source origing - class, method, position etc
     */
    private SourceFileInfo _info;

    /**
     * Soure instruction sequence that should be optimized
     */
    private InstructionSequence _source;

    /**
     * Optimized sequence, may be null
     */
    private InstructionSequence _optimized;

    public PatternItem(SourceFileInfo fileInfo, InstructionSequence source) {
        this._info = fileInfo;
        this._source = source;
    }

    public SourceFileInfo getInfo() {
        return _info;
    }

    public void setOptimized(InstructionSequence optimized) {
        _optimized = optimized;
    }

    public InstructionSequence getSource() {
        return _source;
    }

    public InstructionSequence getOptimized() {
        return _optimized;
    }
}
