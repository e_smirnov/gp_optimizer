package ru.ifmo.gpo.protocol;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: jedi-philosopher
 * Date: 26.03.2010
 * Time: 23:15:41
 * To change this template use File | Settings | File Templates.
 */
public class ServerVersionInfo implements Serializable {

    public int protocolVersion;

    public SupportedOperationsInfo supportedOperations;

    public String languageId;

    private static final long serialVersionUID = 1;

    public ServerVersionInfo(int protocolVersion, String languageId, SupportedOperationsInfo supportedOperations) {
        this.protocolVersion = protocolVersion;
        this.languageId = languageId;
        this.supportedOperations = supportedOperations;
    }
}
