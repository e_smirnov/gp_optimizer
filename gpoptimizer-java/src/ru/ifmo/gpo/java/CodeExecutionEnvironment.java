package ru.ifmo.gpo.java;

import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.ICodeExecutionEnvironment;
import ru.ifmo.gpo.core.UnsupportedOperandTypeException;
import ru.ifmo.gpo.java.vmstate.VerifyableOperandStackItem;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Created by IntelliJ IDEA.
 * User: jedi-philosopher
 * Date: 17.01.2010
 * Time: 17:27:19
 * <p/>
 * Represents environment for single code execution. Currently holds function parameters, so that
 * different functions (reference and tested ones) could be run with same parameters and their
 * execution results could be compared
 */
public class CodeExecutionEnvironment implements ICodeExecutionEnvironment {

    private Type[] arguments;
    private Type[] codeDependentArgs;
    private Object[] values;

    private List<Class> argumentClassesCached;

    static private Random _random = new Random();

    public CodeExecutionEnvironment(Type[] types, Type[] codeDependentArgs) {
        arguments = types;
        this.codeDependentArgs = codeDependentArgs;
    }

    public CodeExecutionEnvironment(Type[] types, List<VerifyableOperandStackItem> underflowStack) {
        arguments = types;
        codeDependentArgs = new Type[underflowStack.size()];
        for (int i = 0; i < underflowStack.size(); ++i) {
            codeDependentArgs[i] = underflowStack.get(i).getType();
        }
    }

    public void set(CodeExecutionEnvironment env) {
        for (int i = 0; i < Math.min(arguments.length, env.arguments.length); ++i) {
            if (arguments[i].equals(env.arguments[i])) {
                values[i] = env.values[i];
            }
        }
    }

    private void init() {
        if (arguments == null) {
            throw new IllegalStateException("Argumetns array is null");
        }

        if (values == null) {
            values = new Object[arguments.length];
        }
    }

    @Override
    public void setToZero() throws IllegalStateException {
        init();

        for (int i = 0; i < arguments.length; ++i) {
            if (arguments[i].equals(Type.INT_TYPE) || arguments[i].equals(Type.VOID_TYPE)) {
                values[i] = 0;
            } else if (arguments[i].equals(Type.FLOAT_TYPE)) {
                values[i] = Float.intBitsToFloat(0);
            } else if (arguments[i].equals(Type.LONG_TYPE)) {
                values[i] = 0;
            } else if (arguments[i].getClassName().equals(LinkedList.class.getCanonicalName())) {
                // do nothing
            } else {
                throw new IllegalStateException("Non-Integer parameters are not supported yet");
            }
        }
    }

    @Override
    public void setToFF() throws IllegalStateException {
        init();

        for (int i = 0; i < arguments.length; ++i) {
            if (arguments[i].equals(Type.INT_TYPE) || arguments[i].equals(Type.VOID_TYPE)) {
                values[i] = 0xFFFFFFFF;
            } else if (arguments[i].equals(Type.FLOAT_TYPE)) {
                values[i] = Float.intBitsToFloat(0xFFFFFFFF);
            } else if (arguments[i].equals(Type.LONG_TYPE)) {
                values[i] = 0xFFFFFFFFFFFFFFFFL;
            } else if (arguments[i].getClassName().equals(LinkedList.class.getCanonicalName())) {
                // do nothing
            } else {
                throw new IllegalStateException("Non-Integer parameters are not supported yet");
            }
        }
    }

    @Override
    public void setRandomValues() throws IllegalStateException {
        init();

        for (int i = 0; i < arguments.length; ++i) {
            if (arguments[i].equals(Type.INT_TYPE) || arguments[i].equals(Type.VOID_TYPE)) {
                values[i] = _random.nextInt();
            } else if (arguments[i].equals(Type.FLOAT_TYPE)) {
                values[i] = _random.nextFloat() * 1000000.0f - 500000.0f;
            } else if (arguments[i].equals(Type.LONG_TYPE)) {
                values[i] = _random.nextLong();
            } else if (arguments[i].getClassName().equals(LinkedList.class.getCanonicalName())) {
                // do nothing
            } else {
                throw new IllegalStateException("Non-Integer parameters are not supported yet");
            }
        }
    }

    public Object[] getArgumentValues() {
        return values;
    }

    public List<Class> getArgumentClasses() {
        if (argumentClassesCached != null) {
            return argumentClassesCached;
        }

        argumentClassesCached = new LinkedList<Class>();
        for (Type argument : arguments) {
            if (argument.equals(Type.INT_TYPE) || argument.equals(Type.VOID_TYPE)) {
                argumentClassesCached.add(int.class);
            } else if (argument.equals(Type.FLOAT_TYPE)) {
                argumentClassesCached.add(float.class);
            } else if (argument.equals(Type.LONG_TYPE)) {
                argumentClassesCached.add(long.class);
            } else if (argument.getClassName().equals(LinkedList.class.getCanonicalName())) {
                argumentClassesCached.add(LinkedList.class);
            } else {

                throw new UnsupportedOperandTypeException(argument);
            }
        }
        return argumentClassesCached;
    }

}
