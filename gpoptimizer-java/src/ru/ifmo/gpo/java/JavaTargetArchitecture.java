package ru.ifmo.gpo.java;

import ru.ifmo.gpo.core.*;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.IGenericInstruction;
import ru.ifmo.gpo.core.vmstate.IInstructionSequenceAnalyzer;
import ru.ifmo.gpo.core.vmstate.IMachineState;
import ru.ifmo.gpo.core.vmstate.IStateDelta;
import ru.ifmo.gpo.core.vmstate.IStateSchema;
import ru.ifmo.gpo.java.codegen.ClassUnloader;
import ru.ifmo.gpo.java.instructions.InstructionSequenceGenerator;
import ru.ifmo.gpo.java.instructions.JVMInstruction;
import ru.ifmo.gpo.java.instructions.JavaOpcodes;
import ru.ifmo.gpo.java.vmstate.JVMStateSchema;
import ru.ifmo.gpo.java.vmstate.VMState;
import ru.ifmo.gpo.java.vmstate.VMStateDelta;

/**
 * User: e_smirnov
 * Date: 20.01.2011
 * Time: 17:40:49
 */
public class JavaTargetArchitecture implements ITargetArchitecture {

    private ICodeGenerator codeGenerator = new JavaBytecodeGenerator();
    private ICodeExecutor codeExecutor = new JavaBytecodeExecutor();
    private InstructionSequenceGenerator sequenceGenerator = new InstructionSequenceGenerator(this);

    public final static String id = "JVM";
    private static final long serialVersionUID = -8575059508253223151L;

    static {
        SupportedArchitectures.register(id, JavaTargetArchitecture.class);
    }

    public JavaTargetArchitecture() {
    }

    @Override
    public InstructionSequence preprocessSource(InstructionSequence source) {
        return source;
    }

    @Override
    public IInstructionSequenceAnalyzer analyzeCode(InstructionSequence code, IStateSchema stateSchema) {
        return new JavaBytecodeAnalyzer(code, (JVMStateSchema) stateSchema);
    }

    @Override
    public IMachineState cloneState(IMachineState state) {
        if (!(state instanceof VMState)) {
            throw new IllegalArgumentException();
        }
        return new VMState((VMState) state);
    }

    @Override
    public IStateDelta getStateDelta(IMachineState first, IMachineState second) {
        if (!(first instanceof VMState && second instanceof VMState)) {
            throw new IllegalArgumentException();
        }
        return VMStateDelta.create((VMState) first, (VMState) second);
    }

    @Override
    public boolean isDeltaZero(IMachineState first, IMachineState second) {
        return VMStateDelta.statesEqual((VMState) first, (VMState) second);
    }

    @Override
    public IStateSchema buildStateSchema(InstructionSequence code) {
        return new JVMStateSchema(code);
    }

    @Override
    public IMachineState createInitialState(IStateSchema schema) {
        return new VMState((JVMStateSchema) schema);
    }

    @Override
    public IMachineState evaluateInstruction(IMachineState sourceState, IGenericInstruction insn) {
        return JavaBytecodeEvaluator.evaluateInstruction((VMState) sourceState, (JVMInstruction) insn);
    }

    @Override
    public ICodeGenerator getCodeGenerator() {
        return codeGenerator;
    }

    @Override
    public ICodeExecutor getCodeExecutor() {
        return codeExecutor;
    }

    @Override
    public IInstructionSequenceGenerator<VMState, VMStateDelta> getInstructionSequenceGenerator() {
        return sequenceGenerator;
    }

    @Override
    public IGenericInstruction cloneInstruction(IGenericInstruction instruction) {
        JVMInstruction casted = (JVMInstruction) instruction;
        return new JVMInstruction(casted);
    }

    @Override
    public IGenericInstruction createNOPInstruction() {
        return new JVMInstruction(JavaOpcodes.NOP);
    }

    @Override
    public boolean isInstructionSequenceValid(InstructionSequence seq) {
        return true;
    }

    public void setCodeExecutor(ICodeExecutor newVal) {
        codeExecutor = newVal;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void configurationCreated(GPConfiguration gpConfiguration) {
        gpConfiguration.addListener(new ClassUnloader(this));
    }
}
