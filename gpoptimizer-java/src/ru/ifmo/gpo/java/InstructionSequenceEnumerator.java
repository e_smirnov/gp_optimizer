package ru.ifmo.gpo.java;

import org.jgap.InvalidConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ifmo.gpo.core.GPConfiguration;
import ru.ifmo.gpo.core.MainIslandGPConfiguration;
import ru.ifmo.gpo.core.SolutionStorage;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.vmstate.NoSuitableOperandFoundException;
import ru.ifmo.gpo.java.instructions.JVMInstruction;
import ru.ifmo.gpo.java.instructions.JavaOpcodes;
import ru.ifmo.gpo.java.vmstate.JVMStateSchema;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Enumerates all possible instruction sequences of desired length and tries to find better solutions for
 * provided sample. Used to compare performance of genetic process with exhaustive search performance
 *
 * @author jedi-philosopher
 */
public class InstructionSequenceEnumerator {
    private long totalCount = 0;
    private long successfullCount = 0;
    private long startTime;
    private GPConfiguration conf;

    private int desiredLength;

    private static class ExtendedSolutionInfo {
        public InstructionSequence solution;
        public long number;
        public long discoverTime;

        public ExtendedSolutionInfo(InstructionSequence solution, long number,
                                    long discoverTime) {
            super();
            this.solution = solution;
            this.number = number;
            this.discoverTime = discoverTime;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result
                    + (int) (discoverTime ^ (discoverTime >>> 32));
            result = prime * result + (int) (number ^ (number >>> 32));
            result = prime * result
                    + ((solution == null) ? 0 : solution.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            ExtendedSolutionInfo other = (ExtendedSolutionInfo) obj;
            if (discoverTime != other.discoverTime)
                return false;
            if (number != other.number)
                return false;
            if (solution == null) {
                if (other.solution != null)
                    return false;
            } else if (!solution.equals(other.solution))
                return false;
            return true;
        }

    }

    private Set<ExtendedSolutionInfo> discoveredSolutions = new HashSet<ExtendedSolutionInfo>();

    private Logger logger = LoggerFactory.getLogger(InstructionSequenceEnumerator.class);

    public InstructionSequenceEnumerator(InstructionSequence seq, int desiredLength) {
        try {
            conf = new MainIslandGPConfiguration(new JavaTargetArchitecture());
            conf.setEnableFailureCounting(false);
            conf.setReferenceChromosome(seq);
            conf.setUpConfiguration();
        } catch (InvalidConfigurationException e) {
            logger.error("Failed to set up configuration", e);
        }
        this.desiredLength = desiredLength;
    }

    /**
     * Performs search
     *
     * @param onlyRunnableVariants If true, only code sequences that perform same modifications to VM state will be generated (like it
     *                             happens in genetics), otherwise all combinations of instructions will be tested
     */
    public void process(boolean onlyRunnableVariants) {
        startTime = System.currentTimeMillis();
        extendOrEvaluateInstructionSequence(new InstructionSequence(), onlyRunnableVariants);
        final long elapsed = (System.currentTimeMillis() - startTime) / 1000;
        logger.info("Process completed in {} seconds, {} sequences enumerated, {} solutions found", new Object[]{elapsed, totalCount, discoveredSolutions.size()});
    }

    public SolutionStorage getSolutions() {
        return conf.getSolutions();
    }

    private void checkSequence(InstructionSequence seq) {
        totalCount++;
        conf.getListeners().get(0).generationProcessed(null, 5); // hack to manually call ClassUnloader listener
        try {
            double value = conf.getFitnessFunc().getFitnessValue(seq.toChromosome(conf));
            successfullCount++;
            if (value > 1.0) {
                discoveredSolutions.add(new ExtendedSolutionInfo(seq, totalCount, System.currentTimeMillis() - startTime));
            }
        } catch (VerifyError error) {
            // ignore
        } catch (Exception ex) {
            if (ex.getCause() != null && ex.getCause() instanceof VerifyError) {
                // ignore
            } else {
                logger.error("Exception", ex);
            }
        }
        if ((totalCount % 1000) == 0) {
            logger.info("Processed {} sequnces, {} of them completed without exceptions, found {} solutions in {} seconds", new Object[]{totalCount, successfullCount, discoveredSolutions.size(), (System.currentTimeMillis() - startTime) / 1000});
        }
    }

    private void extendOrEvaluateInstructionSequence(InstructionSequence seq, boolean onlyRunnable) {
        if (seq.size() >= desiredLength) {
            checkSequence(seq);
            return;
        }
        JavaBytecodeAnalyzer analyzer;
        try {
            analyzer = new JavaBytecodeAnalyzer(seq, (JVMStateSchema) conf.getStateSchema());
        } catch (Exception ex) {
            return;
        }

        JavaOpcodes[] values = JavaOpcodes.values();
        for (JavaOpcodes opcode : values) {
            if (seq.isEmpty()) {
                logger.info("Completed {}%", ((float) opcode.ordinal() / (float) values.length) * 100);
            }
            if (onlyRunnable && !analyzer.getStackAfter().canPopAll(opcode.getExpectedStackTypes())) {
                continue;
            }
            try {
                List<List<GenericOperand>> possibleOperands = opcode.getOperandSelector().getAllPossibleOperandSets(opcode, analyzer.getStateAfter());
                if (possibleOperands == null || possibleOperands.isEmpty()) {
                    InstructionSequence newSeq = new InstructionSequence(seq);
                    newSeq.add(new JVMInstruction(opcode));
                    extendOrEvaluateInstructionSequence(newSeq, onlyRunnable);
                    continue;
                }
                Collections.shuffle(possibleOperands);
                for (List<GenericOperand> operandSet : possibleOperands) {
                    InstructionSequence newSeq = new InstructionSequence(seq);
                    newSeq.add(new JVMInstruction(opcode, operandSet));
                    extendOrEvaluateInstructionSequence(newSeq, onlyRunnable);
                }
            } catch (NoSuitableOperandFoundException ex) {
                continue;
            }
        }
    }

}
