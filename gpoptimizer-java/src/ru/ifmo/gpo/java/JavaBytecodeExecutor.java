package ru.ifmo.gpo.java;

import org.objectweb.asm.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ifmo.gpo.core.*;
import ru.ifmo.gpo.util.ObjectCache;
import ru.ifmo.gpo.util.StatUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;

/**
 * User: jedi-philosopher
 * Date: 01.12.2009
 * Time: 0:48:29
 */
public class JavaBytecodeExecutor extends ClassLoader implements ICodeExecutor {

    private static final int WARMUP_PASSES = 2;
    private static final int PROFILE_PASSES = 5;

    // getMethod() is sooo slooowly. Let's cache its result
    private static ObjectCache<String, Method> methodCache = new ObjectCache<String, Method>(100);

    private Logger logger = LoggerFactory.getLogger(getClass().getCanonicalName());

    public IExecutionResult executeCode(ICodeGenerationResult code, ICodeExecutionEnvironment envI) {
        if (!(code instanceof JavaCodeGenerationResult)) {
            throw new IllegalArgumentException("Java bytecode executor can only process JavaCodeGenerationResult and not " + code.getClass().getSimpleName());
        }
        CodeExecutionEnvironment env = (CodeExecutionEnvironment) envI;

        Class testClass = findLoadedClass(code.getName());
        if (testClass == null) {
            testClass = defineClass(code.getName(), ((JavaCodeGenerationResult) code).getCode(), 0, ((JavaCodeGenerationResult) code).getCode().length);
        }

        Exception invocationEx = null;

        Object[] params = env.getArgumentValues();
        LinkedList<Object> operandStack = new LinkedList<Object>();
        params[params.length - 1] = operandStack;

        // invoke
        try {

            List<Class> paramTypes = env.getArgumentClasses();
            Method method = methodCache.get(code.getName());
            if (method == null) {
                method = testClass.getMethod("run", paramTypes.toArray(new Class[paramTypes.size()]));
                methodCache.put(code.getName(), method);
            }
            method.invoke(null, params);

        } catch (InvocationTargetException ignore) {
            // idiv can cause division by zero in some cases, even though code is valid
            invocationEx = ignore;
        } catch (Exception ex) {
            logger.error("Failed to invoke method for " + code.getName(), ex);
            throw new IllegalStateException(ex);
        } catch (VerifyError ex) {
            logger.error("Verification failed for " + code.getSourceChromosome() + ":");
            logger.error(ex.getMessage());
            logger.warn(code.getSourceChromosome().toString());
            throw new RuntimeException("Verification failed for code " + code.getSourceChromosome(), ex);
        } catch (Error ex) {
            logger.error("Failed to invoke method for " + code.getName(), ex);
            throw ex;
        }

        LinkedList<JavaExecutionResult.OperandsStackItem> result = new LinkedList<JavaExecutionResult.OperandsStackItem>();
        for (Object o : operandStack) {
            if (o instanceof Integer) {
                result.add(new JavaExecutionResult.OperandsStackItem(o, Type.INT));
            } else if (o instanceof Float) {
                result.add(new JavaExecutionResult.OperandsStackItem(o, Type.FLOAT));
            } else if (o instanceof Long) {
                result.add(new JavaExecutionResult.OperandsStackItem(o, Type.LONG));
            } else if (o instanceof Double) {
                result.add(new JavaExecutionResult.OperandsStackItem(o, Type.DOUBLE));
            } else {
                throw new UnsupportedOperandTypeException(o.getClass());
            }
        }
        JavaExecutionResult execResult = new JavaExecutionResult(result, env);
        execResult.setException(invocationEx);
        return execResult;
    }

    @Override
    public long profileCode(ICodeGenerationResult code, ICodeExecutionEnvironment envI) {
        return profileCode(code, envI, WARMUP_PASSES, PROFILE_PASSES);
    }

    public long profileCode(ICodeGenerationResult code, ICodeExecutionEnvironment envI, int warmupPasses, int profilePasses) {
        if (!(code instanceof JavaCodeGenerationResult)) {
            throw new IllegalArgumentException("Java bytecode executor can only process JavaCodeGenerationResult and not " + code.getClass().getSimpleName());
        }
        CodeExecutionEnvironment env = (CodeExecutionEnvironment) envI;

        Class testClass = findLoadedClass(code.getName());
        if (testClass == null) {
            testClass = defineClass(code.getName(), ((JavaCodeGenerationResult) code).getCode(), 0, ((JavaCodeGenerationResult) code).getCode().length);
        }

        List<Class> paramClassesList = env.getArgumentClasses();
        Class[] paramClasses = paramClassesList.toArray(new Class[paramClassesList.size()]);

        long[] array = new long[warmupPasses + profilePasses];
        // do profile
        try {
            Method method = methodCache.get(code.getName());
            if (method == null) {
                method = testClass.getMethod("run", paramClasses);
                methodCache.put(code.getName(), method);
            }

            Object[] params = env.getArgumentValues();
            LinkedList<Object> operandStack = new LinkedList<Object>();
            params[params.length - 1] = operandStack;

            for (int i = 0; i < warmupPasses; ++i) {
                array[i] = Math.abs((Long) method.invoke(null, params));
            }

            for (int i = warmupPasses; i < warmupPasses + profilePasses; ++i) {
                try {
                    array[i] = Math.abs((Long) method.invoke(null, params));
                } catch (InvocationTargetException ignore) {
                    array[i] = -1;
                }
            }

        } catch (Exception ex) {
            logger.error("Exception has happened while trying to profile code for class " + code.getName(), ex);
            throw new RuntimeException(ex);
        } catch (VerifyError ex) {
            logger.error("Verification failed for " + code.getSourceChromosome() + ":", ex);
            logger.warn(code.getSourceChromosome().toString());
            throw ex;
        }

        return StatUtils.avgWithErrorCorrection(array, warmupPasses);
    }

}
