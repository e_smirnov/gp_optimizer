package ru.ifmo.gpo.java;

import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.ICodeExecutionEnvironment;
import ru.ifmo.gpo.core.IExecutionResult;
import ru.ifmo.gpo.util.BigSquareRoot;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: jedi-philosopher
 * Date: 01.12.2009
 * Time: 0:49:07
 * <p/>
 * Represents results of execution of code fragment. Contains contents of operands stack.
 */
public class JavaExecutionResult implements IExecutionResult {

    private Exception _ex;

    private ICodeExecutionEnvironment _env;

    @Override
    public void dispose() {
        // do nothing
    }

    public static class OperandsStackItem {
        public Object item;
        public int type;

        public OperandsStackItem(Object item, int type) {
            this.item = item;
            this.type = type;
        }
    }

    private LinkedList<OperandsStackItem> _operandsStack = null;

    JavaExecutionResult(LinkedList<OperandsStackItem> operandsStack, ICodeExecutionEnvironment env) {
        _operandsStack = operandsStack;
        _env = env;
    }

    public void setException(Exception ex) {
        _ex = ex;
    }

    public Exception getException() {
        return _ex;
    }

    public List<OperandsStackItem> getOperandsStack() {
        return _operandsStack;
    }

    public BigDecimal computeDifference(IExecutionResult reference) {
        if (!(reference instanceof JavaExecutionResult)) {
            throw new IllegalArgumentException("Cannot compute difference between execution results of different types");
        }
        BigDecimal difference = BigDecimal.valueOf(0.0);

        List<OperandsStackItem> referenceStack = ((JavaExecutionResult) reference).getOperandsStack();
        Iterator<OperandsStackItem> refIter = referenceStack.iterator();
        Iterator<OperandsStackItem> myIter = _operandsStack.iterator();

        while (refIter.hasNext() && myIter.hasNext()) {
            OperandsStackItem ref = refIter.next();
            OperandsStackItem my = myIter.next();
            if (my.type != ref.type) {
                throw new IllegalStateException("Stacks have different types");
            }
            switch (my.type) {
                case Type.VOID:
                case Type.INT:
                    //difference += Math.abs((Integer)ref.item - (Integer)my.item) / ((double)Integer.MAX_VALUE - (double)Integer.MIN_VALUE);
                    //difference += Math.abs((Integer)ref.item - (Integer)my.item);

                    difference = difference.add((BigDecimal.valueOf((Integer) ref.item - (Integer) my.item)).pow(2));
                    break;
                case Type.LONG:
                    difference = difference.add((BigDecimal.valueOf((Long) ref.item - (Long) my.item)).pow(2));
                    break;
                case Type.BOOLEAN:
                    throw new UnsupportedOperationException("Boolean values are currently not supported in difference computation");
                case Type.FLOAT:
                    Float first = (Float) ref.item;
                    Float second = (Float) my.item;
                    // if only one element is NaN or infinity - set difference to max value
                    if ((first.isInfinite() ^ second.isInfinite()) || (first.isNaN() ^ second.isNaN())) {
                        difference = difference.add(BigDecimal.valueOf(Float.MAX_VALUE));
                        break;
                    }
                    if (first.isInfinite() || first.isNaN()) {
                        first = Float.MAX_VALUE;
                    }
                    if (second.isInfinite() || second.isNaN()) {
                        second = Float.MAX_VALUE;
                    }
                    difference = difference.add((BigDecimal.valueOf(first - second)).pow(2));
                    break;
                default:
                    throw new IllegalArgumentException("Unsupported stack item type");
            }
        }

        if (refIter.hasNext() || myIter.hasNext()) {
            // results are not equal - perhaps some code generation problem, or failure to support consistency by genetic operations
            throw new IllegalStateException("Stacks have different size");
        }

        return BigSquareRoot.sqrt(difference);
    }

    public BigDecimal maxPossibleDifference() {
        BigDecimal rz = BigDecimal.valueOf(0);
        for (OperandsStackItem item : _operandsStack) {
            switch (item.type) {
                case Type.INT:
                case Type.VOID:
                    rz = rz.add(BigDecimal.valueOf(Integer.MAX_VALUE).pow(2));
                    break;
                case Type.FLOAT:
                    rz = rz.add(BigDecimal.valueOf(Float.MAX_VALUE).pow(2));
                    break;
                case Type.DOUBLE:
                    rz = rz.add(BigDecimal.valueOf(Double.MAX_VALUE).pow(2));
                    break;
                case Type.LONG:
                    rz = rz.add(BigDecimal.valueOf(Long.MAX_VALUE).pow(2));
                    break;
                default:
                    throw new IllegalArgumentException("Unsupported operand type " + item.type);
            }
        }
        return BigSquareRoot.sqrt(rz);
    }
}
