package ru.ifmo.gpo.java;

import org.jgap.IChromosome;
import ru.ifmo.gpo.core.GPConfiguration;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.IGenericInstruction;
import ru.ifmo.gpo.core.vmstate.BaseInstructionSequenceAnalyzer;
import ru.ifmo.gpo.core.vmstate.ConstantStorage;
import ru.ifmo.gpo.core.vmstate.IInstructionSequenceAnalyzer;
import ru.ifmo.gpo.java.instructions.JVMInstruction;
import ru.ifmo.gpo.java.vmstate.*;

import java.util.List;

/**
 * User: jedi-philosopher
 * Date: 25.01.2010
 * Time: 16:42:59
 * <p/>
 * Class used to analyze sequences of JVM bytecode and collect staticstics needed for other parts of code generation
 * and execution
 */
public class JavaBytecodeAnalyzer extends BaseInstructionSequenceAnalyzer<VMState, VMStateDelta> {

    /**
     * Constants that are used inside instruction sequence
     */
    private ConstantStorage constants = new ConstantStorage();

    private JVMStateSchema schema;

    public JavaBytecodeAnalyzer(IChromosome chromosome) {
        this(new InstructionSequence(chromosome), (JVMStateSchema) ((GPConfiguration) chromosome.getConfiguration()).getStateSchema());
    }

    public JavaBytecodeAnalyzer(InstructionSequence source, JVMStateSchema schema) {
        super(source);
        this.schema = schema;
        analyze();
    }


    @Override
    public boolean checkStateEqual(IInstructionSequenceAnalyzer second) {
        return second instanceof JavaBytecodeAnalyzer && checkStateEqual((JavaBytecodeAnalyzer) second);
    }


    public ConstantStorage getConstants() {
        return constants;
    }


    @Override
    public VMStateDelta getStateChange() {
        return stateDelta;
    }

    @Override
    public VMState getStateBefore() {
        return initialState;
    }

    public List<VerifyableOperandStackItem> getNeededStackBefore() {
        // getLast(), as underflow stack only increases over instructions, so most completed stack is after last instruction execution
        return stateAfter.getStackState().getUnderflowStack();
    }

    public VerifyableOperandStack getStackAfter() {
        return stateAfter.getStackState();
    }

    public boolean checkStateEqual(JavaBytecodeAnalyzer other) {
        return VMStateDelta.statesEqual(getStateAfter(), other.getStateAfter());
    }

    protected void analyze() {
        initialState = new VMState(schema);
        initialState.getLocalVars().fillWithDefaultValues();

        if (source.isEmpty()) {
            states.add(initialState);
            stateAfter = initialState;
            stateDelta = VMStateDelta.create(initialState, stateAfter);
            return;
        }

        VMState state = initialState;
        // try to get precalculated stack
        List<VerifyableOperandStackItem> underflowStack = schema.getNeededStackState();

        if (underflowStack == null) {
            // build new stack
            for (IGenericInstruction genericInstr : source) {
                JVMInstruction instr = (JVMInstruction) genericInstr;
                state = JavaBytecodeEvaluator.evaluateInstruction(state, instr);
            }
            underflowStack = state.getStackState().getUnderflowStack(); 
        }

        initialState.getStackState().addAll(underflowStack);
        initialState.getStackState().setAllowUnderflows(false);
        state = initialState;
        for (IGenericInstruction genericInstr : source) {
            JVMInstruction instr = (JVMInstruction) genericInstr;
            state.getStackState().setUnderflowStck(underflowStack);
            state = JavaBytecodeEvaluator.evaluateInstruction(state, instr);
            states.add(state);
        }
        stateAfter = states.get(states.size() - 1);

        stateDelta = VMStateDelta.create(initialState, stateAfter);
    }

}
