package ru.ifmo.gpo.java;

import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.BaseCodeGenerationResult;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.java.vmstate.VerifyableOperandStackItem;

import java.io.FileOutputStream;
import java.util.List;

/**
 * User: jedi-philosopher
 * Date: 15.01.2010
 * Time: 20:51:55
 */
public class JavaCodeGenerationResult extends BaseCodeGenerationResult {

    private byte[] code;
    private Type[] paramTypes;
    private List<VerifyableOperandStackItem> underflowStack;

    public JavaCodeGenerationResult(String name, byte[] code, InstructionSequence source, String methodDescriptor, List<VerifyableOperandStackItem> underflowStack) {
        super(name, source);
        this.code = code;
        this.paramTypes = Type.getArgumentTypes(methodDescriptor);
        this.underflowStack = underflowStack;
    }

    public byte[] getCode() {
        return code;
    }

    public CodeExecutionEnvironment createExecutionEnvironment() {
        CodeExecutionEnvironment env = new CodeExecutionEnvironment(paramTypes, underflowStack);
        env.setRandomValues();
        return env;
    }

    public void dumpOnDisc() {

        try {
            System.out.println("Dumping code " + name);
            FileOutputStream fos = new FileOutputStream(name + ".class");
            fos.write(code);
            fos.close();
        } catch (Exception ignore) {

        }
    }

}
