package ru.ifmo.gpo.java.vmstate;

import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.instructions.generic.GenericStackItem;
import ru.ifmo.gpo.verify.IArithmeticTreeNode;

/**
 * User: jedi-philosopher
 * Date: 05.09.2010
 * Time: 21:00:41
 */
public class VerifyableOperandStackItem extends GenericStackItem {
    private static final long serialVersionUID = 6355399084947344143L;

    public VerifyableOperandStackItem(Type elementType, IArithmeticTreeNode expression) {
        super(expression, elementType);
    }


    @Override
    public String getId() {
        throw new UnsupportedOperationException();
    }


    @Override
    public String toString() {
        return super.toString();
    }
}
