package ru.ifmo.gpo.java.vmstate;

import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.vmstate.BaseStateDelta;
import ru.ifmo.gpo.util.ObjectCache;

import java.util.*;

/**
 * User: e_smirnov
 * Date: 08.12.2010
 * Time: 18:23:45
 * <p/>
 * Class that describes changes, done to a state of VM by executing instruction sequence
 * As this class is often used just to check delta numeric value, or even just to check that this is equal to zero, additional
 * static members are added for that purpose that perform optimized computation. They should be used where possible
 */
public class VMStateDelta extends BaseStateDelta<VMState> {
    private Stack<StackOperation> stackOperations = new Stack<StackOperation>();
    private Map<Integer, Type> localVariableWrites = new HashMap<Integer, Type>();

    /**
     * Shows that this object contains full information about operations.
     */
    private boolean isFullObject;

    /**
     * Constructs a full VMStateDelta object, containing all operations that are needed to transform first state into second
     *
     * @param source Source state for delta computation
     * @param target Target state for delta computation
     */
    private VMStateDelta(VMState source, VMState target) {
        this(source, target, true);
    }

    // when fullObject is false - not all fields may be set in resulting object
    private VMStateDelta(VMState source, VMState target, boolean fullObject) {
        super(source, target);
        isFullObject = fullObject;

        fillStackOperations(!fullObject);
        // if not full object - we need at most 1 diff
        if (fullObject || stackOperations.isEmpty()) {
            fillLocalVars(!fullObject);
        }
    }

    /**
     * Computes list of operations that should be done with stack
     *
     * @param breakOnFirst If true - that means that at most one operation is computed. This is enough to say that states are not equal
     */
    private void fillStackOperations(boolean breakOnFirst) {
        LinkedList<Type> pushes = new LinkedList<Type>();
        LinkedList<Type> pops = new LinkedList<Type>();

        Iterator<VerifyableOperandStackItem> iterBefore = source.getStackState().iterator();
        Iterator<VerifyableOperandStackItem> iterAfter = target.getStackState().iterator();

        boolean equals = true;
        while (iterBefore.hasNext() && iterAfter.hasNext()) {
            final Type before = iterBefore.next().getType();
            final Type after = iterAfter.next().getType();
            if (equals) {
                if (!before.equals(after)) {
                    equals = false;
                }
            }
            if (!equals) {
                pops.addFirst(before);
                pushes.addLast(after);
                if (breakOnFirst) {
                    break;
                }
            }
        }
        while (iterBefore.hasNext()) {
            pops.addLast(iterBefore.next().getType());
            if (breakOnFirst) {
                break;
            }
        }

        while (iterAfter.hasNext()) {
            pushes.addFirst(iterAfter.next().getType());
            if (breakOnFirst) {
                break;
            }
        }

        for (Type pop : pops) {
            stackOperations.push(new StackOperation(pop, StackOperation.Operation.POP));
        }
        for (Type push : pushes) {
            stackOperations.push(new StackOperation(push, StackOperation.Operation.PUSH));
        }
    }

    private void fillLocalVars(boolean breakOnFirst) {
        for (LocalVariable var : target.getLocalVars()) {
            final Type type = var.getType();
            if (type.equals(Type.VOID_TYPE)) {
                // this is a part of multislot local variable. skipping it (only first slot, that has non-void type will be taken into account)
                continue;
            }
            LocalVariable sameVarInSourceState = source.getLocalVars().getLocalVariable(var.getSlot().getIdx());
            if (sameVarInSourceState == null || !sameVarInSourceState.getType().equals(type)) {
                localVariableWrites.put(var.getSlot().getIdx(), type);
                if (breakOnFirst) {
                    return;
                }
            }
        }
    }

    @Override
    public int getDelta() {
        return localVariableWrites.size() + stackOperations.size();
    }

    public Stack<StackOperation> getStackOperations() {
        if (!isFullObject) {
            throw new IllegalStateException("Incomplete (simplified) VMStateDelta can not be queried for stack operations");
        }
        return stackOperations;
    }

    public Map<Integer, Type> getLocalVariableWrites() {
        if (!isFullObject) {
            throw new IllegalStateException("Incomplete (simplified) VMStateDelta can not be queried for local variable operations");
        }
        return localVariableWrites;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VMStateDelta that = (VMStateDelta) o;

        if (localVariableWrites != null ? !localVariableWrites.equals(that.localVariableWrites) : that.localVariableWrites != null)
            return false;
        if (stackOperations != null ? !stackOperations.equals(that.stackOperations) : that.stackOperations != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = stackOperations != null ? stackOperations.hashCode() : 0;
        result = 31 * result + (localVariableWrites != null ? localVariableWrites.hashCode() : 0);
        return result;
    }

    // vm state delta is often created for same states from different parts of code. so let's cache its results

    private static class StatePair {
        public VMState first;
        public VMState second;

        private StatePair(VMState first, VMState second) {
            this.first = first;
            this.second = second;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            StatePair statePair = (StatePair) o;

            if (first != null ? !first.equals(statePair.first) : statePair.first != null) return false;
            if (second != null ? !second.equals(statePair.second) : statePair.second != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = first != null ? first.hashCode() : 0;
            result = 31 * result + (second != null ? second.hashCode() : 0);
            return result;
        }
    }

    private static ObjectCache<StatePair, VMStateDelta> cache = new ObjectCache<StatePair, VMStateDelta>(100);

    public static VMStateDelta create(VMState first, VMState second) {
        VMStateDelta cached = cache.get(new StatePair(first, second));
        if (cached != null) {
            return cached;
        }
        VMStateDelta result = new VMStateDelta(first, second);
        cache.put(new StatePair(first, second), result);
        return result;
    }

    public static boolean statesEqual(VMState first, VMState second) {
        VMStateDelta cached = cache.get(new StatePair(first, second));
        if (cached != null) {
            return cached.getDelta() == 0;
        }
        return new VMStateDelta(first, second, false).getDelta() == 0;
    }
}
