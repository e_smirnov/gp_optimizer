package ru.ifmo.gpo.java.vmstate;

import org.objectweb.asm.Type;
import ru.ifmo.gpo.verify.VariableNameGenerator;
import ru.ifmo.gpo.verify.VariableNode;

import java.util.*;

/**
 * User: jedi-philosopher
 * Date: 05.09.2010
 * Time: 20:37:54
 * <p/>
 * Represents operands stack for stack-based languages. Contains type of item on stack and optionally expression
 * from which item was created (used in verification process)
 */
public class VerifyableOperandStack extends LinkedList<VerifyableOperandStackItem> {

    private LinkedList<VerifyableOperandStackItem> underflowStack = new LinkedList<VerifyableOperandStackItem>();

    private boolean allowUnderflows = true;
    private static final long serialVersionUID = 1453673509068742638L;

    // cached value for a small speedup
    private Integer hashCode = null;

    public VerifyableOperandStack() {
    }

    public VerifyableOperandStack(Collection<? extends VerifyableOperandStackItem> c, boolean allowUnderflows) {
        super(c);
        this.allowUnderflows = allowUnderflows;
        this.underflowStack = new LinkedList<VerifyableOperandStackItem>();
    }

    public VerifyableOperandStack(VerifyableOperandStack source) {
        super(source);
        this.allowUnderflows = source.allowUnderflows;
        this.underflowStack = new LinkedList<VerifyableOperandStackItem>(source.getUnderflowStack());
    }

    private void dirty() {
        hashCode = null;
    }

    public VerifyableOperandStack(boolean allowUnderflows) {
        this.allowUnderflows = allowUnderflows;
    }

    public void setAllowUnderflows(boolean allowUnderflows) {
        this.allowUnderflows = allowUnderflows;
    }

    public void applyPop(Type... expectedTypes) {
        for (Type expectedType : expectedTypes) {
            applyPop(expectedType);
        }
        dirty();
    }

    public VerifyableOperandStackItem applyPop(Type expectedType) {
        if (canPop(expectedType)) {
            dirty();
            if (isEmpty()) {
                // all items left are underflow items
                underflowStack.addFirst(new VerifyableOperandStackItem(expectedType, new VariableNode(VariableNameGenerator.getInstance().getUnderflowStackVarName(underflowStack.size()), expectedType)));
                return underflowStack.getFirst();
            } else {
                VerifyableOperandStackItem itemToPop = removeLast();
                if (itemToPop.getType().equals(Type.VOID_TYPE)) {
                    // 	check that underflow stack has on its top item with undefined type
                    if (!underflowStack.isEmpty() && underflowStack.getFirst().getType().equals(Type.VOID_TYPE)) {
                        underflowStack.getFirst().setType(expectedType);
                    }
                    itemToPop.setType(expectedType);
                }
                return itemToPop;
            }
        } else {
            throw new IllegalArgumentException("Cannot pop type " + expectedType + " from stack");
        }
    }

    /**
     * Puts items on top of stack
     *
     * @param newItems Items to be put on stack in provided order
     */
    public void applyPush(VerifyableOperandStackItem... newItems) {
        addAll(Arrays.asList(newItems));
        dirty();
    }

    /**
     * Gets top item from stack
     *
     * @return Item that is on top of stack, null if stack is empty and contains only underflow items
     */
    @Override
    public VerifyableOperandStackItem peek() {
        if (size() > 0) {
            return peekLast();
        }
        if (allowUnderflows && !underflowStack.isEmpty()) {
            return underflowStack.peekLast();
        }
        return null;
    }

    /**
     * Gets n-th element from top. n=0 equals to peek()
     * @return n-th element
     */
    public VerifyableOperandStackItem peek(int n) {
        if (size() > n) {
            return get(size() - 1 - n);
        }
        int inUnderflow = n - size();
        if (allowUnderflows && underflowStack.size() > inUnderflow) {
            return underflowStack.get(underflowStack.size() - 1 - inUnderflow);
        }
        return null;
    }

    /**
     * Checks if item on top of stack can be of desired type. This is possible either if stack is currently empty and underflows
     * are permitted, or if stack contains item of desired type on its top.
     *
     * @param type Desired type (as a value of {@link org.objectweb.asm.Type}).
     * @return true if item of this type can be popped.
     */
    public boolean canPop(Type type) {
        return (allowUnderflows && (isEmpty()))
                || (!isEmpty() && (peekLast().getType().equals(type) || type == Type.VOID_TYPE || peekLast().getType().equals(Type.VOID_TYPE)));
    }

    public boolean canPopAll(Type... expectedTypes) {
        if (size() < expectedTypes.length && !allowUnderflows) {
            return false;
        }
        if (expectedTypes.length == 0) {
            return true;
        }

        // fast check for first element ( this method is called VERY often, and mostly fails on the first argument, so make it fast)
        if (!underflowStack.isEmpty()
                && underflowStack.getLast().getType() != expectedTypes[0]
                && expectedTypes[0] != Type.VOID_TYPE) {
            return false;
        }

        Iterator<VerifyableOperandStackItem> iter = descendingIterator();
        Iterator<VerifyableOperandStackItem> underflowIter = underflowStack.descendingIterator();

        final int lastIdx = expectedTypes.length - 1;

        for (int i = lastIdx; i >= 0; --i) {
            Type t = expectedTypes[i]; // should start from last one
            Type typeInStack;
            if (iter.hasNext()) {
                typeInStack = iter.next().getType();
            } else if (underflowIter.hasNext()) {
                typeInStack = underflowIter.next().getType();
            } else {
                return allowUnderflows;
            }

            if (typeInStack != t && t != Type.VOID_TYPE) {
                return false;
            }
        }
        return true;
    }

    public List<VerifyableOperandStackItem> getUnderflowStack() {
        return underflowStack;
    }

    public void setUnderflowStck(List<VerifyableOperandStackItem> newUnderflowStack) {
        this.underflowStack = newUnderflowStack instanceof LinkedList ? (LinkedList<VerifyableOperandStackItem>) newUnderflowStack : new LinkedList<VerifyableOperandStackItem>(newUnderflowStack);
        dirty();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        VerifyableOperandStack that = (VerifyableOperandStack) o;

        return underflowStack.equals(that.underflowStack);
    }

    @Override
    public int hashCode() {
        if (hashCode != null) {
            return hashCode;
        }
        int result = super.hashCode();
        result = 31 * result + underflowStack.hashCode();
        hashCode = result;
        return result;
    }

    @Override
    public String toString() {
        return "VerifyableOperandStack [underflowStack=" + underflowStack
                + ", allowUnderflows=" + allowUnderflows + ", toString()="
                + super.toString() + "]";
    }


}
