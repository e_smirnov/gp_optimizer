package ru.ifmo.gpo.java.vmstate;

import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.instructions.generic.IGenericInstruction;
import ru.ifmo.gpo.core.vmstate.IStateSchema;
import ru.ifmo.gpo.java.JavaBytecodeAnalyzer;
import ru.ifmo.gpo.java.JavaBytecodeEvaluator;
import ru.ifmo.gpo.java.instructions.JVMInstruction;

import java.util.ArrayList;
import java.util.List;

/**
 * User: jedi-philosopher
 * Date: 04.03.2011
 * Time: 15:29:45
 * <p/>
 * Class that stores list of available local variable slots. Each slot has its access type and index.
 * All instances of VM states in same genetic process should use same schema, taken from initial chromosome
 */
public class JVMStateSchema implements IStateSchema {

    private ArrayList<LocalVariableSlot> availableSlots = new ArrayList<LocalVariableSlot>();

    private List<VerifyableOperandStackItem> neededStack;

    public JVMStateSchema() {
    }

    public JVMStateSchema(InstructionSequence source) {
        // filling local variables
        for (IGenericInstruction instr : source) {
            JavaBytecodeEvaluator.updateSchema(this, (JVMInstruction) instr);
        }
        // filling underflow stack
        neededStack = new JavaBytecodeAnalyzer(source, this).getNeededStackBefore();
    }

    @Override
    public LocalVariableSlot addOrUpdateSlot(GenericOperand op, Type initialType, GenericOperand.AccessType accessType) {
        if (!(op instanceof LocalVariableSlot)) {
            throw new IllegalArgumentException("Can only add slots for GenericOperands of type LocalVariableSlot");
        }
        return addOrUpdateSlot(((LocalVariableSlot) op).getIdx(), initialType, accessType);
    }


    public List<VerifyableOperandStackItem> getNeededStackState()
    {
        return this.neededStack;
    }

    public LocalVariableSlot addOrUpdateSlot(int slotIdx, Type initialType, GenericOperand.AccessType accessType) {
        LocalVariableSlot slot = getSlot(slotIdx);
        if (slot != null) {
            slot.addAccessType(accessType);
            for (int i = 1; i < initialType.getSize(); ++i) {
                addOrUpdateSlot(slotIdx + i, Type.VOID_TYPE, accessType);
            }
            return slot;
        }
        slot = new LocalVariableSlot(slotIdx);
        slot.setAccessType(accessType);
        slot.setType(initialType);

        if (slotIdx < availableSlots.size()) {
            availableSlots.set(slotIdx, slot);
        } else {
            while (availableSlots.size() < slotIdx) {
                availableSlots.add(null);
            }
            availableSlots.add(slot);
            if (initialType.getSize() > 1) {
                // for local variables that occupy more than one slot
                for (int i = 1; i < initialType.getSize(); ++i) {
                    addOrUpdateSlot(slotIdx + i, Type.VOID_TYPE, accessType);
                }
            }
        }

        return slot;
    }

    public LocalVariableSlot getSlot(int slotIdx) {
        if (slotIdx >= availableSlots.size()) {
            return null;
        }
        return availableSlots.get(slotIdx);
    }

    public int getFirstUnusedIdx() {
        for (int i = availableSlots.size() - 1; i >= 0; --i) {
            if (availableSlots.get(i) != null) {
                if (i != availableSlots.size() - 1) {
                    return i;
                } else {
                    break;
                }
            }
        }
        return availableSlots.size();
    }

    @Override
    public ArrayList<LocalVariableSlot> getSlots() {
        return availableSlots;
    }
}
