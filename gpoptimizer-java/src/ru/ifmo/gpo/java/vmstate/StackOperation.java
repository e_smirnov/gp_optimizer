package ru.ifmo.gpo.java.vmstate;

import org.objectweb.asm.Type;

public class StackOperation {
    public enum Operation {
        POP, PUSH
    }

    public Type type;
    public Operation op;

    public StackOperation(Type type, Operation op) {
        this.type = type;
        this.op = op;
    }
}