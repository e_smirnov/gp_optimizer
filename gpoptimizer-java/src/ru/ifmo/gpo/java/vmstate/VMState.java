package ru.ifmo.gpo.java.vmstate;

import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.instructions.generic.GenericStackItem;
import ru.ifmo.gpo.core.vmstate.ConstantStorage;
import ru.ifmo.gpo.core.vmstate.IMachineState;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: e_smirnov
 * Date: 08.12.2010
 * Time: 19:08:28
 */
public class VMState implements IMachineState {
    private VerifyableOperandStack stackState = new VerifyableOperandStack();
    private LocalVariableStorage localVars;
    private ConstantStorage constants = new ConstantStorage();

    public VMState(JVMStateSchema schema) {
        this.localVars = new LocalVariableStorage(schema);
        this.localVars.fillWithDefaultValues();
    }

    public VMState(VMState proto) {
        this(proto.stackState, proto.localVars, proto.constants);
    }

    public VMState(VerifyableOperandStack stackState, LocalVariableStorage localVars, ConstantStorage constants) {
        this.stackState = new VerifyableOperandStack(stackState);
        this.localVars = new LocalVariableStorage(localVars);
        this.constants = new ConstantStorage(constants);
    }

    public VerifyableOperandStack getStackState() {
        return stackState;
    }

    public LocalVariableStorage getLocalVars() {
        return localVars;
    }

    @Override
    public ConstantStorage getConstants() {
        return constants;
    }

    @Override
    public Map<String, ? extends GenericOperand> getRegisters() {
        Map<String, LocalVariable> result = new HashMap<String, LocalVariable>();
        for (LocalVariable v : localVars) {
            result.put(v.getSlot().getId(), v);
        }
        return result;
    }

    @Override
    public List<? extends GenericStackItem> getStack() {
        return stackState;
    }

    @Override
    public Map<String, ? extends GenericOperand> getMemory() {
        return Collections.emptyMap();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VMState vmState = (VMState) o;
        // no constant compare
        return !(localVars != null ? !localVars.equals(vmState.localVars) : vmState.localVars != null)
                && !(stackState != null ? !stackState.equals(vmState.stackState) : vmState.stackState != null);
    }

    @Override
    public int hashCode() {
        // no constant member
        int result = stackState != null ? stackState.hashCode() : 0;
        result = 31 * result + (localVars != null ? localVars.hashCode() : 0);
        return result;
    }
}
