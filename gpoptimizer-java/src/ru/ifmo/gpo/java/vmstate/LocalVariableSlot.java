package ru.ifmo.gpo.java.vmstate;

import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.verify.IArithmeticTreeNode;

/**
 * User: e_smirnov
 * Date: 21.02.2011
 * Time: 15:56:13
 */
public class LocalVariableSlot extends GenericOperand implements Comparable<LocalVariableSlot> {

    private int id;
    private static final long serialVersionUID = 6592381794761954568L;

    /**
     * Private constructor used by gson serialization
     */
    private LocalVariableSlot() {
        this(0);
    }

    public LocalVariableSlot(int id) {
        this.id = id;
    }

    public LocalVariableSlot(IArithmeticTreeNode expression, Type type, int id) {
        super(expression, type);
        this.id = id;
    }

    public LocalVariableSlot(IArithmeticTreeNode expression, Type type, AccessType accessType, int id) {
        super(expression, type, accessType);
        this.id = id;
    }

    @Override
    public String getId() {
        return Integer.toString(id);
    }

    public int getIdx() {
        return id;
    }

    @Override
    public int compareTo(LocalVariableSlot slot) {
        return this.id - slot.id;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + id;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LocalVariableSlot other = (LocalVariableSlot) obj;
        if (id != other.id)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "" + id;
    }


}
