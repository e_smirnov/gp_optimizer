package ru.ifmo.gpo.java.vmstate;

import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.verify.IArithmeticTreeNode;
import ru.ifmo.gpo.verify.UnknownValueNode;
import ru.ifmo.gpo.verify.VariableNameGenerator;
import ru.ifmo.gpo.verify.VariableNode;

import java.util.*;

/**
 * User: jedi-philosopher
 * Date: 04.11.2010
 * Time: 22:19:02
 * <p/>
 * Contains information about local variable values at some point of code execution
 */
public class LocalVariableStorage implements Iterable<LocalVariable> {
    /**
     * Static description of available local variable slots
     */
    private JVMStateSchema schema;

    /**
     * Actual local variable values
     */
    private Map<LocalVariableSlot, LocalVariable> variableValues = new HashMap<LocalVariableSlot, LocalVariable>();

    public LocalVariableStorage(JVMStateSchema schema) {
        this.schema = schema;
    }

    public LocalVariableStorage(LocalVariableStorage proto) {
        this.schema = proto.schema;
        this.variableValues = new HashMap<LocalVariableSlot, LocalVariable>(proto.variableValues);
    }

    private void remove(LocalVariable var) {
        final Type type = var.getType();
        if (!type.equals(Type.VOID_TYPE)) {
            // this is head of slot chain
            for (int i = 0; i < type.getSize(); ++i) {
                variableValues.remove(schema.getSlot(i));
            }
            return;
        }
        for (int i = var.getSlot().getIdx() - 1; i >= 0; --i) {
            LocalVariable v = variableValues.get(schema.getSlot(i));
            if (v == null) {
                throw new IllegalStateException("Detached multislot sequence starting at " + i);
            }
            if (!v.getType().equals(Type.VOID_TYPE)) {
                remove(v);
                return;
            }
        }
        throw new IllegalStateException("Multislot sequence without starting element with type, starting from idx 0");
    }

    public void setValue(GenericOperand slot, Type type, IArithmeticTreeNode value) {
        setValue(((LocalVariableSlot) slot).getIdx(), type, value);
    }

    public void setValue(int slotIdx, Type type, IArithmeticTreeNode value) {
        LocalVariableSlot slot = schema.getSlot(slotIdx);
        if (slot == null || !slot.isWriteable()) {
            throw new IllegalArgumentException("Can not set value for slot with id " + slotIdx + " as it is not available for writing");
        }
        LocalVariable varInSlot = getLocalVariable(slotIdx);
        if (varInSlot != null && (varInSlot.getType().getSize() > 1 || varInSlot.getType().equals(Type.VOID_TYPE))) {
            // this is one of slots belonging to multi-slot variable. Writing to one of its slots invalidates all others
            remove(varInSlot);
        }

        for (int i = 1; i < type.getSize(); ++i) {
            LocalVariableSlot additionalSlot = schema.getSlot(slotIdx + i);
            if (additionalSlot == null || !additionalSlot.isWriteable()) {
                throw new IllegalArgumentException("Can not set value for slot with id " + (slotIdx + i)
                        + " saving multi-slot local variable to slot " + slotIdx + " as it is not available for writing");
            }
            variableValues.put(additionalSlot, new LocalVariable(new UnknownValueNode(), Type.VOID_TYPE, additionalSlot));
        }

        variableValues.put(slot, new LocalVariable(value, type, slot));
        if (type.getSize() == 1) {
            return;
        }
    }

    public IArithmeticTreeNode getValue(GenericOperand localVarSlot, Type expectedType) {
        return getValue(((LocalVariableSlot) localVarSlot).getIdx(), expectedType);
    }

    public IArithmeticTreeNode getValue(int slotIdx, Type expectedType) {
        LocalVariable result = getLocalVariable(slotIdx);
        if (result == null) {
            throw new IllegalArgumentException("Can not get value from slot " + slotIdx + " as it is not initialized");
        }
        if (expectedType != null && !result.getType().equals(expectedType)) {
            throw new IllegalArgumentException("Slot " + slotIdx + " contains value of type " + result.getType() + " while " + expectedType + " was expected");
        }
        return result.getExpression();
    }

    public IArithmeticTreeNode getValue(int slotIdx) {
        return getValue(slotIdx, null);
    }

    public LocalVariable getLocalVariable(int slotIdx) {
        LocalVariableSlot slot = schema.getSlot(slotIdx);
        if (slot == null) {
            throw new IllegalArgumentException("Can not get value from slot " + slotIdx + " as it does not exist");
        }
        return variableValues.get(slot);

    }

    public Collection<LocalVariableSlot> getSlotsByAccess(GenericOperand.AccessType accessType) {
        Set<LocalVariableSlot> result = new HashSet<LocalVariableSlot>();
        for (LocalVariableSlot slot : schema.getSlots()) {
            if (slot == null) {
                continue;
            }
            if (slot.hasAccessType(accessType)) {
                result.add(slot);
            }
            if (accessType == GenericOperand.AccessType.READ && variableValues.containsKey(slot)) {
                result.add(slot);
            }
        }
        // todo: think about this method
        return result;
    }

    public Collection<LocalVariableSlot> getSlotsByAccessAndType(GenericOperand.AccessType accessType, Type storedType) {
        Collection<LocalVariableSlot> rz = getSlotsByAccess(accessType);
        for (Iterator<LocalVariableSlot> iter = rz.iterator(); iter.hasNext();) {
            LocalVariableSlot slot = iter.next();

            if (!variableValues.containsKey(slot)
                    || !variableValues.get(slot).getType().equals(storedType)) {
                iter.remove();
            }
        }
        return rz;
    }

    public void fillWithDefaultValues() {
        for (LocalVariableSlot slot : schema.getSlots()) {
            if (slot == null) {
                continue;
            }
            if (slot.isReadable()) {
                variableValues.put(slot, new LocalVariable(new VariableNode(VariableNameGenerator.getInstance().getLocalVarName(slot.getIdx()), slot.getType()), slot.getType(), slot));
            }
        }
    }

    public JVMStateSchema getSchema() {
        return schema;
    }

    @Override
    public Iterator<LocalVariable> iterator() {
        return variableValues.values().iterator();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LocalVariableStorage that = (LocalVariableStorage) o;

        if (schema != null ? !schema.equals(that.schema) : that.schema != null) return false;
        if (variableValues != null ? !variableValues.equals(that.variableValues) : that.variableValues != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = schema != null ? schema.hashCode() : 0;
        result = 31 * result + (variableValues != null ? variableValues.hashCode() : 0);
        return result;
    }
}
