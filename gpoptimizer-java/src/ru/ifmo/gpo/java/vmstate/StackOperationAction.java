package ru.ifmo.gpo.java.vmstate;

/**
 * User: e_smirnov
 * Date: 03.12.2010
 * Time: 16:06:39
 */

public enum StackOperationAction {
    NO_ACTION,

    SATISFY_AT_LEAST_ONE
}