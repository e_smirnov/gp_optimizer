package ru.ifmo.gpo.java.vmstate;

import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.instructions.generic.GenericRegister;
import ru.ifmo.gpo.verify.IArithmeticTreeNode;

/**
 * User: jedi-philosopher
 * Date: 04.11.2010
 * Time: 22:19:29
 */
public class LocalVariable extends GenericRegister {
    private LocalVariableSlot slot;
    private static final long serialVersionUID = -341014083864096309L;

    public LocalVariable(IArithmeticTreeNode expression, Type type, LocalVariableSlot slot) {
        this.expression = expression;
        this.type = type;
        this.slot = slot;
    }

    public LocalVariableSlot getSlot() {
        return slot;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LocalVariable that = (LocalVariable) o;

        if (expression != null ? !expression.equals(that.expression) : that.expression != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = expression != null ? expression.hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

    @Override
    public String getId() {
        return slot.getId();
    }
}
