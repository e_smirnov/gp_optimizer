package ru.ifmo.gpo.java.codegen;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import java.util.LinkedList;
import java.util.List;

/**
 * User: e_smirnov
 * Date: 16.11.2010
 * Time: 15:37:36
 */
public class PreparedMethod {

    private List<ICodeFragment> codeFragments = new LinkedList<ICodeFragment>();

    private String name;

    private String descriptor;

    public PreparedMethod(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void addCodeFragment(ICodeFragment fragment) {
        codeFragments.add(fragment);
    }

    public void generateCode(ClassVisitor cw) {
        MethodVisitor mw = cw.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC, name, getMethodDescriptor(), null, null);
        for (ICodeFragment codeFragment : codeFragments) {
            codeFragment.appendCode(mw);
        }
        mw.visitMaxs(0, 0);
        mw.visitEnd();
    }

    public String getMethodDescriptor() {
        return descriptor == null ? generateMethodDescriptor() : descriptor;
    }

    private String generateMethodDescriptor() {
        MethodDescriptor methodDescriptor = new MethodDescriptor();
        for (ICodeFragment codeFragment : codeFragments) {
            codeFragment.collectMethodParameterDescriptions(methodDescriptor);
        }
        descriptor = methodDescriptor.getDescriptor();
        return descriptor;
    }
}
