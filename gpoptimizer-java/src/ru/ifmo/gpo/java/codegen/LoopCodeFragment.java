package ru.ifmo.gpo.java.codegen;

import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

/**
 * User: e_smirnov
 * Date: 16.11.2010
 * Time: 18:25:12
 */
public class LoopCodeFragment implements ICodeFragment {

    private ICodeFragment loopedFragment;
    private Integer iterations;
    private int loopVarIdx;

    public LoopCodeFragment(ICodeFragment loopedFragment, Integer iterations, int loopVarIdx) {
        this.loopedFragment = loopedFragment;
        this.iterations = iterations;
        this.loopVarIdx = loopVarIdx;
    }

    @Override
    public void appendCode(MethodVisitor mw) {
        mw.visitInsn(Opcodes.ICONST_0);
        mw.visitVarInsn(Opcodes.ISTORE, loopVarIdx);
        Label l = new Label();
        mw.visitLabel(l);
        mw.visitVarInsn(Opcodes.ILOAD, loopVarIdx);
        mw.visitLdcInsn(iterations);
        Label target = new Label();
        mw.visitJumpInsn(Opcodes.IF_ICMPGE, target);

        loopedFragment.appendCode(mw);

        mw.visitIincInsn(loopVarIdx, 1);
        mw.visitJumpInsn(Opcodes.GOTO, l);
        mw.visitLabel(target);
    }

    @Override
    public void collectMethodParameterDescriptions(MethodDescriptor methodDescriptor) {
        loopedFragment.collectMethodParameterDescriptions(methodDescriptor);
    }
}
