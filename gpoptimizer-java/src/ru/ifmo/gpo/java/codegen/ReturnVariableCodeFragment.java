package ru.ifmo.gpo.java.codegen;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.UnsupportedOperandTypeException;

/**
 * User: e_smirnov
 * Date: 17.11.2010
 * Time: 16:34:29
 */
public class ReturnVariableCodeFragment implements ICodeFragment {

    private Type varType;
    private int slotIdx;

    public ReturnVariableCodeFragment(Type varType, int slotIdx) {
        this.varType = varType;
        this.slotIdx = slotIdx;
    }

    @Override
    public void appendCode(MethodVisitor mw) {
        int loadInsn;
        int retInsn;

        if (varType == Type.INT_TYPE) {
            loadInsn = Opcodes.ILOAD;
            retInsn = Opcodes.IRETURN;
        } else if (varType == Type.LONG_TYPE) {
            loadInsn = Opcodes.LLOAD;
            retInsn = Opcodes.LRETURN;
        } else if (varType == Type.FLOAT_TYPE) {
            loadInsn = Opcodes.FLOAD;
            retInsn = Opcodes.FRETURN;
        } else {
            throw new UnsupportedOperandTypeException(varType);
        }

        mw.visitVarInsn(loadInsn, slotIdx);
        mw.visitInsn(retInsn);
    }

    @Override
    public void collectMethodParameterDescriptions(MethodDescriptor methodDescriptor) {
        methodDescriptor.setReturnValue(varType.getDescriptor());
    }
}
