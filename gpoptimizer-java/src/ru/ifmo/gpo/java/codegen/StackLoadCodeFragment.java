package ru.ifmo.gpo.java.codegen;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import ru.ifmo.gpo.java.JavaBytecodeAnalyzer;

/**
 * User: e_smirnov
 * Date: 16.11.2010
 * Time: 15:35:21
 */
public class StackLoadCodeFragment implements ICodeFragment {
    private JavaBytecodeAnalyzer analyzer;
    private int referencedLocalVarsNmb;

    public StackLoadCodeFragment(JavaBytecodeAnalyzer analyzer, int referencedLocalVars) {
        this.analyzer = analyzer;
        this.referencedLocalVarsNmb = referencedLocalVars;
    }

    @Override
    public void appendCode(MethodVisitor mw) {
        //TODO: JVM specification says, that method parameters can have length of 255 max, where long and double parameters take 2 length units. Add check here
        // pushing items on stack. Local variable indices for stack items start with referencedLocalVariables.size()
        for (int i = 0; i < analyzer.getNeededStackBefore().size(); ++i) {
            final Type neededType = analyzer.getNeededStackBefore().get(i).getType();
            if (neededType.equals(Type.INT_TYPE) || neededType.equals(Type.VOID_TYPE)) {
                mw.visitVarInsn(Opcodes.ILOAD, i + referencedLocalVarsNmb);
            } else if (neededType.equals(Type.FLOAT_TYPE)) {
                mw.visitVarInsn(Opcodes.FLOAD, i + referencedLocalVarsNmb);
            } else if (neededType.equals(Type.LONG_TYPE)) {
                mw.visitVarInsn(Opcodes.LLOAD, i + referencedLocalVarsNmb);
            } else if (neededType.equals(Type.DOUBLE_TYPE)) {
                mw.visitVarInsn(Opcodes.DLOAD, i + referencedLocalVarsNmb);
            } else {
                throw new IllegalArgumentException("Can not load variable of type " + neededType);
            }
        }
    }

    @Override
    public void collectMethodParameterDescriptions(MethodDescriptor methodDescriptor) {
        int lastAddedIdx = 0;
        for (int i = 0; i < analyzer.getNeededStackBefore().size(); ++i) {
            final Type type = analyzer.getNeededStackBefore().get(i).getType();
            methodDescriptor.addParameter((lastAddedIdx++) + referencedLocalVarsNmb, type.getDescriptor());
            for (int j = 1; j < type.getSize(); ++j) {
                methodDescriptor.addParameter((lastAddedIdx++) + referencedLocalVarsNmb, Type.VOID_TYPE.getDescriptor());
            }
        }
    }
}
