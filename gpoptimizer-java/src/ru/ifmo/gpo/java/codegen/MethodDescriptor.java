package ru.ifmo.gpo.java.codegen;

import org.objectweb.asm.Type;

import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * User: e_smirnov
 * Date: 16.11.2010
 * Time: 16:30:31
 */
public class MethodDescriptor {
    private SortedMap<Integer, String> methodParameters = new TreeMap<Integer, String>();
    private String returnValue;

    public void setReturnValue(String value) {
        if (returnValue != null && !returnValue.equals(value)) {
            throw new IllegalStateException("Cannot set return value type " + value + " as it is already set to " + returnValue);
        }
        returnValue = value;
    }

    public void addParameter(Integer number, String typeDescriptor) {
        String alreadySetType = methodParameters.get(number);
        if (alreadySetType != null && !alreadySetType.equals(typeDescriptor)) {
            throw new IllegalStateException("Cannot set type '" + typeDescriptor + "' for parameter " + number + " as it is already set to " + alreadySetType);
        }
        methodParameters.put(number, typeDescriptor);
    }

    public String getDescriptor() {
        // tree will be iterated in ascending key order
        int lastFilledIdx = 0;
        StringBuffer result = new StringBuffer("(");
        for (Map.Entry<Integer, String> entry : methodParameters.entrySet()) {
            for (; lastFilledIdx < entry.getKey(); lastFilledIdx++) {
                // filling 'holes' in method parameters with dummy int values
                result.append("I");
            }
            final String typeDescriptor = entry.getValue();
            if (!typeDescriptor.equals(Type.VOID_TYPE.getDescriptor())) {
                // void elements are parts of multi-slot parameters, they should not appear in method descriptor
                // in case of Long, methodParameters will look like (long, void) as it takes 2 slot, but actual method descriptor should be just (long)
                result.append(entry.getValue());
            }
            lastFilledIdx++;
        }
        result.append(")");
        result.append(returnValue);
        return result.toString();
    }
}
