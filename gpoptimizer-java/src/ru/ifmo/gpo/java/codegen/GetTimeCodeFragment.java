package ru.ifmo.gpo.java.codegen;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

/**
 * User: e_smirnov
 * Date: 16.11.2010
 * Time: 18:23:03
 */
public class GetTimeCodeFragment implements ICodeFragment {

    private int timeVarIdx;

    public GetTimeCodeFragment(int timeVarIdx) {
        this.timeVarIdx = timeVarIdx;
    }

    @Override
    public void appendCode(MethodVisitor mw) {
        mw.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/System", "nanoTime", "()J");
        mw.visitVarInsn(Opcodes.LSTORE, timeVarIdx);
    }

    @Override
    public void collectMethodParameterDescriptions(MethodDescriptor methodDescriptor) {
    }
}
