package ru.ifmo.gpo.java.codegen;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

/**
 * User: e_smirnov
 * Date: 19.11.2010
 * Time: 15:52:56
 */
public class ReturnVoidCodeFragment implements ICodeFragment {
    @Override
    public void appendCode(MethodVisitor mw) {
        mw.visitInsn(Opcodes.RETURN);
    }

    @Override
    public void collectMethodParameterDescriptions(MethodDescriptor methodDescriptor) {
        methodDescriptor.setReturnValue(Type.VOID_TYPE.getDescriptor());
    }
}
