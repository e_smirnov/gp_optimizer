package ru.ifmo.gpo.java.codegen;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.commons.Method;
import ru.ifmo.gpo.core.UnsupportedOperandTypeException;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.java.JavaBytecodeAnalyzer;
import ru.ifmo.gpo.java.vmstate.LocalVariable;
import ru.ifmo.gpo.java.vmstate.LocalVariableSlot;
import ru.ifmo.gpo.java.vmstate.LocalVariableStorage;
import ru.ifmo.gpo.java.vmstate.VerifyableOperandStackItem;

import java.util.LinkedList;

/**
 * User: e_smirnov
 * Date: 16.11.2010
 * Time: 15:38:27
 */
public class StackContentSavingCodeFragment implements ICodeFragment {

    private JavaBytecodeAnalyzer analyzer;
    private int stackVarIdx;

    public StackContentSavingCodeFragment(JavaBytecodeAnalyzer analyzer, int stackVarIdx) {
        this.analyzer = analyzer;
        this.stackVarIdx = stackVarIdx;
    }

    private void appendSavingCode(MethodVisitor mw, Type t) {

        if (t == Type.INT_TYPE || t == Type.VOID_TYPE) { // void means 'type is unknown and can not be deduced', we treat them as ints
            mw.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;");
        } else if (t == Type.FLOAT_TYPE) {
            mw.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Float", "valueOf", Method.getMethod("Float valueOf(float)").getDescriptor());
        } else if (t == Type.DOUBLE_TYPE) {
            mw.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Double", "valueOf", Method.getMethod("Double valueOf(double)").getDescriptor());
        } else if (t == Type.LONG_TYPE) {
            mw.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Long", "valueOf", Method.getMethod("Long valueOf(long)").getDescriptor());
        } else {
            throw new UnsupportedOperandTypeException(t);
        }
        // loading pointer to list
        mw.visitVarInsn(Opcodes.ALOAD, stackVarIdx);
        mw.visitInsn(Opcodes.SWAP); // swapping (for method invocation list pointer must be deeper)
        mw.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/util/LinkedList", "addLast", Method.getMethod("void addLast(Object)").getDescriptor());
    }


    @Override
    public void appendCode(MethodVisitor mw) {
        // saving stack contents
        for (VerifyableOperandStackItem aStack : analyzer.getStackAfter()) {
            appendSavingCode(mw, aStack.getType());
        }


        // saving local variables
        //TODO: somehow detect if local variable is no longer needed
        final LocalVariableStorage vars = analyzer.getStateAfter().getLocalVars();
        for (LocalVariableSlot localVarSlot : vars.getSlotsByAccess(GenericOperand.AccessType.WRITE)) {
            final int idx = localVarSlot.getIdx();
            LocalVariable localVar = vars.getLocalVariable(idx);
            final Type type = localVar.getType();

            if (type == Type.INT_TYPE) {
                // loading variable
                mw.visitVarInsn(Opcodes.ILOAD, idx);
                appendSavingCode(mw, type);
            } else if (type == Type.FLOAT_TYPE) {
                mw.visitVarInsn(Opcodes.FLOAD, idx);
                appendSavingCode(mw, type);
            } else if (type == Type.LONG_TYPE) {
                mw.visitVarInsn(Opcodes.LLOAD, idx);
                appendSavingCode(mw, type);
            } else if (type == Type.DOUBLE_TYPE) {
                mw.visitVarInsn(Opcodes.DLOAD, idx);
                appendSavingCode(mw, type);
            } else if (type == Type.VOID_TYPE) {
                // local variable can not hold undefined type (like stack does), as all local var operations are typed
                // void shows that this is part of multi-slot local var, that does not need its own save operation
                continue;
            } else {
                throw new UnsupportedOperandTypeException(type);
            }
        }

    }

    @Override
    public void collectMethodParameterDescriptions(MethodDescriptor methodDescriptor) {
        methodDescriptor.addParameter(stackVarIdx, Type.getDescriptor(LinkedList.class));
    }
}
