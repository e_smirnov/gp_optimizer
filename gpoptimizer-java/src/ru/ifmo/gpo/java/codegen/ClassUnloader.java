package ru.ifmo.gpo.java.codegen;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ifmo.gpo.core.IGenerationEventListener;
import ru.ifmo.gpo.core.Island;
import ru.ifmo.gpo.java.JavaBytecodeExecutor;
import ru.ifmo.gpo.java.JavaTargetArchitecture;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryPoolMXBean;
import java.lang.management.MemoryUsage;

/**
 * User: jedi-philosopher
 * Date: 11.09.11
 * Time: 22:02
 */
public class ClassUnloader implements IGenerationEventListener {

    private JavaTargetArchitecture targetArchitecture;

    private MemoryPoolMXBean permGenPool;

    private static final Logger logger = LoggerFactory.getLogger(ClassUnloader.class);

    public ClassUnloader(JavaTargetArchitecture targetArchitecture) {
        this.targetArchitecture = targetArchitecture;

        for (MemoryPoolMXBean memoryPoolMXBean : ManagementFactory.getMemoryPoolMXBeans()) {
            if (memoryPoolMXBean.getName().toLowerCase().contains("perm gen")) {
                permGenPool = memoryPoolMXBean;
                break;
            }
        }
    }


    /**
     * The problem is that classes, loaded by custom classloader, unload only when loader is destroyed
     * We must manually destroy and recreted Bytecode Executor here, as JVM runs out of 'Perm Gen' memory at about 80k loaded classes
     */
    @Override
    public void generationProcessed(Island island, int generationIdx) {
        boolean shallClean = ((generationIdx % 50) == 0 && generationIdx > 0);
        if (permGenPool != null) {
            MemoryUsage usage = permGenPool.getUsage();
            if (usage.getMax() - usage.getUsed() < 16 * 1024 || usage.getCommitted() - usage.getUsed() < 4 * 1024) {
                shallClean = true;
            }
        }

        if (shallClean) {
            logger.debug("Cleaning old classes");
            targetArchitecture.setCodeExecutor(null);
            System.gc();
            try {
                Thread.sleep(500);
            } catch (InterruptedException ignore) {
            }
            System.gc();
            targetArchitecture.setCodeExecutor(new JavaBytecodeExecutor());
        }
    }
}
