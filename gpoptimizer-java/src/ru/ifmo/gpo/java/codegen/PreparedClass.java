package ru.ifmo.gpo.java.codegen;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import java.util.LinkedList;
import java.util.List;

/**
 * User: e_smirnov
 * Date: 17.11.2010
 * Time: 16:06:56
 */
public class PreparedClass {

    private List<PreparedMethod> methods = new LinkedList<PreparedMethod>();
    private String name;

    private byte[] compiledCode;

    public PreparedClass(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public PreparedMethod createMethod(String name) {
        PreparedMethod method = new PreparedMethod(name);
        methods.add(method);
        return method;
    }

    public void addMethod(PreparedMethod method) {
        methods.add(method);
    }

    public byte[] getCode() {
        if (compiledCode == null) {
            generateCode();
        }
        return compiledCode;
    }

    private void generateCode() {
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS);
        cw.visit(Opcodes.V1_5, Opcodes.ACC_PUBLIC, name, null, "java/lang/Object", null);

        generateCtor(cw);

        for (PreparedMethod method : methods) {
            method.generateCode(cw);
        }

        cw.visitEnd();
        compiledCode = cw.toByteArray();
    }

    private void generateCtor(ClassWriter cw) {
        // creates a MethodWriter for the (implicit) constructor
        MethodVisitor mw = cw.visitMethod(Opcodes.ACC_PUBLIC,
                "<init>",
                "()V",
                null,
                null);
        // pushes the 'this' variable
        mw.visitVarInsn(Opcodes.ALOAD, 0);
        // invokes the super class constructor
        mw.visitMethodInsn(Opcodes.INVOKESPECIAL, "java/lang/Object", "<init>", "()V");
        mw.visitInsn(Opcodes.RETURN);
        // this code uses a maximum of one stack element and one local variable
        mw.visitMaxs(1, 1);
        mw.visitEnd();
        // implicit constructor generated
    }
}
