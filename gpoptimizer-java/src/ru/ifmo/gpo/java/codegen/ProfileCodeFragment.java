package ru.ifmo.gpo.java.codegen;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;


/**
 * User: e_smirnov
 * Date: 19.11.2010
 * Time: 16:03:00
 */
public class ProfileCodeFragment implements ICodeFragment {

    private ICodeFragment profileTarget;
    private int localVarIdx;

    public ProfileCodeFragment(int localVarIdx, ICodeFragment profileTarget) {
        this.localVarIdx = localVarIdx;
        this.profileTarget = profileTarget;
    }

    @Override
    public void appendCode(MethodVisitor mw) {
        mw.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/System", "nanoTime", "()J");
        mw.visitVarInsn(Opcodes.LSTORE, localVarIdx);
        profileTarget.appendCode(mw);
        mw.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/System", "nanoTime", "()J");
        mw.visitVarInsn(Opcodes.LLOAD, localVarIdx);
        mw.visitInsn(Opcodes.LSUB);
        mw.visitVarInsn(Opcodes.LSTORE, localVarIdx);
    }

    @Override
    public void collectMethodParameterDescriptions(MethodDescriptor methodDescriptor) {
        profileTarget.collectMethodParameterDescriptions(methodDescriptor);
    }
}
