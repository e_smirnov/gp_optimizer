package ru.ifmo.gpo.java.codegen;

import org.objectweb.asm.MethodVisitor;

import java.util.LinkedList;
import java.util.List;

/**
 * User: e_smirnov
 * Date: 16.11.2010
 * Time: 18:34:52
 */
public class CodeFragmentList implements ICodeFragment {
    private List<ICodeFragment> fragments;

    public CodeFragmentList(ICodeFragment... fragments) {
        this.fragments = new LinkedList<ICodeFragment>();
        for (ICodeFragment fr : fragments) {
            this.fragments.add(fr);
        }
    }

    @Override
    public void appendCode(MethodVisitor mw) {
        for (ICodeFragment fr : fragments) {
            fr.appendCode(mw);
        }
    }

    @Override
    public void collectMethodParameterDescriptions(MethodDescriptor methodDescriptor) {
        for (ICodeFragment fr : fragments) {
            fr.collectMethodParameterDescriptions(methodDescriptor);
        }
    }
}
