package ru.ifmo.gpo.java.codegen;

import org.objectweb.asm.MethodVisitor;

/**
 * User: e_smirnov
 * Date: 16.11.2010
 * Time: 15:33:13
 * <p/>
 * Represents small piece of code, used by PreparedMethod to generate code.
 */
public interface ICodeFragment {
    /**
     * Generates bytecode from this fragment
     *
     * @param mw MethodVisitor used to generate bytecode
     */
    public void appendCode(MethodVisitor mw);

    /**
     * Reports information about necessary method parameters, used to generate method descriptors.
     *
     * @param methodDescriptor Object containing list of desired method parameter types.
     */
    public void collectMethodParameterDescriptions(MethodDescriptor methodDescriptor);
}
