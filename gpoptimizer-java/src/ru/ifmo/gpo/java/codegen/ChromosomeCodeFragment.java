package ru.ifmo.gpo.java.codegen;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.IGenericInstruction;
import ru.ifmo.gpo.java.instructions.JVMInstruction;
import ru.ifmo.gpo.java.instructions.JavaOpcodes;
import ru.ifmo.gpo.java.vmstate.JVMStateSchema;
import ru.ifmo.gpo.java.vmstate.LocalVariableSlot;

/**
 * User: e_smirnov
 * Date: 16.11.2010
 * Time: 15:33:51
 */
public class ChromosomeCodeFragment implements ICodeFragment {

    private InstructionSequence source;

    private JVMStateSchema JVMStateSchema;

    public ChromosomeCodeFragment(InstructionSequence source, JVMStateSchema JVMStateSchema) {
        this.source = source;
        this.JVMStateSchema = JVMStateSchema;
    }

    @Override
    public void appendCode(MethodVisitor mw) {
        for (IGenericInstruction instr : source) {
            if (instr.getParameters().isEmpty()) {
                // this is a zero operand instruction
                mw.visitInsn(instr.getOpcode());
            } else if (instr.getOpcode() == JavaOpcodes.BIPUSH.getRealOpcode()) {
                mw.visitIntInsn(instr.getOpcode(), ((JVMInstruction) instr).getIntParameter(0));
            } else if (instr.getOpcode() == JavaOpcodes.IINC.getRealOpcode()) {
                mw.visitIincInsn(((LocalVariableSlot) ((JVMInstruction) instr).getParameter(0)).getIdx(), ((JVMInstruction) instr).getIntParameter(1));
            } else {
                //TODO: now only load/store instructions possible here, but after implementation of full set of bytecodes additional checks are needed here
                mw.visitVarInsn(instr.getOpcode(), ((LocalVariableSlot) ((JVMInstruction) instr).getParameter(0)).getIdx());
            }
        }
    }

    @Override
    public void collectMethodParameterDescriptions(MethodDescriptor methodDescriptor) {
        int idx = 0;
        for (LocalVariableSlot slot : JVMStateSchema.getSlots()) {
            if (slot == null) {
                // dummy slot
                methodDescriptor.addParameter(idx, Type.INT_TYPE.getDescriptor());
            } else if (slot.isReadable()) {
                methodDescriptor.addParameter(slot.getIdx(), slot.getType().getDescriptor());
            }
            ++idx;
        }
    }
}
