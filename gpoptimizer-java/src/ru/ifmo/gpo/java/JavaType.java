package ru.ifmo.gpo.java;

import ru.ifmo.gpo.core.Type;

/**
 * User: e_smirnov
 * Date: 29.08.2011
 * Time: 14:52:59
 * <p/>
 * Contains some helper methods for handling java types
 */
public class JavaType {

    public static int fromClass(Class c) {
        if (c.equals(int.class)) {
            return Type.INT_TYPE;
        }

        if (c.equals(long.class)) {
            return Type.LONG_TYPE;
        }

        if (c.equals(float.class)) {
            return Type.FLOAT_TYPE;
        }
        if (c.equals(double.class)) {
            return Type.DOUBLE_TYPE;
        }
        return Type.POINTER_TYPE;
    }

    public static int getSize(int type) {
        switch (type) {
            case Type.LONG_TYPE:
            case Type.DOUBLE_TYPE:
                return 2;
            default:
                return 1;
        }
    }
}
