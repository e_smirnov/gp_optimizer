package ru.ifmo.gpo.java.instructions;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.instructions.IOperandSelector;
import ru.ifmo.gpo.core.instructions.selectors.DummyOperandSelector;
import ru.ifmo.gpo.core.instructions.selectors.OpcodeDisabler;
import ru.ifmo.gpo.java.instructions.selectors.*;
import ru.ifmo.gpo.verify.GenericOperator;

/**
 * User: e_smirnov
 * Date: 22.10.2010
 * Time: 14:40:13
 */
public enum JavaOpcodes {
    NOP(Opcodes.NOP, new Type[]{}, new Type[]{}),
    ICONST_M1(Opcodes.ICONST_M1, new Type[]{}, new Type[]{Type.INT_TYPE}),
    ICONST_0(Opcodes.ICONST_0, new Type[]{}, new Type[]{Type.INT_TYPE}),
    ICONST_1(Opcodes.ICONST_1, new Type[]{}, new Type[]{Type.INT_TYPE}),
    ICONST_2(Opcodes.ICONST_2, new Type[]{}, new Type[]{Type.INT_TYPE}),
    ICONST_3(Opcodes.ICONST_3, new Type[]{}, new Type[]{Type.INT_TYPE}),

    FCONST_0(Opcodes.FCONST_0, new Type[]{}, new Type[]{Type.FLOAT_TYPE}),
    FCONST_1(Opcodes.FCONST_1, new Type[]{}, new Type[]{Type.FLOAT_TYPE}),
    FCONST_2(Opcodes.FCONST_2, new Type[]{}, new Type[]{Type.FLOAT_TYPE}),

    POP(Opcodes.POP, new Type[]{Type.VOID_TYPE}, new Type[]{}, new TopStackElementCompTypeChecker()),  // POP requires top of stack to belong to 1st JVM computational type
    DUP(Opcodes.DUP, new Type[]{Type.VOID_TYPE}, new Type[]{Type.VOID_TYPE}, new TopStackElementCompTypeChecker()), // todo: backreferences
    SWAP(Opcodes.SWAP, new Type[]{Type.VOID_TYPE, Type.VOID_TYPE}, new Type[]{Type.VOID_TYPE, Type.VOID_TYPE}, new OpcodeDisabler()/*new TopStackElementCompTypeChecker(2)*/),

    IADD(Opcodes.IADD, GenericOperator.PLUS, new Type[]{Type.INT_TYPE, Type.INT_TYPE}, new Type[]{Type.INT_TYPE}),
    ISUB(Opcodes.ISUB, GenericOperator.MINUS, new Type[]{Type.INT_TYPE, Type.INT_TYPE}, new Type[]{Type.INT_TYPE}),
    IMUL(Opcodes.IMUL, GenericOperator.MUL, new Type[]{Type.INT_TYPE, Type.INT_TYPE}, new Type[]{Type.INT_TYPE}),
    IDIV(Opcodes.IDIV, GenericOperator.DIV, new Type[]{Type.INT_TYPE, Type.INT_TYPE}, new Type[]{Type.INT_TYPE}),
    ISHR(Opcodes.ISHR, GenericOperator.SHR, new Type[]{Type.INT_TYPE, Type.INT_TYPE}, new Type[]{Type.INT_TYPE}),
    ISHL(Opcodes.ISHL, GenericOperator.SHL, new Type[]{Type.INT_TYPE, Type.INT_TYPE}, new Type[]{Type.INT_TYPE}),
    IREM(Opcodes.IREM, GenericOperator.REM, new Type[]{Type.INT_TYPE, Type.INT_TYPE}, new Type[]{Type.INT_TYPE}),
    IOR(Opcodes.IOR, GenericOperator.OR, new Type[]{Type.INT_TYPE, Type.INT_TYPE}, new Type[]{Type.INT_TYPE}),
    IXOR(Opcodes.IXOR, GenericOperator.XOR, new Type[]{Type.INT_TYPE, Type.INT_TYPE}, new Type[]{Type.INT_TYPE}),
    IAND(Opcodes.IAND, GenericOperator.AND, new Type[]{Type.INT_TYPE, Type.INT_TYPE}, new Type[]{Type.INT_TYPE}),
    INEG(Opcodes.INEG, GenericOperator.NEG, new Type[]{Type.INT_TYPE}, new Type[]{Type.INT_TYPE}),
    ILOAD(Opcodes.ILOAD, new Type[]{}, new Type[]{Type.INT_TYPE}, new ReadVariableIdxSelector(Type.INT_TYPE)),
    ISTORE(Opcodes.ISTORE, new Type[]{Type.INT_TYPE}, new Type[]{}, new WriteVariableIdxSelector(Type.INT_TYPE)),

    IINC(Opcodes.IINC, new Type[]{}, new Type[]{}, new IINCOperandSelector()),

    // constant loading
    BIPUSH(Opcodes.BIPUSH, new Type[]{Type.INT_TYPE}, new Type[]{}, new ConstantOperandSelector(Type.INT_TYPE)),

    ////////////////////// float

    FADD(Opcodes.FADD, GenericOperator.FADD, new Type[]{Type.FLOAT_TYPE, Type.FLOAT_TYPE}, new Type[]{Type.FLOAT_TYPE}),
    FSUB(Opcodes.FSUB, GenericOperator.FSUB, new Type[]{Type.FLOAT_TYPE, Type.FLOAT_TYPE}, new Type[]{Type.FLOAT_TYPE}),
    FMUL(Opcodes.FMUL, GenericOperator.FMUL, new Type[]{Type.FLOAT_TYPE, Type.FLOAT_TYPE}, new Type[]{Type.FLOAT_TYPE}),
    FDIV(Opcodes.FDIV, GenericOperator.FDIV, new Type[]{Type.FLOAT_TYPE, Type.FLOAT_TYPE}, new Type[]{Type.FLOAT_TYPE}),
    FREM(Opcodes.FREM, GenericOperator.FREM, new Type[]{Type.FLOAT_TYPE, Type.FLOAT_TYPE}, new Type[]{Type.FLOAT_TYPE}),
    FNEG(Opcodes.FNEG, GenericOperator.FNEG, new Type[]{Type.FLOAT_TYPE}, new Type[]{Type.FLOAT_TYPE}),
    FLOAD(Opcodes.FLOAD, new Type[]{}, new Type[]{Type.FLOAT_TYPE}, new ReadVariableIdxSelector(Type.FLOAT_TYPE)),
    FSTORE(Opcodes.FSTORE, new Type[]{Type.FLOAT_TYPE}, new Type[]{}, new WriteVariableIdxSelector(Type.FLOAT_TYPE)),

    I2F(Opcodes.I2F, new Type[]{Type.INT_TYPE}, new Type[]{Type.FLOAT_TYPE}),
    F2I(Opcodes.F2I, new Type[]{Type.FLOAT_TYPE}, new Type[]{Type.INT_TYPE}),

    //////////////////////// long

    LLOAD(Opcodes.LLOAD, new Type[]{}, new Type[]{Type.LONG_TYPE}, new ReadVariableIdxSelector(Type.LONG_TYPE)),
    LSTORE(Opcodes.LSTORE, new Type[]{Type.LONG_TYPE}, new Type[]{}, new WriteVariableIdxSelector(Type.LONG_TYPE)),

    LCONST_0(Opcodes.LCONST_0, new Type[]{}, new Type[]{Type.LONG_TYPE}),
    LCONST_1(Opcodes.LCONST_1, new Type[]{}, new Type[]{Type.LONG_TYPE}),

    LADD(Opcodes.LADD, GenericOperator.PLUS, new Type[]{Type.LONG_TYPE, Type.LONG_TYPE}, new Type[]{Type.LONG_TYPE}),
    LSUB(Opcodes.LSUB, GenericOperator.MINUS, new Type[]{Type.LONG_TYPE, Type.LONG_TYPE}, new Type[]{Type.LONG_TYPE}),
    LMUL(Opcodes.LMUL, GenericOperator.MUL, new Type[]{Type.LONG_TYPE, Type.LONG_TYPE}, new Type[]{Type.LONG_TYPE}),
    LDIV(Opcodes.LDIV, GenericOperator.DIV, new Type[]{Type.LONG_TYPE, Type.LONG_TYPE}, new Type[]{Type.LONG_TYPE}),
    LSHR(Opcodes.LSHR, GenericOperator.SHR, new Type[]{Type.LONG_TYPE, Type.INT_TYPE}, new Type[]{Type.LONG_TYPE}),
    LSHL(Opcodes.LSHL, GenericOperator.SHL, new Type[]{Type.LONG_TYPE, Type.INT_TYPE}, new Type[]{Type.LONG_TYPE}),
    LREM(Opcodes.LREM, GenericOperator.REM, new Type[]{Type.LONG_TYPE, Type.LONG_TYPE}, new Type[]{Type.LONG_TYPE}),
    LOR(Opcodes.LOR, GenericOperator.OR, new Type[]{Type.LONG_TYPE, Type.LONG_TYPE}, new Type[]{Type.LONG_TYPE}),
    LXOR(Opcodes.LXOR, GenericOperator.XOR, new Type[]{Type.LONG_TYPE, Type.LONG_TYPE}, new Type[]{Type.LONG_TYPE}),
    LAND(Opcodes.LAND, GenericOperator.AND, new Type[]{Type.LONG_TYPE, Type.LONG_TYPE}, new Type[]{Type.LONG_TYPE}),
    LNEG(Opcodes.LNEG, GenericOperator.NEG, new Type[]{Type.LONG_TYPE}, new Type[]{Type.LONG_TYPE}),

    I2L(Opcodes.I2L, new Type[]{Type.INT_TYPE}, new Type[]{Type.LONG_TYPE}),
    L2I(Opcodes.L2I, new Type[]{Type.LONG_TYPE}, new Type[]{Type.INT_TYPE}),

    L2F(Opcodes.L2F, new Type[]{Type.LONG_TYPE}, new Type[]{Type.FLOAT_TYPE}),
    F2L(Opcodes.F2L, new Type[]{Type.FLOAT_TYPE}, new Type[]{Type.LONG_TYPE}),;

    private int realOpcode;

    private GenericOperator operator;

    private Type[] expectedStackTypes;
    private Type[] addedStackTypes;

    private IOperandSelector operandSelector;

    JavaOpcodes(int realOpcode, Type[] expectedStackTypes, Type[] addedStackTypes) {
        this.realOpcode = realOpcode;
        this.expectedStackTypes = expectedStackTypes;
        this.addedStackTypes = addedStackTypes;
        this.operandSelector = DummyOperandSelector.getInstance();
    }

    JavaOpcodes(int realOpcode, GenericOperator operator, Type[] expectedStackTypes, Type[] addedStackTypes) {
        this.realOpcode = realOpcode;
        this.expectedStackTypes = expectedStackTypes;
        this.operator = operator;
        this.addedStackTypes = addedStackTypes;
        this.operandSelector = DummyOperandSelector.getInstance();
    }

    JavaOpcodes(int realOpcode, Type[] expectedStackTypes, Type[] addedStackTypes, IOperandSelector operandSelector) {
        this.realOpcode = realOpcode;
        this.expectedStackTypes = expectedStackTypes;
        this.addedStackTypes = addedStackTypes;
        this.operandSelector = operandSelector;
    }

    public GenericOperator getOperator() {
        return operator;
    }

    public static JavaOpcodes valueOf(int opcode) {
        for (JavaOpcodes op : values()) {
            if (op.getRealOpcode() == opcode) {
                return op;
            }
        }
        throw new IllegalArgumentException(opcode + " is not a valid JavaOpcodes value");
    }

    public int getRealOpcode() {
        return realOpcode;
    }

    public Type[] getExpectedStackTypes() {
        return expectedStackTypes;
    }

    public Type[] getAddedStackTypes() {
        return addedStackTypes;
    }

    public IOperandSelector getOperandSelector() {
        return operandSelector;
    }
}
