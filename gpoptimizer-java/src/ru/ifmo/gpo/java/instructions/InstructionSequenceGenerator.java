package ru.ifmo.gpo.java.instructions;

import ru.ifmo.gpo.core.BaseInstructionSequenceGenerator;
import ru.ifmo.gpo.core.ITargetArchitecture;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.instructions.generic.IGenericInstruction;
import ru.ifmo.gpo.core.vmstate.FailedToSatisfyRestrictionsException;
import ru.ifmo.gpo.core.vmstate.IMachineState;
import ru.ifmo.gpo.java.vmstate.StackOperationAction;
import ru.ifmo.gpo.java.vmstate.VMState;
import ru.ifmo.gpo.java.vmstate.VMStateDelta;

import java.util.List;

/**
 * User: jedi-philosopher
 * Date: 12.12.2010
 * Time: 18:22:27
 */
public class InstructionSequenceGenerator extends BaseInstructionSequenceGenerator<VMState, VMStateDelta> {

    public InstructionSequenceGenerator(ITargetArchitecture target) {
        super(target);
    }

    @Override
    protected void preprocessState(IMachineState state) {
        ((VMState) state).getStackState().setAllowUnderflows(false);
    }

    @Override
    protected double computeBreakProbability(int resultSize, int desiredLength, int stateDelta) {
        if (stateDelta != 0) {
            return -1;
        }
        return resultSize / (double) desiredLength - 0.5;
    }

    @Override
    protected IGenericInstruction selectInstructionSatisfyRestrictions(InstructionSequence alreadyGenerated, VMState state, VMStateDelta delta) throws FailedToSatisfyRestrictionsException {
        JVMInstruction instr = (JVMInstruction) JavaSimpleOpcodeFactory.getSuitableInstruction(state, delta, StackOperationAction.SATISFY_AT_LEAST_ONE);
        List<GenericOperand> operands = instr.getOpcodeInternal().getOperandSelector().selectOparandsSatisfyRestrictions(instr.getOpcodeInternal(), state, delta.getTarget(), delta);
        if (!operands.isEmpty()) {
            instr.setParameters(operands);
        }
        return instr;
    }

    @Override
    protected IGenericInstruction selectAnyInstruction(InstructionSequence alreadyGenerated, VMState state) throws FailedToSatisfyRestrictionsException {
        JVMInstruction jvmInstruction = (JVMInstruction) JavaSimpleOpcodeFactory.getSuitableInstruction(state, null, StackOperationAction.NO_ACTION);
        List<GenericOperand> operands = jvmInstruction.getOpcodeInternal().getOperandSelector().selectOperandsRandom(jvmInstruction.getOpcodeInternal(), state);
        if (!operands.isEmpty()) {
            jvmInstruction.setParameters(operands);
        }
        return jvmInstruction;
    }
}
