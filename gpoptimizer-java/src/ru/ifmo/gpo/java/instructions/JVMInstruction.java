package ru.ifmo.gpo.java.instructions;

import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.instructions.generic.IGenericInstruction;
import ru.ifmo.gpo.core.instructions.generic.NumericConstant;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * User: jedi-philosopher
 * Date: 02.05.2010
 * Time: 22:13:47
 * <p/>
 * Represents single instruction for JVM. Contains instruction opcode and optional list of instruction arguments
 */
public class JVMInstruction implements Serializable, IGenericInstruction {
    /**
     * Instruction opcode information
     */
    private JavaOpcodes opcode;

    /**
     * List of instruction operands. Null if instruction does not require any parameters
     */
    private List<GenericOperand> operands;
    private static final long serialVersionUID = 4714393418746571294L;

    public JVMInstruction() {
        this.opcode = JavaOpcodes.NOP;
        this.operands = null;
    }

    public JVMInstruction(JavaOpcodes _opcode) {
        this.opcode = _opcode;
        this.operands = null;
    }

    public JVMInstruction(JavaOpcodes opcode, Collection<GenericOperand> operands) {
        this.opcode = opcode;
        if (operands == null || operands.isEmpty()) {
            this.operands = null;
        } else {
            this.operands = new ArrayList<GenericOperand>(operands);
        }
    }

    public JVMInstruction(JavaOpcodes _opcode, GenericOperand... operands) {
        this.opcode = _opcode;
        if (operands == null || operands.length == 0) {
            this.operands = null;
        } else {
            this.operands = new ArrayList<GenericOperand>(2);
            Collections.addAll(this.operands, operands);
        }
    }

    public JVMInstruction(JVMInstruction proto) {
        this.opcode = proto.opcode;
        if (proto.operands != null) {
            this.operands = new ArrayList<GenericOperand>(proto.operands);
        }
    }

    // Methods taking int are too slow, as valueOf() leads to array copying, use methods with JavaOpcodes where possible
    @Deprecated
    public JVMInstruction(int opcode, GenericOperand... operands) {
        this(JavaOpcodes.valueOf(opcode), operands);
    }

    @Deprecated
    public JVMInstruction(int opcode) {
        this(JavaOpcodes.valueOf(opcode));
    }

    @Override
    public int getOpcode() {
        return opcode.getRealOpcode();
    }

    public void setParameters(List<GenericOperand> newParams) {
        operands = newParams;
    }

    @Override
    public List<GenericOperand> getParameters() {
        if (operands == null) {
            return Collections.emptyList();
        }

        return operands;
    }

    public GenericOperand getParameter(int i) {
        if (operands == null) {
            return null;
        }
        return operands.get(i);
    }

    public JavaOpcodes getOpcodeInternal() {
        return opcode;
    }

    @Override
    public String toString() {
        String rz = opcode.toString();
        if (operands != null) {
            rz += " " + operands.toString();
        }
        return rz;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JVMInstruction that = (JVMInstruction) o;

        if (opcode != that.opcode) return false;
        if (operands != null ? !operands.equals(that.operands) : that.operands != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = opcode != null ? opcode.getRealOpcode() : 0;
        result = 31 * result + (operands != null ? operands.hashCode() : 0);
        return result;
    }

    public int getIntParameter(int i) {
        GenericOperand op = getParameter(i);
        if (!(op instanceof NumericConstant)) {
            throw new IllegalArgumentException("Argument " + i + " for instruction " + opcode + " is not an integer");
        }
        return ((NumericConstant) op).intValue();
    }
}
