package ru.ifmo.gpo.java.instructions;

/**
 * User: e_smirnov
 * Date: 29.10.2010
 * Time: 13:55:07
 */
public enum OpcodeOperandType {
    NO_OPERAND,
    INT_LOCAL_VAR_READ,
    FLOAT_LOCAL_VAR_READ,
    INT_LOCAL_VAR_WRITE,
    FLOAT_LOCAL_VAR_WRITE;
}
