package ru.ifmo.gpo.java.instructions.selectors;

import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.instructions.IOperandSelector;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand.AccessType;
import ru.ifmo.gpo.core.instructions.generic.NumericConstant;
import ru.ifmo.gpo.core.vmstate.NoSuitableOperandFoundException;
import ru.ifmo.gpo.java.instructions.JavaOpcodes;
import ru.ifmo.gpo.java.vmstate.LocalVariableSlot;
import ru.ifmo.gpo.java.vmstate.VMState;
import ru.ifmo.gpo.java.vmstate.VMStateDelta;
import ru.ifmo.gpo.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Special operand selector for IINC instruction. Selects random int local variable and increments
 *
 * @author jedi-philosopher
 */

public class IINCOperandSelector implements IOperandSelector<JavaOpcodes, VMState, VMStateDelta> {

    @Override
    public List<GenericOperand> selectOperandsRandom(JavaOpcodes opcode, VMState state)
            throws NoSuitableOperandFoundException {
        if (state.getConstants().getIntConstantNumber() == 0) {
            throw new NoSuitableOperandFoundException(opcode.getRealOpcode());
        }
        List<GenericOperand> rz = new ArrayList<GenericOperand>(2);
        Collection<LocalVariableSlot> availableSlots = state.getLocalVars().getSlotsByAccessAndType(AccessType.ANY, Type.INT_TYPE);
        if (availableSlots.isEmpty()) {
            throw new NoSuitableOperandFoundException(opcode.getRealOpcode());
        }
        rz.add(CollectionUtils.selectRandomElement(availableSlots));
        rz.add(new NumericConstant(state.getConstants().getIntConstant()));
        return rz;
    }

    @Override
    public List<GenericOperand> selectOparandsSatisfyRestrictions(JavaOpcodes opcode,
                                                                  VMState stateBefore, VMState desiredState,
                                                                  VMStateDelta diffWithDesiredState)
            throws NoSuitableOperandFoundException {
        throw new NoSuitableOperandFoundException(opcode.getRealOpcode()); // IINC does not affect state
    }

    @Override
    public List<List<GenericOperand>> getAllPossibleOperandSets(
            JavaOpcodes opcode, VMState state)
            throws NoSuitableOperandFoundException {
        // TODO Auto-generated method stub

        Collection<LocalVariableSlot> availableSlots = state.getLocalVars().getSlotsByAccessAndType(AccessType.ANY, Type.INT_TYPE);
        if (availableSlots.isEmpty()) {
            throw new NoSuitableOperandFoundException(opcode.getRealOpcode());
        }
        List<List<GenericOperand>> rz = new LinkedList<List<GenericOperand>>();
        for (Integer i : state.getConstants().getIntConstants()) {
            for (LocalVariableSlot slot : availableSlots) {
                List<GenericOperand> listToAdd = new ArrayList<GenericOperand>(2);
                listToAdd.add(slot);
                listToAdd.add(new NumericConstant(i));
                rz.add(listToAdd);
            }
        }
        return rz;
    }

}
