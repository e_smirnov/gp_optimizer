package ru.ifmo.gpo.java.instructions.selectors;

import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.instructions.IOperandSelector;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.vmstate.NoSuitableOperandFoundException;
import ru.ifmo.gpo.java.instructions.JavaOpcodes;
import ru.ifmo.gpo.java.vmstate.VMState;
import ru.ifmo.gpo.java.vmstate.VMStateDelta;

import java.util.Collections;
import java.util.List;

/**
 * User: e_smirnov
 * Date: 15.09.2011
 * Time: 17:30:28
 *
 * Some opcodes like DUP or POP can only operate on stack items of 1st computational type (all types except LONG and DOUBLE)
 * Check it here and throw excetpion
 */
public class TopStackElementCompTypeChecker implements IOperandSelector<JavaOpcodes, VMState, VMStateDelta> {

    private int depth;

    public TopStackElementCompTypeChecker() {
        this.depth = 1;
    }

    public TopStackElementCompTypeChecker(int depth) {
        this.depth = depth;
    }

    @Override
    public List<GenericOperand> selectOperandsRandom(JavaOpcodes opcode, VMState state) throws NoSuitableOperandFoundException {
        for (int i = 0; i < depth; ++i) {
            final Type t = state.getStackState().peek(i).getType();
            if (t.getSize() == 2) {
                throw new NoSuitableOperandFoundException(opcode.getRealOpcode());
            }
        }
        return Collections.emptyList();
    }

    @Override
    public List<GenericOperand> selectOparandsSatisfyRestrictions(JavaOpcodes opcode, VMState stateBefore, VMState desiredState, VMStateDelta diffWithDesiredState) throws NoSuitableOperandFoundException {
        return selectOperandsRandom(opcode, stateBefore);
    }

    @Override
    public List<List<GenericOperand>> getAllPossibleOperandSets(JavaOpcodes opcode, VMState state) throws NoSuitableOperandFoundException {
        return Collections.emptyList();
    }
}
