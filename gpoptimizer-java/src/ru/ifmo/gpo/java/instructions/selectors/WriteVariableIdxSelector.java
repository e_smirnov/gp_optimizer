package ru.ifmo.gpo.java.instructions.selectors;

import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.instructions.IOperandSelector;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.vmstate.NoSuitableOperandFoundException;
import ru.ifmo.gpo.java.instructions.JVMInstruction;
import ru.ifmo.gpo.java.instructions.JavaOpcodes;
import ru.ifmo.gpo.java.vmstate.LocalVariableSlot;
import ru.ifmo.gpo.java.vmstate.VMState;
import ru.ifmo.gpo.java.vmstate.VMStateDelta;
import ru.ifmo.gpo.util.CollectionUtils;

import java.util.*;

/**
 * User: jedi-philosopher
 * Date: 11.12.2010
 * Time: 15:54:14
 */
public class WriteVariableIdxSelector implements IOperandSelector<JavaOpcodes, VMState, VMStateDelta> {
    private Type type;

    public WriteVariableIdxSelector(Type type) {
        this.type = type;
    }

    private boolean checkSlotHasEnoughAvailableNeighbours(VMState state, int slotIdx) {
        for (int i = 1; i < type.getSize(); ++i) {
            LocalVariableSlot nextSlot = state.getLocalVars().getSchema().getSlot(slotIdx + i);
            if (nextSlot == null || !nextSlot.isWriteable()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public List<GenericOperand> selectOperandsRandom(JavaOpcodes opcode, VMState state) throws NoSuitableOperandFoundException {
        final Collection<LocalVariableSlot> slots = state.getLocalVars().getSlotsByAccess(GenericOperand.AccessType.WRITE);
        if (slots.isEmpty()) {
            throw new NoSuitableOperandFoundException(new JVMInstruction(opcode));
        }
        List<GenericOperand> rz = new ArrayList<GenericOperand>(1);
        if (type.getSize() == 1) {
            LocalVariableSlot slot = CollectionUtils.selectRandomElement(slots);
            rz.add(slot);
            return rz;
        }

        // if type.size() > 1, must collect slots that also have needed amount of free slots next to them to store multi-slot type
        Set<LocalVariableSlot> slotsToUse = new HashSet<LocalVariableSlot>();
        for (LocalVariableSlot candidate : slots) {
            if (checkSlotHasEnoughAvailableNeighbours(state, candidate.getIdx())) {
                slotsToUse.add(candidate);
            }
        }

        if (slotsToUse.isEmpty()) {
            throw new NoSuitableOperandFoundException(new JVMInstruction(opcode));
        }
        LocalVariableSlot slot = CollectionUtils.selectRandomElement(slotsToUse);
        rz.add(slot);
        return rz;
    }

    @Override
    public List<GenericOperand> selectOparandsSatisfyRestrictions(JavaOpcodes opcode, VMState stateBefore, VMState desiredState, VMStateDelta diffWithDesiredState) throws NoSuitableOperandFoundException {
        if (diffWithDesiredState == null) {
            diffWithDesiredState = VMStateDelta.create(stateBefore, desiredState);
        }
        Set<Integer> desiredVarIndices = CollectionUtils.getMapKeysForValue(diffWithDesiredState.getLocalVariableWrites(), type);
        if (desiredVarIndices.isEmpty()) {
            throw new NoSuitableOperandFoundException(new JVMInstruction(opcode));
        }
        // no additional checks for multi-slot types needed, as if this index is in state delta, than in desired state next local vars must be available for wirting
        List<GenericOperand> rz = new ArrayList<GenericOperand>(1);
        Integer idx = CollectionUtils.selectRandomElement(desiredVarIndices);
        rz.add(new LocalVariableSlot(idx));
        return rz;
    }

    @Override
    public List<List<GenericOperand>> getAllPossibleOperandSets(
            JavaOpcodes opcode, VMState state)
            throws NoSuitableOperandFoundException {
        List<List<GenericOperand>> rz = new LinkedList<List<GenericOperand>>();
        Collection<LocalVariableSlot> localVars = state.getLocalVars().getSlotsByAccess(GenericOperand.AccessType.WRITE);
        if (localVars.isEmpty()) {
            throw new NoSuitableOperandFoundException(new JVMInstruction(opcode));
        }
        for (LocalVariableSlot slot : localVars) {
            List<GenericOperand> listToAdd = new ArrayList<GenericOperand>(1);
            listToAdd.add(slot);
            rz.add(listToAdd);
        }
        return rz;
    }
}
