package ru.ifmo.gpo.java.instructions.selectors;

import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.instructions.IOperandSelector;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.vmstate.NoSuitableOperandFoundException;
import ru.ifmo.gpo.java.instructions.JVMInstruction;
import ru.ifmo.gpo.java.instructions.JavaOpcodes;
import ru.ifmo.gpo.java.vmstate.LocalVariableSlot;
import ru.ifmo.gpo.java.vmstate.StackOperation;
import ru.ifmo.gpo.java.vmstate.VMState;
import ru.ifmo.gpo.java.vmstate.VMStateDelta;
import ru.ifmo.gpo.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * User: jedi-philosopher
 * Date: 11.12.2010
 * Time: 0:48:20
 * <p/>
 * Selects operand for xLOAD instructions
 */
public class ReadVariableIdxSelector implements IOperandSelector<JavaOpcodes, VMState, VMStateDelta> {
    private Type variableType;

    public ReadVariableIdxSelector(Type variableType) {
        this.variableType = variableType;
    }

    @Override
    public List<GenericOperand> selectOperandsRandom(JavaOpcodes opcode, VMState state) throws NoSuitableOperandFoundException {
        Collection<LocalVariableSlot> localVars = state.getLocalVars().getSlotsByAccessAndType(GenericOperand.AccessType.READ, variableType);
        if (localVars.isEmpty()) {
            throw new NoSuitableOperandFoundException(new JVMInstruction(opcode));
        }
        List<GenericOperand> rz = new LinkedList<GenericOperand>();
        rz.add(new LocalVariableSlot(CollectionUtils.selectRandomElement(localVars).getIdx()));
        return rz;
    }

    @Override
    public List<GenericOperand> selectOparandsSatisfyRestrictions(JavaOpcodes opcode, VMState stateBefore, VMState desiredState, VMStateDelta diffWithDesiredState) throws NoSuitableOperandFoundException {
        if (diffWithDesiredState.getStackOperations().isEmpty()
                || !diffWithDesiredState.getStackOperations().peek().type.equals(variableType)
                || !diffWithDesiredState.getStackOperations().peek().op.equals(StackOperation.Operation.PUSH)) {
            // pushing value on stack will increase delta
            throw new NoSuitableOperandFoundException(opcode.getRealOpcode());
        }
        // reading variables does not change vm state
        return selectOperandsRandom(opcode, stateBefore);
    }

    @Override
    public List<List<GenericOperand>> getAllPossibleOperandSets(
            JavaOpcodes opcode, VMState state)
            throws NoSuitableOperandFoundException {
        List<List<GenericOperand>> rz = new LinkedList<List<GenericOperand>>();
        Collection<LocalVariableSlot> localVars = state.getLocalVars().getSlotsByAccessAndType(GenericOperand.AccessType.READ, variableType);
        if (localVars.isEmpty()) {
            throw new NoSuitableOperandFoundException(new JVMInstruction(opcode));
        }
        for (LocalVariableSlot slot : localVars) {
            List<GenericOperand> listToAdd = new ArrayList<GenericOperand>(1);
            listToAdd.add(slot);
            rz.add(listToAdd);
        }
        return rz;
    }
}
