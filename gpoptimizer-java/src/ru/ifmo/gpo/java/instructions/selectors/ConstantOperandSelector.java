package ru.ifmo.gpo.java.instructions.selectors;

import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.instructions.IOperandSelector;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.instructions.generic.NumericConstant;
import ru.ifmo.gpo.core.vmstate.NoSuitableOperandFoundException;
import ru.ifmo.gpo.java.instructions.JavaOpcodes;
import ru.ifmo.gpo.java.vmstate.StackOperation;
import ru.ifmo.gpo.java.vmstate.VMState;
import ru.ifmo.gpo.java.vmstate.VMStateDelta;
import ru.ifmo.gpo.util.CollectionUtils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ConstantOperandSelector implements
        IOperandSelector<JavaOpcodes, VMState, VMStateDelta> {

    private Type constantType;

    public ConstantOperandSelector(Type constantType) {
        this.constantType = constantType;
    }

    @Override
    public List<GenericOperand> selectOperandsRandom(JavaOpcodes opcode, VMState state)
            throws NoSuitableOperandFoundException {
        if (state.getConstants().getIntConstants().isEmpty()) {
            throw new NoSuitableOperandFoundException(opcode.getRealOpcode());
        }
        Number constant = CollectionUtils.selectRandomElement(state.getConstants().getIntConstants());
        List<GenericOperand> rz = new ArrayList<GenericOperand>(1);
        rz.add(new NumericConstant(constant));
        return rz;
    }

    @Override
    public List<GenericOperand> selectOparandsSatisfyRestrictions(JavaOpcodes opcode,
                                                                  VMState stateBefore, VMState desiredState,
                                                                  VMStateDelta diffWithDesiredState)
            throws NoSuitableOperandFoundException {
        if (diffWithDesiredState.getStackOperations().isEmpty()
                || !diffWithDesiredState.getStackOperations().peek().type.equals(constantType)
                || !diffWithDesiredState.getStackOperations().peek().op.equals(StackOperation.Operation.PUSH)) {
            // pushing value on stack will increase delta
            throw new NoSuitableOperandFoundException(opcode.getRealOpcode());
        }
        return selectOperandsRandom(opcode, stateBefore);
    }

    @Override
    public List<List<GenericOperand>> getAllPossibleOperandSets(
            JavaOpcodes opcode, VMState state)
            throws NoSuitableOperandFoundException {
        List<List<GenericOperand>> rz = new LinkedList<List<GenericOperand>>();
        for (Integer i : state.getConstants().getIntConstants()) {
            List<GenericOperand> listToAdd = new LinkedList<GenericOperand>();
            listToAdd.add(new NumericConstant(i));
            rz.add(listToAdd);
        }
        return rz;
    }

}
