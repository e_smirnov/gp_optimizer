package ru.ifmo.gpo.java.instructions;

import org.objectweb.asm.Opcodes;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.instructions.generic.IGenericInstruction;
import ru.ifmo.gpo.core.vmstate.NoSuitableInstructionFoundException;
import ru.ifmo.gpo.core.vmstate.NoSuitableOperandFoundException;
import ru.ifmo.gpo.java.JavaBytecodeEvaluator;
import ru.ifmo.gpo.java.vmstate.StackOperationAction;
import ru.ifmo.gpo.java.vmstate.VMState;
import ru.ifmo.gpo.java.vmstate.VMStateDelta;
import ru.ifmo.gpo.protocol.SupportedOperationsInfo;
import ru.ifmo.gpo.util.CollectionUtils;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Created by IntelliJ IDEA.
 * User: jedi-philosopher
 * Date: 14.01.2010
 * Time: 15:13:48
 * <p/>
 * Simple implementation of Opcode Factory for test purposes. It emits only limited number of integer arithmetic opcodes
 */
public class JavaSimpleOpcodeFactory {

    static private java.util.Random random = new Random();
    static private JavaOpcodes[] supportedInstructions = JavaOpcodes.values();

    public static SupportedOperationsInfo getSupportedOperations() {
        HashSet<Integer> rz = new HashSet<Integer>();
        for (JavaOpcodes opcode : supportedInstructions) {
            rz.add(opcode.getRealOpcode());
        }

        HashSet<Class> supportedConstantTypes = new HashSet<Class>();
        supportedConstantTypes.add(Integer.class);
        return new SupportedOperationsInfo(rz, supportedConstantTypes);
    }

    public static IGenericInstruction getSuitableInstruction(VMState context, VMStateDelta stateDelta, StackOperationAction action) throws NoSuitableInstructionFoundException {
        List<JavaOpcodes> suitableInstructions = new LinkedList<JavaOpcodes>();
        if (action == StackOperationAction.SATISFY_AT_LEAST_ONE) {
            for (JavaOpcodes op : supportedInstructions) {
                if (!context.getStackState().canPopAll(op.getExpectedStackTypes())) {
                    continue;
                }

                GenericOperand operand;
                try {
                    List<GenericOperand> operands = op.getOperandSelector().selectOparandsSatisfyRestrictions(op, context, stateDelta.getTarget(), stateDelta);
                    operand = !operands.isEmpty() ? operands.get(0) : null;
                } catch (NoSuitableOperandFoundException e) {
                    continue;
                }
                JVMInstruction instructionToEvaluate = new JVMInstruction(op, operand);

                VMState contextAfterInstruction = JavaBytecodeEvaluator.evaluateInstruction(context, instructionToEvaluate);
                VMStateDelta newStateDelta = VMStateDelta.create(contextAfterInstruction, (VMState) stateDelta.getTarget());
                if ((newStateDelta.getDelta() < stateDelta.getDelta())
                        || newStateDelta.getDelta() == 0) {
                    suitableInstructions.add(op);
                }
            }
            if (!suitableInstructions.isEmpty()) {
                return new JVMInstruction(CollectionUtils.selectRandomElement(suitableInstructions));
            } else {
                throw new NoSuitableInstructionFoundException();
            }
        }

        for (JavaOpcodes op : supportedInstructions) {
            try {
                if (context.getStackState().canPopAll(op.getExpectedStackTypes())) {
                    // ignoring return value, just checking that there will be no exception
                    op.getOperandSelector().selectOperandsRandom(op, context);
                    suitableInstructions.add(op);
                }
            } catch (NoSuitableOperandFoundException e) {
                // nothing, just skip op
            }
        }
        if (suitableInstructions.isEmpty()) {
            throw new NoSuitableInstructionFoundException();
        }

        return new JVMInstruction(CollectionUtils.selectRandomElement(suitableInstructions));
    }


    public static String getInstructionName(int instructionID) throws IllegalArgumentException, UnsupportedOperationException {
        switch (instructionID) {
            case Opcodes.NOP:
                return "NOP";
            case Opcodes.LDC:
                return "LDC";
            case Opcodes.ICONST_M1:
                return "ICONST_M1";
            case Opcodes.ICONST_0:
                return "ICONST_0";
            case Opcodes.ICONST_1:
                return "ICONST_1";
            case Opcodes.ICONST_2:
                return "ICONST_2";
            case Opcodes.POP:
                return "POP";
            case Opcodes.DUP:
                return "DUP";
            case Opcodes.SWAP:
                return "SWAP";
            case Opcodes.IADD:
                return "IADD";
            case Opcodes.ISUB:
                return "ISUB";
            case Opcodes.IMUL:
                return "IMUL";
            case Opcodes.IDIV:
                return "IDIV";
            case Opcodes.ILOAD:
                return "ILOAD";
            case Opcodes.ISTORE:
                return "ISTORE";
            case Opcodes.INEG:
                return "INEG";
            case Opcodes.ISHR:
                return "ISHR";
            case Opcodes.ISHL:
                return "ISHL";
            case Opcodes.IREM:
                return "IREM";
            case Opcodes.IOR:
                return "IOR";
            case Opcodes.IXOR:
                return "IXOR";
            case Opcodes.IAND:
                return "IAND";
            default:
                throw new IllegalArgumentException("No such instruction with ID " + instructionID + " in JavaSimpleOpcodeFactory");
        }
    }

    public static String getLanguageId() {
        return "JVM_0.7";
    }
}
