package ru.ifmo.gpo.java;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.UnsupportedInstructionException;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.instructions.generic.NumericConstant;
import ru.ifmo.gpo.java.instructions.JVMInstruction;
import ru.ifmo.gpo.java.instructions.JavaOpcodes;
import ru.ifmo.gpo.java.vmstate.JVMStateSchema;
import ru.ifmo.gpo.java.vmstate.VMState;
import ru.ifmo.gpo.java.vmstate.VerifyableOperandStackItem;
import ru.ifmo.gpo.verify.*;

/**
 * User: jedi-philosopher
 * Date: 25.09.2010
 * Time: 22:16:59
 */
public abstract class JavaBytecodeEvaluator {


    private static void applyBinaryOpcode(VMState state, JavaOpcodes opcode) {
        VerifyableOperandStackItem first = state.getStackState().applyPop(opcode.getExpectedStackTypes()[1]);
        VerifyableOperandStackItem second = state.getStackState().applyPop(opcode.getExpectedStackTypes()[0]);

        state.getStackState().applyPush(new VerifyableOperandStackItem(opcode.getAddedStackTypes()[0], new BinaryOperatorNode(second.getExpression(), first.getExpression(), opcode.getOperator())));
    }

    private static void applyUnaryOpcode(VMState state, JavaOpcodes opcode) {
        VerifyableOperandStackItem first = state.getStackState().applyPop(opcode.getExpectedStackTypes()[0]);
        state.getStackState().applyPush(new VerifyableOperandStackItem(opcode.getAddedStackTypes()[0], new UnaryOperatorNode(first.getExpression(), opcode.getOperator(), UnaryOperatorNode.OperatorType.TYPE_PREFIX)));
    }

    private static void applyConvertOpcode(VMState state, JavaOpcodes opcode) {
        // conversion bytecode do not influence expression trees, only change type
        VerifyableOperandStackItem first = state.getStackState().applyPop(opcode.getExpectedStackTypes()[0]);
        state.getStackState().applyPush(new VerifyableOperandStackItem(opcode.getAddedStackTypes()[0], first.getExpression()));
    }

    public static void updateSchema(JVMStateSchema schema, JVMInstruction instr) {
        switch (instr.getOpcode()) {
            case Opcodes.ILOAD:
                schema.addOrUpdateSlot(instr.getParameter(0), Type.INT_TYPE, GenericOperand.AccessType.READ);
                break;
            case Opcodes.ISTORE:
                schema.addOrUpdateSlot(instr.getParameter(0), Type.INT_TYPE, GenericOperand.AccessType.WRITE);
                break;
            case Opcodes.FLOAD:
                schema.addOrUpdateSlot(instr.getParameter(0), Type.FLOAT_TYPE, GenericOperand.AccessType.READ);
                break;
            case Opcodes.FSTORE:
                schema.addOrUpdateSlot(instr.getParameter(0), Type.FLOAT_TYPE, GenericOperand.AccessType.WRITE);
                break;
            case Opcodes.LSTORE:
                schema.addOrUpdateSlot(instr.getParameter(0), Type.LONG_TYPE, GenericOperand.AccessType.WRITE);
                break;
            case Opcodes.LLOAD:
                schema.addOrUpdateSlot(instr.getParameter(0), Type.LONG_TYPE, GenericOperand.AccessType.READ);
                break;
            case Opcodes.IINC:
                schema.addOrUpdateSlot(instr.getParameter(0), Type.INT_TYPE, GenericOperand.AccessType.ANY);
                break;
        }
    }

    public static VMState evaluateInstruction(VMState vmState, JVMInstruction instr) {
        VMState result = new VMState(vmState);
        switch (instr.getOpcode()) {
            case Opcodes.DUP: {
                VerifyableOperandStackItem item = result.getStackState().applyPop(Type.VOID_TYPE);
                final Type type = item.getType();
                result.getStackState().applyPush(new VerifyableOperandStackItem(type, item.getExpression()));
                result.getStackState().applyPush(new VerifyableOperandStackItem(type, item.getExpression()));
                break;
            }

            case Opcodes.LDC: {
                // TODO: constant type
                result.getStackState().applyPush(new VerifyableOperandStackItem(Type.INT_TYPE, new IntegerConstantTreeNode(instr.getIntParameter(0))));
                result.getConstants().addIntConstant(instr.getIntParameter(0));
                break;
            }
            case Opcodes.I2F:
            case Opcodes.F2I:
            case Opcodes.L2F:
            case Opcodes.F2L:
            case Opcodes.I2L:
            case Opcodes.L2I:
                applyConvertOpcode(result, instr.getOpcodeInternal());
                break;

            case Opcodes.INEG:
            case Opcodes.FNEG:
            case Opcodes.LNEG: {
                applyUnaryOpcode(result, instr.getOpcodeInternal());
                break;
            }
            case Opcodes.IINC: {
                final NumericConstant secondArg = (NumericConstant) instr.getParameters().get(1);
                Integer secondArgValue = (Integer) (secondArg.getValue());
                result.getConstants().addIntConstant(secondArgValue);
                IArithmeticTreeNode localVariableContents = result.getLocalVars().getValue(instr.getParameter(0), Type.INT_TYPE);
                result.getLocalVars().setValue(instr.getParameter(0), Type.INT_TYPE, new BinaryOperatorNode(localVariableContents, secondArg.getExpression(), GenericOperator.PLUS));
                break;
            }
            case Opcodes.BIPUSH: {
                result.getStackState().applyPush(new VerifyableOperandStackItem(Type.INT_TYPE, new NumericConstantTreeNode(Type.INT_TYPE, instr.getIntParameter(0))));
                result.getConstants().addIntConstant(instr.getIntParameter(0));
                break;
            }
            case Opcodes.ISHR:
            case Opcodes.ISHL:
                applyShiftOpcode(result, instr.getOpcodeInternal(), 5);
                break;
            case Opcodes.LSHR:
            case Opcodes.LSHL:
                applyShiftOpcode(result, instr.getOpcodeInternal(), 6);
                break;
            case Opcodes.IADD:
            case Opcodes.IDIV:
            case Opcodes.IMUL:
            case Opcodes.IREM:
            case Opcodes.ISUB:
            case Opcodes.IOR:
            case Opcodes.IXOR:
            case Opcodes.IAND:
            case Opcodes.FADD:
            case Opcodes.FDIV:
            case Opcodes.FMUL:
            case Opcodes.FREM:
            case Opcodes.FSUB:
            case Opcodes.LADD:
            case Opcodes.LDIV:
            case Opcodes.LMUL:
            case Opcodes.LREM:
            case Opcodes.LSUB:
            case Opcodes.LOR:
            case Opcodes.LXOR:
            case Opcodes.LAND:
                applyBinaryOpcode(result, instr.getOpcodeInternal());
                break;
            case Opcodes.ICONST_M1:
                result.getStackState().applyPush(new VerifyableOperandStackItem(Type.INT_TYPE, new IntegerConstantTreeNode(-1)));
                break;
            case Opcodes.ICONST_0:
                result.getStackState().applyPush(new VerifyableOperandStackItem(Type.INT_TYPE, new IntegerConstantTreeNode(0)));
                break;
            case Opcodes.ICONST_1:
                result.getStackState().applyPush(new VerifyableOperandStackItem(Type.INT_TYPE, new IntegerConstantTreeNode(1)));
                break;
            case Opcodes.ICONST_2:
                result.getStackState().applyPush(new VerifyableOperandStackItem(Type.INT_TYPE, new IntegerConstantTreeNode(2)));
                break;
            case Opcodes.ICONST_3:
                result.getStackState().applyPush(new VerifyableOperandStackItem(Type.INT_TYPE, new IntegerConstantTreeNode(3)));
                break;
            case Opcodes.ICONST_4:
                result.getStackState().applyPush(new VerifyableOperandStackItem(Type.INT_TYPE, new IntegerConstantTreeNode(4)));
                break;
            case Opcodes.ICONST_5:
                result.getStackState().applyPush(new VerifyableOperandStackItem(Type.INT_TYPE, new IntegerConstantTreeNode(5)));
                break;
            case Opcodes.FCONST_0:
                result.getStackState().applyPush(new VerifyableOperandStackItem(Type.FLOAT_TYPE, new NumericConstantTreeNode(Type.FLOAT_TYPE, 0.0f)));
                break;
            case Opcodes.FCONST_1:
                result.getStackState().applyPush(new VerifyableOperandStackItem(Type.FLOAT_TYPE, new NumericConstantTreeNode(Type.FLOAT_TYPE, 1.0f)));
                break;
            case Opcodes.FCONST_2:
                result.getStackState().applyPush(new VerifyableOperandStackItem(Type.FLOAT_TYPE, new NumericConstantTreeNode(Type.FLOAT_TYPE, 2.0f)));
                break;
            case Opcodes.LCONST_0:
                result.getStackState().applyPush(new VerifyableOperandStackItem(Type.LONG_TYPE, new NumericConstantTreeNode(Type.LONG_TYPE, 0L)));
                break;
            case Opcodes.LCONST_1:
                result.getStackState().applyPush(new VerifyableOperandStackItem(Type.LONG_TYPE, new NumericConstantTreeNode(Type.LONG_TYPE, 1L)));
                break;
            case Opcodes.ILOAD: {
                IArithmeticTreeNode localVariableContents = result.getLocalVars().getValue(instr.getParameter(0), Type.INT_TYPE);
                result.getStackState().applyPush(new VerifyableOperandStackItem(Type.INT_TYPE, localVariableContents));
                break;
            }
            case Opcodes.ISTORE: {
                VerifyableOperandStackItem item = result.getStackState().applyPop(Type.INT_TYPE);
                result.getLocalVars().setValue(instr.getParameter(0), Type.INT_TYPE, item.getExpression());
                break;
            }
            case Opcodes.FLOAD: {
                IArithmeticTreeNode localVariableContents = result.getLocalVars().getValue(instr.getParameter(0), Type.FLOAT_TYPE);
                result.getStackState().applyPush(new VerifyableOperandStackItem(Type.FLOAT_TYPE, localVariableContents));
                break;
            }
            case Opcodes.FSTORE: {
                VerifyableOperandStackItem item = result.getStackState().applyPop(Type.FLOAT_TYPE);
                result.getLocalVars().setValue(instr.getParameter(0), Type.FLOAT_TYPE, item.getExpression());
                break;
            }
            case Opcodes.LLOAD: {
                IArithmeticTreeNode localVariableContents = result.getLocalVars().getValue(instr.getParameter(0), Type.LONG_TYPE);
                result.getStackState().applyPush(new VerifyableOperandStackItem(Type.LONG_TYPE, localVariableContents));
                break;
            }
            case Opcodes.LSTORE: {
                VerifyableOperandStackItem item = result.getStackState().applyPop(Type.LONG_TYPE);
                result.getLocalVars().setValue(instr.getParameter(0), Type.LONG_TYPE, item.getExpression());
                break;
            }
            case Opcodes.SWAP: {

                VerifyableOperandStackItem val1 = result.getStackState().applyPop(Type.VOID_TYPE);
                VerifyableOperandStackItem val2 = result.getStackState().applyPop(Type.VOID_TYPE);

                result.getStackState().applyPush(val1);
                result.getStackState().applyPush(val2);
                break;
            }
            case Opcodes.NOP: {
                break;
            }
            case Opcodes.POP: {
                result.getStackState().applyPop(Type.VOID_TYPE);
                break;
            }
            default: {
                // All other bytecode are currently not supported
                throw new UnsupportedInstructionException(instr);
            }
        }
        return result;
    }

    // Shift operations require special handling, as they use only some bits of their second operand
    private static void applyShiftOpcode(VMState state, JavaOpcodes opcode, int significantBits) {

        VerifyableOperandStackItem first = state.getStackState().applyPop(opcode.getExpectedStackTypes()[1]);
        VerifyableOperandStackItem second = state.getStackState().applyPop(opcode.getExpectedStackTypes()[0]);

        IArithmeticTreeNode shiftByArg = new BinaryOperatorNode(first.getExpression(), new NumericConstantTreeNode(Type.INT_TYPE, (int) (Math.pow(2, significantBits) - 1)), GenericOperator.AND);

        state.getStackState().applyPush(new VerifyableOperandStackItem(opcode.getAddedStackTypes()[0], new BinaryOperatorNode(second.getExpression(), shiftByArg, opcode.getOperator())));

    }
}
