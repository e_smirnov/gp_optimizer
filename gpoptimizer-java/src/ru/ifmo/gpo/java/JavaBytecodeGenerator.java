package ru.ifmo.gpo.java;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.Type;
import org.objectweb.asm.util.CheckClassAdapter;
import ru.ifmo.gpo.core.ICodeGenerationResult;
import ru.ifmo.gpo.core.ICodeGenerator;
import ru.ifmo.gpo.core.Settings;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.vmstate.IStateSchema;
import ru.ifmo.gpo.java.codegen.*;
import ru.ifmo.gpo.java.vmstate.JVMStateSchema;
import ru.ifmo.gpo.java.vmstate.VerifyableOperandStackItem;
import ru.ifmo.gpo.util.DummyOutputStream;
import ru.ifmo.gpo.util.ObjectCache;

import java.io.*;
import java.util.List;


/**
 * Created by IntelliJ IDEA.
 * User: jedi-philosopher
 * Date: 01.12.2009
 * Time: 0:47:34
 * <p/>
 * Generates Java bytecode based on provided chromosome contents. Has two generation modes: sandboxed mode (@see JavaBytecodeGenerator..generateSandboxedCode)
 * and profile mode (@see JavaBytecodeGenerator.generateProfileCode).
 * First mode appends additional instruction to beginning and
 * end of chromosome code, which extract contents of operands stack. Result of exectution is a class with single method run(),
 * which parmeters are local variables used in instruction sequence and which has a stack of items where contents
 * of JVM operands stack are stored for futher analysis.
 * Second mode appends additional profiling instructions. run() method returns long, which specifies amount of nanoseconds it has been working.
 * <p/>
 * Attention: code generated for profile should not be cached! Otherwise, cached instances will be executed more times, and JIT will optimize them better,
 * making meaningless all profiling results
 */

public class JavaBytecodeGenerator implements ICodeGenerator {

    private static int _classNumber = 0;

    private static final int cacheMaxSize = 10;

    private static final int profileIterations = 10;

    private static ObjectCache<InstructionSequence, JavaCodeGenerationResult> sandboxedCache = new ObjectCache<InstructionSequence, JavaCodeGenerationResult>(cacheMaxSize);

    public JavaBytecodeGenerator() {
    }

    public static void cleanCache() {
        sandboxedCache.clear();
    }

    public static void dumpCode(String name, byte[] code) {
        OutputStream os = null;
        try {
            os = new FileOutputStream("generated/" + name + ".class");
            os.write(code);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    // ignore
                }
            }
        }
    }

    private int computeNumberOfSlotsForUnderflowStack(List<VerifyableOperandStackItem> stack) {
        int rz = 0;
        for (VerifyableOperandStackItem item : stack) {
            rz += item.getType().getSize();
        }
        return rz;
    }

    /**
     * Surrounds chromosome code with additional instructions to retrieve values
     * from operands stack. If chromosome contents looked like
     * <p/>
     * iload 1
     * iload 2
     * mul
     * <p/>
     * then after sandboxing will turn into class "test" with function
     * void run(int var1, int var2, LinkedList<Object> stack)
     * iload 1
     * iload 2
     * mul
     * // stack here will contain single integer
     * aload_2
     * swap // int value will be swapped with reference to List
     * invokestatic java/lang/Integer.valueOf(I)Ljava/lang/Integer // Performing boxing into Integer
     * invokevirtual java/util/LinkedList.add(Ljava/lang/Object;)Z // Storing Integer into List
     */
    @Override
    public ICodeGenerationResult generateSandboxedCode(InstructionSequence source, IStateSchema stateSchema, Object refAnalyzer) {
        JavaCodeGenerationResult cached = sandboxedCache.get(source);
        if (cached != null) {
            return cached;
        }

        JavaBytecodeAnalyzer analyzer = refAnalyzer == null ? new JavaBytecodeAnalyzer(source, (JVMStateSchema) stateSchema) : (JavaBytecodeAnalyzer) refAnalyzer;

        List<VerifyableOperandStackItem> underflowStack = analyzer.getNeededStackBefore();

        final int firstUnusedLocalVar = analyzer.getStateAfter().getLocalVars().getSchema().getFirstUnusedIdx();
        final int stackVarIdx = firstUnusedLocalVar + computeNumberOfSlotsForUnderflowStack(analyzer.getNeededStackBefore());

        PreparedClass resultClass = new PreparedClass("Gene_" + _classNumber);
        PreparedMethod sandboxedMethod = resultClass.createMethod("run");
        sandboxedMethod.addCodeFragment(new StackLoadCodeFragment(analyzer, firstUnusedLocalVar));
        sandboxedMethod.addCodeFragment(new ChromosomeCodeFragment(source, analyzer.getStateAfter().getLocalVars().getSchema()));
        sandboxedMethod.addCodeFragment(new StackContentSavingCodeFragment(analyzer, stackVarIdx));
        sandboxedMethod.addCodeFragment(new ReturnVoidCodeFragment());

        byte[] compiledCode = resultClass.getCode();

        verifyClass(compiledCode);

        JavaCodeGenerationResult rz = new JavaCodeGenerationResult("Gene_" + (_classNumber++), compiledCode, source, sandboxedMethod.getMethodDescriptor(), underflowStack);
        sandboxedCache.put(source, rz);
        return rz;
    }

    @Override
    public ICodeGenerationResult generateProfileCode(InstructionSequence sequence, IStateSchema stateSchema, Object refAnalyzer) {
        JavaBytecodeAnalyzer analyzer = refAnalyzer == null ? new JavaBytecodeAnalyzer(sequence, (JVMStateSchema) stateSchema) : (JavaBytecodeAnalyzer) refAnalyzer;

        final int firstUnusedVarIdx = analyzer.getStateAfter().getLocalVars().getSchema().getFirstUnusedIdx();
        final int stackVarIdx = firstUnusedVarIdx + computeNumberOfSlotsForUnderflowStack(analyzer.getNeededStackBefore());
        final int timeVarIdx = stackVarIdx + 1;
        final int loopVarIdx = timeVarIdx + 2; // long takes 2 registers
        final int profileVarIdx = loopVarIdx + 1;

        PreparedClass resultClass = new PreparedClass("GeneProfile_" + _classNumber);
        PreparedMethod profileMethod = resultClass.createMethod("run");


        LoopCodeFragment chromosomeLoop = new LoopCodeFragment(new CodeFragmentList(
                new StackLoadCodeFragment(analyzer, firstUnusedVarIdx),
                new ChromosomeCodeFragment(sequence, analyzer.getStateAfter().getLocalVars().getSchema()),
                new StackContentSavingCodeFragment(analyzer, stackVarIdx)
        ), profileIterations, loopVarIdx);


        ProfileCodeFragment profile = new ProfileCodeFragment(profileVarIdx, chromosomeLoop);
        profileMethod.addCodeFragment(profile);

        // adding stack extraction - so that JIT will not throw away entire method, as data generated by it is not used
        // doing this outside of chromosomeLoop, so that saving will not influence profiling results
        //LoopCodeFragment savingLoop = new LoopCodeFragment(new StackContentSavingCodeFragment(analyzer), profileIterations, loopVarIdx);
        //profileMethod.addCodeFragment(savingLoop);
        profileMethod.addCodeFragment(new ReturnVariableCodeFragment(Type.LONG_TYPE, profileVarIdx));

        byte[] compiledCode = resultClass.getCode();

        verifyClass(compiledCode);
        return new JavaCodeGenerationResult("GeneProfile_" + (_classNumber++), compiledCode, sequence, profileMethod.getMethodDescriptor(), analyzer.getNeededStackBefore());
    }

    private void verifyClass(byte[] compiledClass) {
        if (!Settings.verifyGeneratedClasses()) {
            return;
        }
        CheckClassAdapter.verify(new ClassReader(compiledClass), false, new PrintWriter(new DummyOutputStream()));
    }

}
