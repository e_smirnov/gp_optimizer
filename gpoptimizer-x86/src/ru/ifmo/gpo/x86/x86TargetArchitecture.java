package ru.ifmo.gpo.x86;

import ru.ifmo.gpo.core.*;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.IGenericInstruction;
import ru.ifmo.gpo.core.vmstate.IInstructionSequenceAnalyzer;
import ru.ifmo.gpo.core.vmstate.IMachineState;
import ru.ifmo.gpo.core.vmstate.IStateDelta;
import ru.ifmo.gpo.core.vmstate.IStateSchema;
import ru.ifmo.gpo.x86.state.InstructionEvaluator;
import ru.ifmo.gpo.x86.state.StateAnalyzer;
import ru.ifmo.gpo.x86.state.x86ProcessorState;
import ru.ifmo.gpo.x86.state.x86StateDelta;

/**
 * User: e_smirnov
 * Date: 20.01.2011
 * Time: 17:55:50
 */
public class x86TargetArchitecture implements ITargetArchitecture {

    private ICodeGenerator codeGenerator = new x86CodeGenerator();
    private ICodeExecutor codeExecutor = new x86CodeExecutor();
    private IInstructionSequenceGenerator<x86ProcessorState, x86StateDelta> sequenceGenerator = new InstructionSequenceGenerator(this);

    private final static String id = "x86";
    private static final long serialVersionUID = 4652071140524467072L;

    static {
        SupportedArchitectures.register(id, x86TargetArchitecture.class);
    }

    @Override
    public InstructionSequence preprocessSource(InstructionSequence source) {
        return source;
    }

    @Override
    public IInstructionSequenceAnalyzer analyzeCode(InstructionSequence code, IStateSchema stateSchema) {
        return new StateAnalyzer(code);
    }

    @Override
    public IMachineState cloneState(IMachineState state) {
        if (!(state instanceof x86ProcessorState)) {
            throw new IllegalArgumentException();
        }
        return new x86ProcessorState((x86ProcessorState) state);
    }

    @Override
    public IStateDelta getStateDelta(IMachineState first, IMachineState second) {
        if (!(first instanceof x86ProcessorState && second instanceof x86ProcessorState)) {
            throw new IllegalArgumentException();
        }
        return new x86StateDelta(first, second);
    }

    @Override
    public boolean isDeltaZero(IMachineState first, IMachineState second) {
        return getStateDelta(first, second).getDelta() == 0;
    }

    @Override
    public IStateSchema buildStateSchema(InstructionSequence code) {
        // currently not supported as state schema is built into x86ProcessorState, should be fixed later
        return null;
    }

    @Override
    public IMachineState createInitialState(IStateSchema schema) {
        throw new UnsupportedOperationException();
    }

    @Override
    public IMachineState evaluateInstruction(IMachineState sourceState, IGenericInstruction insn) {
        return InstructionEvaluator.evaluateInstruction((x86ProcessorState) sourceState, (x86Instruction) insn, null);
    }

    @Override
    public ICodeGenerator getCodeGenerator() {
        return codeGenerator;
    }

    @Override
    public ICodeExecutor getCodeExecutor() {
        return codeExecutor;
    }

    @Override
    public IInstructionSequenceGenerator<x86ProcessorState, x86StateDelta> getInstructionSequenceGenerator() {
        return sequenceGenerator;
    }

    @Override
    public IGenericInstruction cloneInstruction(IGenericInstruction instruction) {
        return new x86Instruction(instruction.getOpcode(), instruction.getParameters());
    }

    @Override
    public IGenericInstruction createNOPInstruction() {
        return new x86Instruction(x86Opcodes.NOP);
    }

    @Override
    public boolean isInstructionSequenceValid(InstructionSequence seq) {
        return true;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void configurationCreated(GPConfiguration gpConfiguration) {
        // nothing
    }
}
