package ru.ifmo.gpo.x86;

import ru.ifmo.gpo.core.BaseInstructionSequenceGenerator;
import ru.ifmo.gpo.core.ITargetArchitecture;
import ru.ifmo.gpo.core.instructions.IOperandSelector;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.SequenceGenerationFailedException;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.vmstate.FailedToSatisfyRestrictionsException;
import ru.ifmo.gpo.core.vmstate.NoSuitableOperandFoundException;
import ru.ifmo.gpo.util.CollectionUtils;
import ru.ifmo.gpo.x86.state.x86ProcessorState;
import ru.ifmo.gpo.x86.state.x86StateDelta;

import java.util.LinkedList;
import java.util.List;

/**
 * User: jedi-philosopher
 * Date: 25.01.2011
 * Time: 21:21:51
 */
public class InstructionSequenceGenerator extends BaseInstructionSequenceGenerator<x86ProcessorState, x86StateDelta> {

    public InstructionSequenceGenerator(ITargetArchitecture target) {
        super(target);
    }

    @Override
    protected x86Instruction selectAnyInstruction(InstructionSequence alreadyGenerated, x86ProcessorState state) throws FailedToSatisfyRestrictionsException {
        List<x86Instruction> candidates = new LinkedList<x86Instruction>();
        List<GenericOperand> operands = new LinkedList<GenericOperand>();
        for (x86OpcodeWrapper op : x86OpcodeWrapper.values()) {
            try {
                operands.clear();
                IOperandSelector selector = op.getOperandSelector();
                if (selector != null) {
                    operands.addAll(selector.selectOperandsRandom(op, state));
                }
                candidates.add(new x86Instruction(op.getOpcode(), operands));
            } catch (NoSuitableOperandFoundException e) {
                // do nothing
            }
        }

        if (candidates.isEmpty()) {
            throw new FailedToSatisfyRestrictionsException();
        }

        return CollectionUtils.selectRandomElement(candidates);
    }

    @Override
    protected double computeBreakProbability(int resultSize, int desiredLength, int stateDelta) {
        return (resultSize / (double) desiredLength - 0.5) * (1.0 / (1.0 + stateDelta));
    }

    @Override
    protected x86Instruction selectInstructionSatisfyRestrictions(InstructionSequence alreadyGenerated, x86ProcessorState state, x86StateDelta desiredDelta) throws FailedToSatisfyRestrictionsException {
        List<x86Instruction> candidates = new LinkedList<x86Instruction>();
        List<GenericOperand> operands = new LinkedList<GenericOperand>();
        for (x86OpcodeWrapper op : x86OpcodeWrapper.values()) {
            try {
                operands.clear();
                IOperandSelector selector = op.getOperandSelector();
                if (selector != null) {
                    operands.addAll(selector.selectOparandsSatisfyRestrictions(op, state, desiredDelta.getTarget(), desiredDelta));
                }
                candidates.add(new x86Instruction(op.getOpcode(), operands));
            } catch (NoSuitableOperandFoundException e) {
                continue;
            }
        }

        if (candidates.isEmpty()) {
            throw new FailedToSatisfyRestrictionsException();
        }

        return CollectionUtils.selectRandomElement(candidates);
    }

    public InstructionSequence generateInstructionSequence(x86ProcessorState sourceState, x86ProcessorState targetState, int length) throws SequenceGenerationFailedException {
        return generateInstructionSequence(sourceState, new x86StateDelta(sourceState, targetState), length);
    }
}
