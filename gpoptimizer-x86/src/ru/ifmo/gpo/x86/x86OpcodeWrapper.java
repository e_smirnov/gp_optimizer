package ru.ifmo.gpo.x86;

import ru.ifmo.gpo.core.instructions.IOperandSelector;
import ru.ifmo.gpo.x86.state.selectors.DivOperandSelector;
import ru.ifmo.gpo.x86.state.selectors.SingleRegisterSelector;
import ru.ifmo.gpo.x86.state.selectors.TwoAreaSelector;
import ru.ifmo.gpo.x86.state.x86ProcessorState;
import ru.ifmo.gpo.x86.state.x86StateDelta;

/**
 * User: jedi-philosopher
 * Date: 26.01.2011
 * Time: 17:07:15
 */
public enum x86OpcodeWrapper {            //todo: single instance of each selector
    ADD(x86Opcodes.ADD, new TwoAreaSelector()),
    SUB(x86Opcodes.SUB, new TwoAreaSelector()),
    MUL(x86Opcodes.MUL, new DivOperandSelector()),
    DIV(x86Opcodes.DIV, new DivOperandSelector()),
    AND(x86Opcodes.AND, new TwoAreaSelector()),
    OR(x86Opcodes.OR, new TwoAreaSelector()),
    XOR(x86Opcodes.XOR, new TwoAreaSelector()),
    NEG(x86Opcodes.NEG, new SingleRegisterSelector()),
    NOT(x86Opcodes.NOT, new SingleRegisterSelector()),
    SHR(x86Opcodes.SHR, new SingleRegisterSelector()),
    SHL(x86Opcodes.SHL, new SingleRegisterSelector()),
    INC(x86Opcodes.INC, new SingleRegisterSelector()),
    DEC(x86Opcodes.DEC, new SingleRegisterSelector()),
    MOV(x86Opcodes.MOV, new TwoAreaSelector()),
    NOP(x86Opcodes.NOP);

    x86OpcodeWrapper(int opcode, IOperandSelector<x86OpcodeWrapper, x86ProcessorState, x86StateDelta> operandSelector) {
        this.opcode = opcode;
        this.operandSelector = operandSelector;
    }

    x86OpcodeWrapper(int opcode) {
        this.opcode = opcode;
    }

    public int getOpcode() {
        return opcode;
    }

    public IOperandSelector<x86OpcodeWrapper, x86ProcessorState, x86StateDelta> getOperandSelector() {
        return operandSelector;
    }

    private int opcode;
    private IOperandSelector<x86OpcodeWrapper, x86ProcessorState, x86StateDelta> operandSelector;
}
