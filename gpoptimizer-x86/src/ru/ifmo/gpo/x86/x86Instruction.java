package ru.ifmo.gpo.x86;

import ru.ifmo.gpo.core.instructions.generic.BaseGenericInstruction;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;

import java.util.Collection;

/**
 * User: jedi-philosopher
 * Date: 05.01.2011
 * Time: 19:21:36
 */
public class x86Instruction extends BaseGenericInstruction {

    public x86Instruction(int opcode, GenericOperand... params) {
        super(opcode, params);
    }

    public x86Instruction(int opcode, Collection<GenericOperand> params) {
        super(opcode, params);
    }
}
