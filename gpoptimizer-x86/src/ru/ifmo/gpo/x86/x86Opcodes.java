package ru.ifmo.gpo.x86;

/**
 * User: jedi-philosopher
 * Date: 05.01.2011
 * Time: 19:12:50
 */
public interface x86Opcodes {
    int ADD = 0;
    int SUB = 1;
    int MUL = 2;
    int DIV = 3;
    int AND = 4;
    int OR = 5;
    int XOR = 6;
    int NEG = 7;
    int NOT = 8;
    int SHR = 9;
    int SHL = 10;
    int INC = 11;
    int DEC = 12;
    int MOV = 13;
    int NOP = 14;
    int RDTSC = 15;

    // register names
    String eax = "EAX";
    String ebx = "EBX";
    String ecx = "ECX";
    String edx = "EDX";
    String esi = "ESI";
    String edi = "EDI";
    String esp = "ESP";
    String ebp = "EBP";
}
