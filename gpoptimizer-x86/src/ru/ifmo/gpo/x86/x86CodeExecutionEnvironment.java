package ru.ifmo.gpo.x86;

import ru.ifmo.gpo.core.ICodeExecutionEnvironment;
import ru.ifmo.gpo.util.IDisposable;

/**
 * User: jedi-philosopher
 * Date: 13.01.2011
 * Time: 23:18:02
 */
public class x86CodeExecutionEnvironment implements ICodeExecutionEnvironment, IDisposable {

    private int handle;

    public x86CodeExecutionEnvironment(int handle) {
        this.handle = handle;
    }

    public int getHandle() {
        return handle;
    }

    @Override
    protected void finalize() throws Throwable {
        dispose();
        super.finalize();
    }

    @Override
    public native void setToZero() throws IllegalStateException;

    @Override
    public native void setToFF() throws IllegalStateException;

    @Override
    public native void setRandomValues() throws IllegalStateException;

    @Override
    public native void dispose();
}
