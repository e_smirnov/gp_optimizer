package ru.ifmo.gpo.x86;

/**
 * User: jedi-philosopher
 * Date: 05.02.2011
 * Time: 15:50:47
 * <p/>
 * Special exception that is thrown by JNI code
 */
public class NativeCodeException extends RuntimeException {
    private static final long serialVersionUID = -3745642994878439220L;

    public NativeCodeException() {
    }

    public NativeCodeException(String message) {
        super("Exception has happened in native code: " + message);
    }
}
