package ru.ifmo.gpo.x86;

import ru.ifmo.gpo.core.IExecutionResult;
import ru.ifmo.gpo.util.IDisposable;

import java.math.BigDecimal;

/**
 * User: jedi-philosopher
 * Date: 29.01.2011
 * Time: 19:05:16
 */
public class x86ExecutionResult implements IExecutionResult, IDisposable {
    private int handle;

    public x86ExecutionResult(int handle) {
        this.handle = handle;
    }

    public x86ExecutionResult(String param) {
        System.out.println("Wtf?");
    }

    public int getHandle() {
        return handle;
    }

    @Override
    public BigDecimal computeDifference(IExecutionResult reference) {
        return BigDecimal.valueOf(computeDifference0(reference));
    }

    @Override
    public BigDecimal maxPossibleDifference() {
        return BigDecimal.valueOf(maxPossibleDifference0());
    }

    @Override
    public Exception getException() {
        return null;
    }

    @Override
    public native void dispose();

    private native double computeDifference0(IExecutionResult reference);

    private native double maxPossibleDifference0();

    /**
     * Used for testing. Returns length of result vector for this execution (each ouput register/memory value is considered
     * to be one of coordinates of n-dimensional vector)
     *
     * @return Result vector length
     */
    public native double getExecutionResultVectorLength();
}
