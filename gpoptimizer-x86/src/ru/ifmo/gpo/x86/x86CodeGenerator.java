package ru.ifmo.gpo.x86;

import ru.ifmo.gpo.core.ICodeGenerationResult;
import ru.ifmo.gpo.core.ICodeGenerator;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.instructions.generic.IGenericInstruction;
import ru.ifmo.gpo.core.vmstate.IMachineState;
import ru.ifmo.gpo.core.vmstate.IStateSchema;
import ru.ifmo.gpo.x86.softwire.x86;
import ru.ifmo.gpo.x86.state.StateAnalyzer;
import ru.ifmo.gpo.x86.state.x86MemoryOperand;

import java.util.List;

/**
 * User: jedi-philosopher
 * Date: 04.01.2011
 * Time: 17:59:01
 */
public class x86CodeGenerator implements ICodeGenerator {

    private static int generatedClassIdx = 0;

    @Override
    public ICodeGenerationResult generateProfileCode(InstructionSequence chromosome, IStateSchema stateSchema, Object additionalData) {
        StateAnalyzer stateAnalyzer = new StateAnalyzer(chromosome);
        IMachineState initialState = stateAnalyzer.getStateBefore();

        x86 generator = x86.create();
        fillRegisters(initialState, generator);

        applyChromosomeCode(chromosome, generator);

        // not restoring registers as we are not interested in their values

        x86CodeGenerationResult result = new x86CodeGenerationResult(chromosome, "profiled_" + (++generatedClassIdx), generator.generateCode());
        x86.destroy(generator);
        return result;
    }

    @Override
    public ICodeGenerationResult generateSandboxedCode(InstructionSequence chromosome, IStateSchema stateSchema, Object additionalData) {
        StateAnalyzer stateAnalyzer = new StateAnalyzer(chromosome);
        IMachineState initialState = stateAnalyzer.getStateBefore();

        x86 generator = x86.create();
        fillRegisters(initialState, generator);

        applyChromosomeCode(chromosome, generator);

        saveRegisters(stateAnalyzer.getStateAfter(), generator);

        x86CodeGenerationResult result = new x86CodeGenerationResult(chromosome, "sandboxed_" + (++generatedClassIdx), generator.generateCode());
        x86.destroy(generator);
        return result;
    }

    private void saveRegisters(IMachineState state, x86 generator) {
        for (GenericOperand registerOperand : state.getRegisters().values()) {
            if (registerOperand.isWriteable() && registerOperand.hasValue()) {
                generator.applyBinaryInsn(x86Opcodes.MOV, new x86MemoryOperand(registerOperand.getId() + "_output", 4), registerOperand);
            }
        }
    }

    private void fillRegisters(IMachineState initialState, x86 generator) {
        // we must fill registers
        for (GenericOperand registerOperand : initialState.getRegisters().values()) {
            if (!registerOperand.hasValue()) {
                continue;
            }
            generator.applyBinaryInsn(x86Opcodes.MOV, registerOperand, new x86MemoryOperand(registerOperand.getId() + "_source", 4));
        }
    }

    private void applyChromosomeCode(InstructionSequence chromosome, x86 generator) {
        for (IGenericInstruction instr : chromosome) {
            final List<GenericOperand> parameters = instr.getParameters();
            if (parameters.size() == 2) {
                generator.applyBinaryInsn(instr.getOpcode(), parameters.get(0), parameters.get(1));
            } else if (parameters.size() == 1) {
                generator.applyUnaryInsn(instr.getOpcode(), parameters.get(0));
            } else {
                generator.applyInsn(instr.getOpcode());
            }
        }
    }

}
