package ru.ifmo.gpo.x86;

import ru.ifmo.gpo.core.BaseCodeGenerationResult;
import ru.ifmo.gpo.core.ICodeExecutionEnvironment;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.util.IDisposable;

/**
 * User: jedi-philosopher
 * Date: 04.01.2011
 * Time: 18:00:40
 */
public class x86CodeGenerationResult extends BaseCodeGenerationResult implements IDisposable {

    /**
     * Handle for native resources
     */
    private int handle;

    public x86CodeGenerationResult(InstructionSequence source, String name, int handle) {
        super(name, source);
        this.handle = handle;
    }

    public int getHandle() {
        return handle;
    }

    @Override
    public ICodeExecutionEnvironment createExecutionEnvironment() {
        return new x86CodeExecutionEnvironment(createExecutionEnvironment0());
    }

    private native int createExecutionEnvironment0();

    @Override
    public native void dumpOnDisc();

    @Override
    protected void finalize() throws Throwable {
        dispose();
        super.finalize();
    }

    @Override
    public native void dispose();
}
