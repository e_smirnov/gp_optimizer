package ru.ifmo.gpo.x86.state;

import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.instructions.UnsupportedOpcodeException;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.verify.*;
import ru.ifmo.gpo.x86.x86Instruction;
import ru.ifmo.gpo.x86.x86Opcodes;

import java.util.Set;

/**
 * User: e_smirnov
 * Date: 19.01.2011
 * Time: 15:57:36
 */
public abstract class InstructionEvaluator {

    private static void applyArithInstruction(x86ProcessorState state, GenericOperand firstParam, GenericOperand secondParam, GenericOperator operator, Set<GenericOperand> newValues) {
        if (!(secondParam instanceof x86ConstantOperand) && !state.hasValue(secondParam) && newValues != null) {
            newValues.add(secondParam);
        }
        if (!state.hasValue(firstParam) && newValues != null) {
            newValues.add(firstParam);
        }
        state.putValue(firstParam, new BinaryOperatorNode(state.getValue(firstParam), state.getValue(secondParam), operator));
    }

    private static void applyUnaryArithInstruction(x86ProcessorState state, GenericOperand target, GenericOperator operator, Set<GenericOperand> newValues) {
        if (!(target instanceof x86ConstantOperand) && !state.hasValue(target) && newValues != null) {
            newValues.add(target);
        }
        state.putValue(target, new UnaryOperatorNode(state.getValue(target), operator, UnaryOperatorNode.OperatorType.TYPE_PREFIX));
    }

    public static x86ProcessorState evaluateInstruction(x86ProcessorState state, x86Instruction instr, Set<GenericOperand> newValues) {
        x86ProcessorState result = new x86ProcessorState(state);
        final GenericOperand input = !instr.getParameters().isEmpty() ? instr.getParameters().get(0) : null;
        switch (instr.getOpcode()) {
            case x86Opcodes.ADD:
                applyArithInstruction(result, input, instr.getParameters().get(1), GenericOperator.PLUS, newValues);
                break;
            case x86Opcodes.SUB:
                applyArithInstruction(result, input, instr.getParameters().get(1), GenericOperator.MINUS, newValues);
                break;
            case x86Opcodes.MUL: {
                if (!(input instanceof x86ConstantOperand) && !state.hasValue(input) && newValues != null) {
                    newValues.add(input);
                }
                GenericOperand eax = new x86Register(x86Opcodes.eax);
                GenericOperand edx = new x86Register(x86Opcodes.edx);
                final IArithmeticTreeNode eaxCasted = new TypeCastNode(state.getValue(eax), Type.LONG_TYPE);  // must cast to long both registers to correctly verify multiplication
                final IArithmeticTreeNode inputCasted = new TypeCastNode(state.getValue(input), Type.LONG_TYPE);
                final ValuePairNode varPair = new ValuePairNode(new BinaryOperatorNode(eaxCasted, inputCasted, GenericOperator.MUL));
                state.putValue(eax, varPair.getLowPart());
                state.putValue(edx, varPair.getHighPart());
                break;
            }
            case x86Opcodes.DIV: {
                if (!(input instanceof x86ConstantOperand) && !state.hasValue(input) && newValues != null) {
                    newValues.add(input);
                }
                GenericOperand eax = new x86Register(x86Opcodes.eax);
                GenericOperand edx = new x86Register(x86Opcodes.edx);
                ValuePairNode valToDivide = new ValuePairNode(state.getValue(edx), state.getValue(eax));
                state.putValue(eax, new BinaryOperatorNode(valToDivide.getFullValue(), state.getValue(input), GenericOperator.DIV));
                state.putValue(edx, new BinaryOperatorNode(valToDivide.getFullValue(), state.getValue(input), GenericOperator.REM));
                break;
            }
            case x86Opcodes.AND:
                applyArithInstruction(result, input, instr.getParameters().get(1), GenericOperator.AND, newValues);
                break;
            case x86Opcodes.OR:
                applyArithInstruction(result, input, instr.getParameters().get(1), GenericOperator.OR, newValues);
                break;
            case x86Opcodes.XOR:
                applyArithInstruction(result, input, instr.getParameters().get(1), GenericOperator.XOR, newValues);
                break;
            case x86Opcodes.NEG:
                applyUnaryArithInstruction(result, input, GenericOperator.NEG, newValues);
                break;
            case x86Opcodes.NOT:
                applyUnaryArithInstruction(result, input, GenericOperator.NOT, newValues);
                break;
            case x86Opcodes.SHR:
                applyUnaryArithInstruction(result, input, GenericOperator.SHR, newValues);
                break;
            case x86Opcodes.SHL:
                applyUnaryArithInstruction(result, input, GenericOperator.SHL, newValues);
                break;
            case x86Opcodes.INC: {
                final IArithmeticTreeNode node = new BinaryOperatorNode(state.getValue(input), new NumericConstantTreeNode(Type.INT_TYPE, 1), GenericOperator.PLUS);
                result.putValue(input, node);
                break;
            }
            case x86Opcodes.DEC: {
                final IArithmeticTreeNode node = new BinaryOperatorNode(state.getValue(input), new NumericConstantTreeNode(Type.INT_TYPE, 1), GenericOperator.MINUS);
                result.putValue(input, node);
                break;
            }
            case x86Opcodes.MOV: {
                GenericOperand source = instr.getParameters().get(1);
                if (!result.hasValue(source) && !(source instanceof x86ConstantOperand)) {
                    newValues.add(source);
                }
                result.putValue(input, result.getValue(source));
                break;
            }
            case x86Opcodes.NOP:
                break;
            default:
                throw new UnsupportedOpcodeException(instr.getOpcode());
        }

        return result;
    }
}
