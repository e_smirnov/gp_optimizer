package ru.ifmo.gpo.x86.state;

import ru.ifmo.gpo.core.instructions.generic.GenericOperand;

public class x86MemoryOperand extends GenericOperand {
    public String id; // public to simplify acces from C++ code via JNI
    public int size;
    private static final long serialVersionUID = -6363927938189518694L;

    public x86MemoryOperand(String id, int size) {
        this.id = id;
        this.size = size;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return id;
    }
}
