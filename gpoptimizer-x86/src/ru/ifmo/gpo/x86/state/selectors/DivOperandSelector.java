package ru.ifmo.gpo.x86.state.selectors;

import ru.ifmo.gpo.core.instructions.IOperandSelector;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.vmstate.NoSuitableOperandFoundException;
import ru.ifmo.gpo.util.CollectionUtils;
import ru.ifmo.gpo.x86.state.x86ConstantOperand;
import ru.ifmo.gpo.x86.state.x86ProcessorState;
import ru.ifmo.gpo.x86.state.x86StateDelta;
import ru.ifmo.gpo.x86.x86OpcodeWrapper;
import ru.ifmo.gpo.x86.x86Opcodes;

import java.util.Collection;
import java.util.List;

/**
 * User: e_smirnov
 * Date: 09.02.2011
 * Time: 16:11:25
 * <p/>
 * DIV and MUL instructions have first implicit argument - eax. They div/mul eax by their argument and put result to eax:edx
 */
public class DivOperandSelector implements IOperandSelector<x86OpcodeWrapper, x86ProcessorState, x86StateDelta> {

    private void checkEaxEdx(int opcode, x86ProcessorState state) throws NoSuitableOperandFoundException {
        GenericOperand eax = state.getRegisters().get(x86Opcodes.eax);
        if (eax == null || !eax.isReadable() || !eax.isWriteable()) {
            throw new NoSuitableOperandFoundException(opcode);
        }

        GenericOperand ebx = state.getRegisters().get(x86Opcodes.ebx);
        if (ebx == null || !ebx.isReadable() || !ebx.isWriteable()) {
            throw new NoSuitableOperandFoundException(opcode);
        }
    }

    private GenericOperand selectConstant(x86ProcessorState state) {
        return new x86ConstantOperand(state.getConstants().getIntConstant());
    }

    private GenericOperand selectRegister(x86ProcessorState state) throws NoSuitableOperandFoundException {
        List<GenericOperand> readableRegisters = GenericOperand.filterReadable((Collection<GenericOperand>) state.getRegisters().values());
        if (readableRegisters.isEmpty()) {
            return null;
        }
        return CollectionUtils.selectRandomElement(readableRegisters);
    }

    @Override
    public List<GenericOperand> selectOperandsRandom(x86OpcodeWrapper opcode, x86ProcessorState state) throws NoSuitableOperandFoundException {
        checkEaxEdx(opcode.getOpcode(), state);
        if (Math.random() > 0.5) {
            return CollectionUtils.toList(selectConstant(state));
        } else {
            GenericOperand rz = selectRegister(state);
            if (rz == null) {
                rz = selectConstant(state);
            }
            return CollectionUtils.toList(rz);
        }
    }

    @Override
    public List<GenericOperand> selectOparandsSatisfyRestrictions(x86OpcodeWrapper opcode, x86ProcessorState stateBefore, x86ProcessorState desiredState, x86StateDelta diffWithDesiredState) throws NoSuitableOperandFoundException {
        checkEaxEdx(opcode.getOpcode(), stateBefore);
        boolean eaxFound = false;
        boolean edxFound = false;
        for (GenericOperand op : diffWithDesiredState.getRegisterContents()) {
            if (op.getId().equals(x86Opcodes.eax)) {
                eaxFound = true;
            } else if (op.getId().equals(x86Opcodes.edx)) {
                edxFound = true;
            }
        }

        if (!eaxFound || !edxFound) {
            throw new NoSuitableOperandFoundException(opcode.getOpcode());
        }
        return selectOperandsRandom(opcode, stateBefore);
    }

    @Override
    public List<List<GenericOperand>> getAllPossibleOperandSets(
            x86OpcodeWrapper opcode, x86ProcessorState state)
            throws NoSuitableOperandFoundException {
        throw new UnsupportedOperationException();
    }
}
