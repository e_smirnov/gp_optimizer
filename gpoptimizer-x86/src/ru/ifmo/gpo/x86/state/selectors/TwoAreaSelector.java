package ru.ifmo.gpo.x86.state.selectors;

import ru.ifmo.gpo.core.instructions.IOperandSelector;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.vmstate.NoSuitableOperandFoundException;
import ru.ifmo.gpo.util.CollectionUtils;
import ru.ifmo.gpo.x86.state.x86ConstantOperand;
import ru.ifmo.gpo.x86.state.x86MemoryOperand;
import ru.ifmo.gpo.x86.state.x86ProcessorState;
import ru.ifmo.gpo.x86.state.x86StateDelta;
import ru.ifmo.gpo.x86.x86OpcodeWrapper;

import java.util.*;

/**
 * User: e_smirnov
 * Date: 01.02.2011
 * Time: 15:36:56
 * <p/>
 * 2 arguments, first either register or memory, second reg, mem or constant. can not select both memory operands
 */
public class TwoAreaSelector implements IOperandSelector<x86OpcodeWrapper, x86ProcessorState, x86StateDelta> {


    enum OperandType {
        Reg,
        Mem,
        Imm
    }

    private GenericOperand selectWriteable(Collection<? extends GenericOperand> coll) {
        List<GenericOperand> candidates = new LinkedList<GenericOperand>();
        for (GenericOperand op : coll) {
            if (op.isWriteable()) {
                candidates.add(op);
            }
        }
        return CollectionUtils.selectRandomElement(candidates);
    }

    private GenericOperand selectReadable(Collection<? extends GenericOperand> coll) {
        List<GenericOperand> candidates = new LinkedList<GenericOperand>();
        for (GenericOperand op : coll) {
            if (op.isReadable()) {
                candidates.add(op);
            }
        }
        return CollectionUtils.selectRandomElement(candidates);
    }

    private GenericOperand select(x86ProcessorState state, GenericOperand.AccessType access, OperandType... types) {
        List<OperandType> ts = new ArrayList<OperandType>();
        Collections.addAll(ts, types);
        Collections.shuffle(ts);

        for (OperandType t : ts) {
            if (t == OperandType.Reg) {
                GenericOperand rz = (access == GenericOperand.AccessType.WRITE) ? selectWriteable(state.getRegisters().values()) : selectReadable(state.getRegisters().values());
                if (rz != null) {
                    return rz;
                } else {
                    continue;
                }
            }

            if (t == OperandType.Mem) {
                GenericOperand rz = (access == GenericOperand.AccessType.WRITE) ? selectWriteable(state.getMemory().values()) : selectReadable(state.getMemory().values());
                if (rz != null) {
                    return rz;
                } else {
                    continue;
                }
            }

            if (t == OperandType.Imm) {
                return new x86ConstantOperand(state.getConstants().getIntConstant());
            }
        }
        return null;
    }

    private GenericOperand selectSatisfy(x86StateDelta stateDelta, GenericOperand.AccessType access, OperandType... types) {
        OperandType t = CollectionUtils.selectRandomElement(types);
        if (t == OperandType.Reg) {
            return access == GenericOperand.AccessType.WRITE ? selectWriteable(stateDelta.getRegisterContents()) : selectReadable(stateDelta.getRegisterContents());
        } else if (t == OperandType.Mem) {
            return access == GenericOperand.AccessType.WRITE ? selectWriteable(stateDelta.getMemoryWrites()) : selectReadable(stateDelta.getMemoryWrites());
        } else if (t == OperandType.Imm) {
            return new x86ConstantOperand(stateDelta.getTarget().getConstants().getIntConstant());
        } else throw new IllegalArgumentException();
    }

    @Override
    public List<GenericOperand> selectOperandsRandom(x86OpcodeWrapper opcode, x86ProcessorState state) throws NoSuitableOperandFoundException {
        List<GenericOperand> result = new LinkedList<GenericOperand>();
        GenericOperand first, second;

        final double randVal = Math.random();
        if (randVal > 0.5) {
            second = select(state, GenericOperand.AccessType.READ, OperandType.Reg, OperandType.Mem, OperandType.Imm);
            first = (second instanceof x86MemoryOperand) ? select(state, GenericOperand.AccessType.WRITE, OperandType.Reg) : select(state, GenericOperand.AccessType.WRITE, OperandType.Reg, OperandType.Mem);

        } else {
            first = select(state, GenericOperand.AccessType.WRITE, OperandType.Reg, OperandType.Mem);
            second = (first instanceof x86MemoryOperand) ? select(state, GenericOperand.AccessType.READ, OperandType.Reg, OperandType.Imm) : select(state, GenericOperand.AccessType.READ, OperandType.Reg, OperandType.Mem, OperandType.Imm);

        }
        if ((first == null) || (second == null)) {
            throw new NoSuitableOperandFoundException(opcode.getOpcode());
        }
        result.add(first);
        result.add(second);
        return result;
    }

    @Override
    public List<GenericOperand> selectOparandsSatisfyRestrictions(x86OpcodeWrapper opcode, x86ProcessorState stateBefore, x86ProcessorState desiredState, x86StateDelta diffWithDesiredState) throws NoSuitableOperandFoundException {
        List<GenericOperand> result = new LinkedList<GenericOperand>();
        GenericOperand first, second;

        final double randVal = Math.random();
        if (randVal > 0.5) {
            second = selectSatisfy(diffWithDesiredState, GenericOperand.AccessType.READ, OperandType.Reg, OperandType.Mem/*, OperandType.Imm*/);
            first = (second instanceof x86MemoryOperand) ? selectSatisfy(diffWithDesiredState, GenericOperand.AccessType.WRITE, OperandType.Reg) : selectSatisfy(diffWithDesiredState, GenericOperand.AccessType.WRITE, OperandType.Reg, OperandType.Mem);

        } else {
            first = selectSatisfy(diffWithDesiredState, GenericOperand.AccessType.WRITE, OperandType.Reg, OperandType.Mem);
            second = (first instanceof x86MemoryOperand) ? selectSatisfy(diffWithDesiredState, GenericOperand.AccessType.READ, OperandType.Reg/*, OperandType.Imm*/) : selectSatisfy(diffWithDesiredState, GenericOperand.AccessType.READ, OperandType.Reg, OperandType.Mem/*, OperandType.Imm*/);

        }

        if ((first == null) || (second == null)) {
            throw new NoSuitableOperandFoundException(opcode.getOpcode());
        }

        result.add(first);
        result.add(second);
        return result;
    }

    @Override
    public List<List<GenericOperand>> getAllPossibleOperandSets(
            x86OpcodeWrapper opcode, x86ProcessorState state)
            throws NoSuitableOperandFoundException {
        throw new UnsupportedOperationException();
    }
}
