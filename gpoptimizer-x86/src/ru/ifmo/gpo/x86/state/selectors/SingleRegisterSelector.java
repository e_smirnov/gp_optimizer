package ru.ifmo.gpo.x86.state.selectors;

import ru.ifmo.gpo.core.instructions.IOperandSelector;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.vmstate.NoSuitableOperandFoundException;
import ru.ifmo.gpo.util.CollectionUtils;
import ru.ifmo.gpo.x86.state.x86ProcessorState;
import ru.ifmo.gpo.x86.state.x86StateDelta;
import ru.ifmo.gpo.x86.x86OpcodeWrapper;

import java.util.LinkedList;
import java.util.List;

/**
 * User: e_smirnov
 * Date: 01.02.2011
 * Time: 15:29:37
 */
public class SingleRegisterSelector implements IOperandSelector<x86OpcodeWrapper, x86ProcessorState, x86StateDelta> {
    @Override
    public List<GenericOperand> selectOperandsRandom(x86OpcodeWrapper opcode, x86ProcessorState state) throws NoSuitableOperandFoundException {
        List<GenericOperand> candidates = new LinkedList<GenericOperand>();
        for (GenericOperand op : state.getRegisters().values()) {
            if (op.isWriteable() && op.isReadable()) {
                candidates.add(op);
            }
        }
        if (candidates.isEmpty()) {
            throw new NoSuitableOperandFoundException(opcode.getOpcode());
        }
        GenericOperand rz = CollectionUtils.selectRandomElement(candidates);
        candidates.clear();
        candidates.add(rz);
        return candidates;
    }

    @Override
    public List<GenericOperand> selectOparandsSatisfyRestrictions(x86OpcodeWrapper opcode, x86ProcessorState stateBefore, x86ProcessorState desiredState, x86StateDelta diffWithDesiredState) throws NoSuitableOperandFoundException {
        List<GenericOperand> candidates = new LinkedList<GenericOperand>();
        for (GenericOperand op : diffWithDesiredState.getRegisterContents()) {
            if (op.isWriteable() && op.isReadable()) {
                candidates.add(op);
            }
        }

        if (candidates.isEmpty()) {
            throw new NoSuitableOperandFoundException(opcode.getOpcode());
        }

        GenericOperand rz = CollectionUtils.selectRandomElement(candidates);
        candidates.clear();
        candidates.add(rz);
        return candidates;
    }

    @Override
    public List<List<GenericOperand>> getAllPossibleOperandSets(
            x86OpcodeWrapper opcode, x86ProcessorState state)
            throws NoSuitableOperandFoundException {
        throw new UnsupportedOperationException();
    }
}
