package ru.ifmo.gpo.x86.state;

import ru.ifmo.gpo.core.instructions.generic.GenericRegister;

/**
 * User: jedi-philosopher
 * Date: 06.01.2011
 * Time: 18:26:59
 */
public class x86Register extends GenericRegister {
    public String name; // public to simplify acces from C++ code via JNI
    private static final long serialVersionUID = -6374810753669411664L;

    public x86Register(String name) {
        this.name = name;
    }

    @Override
    public String getId() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        x86Register other = (x86Register) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

}
