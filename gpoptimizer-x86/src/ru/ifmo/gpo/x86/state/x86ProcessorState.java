package ru.ifmo.gpo.x86.state;

import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.instructions.generic.GenericStackItem;
import ru.ifmo.gpo.core.vmstate.ConstantStorage;
import ru.ifmo.gpo.core.vmstate.IMachineState;
import ru.ifmo.gpo.verify.IArithmeticTreeNode;
import ru.ifmo.gpo.verify.VariableNode;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: jedi-philosopher
 * Date: 17.01.2011
 * Time: 23:56:14
 */
public class x86ProcessorState implements IMachineState {

    private Map<String, x86Register> registers;
    private Map<String, x86MemoryOperand> memory;
    private ConstantStorage constants;

    public x86ProcessorState() {
        this.registers = new HashMap<String, x86Register>();
        this.memory = new HashMap<String, x86MemoryOperand>();
        this.constants = new ConstantStorage();
    }

    public x86ProcessorState(Map<String, x86Register> registers, Map<String, x86MemoryOperand> memory, ConstantStorage constants) {
        this.registers = new HashMap<String, x86Register>(registers);
        this.memory = new HashMap<String, x86MemoryOperand>(memory);
        this.constants = new ConstantStorage(constants);
    }

    public x86ProcessorState(x86ProcessorState proto) {
        this(proto.registers, proto.memory, proto.constants);
    }

    private void addWriteAccess(GenericOperand operand) {
        if (operand.getAccessType() == GenericOperand.AccessType.READ) {
            operand.setAccessType(GenericOperand.AccessType.ANY);
        } else if (operand.getAccessType() == GenericOperand.AccessType.NONE) {
            operand.setAccessType(GenericOperand.AccessType.ANY);
        }
    }

    public void putValue(GenericOperand operand, IArithmeticTreeNode value) {
        GenericOperand content = get(operand);
        if (content == null) {
            operand.setExpression(value);
            addWriteAccess(operand);
            put(operand);
        } else {
            content.setExpression(value);
            addWriteAccess(content);
        }
    }

    public boolean hasValue(GenericOperand operand) {
        return get(operand) != null;
    }

    public IArithmeticTreeNode getValue(GenericOperand operand) {
        if (operand instanceof x86ConstantOperand) {
            x86ConstantOperand constant = (x86ConstantOperand) operand;
            if (!constant.getType().equals(Type.INT_TYPE)) {
                throw new IllegalArgumentException("Non-integer constants not supported");
            }
            constants.addIntConstant(constant.getConstantBits());
            return operand.getExpression();
        }
        GenericOperand content = get(operand);
        if (content != null) {
            if (content.getAccessType() == GenericOperand.AccessType.WRITE) {
                content.setAccessType(GenericOperand.AccessType.ANY);
            }
            return content.getExpression();
        }
        IArithmeticTreeNode value = new VariableNode(operand.getId(), Type.INT_TYPE);
        operand.setExpression(value);
        operand.setAccessType(GenericOperand.AccessType.READ);
        put(operand);
        return value;
    }

    public void put(GenericOperand operand) {
        if (operand instanceof x86Register) {
            registers.put(((x86Register) operand).name, (x86Register) operand);
        } else if (operand instanceof x86MemoryOperand) {
            memory.put(((x86MemoryOperand) operand).id, (x86MemoryOperand) operand);
        } else {
            throw new IllegalArgumentException("x86ProcessorState does not store operands of type" + operand.getClass());
        }
    }

    public void appendConstants(ConstantStorage constants) {
        this.constants.merge(constants);
    }

    public GenericOperand get(GenericOperand operand) {
        if (operand instanceof x86Register) {
            return registers.get(((x86Register) operand).name);
        } else if (operand instanceof x86MemoryOperand) {
            return memory.get(((x86MemoryOperand) operand).id);
        } else {
            return null;
        }
    }

    @Override
    public Map<String, ? extends GenericOperand> getRegisters() {
        return registers;
    }

    @Override
    public List<? extends GenericStackItem> getStack() {
        return Collections.emptyList(); //todo: support for x86 stack
    }

    @Override
    public Map<String, ? extends GenericOperand> getMemory() {
        return memory;
    }

    @Override
    public ConstantStorage getConstants() {
        return constants;
    }
}
