package ru.ifmo.gpo.x86.state;

import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.vmstate.BaseInstructionSequenceAnalyzer;
import ru.ifmo.gpo.core.vmstate.IInstructionSequenceAnalyzer;
import ru.ifmo.gpo.x86.x86Instruction;

import java.util.HashSet;
import java.util.Set;

/**
 * User: e_smirnov
 * Date: 20.01.2011
 * Time: 15:34:04
 */
public class StateAnalyzer extends BaseInstructionSequenceAnalyzer<x86ProcessorState, x86StateDelta> {


    private Set<GenericOperand> necessaryContents = new HashSet<GenericOperand>();

    public StateAnalyzer(InstructionSequence sequence) {
        super(sequence);
        analyze();
    }

    public StateAnalyzer(InstructionSequence sequence, int from, int len) {
        super(sequence, from, len);
        analyze();
    }

    @Override
    public boolean checkStateEqual(IInstructionSequenceAnalyzer second) {
        return second instanceof StateAnalyzer && ((new x86StateDelta(getStateAfter(), second.getStateAfter())).getDelta() == 0);
    }

    private void fixState(x86ProcessorState s) {
        s.appendConstants(stateAfter.getConstants()); // each state must have full amount of constants, and most complete set is stored in last state
        // each state must have full amount of write-available registers
        for (GenericOperand reg : stateAfter.getRegisters().values()) {
            if (!reg.isWriteable()) {
                continue;
            }
            GenericOperand valueInState = s.get(reg);
            if (valueInState == null || !valueInState.isWriteable()) {
                x86Register writeReg = new x86Register(reg.getId());
                writeReg.setAccessType(GenericOperand.AccessType.WRITE);
                s.put(writeReg);
            }

        }
        // same with memory
        for (GenericOperand mem : stateAfter.getMemory().values()) {
            if (!mem.isWriteable()) {
                continue;
            }
            GenericOperand valueInState = s.get(mem);
            if (valueInState == null || !valueInState.isWriteable()) {
                x86MemoryOperand writeMem = new x86MemoryOperand(mem.getId(), 4);
                writeMem.setAccessType(GenericOperand.AccessType.WRITE);
                s.put(writeMem);
            }

        }
    }

    protected void analyze() {
        x86ProcessorState state = new x86ProcessorState();
        for (int i = from; i < from + rangeLength; ++i) {
            x86Instruction instr = (x86Instruction) source.get(i);
            state = InstructionEvaluator.evaluateInstruction(state, instr, necessaryContents);
            states.add(state);
        }

        stateAfter = states.get(states.size() - 1);

        initialState = new x86ProcessorState();
        for (GenericOperand op : necessaryContents) {
            GenericOperand newOp;
            if (op instanceof x86Register) {
                newOp = new x86Register(op.getId());
            } else {
                newOp = new x86MemoryOperand(op.getId(), 4);
            }
            newOp.setExpression(op.getExpression());
            newOp.setAccessType(stateAfter.get(op).getAccessType());

            initialState.put(newOp);
        }
        fixState(initialState);
        for (x86ProcessorState s : states) {
            fixState(s);
        }


        stateDelta = new x86StateDelta(initialState, stateAfter);
    }
}
