package ru.ifmo.gpo.x86.state;

import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.UnsupportedOperandTypeException;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.verify.NumericConstantTreeNode;

/**
 * User: jedi-philosopher
 * Date: 16.01.2011
 * Time: 16:16:47
 */
public class x86ConstantOperand extends GenericOperand {
    private static final long serialVersionUID = -3927727311789380182L;

    public x86ConstantOperand(Type t, Number constantValue) {
        super(new NumericConstantTreeNode(t, constantValue), t);

    }

    public x86ConstantOperand(Integer i) {
        this(Type.INT_TYPE, i);
    }

    @Override
    public String getId() {
        return expression.toString();
    }

    public int getConstantBits() {
        if (!(expression instanceof NumericConstantTreeNode)) {
            throw new IllegalStateException("x86ConstantOperand may contain only expressions of type NumericConstantTreeNode");
        }
        Number n = ((NumericConstantTreeNode) expression).getConstantValue();
        if (n instanceof Integer) {
            return n.intValue();
        } else if (n instanceof Float) {
            return Float.floatToRawIntBits((Float) n);
        } else {
            throw new UnsupportedOperandTypeException(n.getClass());
        }
    }

    @Override
    public String toString() {
        return expression.toString();
    }
}
