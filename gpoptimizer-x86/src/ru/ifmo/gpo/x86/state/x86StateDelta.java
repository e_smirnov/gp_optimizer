package ru.ifmo.gpo.x86.state;

import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.vmstate.BaseStateDelta;
import ru.ifmo.gpo.core.vmstate.IMachineState;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * User: e_smirnov
 * Date: 20.01.2011
 * Time: 15:40:27
 */
public class x86StateDelta extends BaseStateDelta<IMachineState> {

    private Set<GenericOperand> memoryWrites = new HashSet<GenericOperand>();
    private Set<GenericOperand> registerContents = new HashSet<GenericOperand>();

    public x86StateDelta(IMachineState source, IMachineState target) {
        super(source, target);
        calculateDelta();
    }

    private void calculateDelta() {
        processArea((Map<String, GenericOperand>) source.getMemory(), (Map<String, GenericOperand>) target.getMemory(), memoryWrites);
        processArea((Map<String, GenericOperand>) source.getRegisters(), (Map<String, GenericOperand>) target.getRegisters(), registerContents);
    }

    public Set<GenericOperand> getMemoryWrites() {
        return memoryWrites;
    }

    public Set<GenericOperand> getRegisterContents() {
        return registerContents;
    }

    @Override
    public int getDelta() {
        return memoryWrites.size() + registerContents.size();
    }
}
