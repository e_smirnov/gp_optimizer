package ru.ifmo.gpo.x86.softwire;

import ru.ifmo.gpo.core.Settings;

/**
 * User: jedi-philosopher
 * Date: 05.01.2011
 * Time: 19:41:40
 * <p/>
 * Wrapper for native class from SoftWire library.
 */
public class x86 {

    static {
        String platform = System.getProperty("sun.arch.data.model");
        String libraryName;
        if (platform.equals("32")) {
            libraryName = "x86CodeGenerator";
        } else if (platform.equals("64")) {
            libraryName = "x86CodeGenerator_64";
        } else {
            System.err.println("Failed to resolve type of system JVM is run on, can not load native library x86CodeGenerator");
            libraryName = null;
        }

        if (libraryName != null) {
            if (Settings.useDebugNativeLibraries()) {
                libraryName += "_d";
            }
            System.loadLibrary(libraryName);
        }

    }

    private int handle;

    private x86() {
        handle = 0;
    }

    private x86(String s) {
        System.err.println("Wtf?");
    }

    public int getHandle() {
        return handle;
    }

    public void setHandle(int handle) {
        this.handle = handle;
    }

    public static x86 create() {
        x86 rz = new x86();
        rz.init();
        return rz;
    }

    public static void destroy(x86 obj) {
        obj.term();
    }

    private native void init();

    private native void term();

    public native void applyInsn(int opcode);

    public native void applyUnaryInsn(int opcode, Object param);

    public native void applyBinaryInsn(int opcode, Object param1, Object param2);

    public native int generateCode();
}
