#ifndef Parameters_h__
#define Parameters_h__

#include <string>
#include <Assembler.hpp>
#include <codegen/SupportedOpcodes.h>

namespace codegen
{
    struct IGenericOperand
    {
        int type;

        IGenericOperand(int t):type(t){}

        virtual const char* getId() const = 0;

        static const int REG = 0;
        static const int MEM = 1;
        static const int CONSTANT = 2;
    };

    struct RegisterOperand: public IGenericOperand
    {
        RegisterOperand(const char* name):
            IGenericOperand(REG)
            , regName(name)    
        {
            
        }

        std::string regName;

        virtual const char* getId() const { return regName.c_str(); }
        
        const SoftWire::OperandREG32& getRegister(SoftWire::Assembler* x86)
        {
            if (regName == RegisterNames::RegNameEax) {
                return x86->eax;
            } else if (regName == RegisterNames::RegNameEbx) {
                return x86->ebx;
            } else if (regName == RegisterNames::RegNameEcx) {
                return x86->ecx;
            } else if (regName == RegisterNames::RegNameEdx) {
                return x86->edx;
            } else if (regName == RegisterNames::RegNameEsi) {
                return x86->esi;
            } else if (regName == RegisterNames::RegNameEdi) {
                return x86->edi;
            } else if (regName == RegisterNames::RegNameEsp) {
                return x86->esp;
            } else if (regName == RegisterNames::RegNameEbp) {
                return x86->ebp;
            }
            //to do: error notification
            return x86->eax;
        }
    };

    struct MemoryOperand: public IGenericOperand
    {
        std::string id;
        int size;

        MemoryOperand(const char* i, int s)
            : IGenericOperand(MEM)
            , id(i)
            , size(s)
        {

        }

        virtual const char* getId() const
        {
            return id.c_str();
        }
    };

    template<class T>
    struct ConstantOperand: public IGenericOperand
    {
        T constantValue;

        ConstantOperand(T c)
            : IGenericOperand(CONSTANT)
            , constantValue(c)
        {

        }

        virtual const char* getId() const 
        {
            return NULL; // const operands are not placed in map
        }
    };

}
#endif // Parameters_h__