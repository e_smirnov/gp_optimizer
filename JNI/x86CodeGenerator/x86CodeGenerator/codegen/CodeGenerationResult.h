#ifndef CodeGenerationResult_h__
#define CodeGenerationResult_h__

#include <jni.h>
#include <string>
#include <Assembler.hpp>
#include <common/MemoryAreaMap.h>
#include <common/BaseInterfaceObject.h>

namespace codegen
{

class CodeExecutionEnvironment;

class CodeGenerationResult: public common::BaseInterfaceObject<CodeGenerationResult>
{
    void (*generatedCode)(void);

    common::MemoryAreaMap memoryAreaMap;

    std::string listing;

    SoftWire::Assembler* assembler;

public:

    CodeGenerationResult(jint handle, SoftWire::Assembler* assembler, common::MemoryAreaMap& memMap, const std::string& code)
        : common::BaseInterfaceObject<CodeGenerationResult>(handle)
        , assembler(assembler)
        , generatedCode((void(*)())assembler->callable())
        , listing(code)
    {
        memoryAreaMap.setValuesDoNotCopyMemory(memMap); // this method will only copy pointers, as now memory addresses are hardcoded in generated code
    }

    ~CodeGenerationResult();

    void setValues(CodeExecutionEnvironment* env);

    const common::MemoryAreaMap& getMemoryMap() const { return memoryAreaMap; }

    typedef void(*FuncType)();

    FuncType getCode() { return generatedCode; }

    void dumpOnDisc();

};

}

#endif // CodeGenerationResult_h__