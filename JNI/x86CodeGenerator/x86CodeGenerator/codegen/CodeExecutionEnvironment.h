#ifndef CodeExecutionEnvironment_h__
#define CodeExecutionEnvironment_h__

#include <common/BaseInterfaceObject.h>
#include <common/MemoryAreaMap.h>

namespace codegen {

    class CodeExecutionEnvironment: public common::BaseInterfaceObject<CodeExecutionEnvironment>
    {
    private:
        common::MemoryAreaMap values;
    public:
        CodeExecutionEnvironment(int handle, const common::MemoryAreaMap& sourceMap)
            : common::BaseInterfaceObject<CodeExecutionEnvironment>(handle)
            , values(sourceMap)
        {
        }

        ~CodeExecutionEnvironment()
        {
        }

        const common::MemoryAreaMap& getMemory() const 
        {
            return values;
        }

        void setToZero()
        {
            for (common::MemoryAreaMap::iterator iter = values.begin(); iter != values.end(); ++iter) {
                memset(iter->second->start, 0, iter->second->size);
            }
        }

        void setToFF()
        {
            for (common::MemoryAreaMap::iterator iter = values.begin(); iter != values.end(); ++iter) {
                memset(iter->second->start, 0xffffffff, iter->second->size);
            }
        }

        void setRandom()
        {
            for (common::MemoryAreaMap::iterator iter = values.begin(); iter != values.end(); ++iter) {
                memset(iter->second->start, rand(), iter->second->size);
            }
        }
    };

}

#endif // CodeExecutionEnvironment_h__