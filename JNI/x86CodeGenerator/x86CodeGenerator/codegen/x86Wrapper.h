#ifndef x86Wrapper_h__
#define x86Wrapper_h__

#include <map>
#include <list>
#include <jni.h>
#include <sstream>
#include <Assembler.hpp>
#include <Windows.h>

#include <common/BaseInterfaceObject.h>
#include <codegen/CodeGenerationResult.h>

namespace codegen
{
    struct IGenericOperand;

    typedef unsigned char byte;

    class x86Wrapper: public common::BaseInterfaceObject<x86Wrapper>
    {
    private:
        SoftWire::Assembler* x86;
        common::MemoryAreaMap memoryMap;
        bool ownsAssembler;
        std::ostringstream listing;

        IGenericOperand* decodeOperand(JNIEnv* env, jobject operand);
        
        template<class FirstParamType>
        void decodeUnaryInstruction(int instr, FirstParamType firstParam)
        {
            switch (instr) {
                case NEG:
                    x86->neg(firstParam);
                    break;
                case NOT:
                    x86->not(firstParam);
                    break;
                case INC:
                    x86->inc(firstParam);
                    break;
                case DEC:
                    x86->dec(firstParam);
                    break;
                case SHR:
                    x86->shr(firstParam, 1);
                    break;
                case SHL:
                    x86->shl(firstParam, 1); //todo other shifts
                    break;
                case DIV:
                    x86->div(firstParam);
                    break;
                default:
                    // todo: error notification
                    x86->nop();
            }
        }

        template<class FirstParamType, class SecondParamType>
        void decodeBinaryInstruction(int instr, FirstParamType firstParam, SecondParamType secondParam)
        {
            switch (instr) {
                case ADD:
                    x86->add(firstParam, secondParam);
                    break;
                case SUB:
                    x86->sub(firstParam, secondParam);
                    break;
                case IMUL:
                    x86->imul(firstParam, secondParam);
                    break;
                case OR:
                    x86->or(firstParam, secondParam);
                    break;
                case XOR:
                    x86->xor(firstParam, secondParam);
                    break;
                case AND:
                    x86->and(firstParam, secondParam);
                    break;
                case MOV:
                    x86->mov(firstParam, secondParam);
                    break;
                default:
                    x86->nop();
                    break;
            }
        }
        
    public:
        x86Wrapper(int h)
            : common::BaseInterfaceObject<x86Wrapper>(h)
            , ownsAssembler(true)
        {
#ifdef _WIN64
			x86 = new SoftWire::Assembler(true);
#else 
            x86 = new SoftWire::Assembler(false);
#endif
            x86->enableListing();
            x86->setEchoStream(&listing);
        }

        ~x86Wrapper()
        {
            if (ownsAssembler) {
                // no codegen results were created from this code generator, we exclusively own assembler and memory map instances
                delete x86;
            } else {
                memoryMap.getMap().clear(); // manually clearing map so that it will not delte contained area descriptors on dtor, as they are now owned by codegen result
            }
        }

        void applyInstruction(jint opcode, JNIEnv* env);
        void applyUnaryInstruction(jint opcode, JNIEnv* env, jobject operand);
        void applyBinaryInstruction(jint opcode, JNIEnv* env, jobject firstOperand, jobject secondOperand);

        jint generateCode(JNIEnv* env);
    };
}

#endif // x86Wrapper_h__