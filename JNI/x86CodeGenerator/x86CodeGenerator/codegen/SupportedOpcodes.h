#ifndef SupportedOpcodes_h__
#define SupportedOpcodes_h__

namespace codegen
{
// must be in sync with java enum ru.ifmo.gpo.x86.x86Opcodes
enum Opcodes
{
    ADD = 0
    , SUB
    , IMUL
    , DIV
    , AND
    , OR
    , XOR
    , NEG
    , NOT
    , SHR
    , SHL
    , INC
    , DEC
    , MOV
    , NOP
    , RDTSC
};

class RegisterNames
{
public:
    static const char* RegNameEax;
    static const char*  RegNameEbx;
    static const char*  RegNameEcx;
    static const char*  RegNameEdx;
    static const char*  RegNameEsi;
    static const char*  RegNameEdi;
    static const char*  RegNameEsp;
    static const char*  RegNameEbp;
};

}


#endif // SupportedOpcodes_h__