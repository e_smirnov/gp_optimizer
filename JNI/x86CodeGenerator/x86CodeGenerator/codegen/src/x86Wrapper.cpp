#include <codegen/x86Wrapper.h>
#include <codegen/Parameters.h>
#include <codegen/SupportedOpcodes.h>
#include <codegen/CodeGenerationResult.h>

#include <common/JVMException.h>

using namespace codegen;

const char*  RegisterNames::RegNameEax = "EAX";
const char*  RegisterNames::RegNameEbx = "EBX";
const char*  RegisterNames::RegNameEcx = "ECX";
const char*  RegisterNames::RegNameEdx = "EDX";
const char*  RegisterNames::RegNameEsi = "ESI";
const char*  RegisterNames::RegNameEdi = "EDI";
const char*  RegisterNames::RegNameEsp = "ESP";
const char*  RegisterNames::RegNameEbp = "EBP";

IGenericOperand* x86Wrapper::decodeOperand(JNIEnv* env, jobject operand)
{
    jclass classDescr = env->GetObjectClass(operand);
    jclass registerParameterClass = env->FindClass("ru/ifmo/gpo/x86/state/x86Register");
    jclass memoryParameterClass = env->FindClass("ru/ifmo/gpo/x86/state/x86MemoryOperand");
    jclass constantParameterClass = env->FindClass("ru/ifmo/gpo/x86/state/x86ConstantOperand");
    
    if (env->IsInstanceOf(operand, registerParameterClass)) {
        jboolean dummy;
        jfieldID fieldId = env->GetFieldID(classDescr, "name", "Ljava/lang/String;");
        if (!fieldId) {
            return NULL;
        }
        jstring stringVal = (jstring)env->GetObjectField(operand, fieldId);
        const char* registerNameValue = env->GetStringUTFChars(stringVal, &dummy);
        return new RegisterOperand(registerNameValue);
    } else if (env->IsInstanceOf(operand, memoryParameterClass)) {
        jboolean dummy;
        jfieldID fieldId = env->GetFieldID(classDescr, "id", "Ljava/lang/String;");
        if (!fieldId){
            return NULL;
        }
        jstring stringVal = (jstring)env->GetObjectField(operand, fieldId);
        const char* memoryId = env->GetStringUTFChars(stringVal, &dummy);

        jfieldID sizeFieldId = env->GetFieldID(classDescr, "size", "I");
        jint size = env->GetIntField(operand, sizeFieldId);

        if (!memoryMap.containsKey(memoryId)) {
            memoryMap[memoryId] = new common::MemoryAreaDescriptor(memoryId, size);
        }
        return new MemoryOperand(memoryId, size);
    } else if (env->IsInstanceOf(operand, constantParameterClass)) {
        jmethodID methodId = env->GetMethodID(classDescr, "getConstantBits", "()I");
        
        if (!methodId) {
            return NULL;
        }

        jint result = env->CallIntMethod(operand, methodId);
        return new ConstantOperand<jint>(result);
    }

    return NULL;
}

void x86Wrapper::applyInstruction( jint opcode, JNIEnv* env)
{
    switch (opcode) {
        case codegen::NOP:
            x86->nop();
            break;
        case codegen::RDTSC:
            x86->rdtsc();
            break;
        default:
            ;// raise exception
    }
}

void x86Wrapper::applyUnaryInstruction(jint opcode, JNIEnv* env, jobject operand)
{
    IGenericOperand* decodedOperand = decodeOperand(env, operand);
    if (decodedOperand->type == IGenericOperand::REG) {
        decodeUnaryInstruction(opcode, ((RegisterOperand*)decodedOperand)->getRegister(x86));
    } else if (decodedOperand->type == IGenericOperand::MEM) {
        decodeUnaryInstruction(opcode, x86->dword_ptr[memoryMap[((MemoryOperand*)decodedOperand)->getId()]->start]);
    } else if (decodedOperand->type == IGenericOperand::CONSTANT) {
        // unary operators do not support immediate values
    }
}

void x86Wrapper::applyBinaryInstruction(jint opcode, JNIEnv* env, jobject firstOperand, jobject secondOperand)
{
    IGenericOperand* first = decodeOperand(env, firstOperand);
    IGenericOperand* second= decodeOperand(env, secondOperand);

    if (!first || !second){
        JVM_THROW_VOID("Faield to decode operands");
    }
    //todo: write better
    if (first->type == IGenericOperand::REG && second->type == IGenericOperand::REG) {
        decodeBinaryInstruction(opcode, ((RegisterOperand*)first)->getRegister(x86), ((RegisterOperand*)second)->getRegister(x86));
    } else if (first->type == IGenericOperand::REG && second->type == IGenericOperand::MEM) {
        decodeBinaryInstruction(opcode, ((RegisterOperand*)first)->getRegister(x86), x86->dword_ptr[memoryMap[((MemoryOperand*)second)->getId()]->start]);
    } else if (first->type == IGenericOperand::MEM && second->type == IGenericOperand::REG) {
        decodeBinaryInstruction(opcode, x86->dword_ptr[memoryMap[((MemoryOperand*)first)->getId()]->start], ((RegisterOperand*)second)->getRegister(x86));
    }  else if (first->type == IGenericOperand::REG && second->type == IGenericOperand::CONSTANT) {
        decodeBinaryInstruction(opcode, ((RegisterOperand*)first)->getRegister(x86), (unsigned int)((ConstantOperand<jint>*)second)->constantValue);
    }  else if (first->type == IGenericOperand::MEM && second->type == IGenericOperand::CONSTANT) {
        decodeBinaryInstruction(opcode, x86->dword_ptr[memoryMap[((MemoryOperand*)first)->getId()]->start], (unsigned int)((ConstantOperand<jint>*)second)->constantValue);
    }

}

jint x86Wrapper::generateCode(JNIEnv* env)
{
    if (!ownsAssembler){
        JVM_THROW("generateDode() called for second time for same Assembler instance", 0);
    }
    x86->ret();
    jint handle = CodeGenerationResult::getFirstFreeHandle();
    codegen::CodeGenerationResult* rz = new CodeGenerationResult(handle, x86, memoryMap, listing.str()); // on ctor it will put itself into a static map
    ownsAssembler = false;
    return handle;
}