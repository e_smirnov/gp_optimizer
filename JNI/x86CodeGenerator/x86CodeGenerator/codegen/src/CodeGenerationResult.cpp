#include <codegen/CodeGenerationResult.h>
#include <codegen/CodeExecutionEnvironment.h>

#include <fstream>
using namespace codegen;


CodeGenerationResult::~CodeGenerationResult()
{
    delete assembler;
}

void CodeGenerationResult::setValues(CodeExecutionEnvironment* env)
{
   memoryAreaMap.copyValues(env->getMemory());
}

void CodeGenerationResult::dumpOnDisc()
{
    char fileName[32];
    sprintf(fileName, "out/dump_%d.asm", handle);
    std::ofstream stream(fileName);
    stream << listing;
    stream.close();
}