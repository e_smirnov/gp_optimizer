#ifndef SEH_h__
#define SEH_h__

#include <Windows.h>

namespace common {

inline DWORD defaultExceptionFilter(DWORD exceptionCode, _EXCEPTION_POINTERS* exceptionInformation)
{
    return EXCEPTION_EXECUTE_HANDLER;
}


}


#endif // SEH_h__