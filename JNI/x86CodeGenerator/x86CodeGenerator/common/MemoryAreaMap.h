#ifndef MemoryAreaMap_h__
#define MemoryAreaMap_h__

#include <string>
#include <map>
#include <common/MemoryAreaDescriptor.h>

namespace common {


class MemoryAreaMap
{
    typedef std::map<std::string, MemoryAreaDescriptor*> MemoryMap;

    MemoryMap memoryAreaMap;

public:

    typedef MemoryMap::iterator iterator;
    typedef MemoryMap::const_iterator const_iterator;

    const_iterator begin() const;
    const_iterator end() const;
    iterator begin();
    iterator end();

    size_t size() const;

    MemoryAreaDescriptor* find(const std::string& key) const;
    bool containsKey(const std::string& key) const;
    MemoryAreaDescriptor*& operator[](const std::string& key);

    MemoryAreaMap();
    MemoryAreaMap(const MemoryAreaMap& proto);
    ~MemoryAreaMap();

    void setValues(const MemoryAreaMap& proto);
    void copyValues(const MemoryAreaMap& proto);
    void setValuesDoNotCopyMemory(const MemoryAreaMap& proto);
    void clear();

    MemoryMap& getMap();

    bool isInputMemoryAreaName(const char* name);
    bool isInputMemoryAreaName(const std::string& name);
};

}
#endif // MemoryAreaMap_h__