#ifndef BaseInterfaceObject_h__
#define BaseInterfaceObject_h__

#include <jni.h>
#include <map>

namespace common
{
    template<class T>
    class BaseInterfaceObject
    {
    public:
        typedef jint HandleType;
    private:
        static HandleType firstFreeHandle;
        
        typedef std::map<HandleType, T*> HandleToObjectMap; 
        static HandleToObjectMap objectMap; 
   
    protected:
    
        HandleType handle;
        
        BaseInterfaceObject(HandleType h)
         : handle(h)
        {
            objectMap[h] = (T*)this;
        }
        
        ~BaseInterfaceObject()
        {
			if (objectMap.empty()) {
				return;
			}
            HandleToObjectMap::iterator iter = objectMap.find(handle);
            if (iter != objectMap.end()) {
                objectMap.erase(iter);
            }
        }
    
    public:    

        static HandleType getFirstFreeHandle()
        {
            return firstFreeHandle++;
        }

        static HandleType getHandleValue(JNIEnv* env, jobject object)
        {
            jclass javaClass = env->GetObjectClass(object);
            jfieldID handleFieldId = env->GetFieldID(javaClass, "handle", "I");
            return env->GetIntField(object, handleFieldId);
        }
        
        static T* getFromMap(JNIEnv* env, jobject object)
        {
            HandleType handleValue = getHandleValue(env, object);
            HandleToObjectMap::iterator iter = objectMap.find(handleValue);
            if (iter != objectMap.end()) {
                return iter->second;
            }
            return NULL;
        }

        static T* getFromMap(HandleType id) {
            HandleToObjectMap::iterator iter = objectMap.find(id);
            if (iter != objectMap.end()) {
                return iter->second;
            }
            return NULL;
        }

        static void dispose(JNIEnv* env, jobject object)
        {
			if (objectMap.empty()) {
				return;
			}
            HandleType handleValue = getHandleValue(env, object);
            HandleToObjectMap::iterator iter = objectMap.find(handleValue);
            if (iter != objectMap.end()) {
                delete iter->second;
                // not removing from map here, as it will be done on dtor
            }
        }
        
        HandleType getHandle() const
        {
            return handle;
        }
    };    
	
	template<class T>
    typename BaseInterfaceObject<T>::HandleToObjectMap BaseInterfaceObject<T>::objectMap; 

	template<class T>
    typename BaseInterfaceObject<T>::HandleType BaseInterfaceObject<T>::firstFreeHandle;   
}

#endif BaseInterfaceObject_h__