#ifndef MemoryAreaDescriptor_h__
#define MemoryAreaDescriptor_h__

#include <string>
#include <map>
#include <jni.h>

namespace common
{
    typedef unsigned char byte;

    struct MemoryAreaDescriptor
    {
        std::string id;
        byte* start;
        jint size;

        MemoryAreaDescriptor(const char* id, jint size)
            : id(id)
            , size(size)
        {
            start = new byte[size];
        }

        ~MemoryAreaDescriptor()
        {
            delete[] start;
        }
    };

}


#endif // MemoryAreaDescriptor_h__