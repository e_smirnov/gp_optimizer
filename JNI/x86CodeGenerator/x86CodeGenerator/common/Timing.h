#ifndef __timing_h__
#define __timing_h__

#include <Windows.h>

namespace common {

	class Timing {

	public:
	  
		void startTiming();

		void stopTiming();

	    double getUserSeconds() const {
			return value;
		}

	private:

		DWORD_PTR oldmask;
		
		double value;
		
		LARGE_INTEGER time1;
	};

}

#endif // __timing_h__