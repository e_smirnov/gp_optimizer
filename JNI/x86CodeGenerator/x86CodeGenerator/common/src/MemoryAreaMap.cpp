#include <string.h>

#include <common/MemoryAreaMap.h>

using namespace common;

MemoryAreaMap::MemoryAreaMap()
{
}

MemoryAreaMap::MemoryAreaMap(const MemoryAreaMap& proto)
{
   setValues(proto);
}

MemoryAreaMap::~MemoryAreaMap()
{
    clear();
}

size_t MemoryAreaMap::size() const
{
    return memoryAreaMap.size();
}

MemoryAreaDescriptor* MemoryAreaMap::find(const std::string& key) const
{
    const_iterator iter = memoryAreaMap.find(key);
    return (iter != memoryAreaMap.end() ? iter->second : NULL);
}

MemoryAreaDescriptor*& MemoryAreaMap::operator[](const std::string& key)
{
    return memoryAreaMap[key];
}

bool MemoryAreaMap::containsKey(const std::string& key) const
{
    return (memoryAreaMap.find(key) != memoryAreaMap.end());
}

void MemoryAreaMap::clear()
{
    for (iterator iter = memoryAreaMap.begin(); iter != memoryAreaMap.end(); ++iter) {
        delete iter->second;
    }
    memoryAreaMap.clear();
}

void MemoryAreaMap::setValues(const MemoryAreaMap& proto)
{
    clear();
    for (const_iterator iter = proto.begin(); iter != proto.end(); ++iter) {
        common::MemoryAreaDescriptor* descr = new common::MemoryAreaDescriptor(iter->first.c_str(), iter->second->size);
        memcpy(descr->start, iter->second->start, iter->second->size);
        memoryAreaMap[iter->first.c_str()] = descr;
    }
}

void MemoryAreaMap::copyValues(const MemoryAreaMap& proto)
{
    for (const_iterator iter = proto.begin(); iter != proto.end(); ++iter) {
        iterator ourIter = memoryAreaMap.find(iter->first);
        if (ourIter == memoryAreaMap.end()) {
            continue;
        }
        common::MemoryAreaDescriptor* descr = ourIter->second;
        memcpy(descr->start, iter->second->start, iter->second->size);
    }
}

MemoryAreaMap::MemoryMap& MemoryAreaMap::getMap()
{
    return memoryAreaMap;
}

void MemoryAreaMap::setValuesDoNotCopyMemory(const MemoryAreaMap& proto)
{
    clear();
    memoryAreaMap = MemoryMap(proto.memoryAreaMap);
}

bool MemoryAreaMap::isInputMemoryAreaName(const char* name)
{
    return strstr(name, "_input") != NULL;
}

bool MemoryAreaMap::isInputMemoryAreaName(const std::string& name)
{
    return name.find("_input") != std::string::npos;
}

MemoryAreaMap::const_iterator MemoryAreaMap::begin() const
{
    return memoryAreaMap.begin();
}

MemoryAreaMap::const_iterator MemoryAreaMap::end() const
{
    return memoryAreaMap.end();
}

MemoryAreaMap::iterator MemoryAreaMap::begin()
{
    return memoryAreaMap.begin();
}

MemoryAreaMap::iterator MemoryAreaMap::end()
{
    return memoryAreaMap.end();
}
