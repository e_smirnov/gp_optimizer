#include <common/Timing.h>

using namespace common;

void Timing::startTiming()
{         
  oldmask = SetThreadAffinityMask(::GetCurrentThread(), 1);
  QueryPerformanceCounter(&time1);
}  

void Timing::stopTiming()
{  
  LARGE_INTEGER performance_frequency, time2;
  QueryPerformanceFrequency(&performance_frequency);
  QueryPerformanceCounter(&time2);  
  SetThreadAffinityMask(::GetCurrentThread(), oldmask);
  value = (double)(time2.QuadPart - time1.QuadPart);
  value /= performance_frequency.QuadPart;
} 