#ifndef JVMException_h__
#define JVMException_h__

#include <jni.h>

namespace common {

#define JVM_THROW_VOID(message) {common::throwJVMException(env, (message)); return;}
#define JVM_THROW(message, returnValue) {common::throwJVMException(env, (message)); return (returnValue);}

    inline void throwJVMException(JNIEnv* env, const char* message) {
        static jclass exceptionClass = env->FindClass("ru/ifmo/gpo/x86/NativeCodeException");
        env->ThrowNew(exceptionClass, message);
    }

}
#endif // JVMException_h__