#include <interface/ru_ifmo_gpo_x86_x86CodeGenerationResult.h>
#include <codegen/CodeGenerationResult.h>
#include <codegen/CodeExecutionEnvironment.h>
#include <common/JVMException.h>
#include <common/SEH.h>

JNIEXPORT void JNICALL Java_ru_ifmo_gpo_x86_x86CodeGenerationResult_dispose
(JNIEnv * env, jobject objectToDispose)
{
    __try {
        codegen::CodeGenerationResult::dispose(env, objectToDispose);
    } __except(common::defaultExceptionFilter(GetExceptionCode(), GetExceptionInformation())) {
        char errorMsg[128];
        codegen::CodeGenerationResult* errorRz = codegen::CodeGenerationResult::getFromMap(env, objectToDispose);
        if (errorRz) {
            sprintf(errorMsg, "Fatal error when trying to dispose CodeGenerationResult with handle %s", errorRz->getHandle());
        } else {
            sprintf(errorMsg, "Fatal error when trying to dispose non-existing CodeGenerationResult object");
        }
        JVM_THROW_VOID(errorMsg);
    }
}

jint doCreateExecutionEnvironment(JNIEnv *env, jobject codeGenResult)
{
    jint handle = codegen::CodeExecutionEnvironment::getFirstFreeHandle();
    codegen::CodeGenerationResult* codegenResult = codegen::CodeGenerationResult::getFromMap(env, codeGenResult);
    if (!codegenResult) {
        JVM_THROW("Failed to get CodeGenerationResult", 0);
    }

    codegen::CodeExecutionEnvironment* rz = new codegen::CodeExecutionEnvironment(handle, codegenResult->getMemoryMap());
    return handle;
}

JNIEXPORT jint JNICALL Java_ru_ifmo_gpo_x86_x86CodeGenerationResult_createExecutionEnvironment0
(JNIEnv *env, jobject codeGenResult)
{
   __try {
        return doCreateExecutionEnvironment(env, codeGenResult);
    } __except(common::defaultExceptionFilter(GetExceptionCode(), GetExceptionInformation())) {
        char errorMsg[128];
        codegen::CodeGenerationResult* errorRz = codegen::CodeGenerationResult::getFromMap(env, codeGenResult);
        if (errorRz) {
            sprintf(errorMsg, "Fatal error when trying to create ExecutionEnvironment from CodeGenerationResult with id %d", errorRz->getHandle());
        } else {
            sprintf(errorMsg, "Fatal error when trying to create ExecutionEnvironment from non-existing CodeGenerationResult object");
        }
        JVM_THROW(errorMsg, 0);
    }
}

/*
* Class:     ru_ifmo_gpo_x86_x86CodeGenerationResult
* Method:    dumpOnDisc
* Signature: ()V
*/
JNIEXPORT void JNICALL Java_ru_ifmo_gpo_x86_x86CodeGenerationResult_dumpOnDisc
(JNIEnv *env, jobject obj)
{
    codegen::CodeGenerationResult* rez = codegen::CodeGenerationResult::getFromMap(env, obj);
    if (!rez) {
        JVM_THROW_VOID("Failed to get CodeGenerationResult");
    }
    rez->dumpOnDisc();
}