#include <Windows.h>

#include <interface/ru_ifmo_gpo_x86_x86CodeExecutor.h>
#include <codegen/CodeGenerationResult.h>
#include <codegen/CodeExecutionEnvironment.h>
#include <executor/ExecutionResult.h>

#include <common/JVMException.h>
#include <common/SEH.h>
#include <common/Timing.h>

inline bool safeExecute(JNIEnv* env, void(*code)())
{
    __try {
#ifndef _WIN64
        __asm {
            pusha; // not sure if this is actually necessary, and x64 even does not have this instruction
        }
#endif
        code();

#ifndef _WIN64
        __asm {
            popa;
        }
#endif
    }
    __except(EXCEPTION_EXECUTE_HANDLER)
    {
        switch (GetExceptionCode()) {
            case EXCEPTION_INT_DIVIDE_BY_ZERO:
                {
                    jclass exceptionClass = env->FindClass("java/lang/ArithmeticException");
                    env->ThrowNew(exceptionClass, "/ by zero");
                    break;
                }
            default:
                {
                    JVM_THROW("Exception while executing code", false);
                }                
        }
        return false;
    }

    return true;
}

JNIEXPORT jlong JNICALL Java_ru_ifmo_gpo_x86_x86CodeExecutor_profileCode0
(JNIEnv * env, jobject execRes, jint codeId, jint envId)
{
    __try {
        codegen::CodeGenerationResult* code = codegen::CodeGenerationResult::getFromMap(codeId);
        if (code == NULL) {
            JVM_THROW("Failed to load CodeGenerationResult", 0);
        }
        codegen::CodeExecutionEnvironment* executionEnvironment = codegen::CodeExecutionEnvironment::getFromMap(envId);
        if (executionEnvironment == NULL) {
            JVM_THROW("Failed to load CodeExecutionEnvironment", 0);
        }

        code->setValues(executionEnvironment);

        void (*realCode)() = code->getCode();

		common::Timing timing;
		// warmup pass
		for (int i = 0; i < 500000; ++i) {
			if (!safeExecute(env, realCode)) {
				return 0;
			}
		}
        
		// measure pass
		timing.startTiming();
		for (int i = 0; i < 500000; ++i) {
			if (!safeExecute(env, realCode)) {
				return 0;
			}
		}
		timing.stopTiming();        

		return ((long)(timing.getUserSeconds() * 1000000));
    } __except(common::defaultExceptionFilter(GetExceptionCode(), GetExceptionInformation())) {
        JVM_THROW("SEH exception", 0);
    }
}

jint doExecuteCode(JNIEnv* env, jobject codeExecutor, jint codeId, jint execEnvId)
{

    codegen::CodeGenerationResult* code = codegen::CodeGenerationResult::getFromMap(codeId);
    if (code == NULL) {
        JVM_THROW("Failed to load CodeGenerationResult", 0);
    }
    codegen::CodeExecutionEnvironment* executionEnvironment = codegen::CodeExecutionEnvironment::getFromMap(execEnvId);
    if (executionEnvironment == NULL) {
        JVM_THROW("Failed to load CodeExecutionEnvironment", 0);
    }

    code->setValues(executionEnvironment);

    void (*realCode)() = code->getCode();
    if (!safeExecute(env, realCode)) {
        return 0;
    }

    executor::ExecutionResult* execRz = new executor::ExecutionResult(executor::ExecutionResult::getFirstFreeHandle(), code->getMemoryMap());
    return execRz->getHandle();
}

/*
* Class:     ru_ifmo_gpo_x86_x86CodeExecutor
* Method:    executeCode0
* Signature: (II)Lru/ifmo/gpo/core/IExecutionResult;
*/
JNIEXPORT jint JNICALL Java_ru_ifmo_gpo_x86_x86CodeExecutor_executeCode0
(JNIEnv *env, jobject codeExecutor, jint codeId, jint execEnvId)
{
    __try {
        return doExecuteCode(env, codeExecutor, codeId, execEnvId);
    } __except(common::defaultExceptionFilter(GetExceptionCode(), GetExceptionInformation())) {
        char errorMsg[128];
        codegen::CodeGenerationResult* errorRz = codegen::CodeGenerationResult::getFromMap(codeId);
        if (errorRz) {
            sprintf(errorMsg, "Fatal error when trying to execute code with id %d", errorRz->getHandle());
        } else {
            sprintf(errorMsg, "Fatal error when trying to execute non-existing object");
        }
        JVM_THROW(errorMsg, 0);
    }
}