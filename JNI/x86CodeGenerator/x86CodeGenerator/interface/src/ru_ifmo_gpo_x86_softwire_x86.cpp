#include <interface/ru_ifmo_gpo_x86_softwire_x86.h>
#include <codegen/x86Wrapper.h>
#include <common/JVMException.h>
#include <common/SEH.h>

/*
* Class:     ru_ifmo_gpo_x86_softwire_x86
* Method:    init
* Signature: ()V
*/

JNIEXPORT void JNICALL Java_ru_ifmo_gpo_x86_softwire_x86_init
(JNIEnv *env, jobject x86Iface)
{
    jclass javaClass = env->GetObjectClass(x86Iface);
    int handle = codegen::x86Wrapper::getFirstFreeHandle();

    codegen::x86Wrapper* wrapper = new codegen::x86Wrapper(handle); //will be autoadded on ctor

    jfieldID handleFieldId = env->GetFieldID(javaClass, "handle", "I");
    env->SetIntField(x86Iface, handleFieldId, handle);
}

/*
* Class:     ru_ifmo_gpo_x86_softwire_x86
* Method:    term
* Signature: ()V
*/
JNIEXPORT void JNICALL Java_ru_ifmo_gpo_x86_softwire_x86_term
(JNIEnv *env, jobject x86Iface)
{
    __try {
        codegen::x86Wrapper::dispose(env, x86Iface);
    } __except(common::defaultExceptionFilter(GetExceptionCode(), GetExceptionInformation())) {
        char errorMsg[128];
        codegen::x86Wrapper* errorRz = codegen::x86Wrapper::getFromMap(env, x86Iface);
        if (errorRz) {
            sprintf(errorMsg, "Fatal error when trying to dispose Assembler with handle %d", errorRz->getHandle());
        } else {
            sprintf(errorMsg, "Fatal error when trying to dispose non-existing Assembler object");
        }
        JVM_THROW_VOID(errorMsg);
    }
}

/*
* Class:     ru_ifmo_gpo_x86_softwire_x86
* Method:    applyInsn
* Signature: (I)V
*/
JNIEXPORT void JNICALL Java_ru_ifmo_gpo_x86_softwire_x86_applyInsn
(JNIEnv *env, jobject x86Iface, jint opcode)
{
    codegen::x86Wrapper* assembler;
    __try {
        assembler = codegen::x86Wrapper::getFromMap(env, x86Iface);
        if (!assembler){
            JVM_THROW_VOID("Failed to get assembler");
        }
        assembler->applyInstruction(opcode, env);
    } __except(common::defaultExceptionFilter(GetExceptionCode(), GetExceptionInformation())) {
        JVM_THROW_VOID("SEH exception");
    }
}

/*
* Class:     ru_ifmo_gpo_x86_softwire_x86
* Method:    applyUnaryInsn
* Signature: (ILjava/lang/Object;)V
*/
JNIEXPORT void JNICALL Java_ru_ifmo_gpo_x86_softwire_x86_applyUnaryInsn
(JNIEnv *env, jobject x86Iface, jint opcode, jobject operand)
{
    codegen::x86Wrapper* assembler;
    __try {
        assembler = codegen::x86Wrapper::getFromMap(env, x86Iface);
        if (!assembler){
            JVM_THROW_VOID("Failed to get assembler");
        }
        assembler->applyUnaryInstruction(opcode, env, operand);
    } __except(common::defaultExceptionFilter(GetExceptionCode(), GetExceptionInformation())) {
        JVM_THROW_VOID("SEH exception");
    }
}

/*
* Class:     ru_ifmo_gpo_x86_softwire_x86
* Method:    applyBinaryInsn
* Signature: (ILjava/lang/Object;Ljava/lang/Object;)V
*/
JNIEXPORT void JNICALL Java_ru_ifmo_gpo_x86_softwire_x86_applyBinaryInsn
(JNIEnv *env, jobject x86Iface, jint opcode, jobject first, jobject second)
{
    codegen::x86Wrapper* assembler;
    __try {
        assembler = codegen::x86Wrapper::getFromMap(env, x86Iface);
        if (!assembler){
            JVM_THROW_VOID("Failed to get assembler");
        }
        assembler->applyBinaryInstruction(opcode, env, first, second);
    } __except(common::defaultExceptionFilter(GetExceptionCode(), GetExceptionInformation())) {
        JVM_THROW_VOID("SEH exception");
    }
}

/*
* Class:     ru_ifmo_gpo_x86_softwire_x86
* Method:    generateCode0
* Signature: ()[B
*/
JNIEXPORT jint JNICALL Java_ru_ifmo_gpo_x86_softwire_x86_generateCode
(JNIEnv *env, jobject x86Iface)
{
    __try {
        codegen::x86Wrapper* assembler = codegen::x86Wrapper::getFromMap(env, x86Iface);
        if (!assembler){
            JVM_THROW("Failed to get assembler", 0);
        }
        return assembler->generateCode(env);
    } __except(common::defaultExceptionFilter(GetExceptionCode(), GetExceptionInformation())) {
        JVM_THROW("SEH exception", 0);
    }
}
