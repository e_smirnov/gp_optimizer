#include <interface/ru_ifmo_gpo_x86_x86CodeExecutionEnvironment.h>
#include <codegen/CodeExecutionEnvironment.h>
#include <common/SEH.h>
#include <common/JVMException.h>

/*
 * Class:     ru_ifmo_gpo_x86_x86CodeExecutionEnvironment
 * Method:    setToZero
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_ru_ifmo_gpo_x86_x86CodeExecutionEnvironment_setToZero
  (JNIEnv *env, jobject obj)
  {
      codegen::CodeExecutionEnvironment* execEnv = codegen::CodeExecutionEnvironment::getFromMap(env, obj);
      if (!execEnv) {
          JVM_THROW_VOID("Failed to find ExecutionEnvironment object");
      }
      execEnv->setToZero();
  }

/*
 * Class:     ru_ifmo_gpo_x86_x86CodeExecutionEnvironment
 * Method:    setToFF
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_ru_ifmo_gpo_x86_x86CodeExecutionEnvironment_setToFF
  (JNIEnv *env, jobject obj)
  {
      codegen::CodeExecutionEnvironment* execEnv = codegen::CodeExecutionEnvironment::getFromMap(env, obj);
      if (!execEnv) {
          JVM_THROW_VOID("Failed to find ExecutionEnvironment object");
      }
      execEnv->setToFF();
  }

/*
 * Class:     ru_ifmo_gpo_x86_x86CodeExecutionEnvironment
 * Method:    setRandomValues
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_ru_ifmo_gpo_x86_x86CodeExecutionEnvironment_setRandomValues
  (JNIEnv *env, jobject obj)
  {
      codegen::CodeExecutionEnvironment* execEnv = codegen::CodeExecutionEnvironment::getFromMap(env, obj);
      if (!execEnv) {
          JVM_THROW_VOID("Failed to find ExecutionEnvironment object");
      }
      execEnv->setRandom();
  }

/*
 * Class:     ru_ifmo_gpo_x86_x86CodeExecutionEnvironment
 * Method:    dispose
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_ru_ifmo_gpo_x86_x86CodeExecutionEnvironment_dispose
  (JNIEnv * env, jobject obj)
  {
      __try {
        codegen::CodeExecutionEnvironment::dispose(env, obj);
      } __except(common::defaultExceptionFilter(GetExceptionCode(), GetExceptionInformation())) {
          char errorMsg[128];
          codegen::CodeExecutionEnvironment* errorRz = codegen::CodeExecutionEnvironment::getFromMap(env, obj);
          if (errorRz) {
              sprintf(errorMsg, "Fatal error when trying to dispose ExecutionEnvironment with handle %d", errorRz->getHandle());
          } else {
              sprintf(errorMsg, "Fatal error when trying to dispose non-existing ExecutionEnvironment object");
          }
          JVM_THROW_VOID(errorMsg);
      }
  }