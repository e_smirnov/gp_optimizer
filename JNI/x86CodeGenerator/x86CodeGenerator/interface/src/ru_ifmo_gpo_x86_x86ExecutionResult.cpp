#include <interface/ru_ifmo_gpo_x86_x86ExecutionResult.h>
#include <executor/ExecutionResult.h>
#include <common/JVMException.h>
#include <common/SEH.h>
#include <stdio.h>
/*
* Class:     ru_ifmo_gpo_x86_x86ExecutionResult
* Method:    computeDifference0
* Signature: (Lru/ifmo/gpo/core/IExecutionResult;)D
*/
JNIEXPORT jdouble JNICALL Java_ru_ifmo_gpo_x86_x86ExecutionResult_computeDifference0
(JNIEnv *env, jobject firstExecRes, jobject secondExecRes)
{
    executor::ExecutionResult* first = executor::ExecutionResult::getFromMap(env, firstExecRes);
    executor::ExecutionResult* second = executor::ExecutionResult::getFromMap(env, secondExecRes);
    
    if (!first || !second) {
        JVM_THROW("Failed to find execution result", 0);
    }

    return first->computeDifference(*second);
}

/*
* Class:     ru_ifmo_gpo_x86_x86ExecutionResult
* Method:    maxPossibleDifference0
* Signature: ()D
*/
JNIEXPORT jdouble JNICALL Java_ru_ifmo_gpo_x86_x86ExecutionResult_maxPossibleDifference0
(JNIEnv *env, jobject execRes)
{
    executor::ExecutionResult* res = executor::ExecutionResult::getFromMap(env, execRes);

    if (!res) {
        JVM_THROW("Failed to find execution result", 0);
    }

    return res->maxPossibleDifference();
}

JNIEXPORT void JNICALL Java_ru_ifmo_gpo_x86_x86ExecutionResult_dispose
(JNIEnv *env, jobject obj)
{
    __try {
        executor::ExecutionResult::dispose(env, obj);
    } __except(common::defaultExceptionFilter(GetExceptionCode(), GetExceptionInformation())) {
        char errorMsg[128];
        executor::ExecutionResult* errorRz = executor::ExecutionResult::getFromMap(env, obj);
        if (errorRz) {
            sprintf(errorMsg, "Fatal error when trying to dispose ExecutionResult with handle %s", errorRz->getHandle());
        } else {
            sprintf(errorMsg, "Fatal error when trying to dispose non-existing ExecutionResult object");
        }
        JVM_THROW_VOID(errorMsg);
    }
}

JNIEXPORT jdouble JNICALL Java_ru_ifmo_gpo_x86_x86ExecutionResult_getExecutionResultVectorLength
(JNIEnv *env, jobject obj)
{
    executor::ExecutionResult* execRes = executor::ExecutionResult::getFromMap(env, obj);
    if (!execRes) {
        JVM_THROW("Failed to find execution result", 0);
    }
    return execRes->getVectorLength();
}