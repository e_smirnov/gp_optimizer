#ifndef ExecutionResult_h__
#define ExecutionResult_h__

#include <common/BaseInterfaceObject.h>
#include <common/MemoryAreaMap.h>

namespace executor
{
    class ExecutionResult: public common::BaseInterfaceObject<ExecutionResult>
    {
        common::MemoryAreaMap results;

    public:
        ExecutionResult(BaseInterfaceObject<ExecutionResult>::HandleType handle, const common::MemoryAreaMap& data);

        ~ExecutionResult();

        double computeDifference(const ExecutionResult& second);

        double maxPossibleDifference();

        double getVectorLength();
    };

}
#endif // ExecutionResult_h__