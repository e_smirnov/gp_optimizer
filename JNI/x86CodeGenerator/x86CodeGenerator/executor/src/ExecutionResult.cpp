#include <math.h>
#include <limits.h>

#include <executor/ExecutionResult.h>

using namespace executor;

ExecutionResult::ExecutionResult(BaseInterfaceObject<ExecutionResult>::HandleType handle, const common::MemoryAreaMap& data)
: BaseInterfaceObject<ExecutionResult>(handle)
, results(data)
{
}

ExecutionResult::~ExecutionResult()
{
}

double ExecutionResult::computeDifference(const ExecutionResult& second)
{
    double result = 0;
    for (common::MemoryAreaMap::const_iterator iter = results.begin(); iter != results.end(); ++iter) {
        //todo: write for non dword sizes
        common::MemoryAreaDescriptor* other = second.results.find(iter->first);
        if (!other) {
            result += pow((double)*((int*)iter->second->start), 2);
        } else {
            result += pow((double)(*((int*)iter->second->start) - *((int*)other->start)), 2);
        }
    }
    return sqrt(result);
}

double ExecutionResult::getVectorLength()
{
    double rz = 0;
    for (common::MemoryAreaMap::const_iterator iter = results.begin(); iter != results.end(); ++iter) {
        if (iter->first.find("_source")!=std::string::npos) {
            // Memory areas with names containing '_source' are input for registers, we are not interested in them
            continue;
        }
        rz += pow((double)*((int*)iter->second->start), 2);
    }
    return sqrt(rz);
}

double ExecutionResult::maxPossibleDifference()
{
    return sqrt(pow((double)INT_MAX, 2) * results.size());
}
