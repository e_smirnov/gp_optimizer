import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import ru.ifmo.gpo.core.GPOptimizer;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.IGenericInstruction;
import ru.ifmo.gpo.core.instructions.generic.NumericConstant;
import ru.ifmo.gpo.core.statistics.OptimizationReport;
import ru.ifmo.gpo.java.JavaTargetArchitecture;
import ru.ifmo.gpo.java.instructions.JVMInstruction;
import ru.ifmo.gpo.java.instructions.JavaOpcodes;
import ru.ifmo.gpo.java.vmstate.LocalVariableSlot;
import ru.ifmo.gpo.shader.ps_1_1.PixelShaderTargetArchitecture;
import ru.ifmo.gpo.shader.ps_1_1.parser.ShaderParseException;
import ru.ifmo.gpo.shader.ps_1_1.parser.ShaderParser;
import ru.ifmo.gpo.x86.state.x86ConstantOperand;
import ru.ifmo.gpo.x86.state.x86MemoryOperand;
import ru.ifmo.gpo.x86.state.x86Register;
import ru.ifmo.gpo.x86.x86Instruction;
import ru.ifmo.gpo.x86.x86Opcodes;
import ru.ifmo.gpo.x86.x86TargetArchitecture;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 * Created by IntelliJ IDEA.
 * User: jedi-philosopher
 * Date: 08.11.2009
 * Time: 15:33:11
 * <p/>
 * Main method here
 */
public class main {

    static int MAX_GENERATIONS = 200;

    private static InstructionSequence createReferenceChromosome3() {
        // from biginteger.java
        // x0 >> 1 -> x0; (x3 >> 1) XOR x3 AND 0x1 -> x3
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        sequence.add(new JVMInstruction(JavaOpcodes.ISHR));
        sequence.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(3)));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(3)));
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        sequence.add(new JVMInstruction(JavaOpcodes.ISHR));
        sequence.add(new JVMInstruction(JavaOpcodes.IXOR));
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        sequence.add(new JVMInstruction(JavaOpcodes.IAND));

        return sequence;
    }

    private static InstructionSequence createReferenceChromosome4() {
        // from biginteger.java
        // (x ^ -1) & y
        // more optimal is (x&y)^y
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_M1));
        sequence.add(new JVMInstruction(JavaOpcodes.IXOR));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(10)));
        sequence.add(new JVMInstruction(JavaOpcodes.IAND));

        return sequence;
    }


    private static InstructionSequence InstructionSequence() {

        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.IADD));
        sequence.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));

        return sequence;
    }

    private static InstructionSequence createReferenceChromosome() {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.IMUL));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.IMUL));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.IMUL));
        sequence.add(new JVMInstruction(JavaOpcodes.IADD));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.IADD));
        return sequence;
    }

    private static InstructionSequence createFloatRefChromosome() {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.FMUL));
        sequence.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.FMUL));
        sequence.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.FMUL));
        sequence.add(new JVMInstruction(JavaOpcodes.FADD));
        sequence.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.FADD));

        return sequence;
    }

    private static InstructionSequence createX86InstructionSequece() {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new x86Instruction(x86Opcodes.XOR, new x86Register(x86Opcodes.eax), new x86ConstantOperand(-1)));
        sequence.add(new x86Instruction(x86Opcodes.AND, new x86Register(x86Opcodes.eax), new x86Register(x86Opcodes.ebx)));
        return sequence;
    }

    private static InstructionSequence createx86CommonSubexpression() {
        // x <- a + b
        // y <- a + b
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.eax), new x86MemoryOperand("0xaaaaaa", 4)));
        sequence.add(new x86Instruction(x86Opcodes.ADD, new x86Register(x86Opcodes.eax), new x86MemoryOperand("0xbbbbbb", 4)));
        sequence.add(new x86Instruction(x86Opcodes.MOV, new x86MemoryOperand("0xXXXXXX", 4), new x86Register(x86Opcodes.eax)));
        sequence.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.eax), new x86MemoryOperand("0xaaaaaa", 4)));
        sequence.add(new x86Instruction(x86Opcodes.ADD, new x86Register(x86Opcodes.eax), new x86MemoryOperand("0xbbbbbb", 4)));
        sequence.add(new x86Instruction(x86Opcodes.MOV, new x86MemoryOperand("0xYYYYYY", 4), new x86Register(x86Opcodes.eax)));
        return sequence;
    }

    private static InstructionSequence createJVMConstantFolding() {
        // x + 2 + (-1 * 3)
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_2));
        sequence.add(new JVMInstruction(JavaOpcodes.IADD));

        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_M1));
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_3));
        sequence.add(new JVMInstruction(JavaOpcodes.IMUL));
        sequence.add(new JVMInstruction(JavaOpcodes.IADD));
        return sequence;
    }

    private static InstructionSequence createX86InstructionSequece2() {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.eax), new x86MemoryOperand("0xdeadbeef", 4)));
        sequence.add(new x86Instruction(x86Opcodes.MOV, new x86MemoryOperand("0xdeadbeef", 4), new x86Register(x86Opcodes.eax))); // should remove this instruction as it is obviously useless
        return sequence;
    }

    // this chromosome causes fail
    // Exception in thread "main" java.lang.VerifyError: (class: Gene_235, method: run signature: (IIIILjava/util/LinkedList;)V) Unable to pop operand off an empty stack
    private static InstructionSequence createIntFromBenchmark() {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_0));
        sequence.add(new JVMInstruction(JavaOpcodes.IAND));
        sequence.add(new JVMInstruction(JavaOpcodes.DUP));
        sequence.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(1)));
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        sequence.add(new JVMInstruction(JavaOpcodes.IXOR));
        sequence.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(2)));
        return sequence;
    }

    // also causes fail
    private static InstructionSequence createIntFromBenchmark2() {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        sequence.add(new JVMInstruction(JavaOpcodes.IAND));
        sequence.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(2)));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(2)));
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        sequence.add(new JVMInstruction(JavaOpcodes.IXOR));
        sequence.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(3)));
        return sequence;
    }

    private static InstructionSequence createIntFromBenchmark3() {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_2));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.IMUL));
        sequence.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(1)));
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_2));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(2)));
        sequence.add(new JVMInstruction(JavaOpcodes.IADD));
        sequence.add(new JVMInstruction(JavaOpcodes.IMUL));
        sequence.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(3)));
        return sequence;
    }

    private static InstructionSequence createFromServerTest() {
        //ILOAD 0, ILOAD 0, IMUL, ILOAD 0, IMUL, ILOAD 0, IMUL, ISTORE 2, FLOAD 1
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.IMUL));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.IMUL));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.IMUL));
        sequence.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(2)));
        sequence.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(1)));
        return sequence;
    }

    private static InstructionSequence anotherFromBigInteger() {
        /*ICONST_1
          ILOAD [1]
          BIPUSH [31]
          IAND
          ISHL
          IOR*/
        // optimized -> iconst_1, iload 0, ishl, ior 
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.BIPUSH, new NumericConstant(31)));
        sequence.add(new JVMInstruction(JavaOpcodes.IAND));
        sequence.add(new JVMInstruction(JavaOpcodes.ISHL));
        sequence.add(new JVMInstruction(JavaOpcodes.IOR));
        return sequence;
    }

    private static InstructionSequence shaderTest() throws ShaderParseException {
        ShaderParser p = new ShaderParser();
        return p.parse("ps_2_0\ndef c0, 1.0, 2.0, 3.0, 4.0\nmov oC0, c0");
    }

    private static InstructionSequence glowShader() throws ShaderParseException {
        ShaderParser p = new ShaderParser();
        return p.parse("ps_2_0\n" +
                "dcl t0.xyzw\n" +
                "dcl t1.xyzw\n" +
                "dcl t2.xyzw\n" +
                "dcl t3.xyzw\n" +
                "dcl t4.xyzw\n" +
                "dcl t5.xyzw\n" +
                "dcl t6.xyzw\n" +
                "dcl t7.xyzw\n" +
                "dcl_2d s0\n" +
                "texld r0, t0, s0\n" +
                "texld r1, t1, s0\n" +
                "mul   r0, r0, c0\n" +
                "mad   r0, r1, c1, r0\n" +
                "texld r1, t2, s0\n" +
                "texld r2, t3, s0\n" +
                "mad   r0, r1, c2, r0\n" +
                "mad   r0, r2, c3, r0\n" +
                "texld r1, t4, s0\n" +
                "texld r2, t5, s0\n" +
                "mad   r0, r1, c4, r0\n" +
                "mad   r0, r2, c5, r0\n" +
                "texld r1, t6, s0\n" +
                "texld r2, t7, s0\n" +
                "mad   r0, r1, c6, r0\n" +
                "mad   r0, r2, c7, r0\n" +
                "mov oC0, r0");
    }

    private static InstructionSequence nebulaLightShader() throws ShaderParseException {
        return new ShaderParser().parse(" ps_2_0\n" +
                " def c0, 1, 0, 0, 0\n" +
                " dcl t5.xy\n" +
                " dcl_2d s0\n" +
                " texld r0, t5, s0\n" +
                " mov r1.x, t5.xxxx\n" +
                " mov r1.y, t5.yyyy\n" +
                " texld r1, r1, s0\n" +
                " mul r0.xyz, r0, r1.wwww\n" +
                " mov r0.w, c0.xxxx\n" +
                " mov oC0, r0");
    }

    private static InstructionSequence furShader() throws ShaderParseException {
        return new ShaderParser().parse("    ps_2_0\n" +
                "    dcl t0\n" +
                "    dcl t1.xyz\n" +
                "    dcl_2d s0\n" +
                "    dcl_2d s1\n" +
                "    texld r0, t0, s1\n" +
                "    mov r1, t0.w\n" +
                "    mul r0.x, t0.z, t0.z\n" +
                "    mul r0.x, r0.x, r0.x\n" +
                "    mul r2.xy, t0, c0.w\n" +
                "    texld r2, r2, s1\n" +
                "    mad r2.w, r2.x, c0.z, -r0.x\n" +
                "    mul r0.w, r0.w, r2.w\n" +
                "    mov r0, r0.w\n" +
                //"    texkill r1\n" +
                //"    texkill r0\n" +
                "    texld r1, t0, s0\n" +
                "    lrp r3.xyz, c0.y, r1, r2\n" +
                "    mul r0.xyz, r3, t1\n" +
                "    mov oC0, r0");
    }

    public static void main(String[] args) throws ShaderParseException, IOException {
        BasicConfigurator.configure();
        Logger.getLogger("ru.ifmo.gpo.core.operators").setLevel(Level.INFO);
        //GPOptimizer x86Optimizer = new GPOptimizer(new x86TargetArchitecture());
        GPOptimizer jvmOptimizer = new GPOptimizer(new JavaTargetArchitecture());
        //GPOptimizer shaderOptimizer = new GPOptimizer(new PixelShaderTargetArchitecture());
        //InstructionSequence rz = x86Optimizer.optimize(createX86InstructionSequece(), 250);
        OptimizationReport report = jvmOptimizer.optimizeAndGetReport(createReferenceChromosome(), 550);
        //OptimizationReport report = shaderOptimizer.optimizeAndGetReport(glowShader(), Integer.MAX_VALUE);

        File outputDir = new File("./out/reports");
        if (!outputDir.exists()) {
            outputDir.mkdirs();
        }
        final String filePath = String.format(outputDir.getAbsolutePath() + File.separator + "report_" + new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss").format(System.currentTimeMillis()) + ".json");
        report.dumpToJSONFile(filePath);

        System.out.println("========== Results that passed verification: ============");
        for (String rz : report.verifiedResults) {
            System.out.println(rz);
        }
        System.out.println("==========================================================");

        System.out.println();

        if (report.bestResult != null) {
            System.out.println("++++++++++++++Best result code+++++++++++++++++");
            for (IGenericInstruction g : report.bestResult) {
                System.out.println(g.toString());
            }
            System.out.println("+++++++++++++++++++++++++++++++++++++++++++");

            System.out.println("");
        }


    }
}
