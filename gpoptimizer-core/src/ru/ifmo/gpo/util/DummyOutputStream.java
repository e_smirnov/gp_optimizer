package ru.ifmo.gpo.util;

import java.io.IOException;
import java.io.OutputStream;

/**
 * User: e_smirnov
 * Date: 13.09.2010
 * Time: 15:47:36
 * <p/>
 * Dummy ouput stream that does not produce any real output. Needed as stub.
 */
public class DummyOutputStream extends OutputStream {

    @Override
    public void write(int b) throws IOException {
        // do nothing
    }

}
