package ru.ifmo.gpo.util;

import jargs.gnu.CmdLineParser;

import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

public class ClassPathProcessor {

    private CmdLineParser.Option classpath;

    private CmdLineParser cmdParser;

    private String defaultPath;

    private String classPathValue;

    private Collection<File> collectedFiles;

    public ClassPathProcessor(CmdLineParser cmdParser) {
        this.cmdParser = cmdParser;
        this.classpath = cmdParser.addStringOption("classpath");
        this.defaultPath = null;
    }

    public Collection<File> getFiles() {
        if (collectedFiles != null) {
            return collectedFiles;
        }

        String classPathName = getClassPath();
        if (classPathName == null) {
            System.err.println("No classpath argument specified");
            return Collections.emptyList();
        }
        collectedFiles = new HashSet<File>();
        collectFiles(new File(classPathName), collectedFiles);
        return collectedFiles;
    }

    public String getClassPath() {
        if (classPathValue != null) {
            return classPathValue;
        }
        classPathValue = (String) cmdParser.getOptionValue(classpath, defaultPath); // this method can be called only once, as it removes (???) option value
        return classPathValue;
    }

    private void collectFiles(File folder, Collection<File> output) {
        if (!folder.exists() || !folder.isDirectory()) {
            System.err.println("Folder " + folder.getName()
                    + " can not be found");
            System.exit(3);
        }
        ClassFileFilter filter = new ClassFileFilter();
        for (File f : folder.listFiles()) {
            if (f.isFile() && filter.accept(folder, f.getName())) {
                output.add(f);
            } else if (f.isDirectory()) {
                collectFiles(f, output);
            }
        }
    }
}
