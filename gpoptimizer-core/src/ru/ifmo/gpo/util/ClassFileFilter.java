package ru.ifmo.gpo.util;

import java.io.File;
import java.io.FilenameFilter;

/**
 * User: jedi-philosopher
 * Date: 05.12.2010
 * Time: 21:57:11
 */
public class ClassFileFilter implements FilenameFilter {
    @Override
    public boolean accept(File dir, String name) {
        return name.endsWith(".class");
    }
}
