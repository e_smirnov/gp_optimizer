/**
 * User: jedi-philosopher
 * Date: 13.09.2010
 * Time: 23:36:06
 */
package ru.ifmo.gpo.util;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Simple LRU cache
 */
public class ObjectCache<Key, Val> extends LinkedHashMap<Key, Val> {
    private final int MAX_ENTRIES;
    private static final long serialVersionUID = -8097652224960287817L;

    public ObjectCache(int initialCapacity) {
        super(initialCapacity);
        MAX_ENTRIES = initialCapacity;
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry eldest) {
        return size() > MAX_ENTRIES;
    }
}
