package ru.ifmo.gpo.util;

/**
 * User: jedi-philosopher
 * Date: 11.09.11
 * Time: 22:08
 */
public class StatUtils {

    /**
     * Returns average of elements, excluding ones that are erroneous (differ too much from median)
     *
     * @param times  List of values
     * @param offset Index of first element to start from
     */
    public static long avgWithErrorCorrection(long[] times, int offset) {
        long rz = 0;
        // excludes evidently erroneous results

        double y = 0.0;
        int validTimes = 0;

        for (int i = offset; i < times.length; ++i) {
            if (times[i] != -1) {
                y += times[i];
                validTimes++;
            }
        }
        if (validTimes == 1) {
            return (long) y;
        }
        if (validTimes == 0) {
            return -1;
        }

        y /= validTimes;

        while (true) {
            double S2 = 0;
            double maxDiff = -1;
            int diffIdx = -1;
            validTimes = 0;

            for (int i = offset; i < times.length; ++i) {
                if (times[i] == -1) {
                    continue;
                }
                long l = times[i];
                S2 += Math.pow(l - y, 2);
                validTimes++;

                if (Math.abs(l - y) > maxDiff) {
                    maxDiff = Math.abs(l - y);
                    diffIdx = i;
                }
            }
            if (validTimes <= 2) {
                break;
            }

            S2 *= 1 / (double) (validTimes - 1);

            double U = maxDiff / Math.sqrt(S2);
            if (U > 1.2) {
                times[diffIdx] = -1;

            } else {
                break;
            }
        }


        for (int i = offset; i < times.length; ++i) {
            if (times[i] != -1) {
                rz += times[i];
            }
        }
        return rz / validTimes;
    }

}
