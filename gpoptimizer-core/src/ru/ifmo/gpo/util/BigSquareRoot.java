package ru.ifmo.gpo.util;

import java.math.BigDecimal;
import java.math.BigInteger;


/**
 * The following source code (BigSquareRoot.java) is free for you to use in whatever way you wish, with no restrictions and no guarantees.
 * Improvements and constructive suggestions are welcome. Note that, when computing very large square roots, you should use a large scale.
 * For numbers of 100 digits, I use a scale of 50.
 * Compute square root of large numbers using Heron's method
 * Implementation taken from http://www.merriampark.com/bigsqrt.htm
 */
@SuppressWarnings("unused")
public class BigSquareRoot {

    private static BigDecimal ZERO = BigDecimal.valueOf(0);
    private static BigDecimal ONE = BigDecimal.valueOf(1);
    private static BigDecimal TWO = BigDecimal.valueOf(2);
    public static final int DEFAULT_MAX_ITERATIONS = 50;
    public static final int DEFAULT_SCALE = 10;

    private static BigDecimal error;
    private static int iterations;
    private static int scale = DEFAULT_SCALE;
    private static int maxIterations = DEFAULT_MAX_ITERATIONS;

    //---------------------------------------
    // The error is the original number minus
    // (sqrt * sqrt). If the original number
    // was a perfect square, the error is 0.
    //---------------------------------------

    public BigDecimal getError() {
        return error;
    }

    //-------------------------------------------------------------
    // Number of iterations performed when square root was computed
    //-------------------------------------------------------------

    public int getIterations() {
        return iterations;
    }

    //------
    // Scale
    //------

    public static int getScale() {
        return scale;
    }

    public static void setScale(int s) {
        scale = s;
    }

    //-------------------
    // Maximum iterations
    //-------------------

    public int getMaxIterations() {
        return maxIterations;
    }

    public void setMaxIterations(int m) {
        maxIterations = m;
    }

    //--------------------------
    // Get initial approximation
    //--------------------------

    private static BigDecimal getInitialApproximation(BigDecimal n) {
        BigInteger integerPart = n.toBigInteger();
        int length = integerPart.toString().length();
        if ((length % 2) == 0) {
            length--;
        }
        length /= 2;
        return ONE.movePointRight(length);
    }

    //----------------
    // Get square root
    //----------------

    public static BigDecimal sqrt(BigInteger n) {
        return sqrt(new BigDecimal(n));
    }

    public static BigDecimal sqrt(BigDecimal n) {

        if (n.compareTo(ZERO) == 0) {
            return ZERO;
        }
        // Make sure n is a positive number
        if (n.compareTo(ZERO) < 0) {
            throw new IllegalArgumentException("Can not compute square root from negative value " + n);
        }

        BigDecimal initialGuess = getInitialApproximation(n);
        BigDecimal lastGuess;
        BigDecimal guess = new BigDecimal(initialGuess.toString());

        // Iterate

        iterations = 0;
        boolean more = true;
        while (more) {
            lastGuess = guess;
            guess = n.divide(guess, scale, BigDecimal.ROUND_HALF_UP);
            guess = guess.add(lastGuess);
            guess = guess.divide(TWO, scale, BigDecimal.ROUND_HALF_UP);
            error = n.subtract(guess.multiply(guess));
            if (++iterations >= maxIterations) {
                more = false;
            } else if (lastGuess.equals(guess)) {
                more = error.abs().compareTo(ONE) >= 0;
            }
        }
        return guess;

    }

    //----------------------
    // Get random BigInteger
    //----------------------

    public static BigInteger getRandomBigInteger(int nDigits) {
        StringBuffer sb = new StringBuffer();
        java.util.Random r = new java.util.Random();
        for (int i = 0; i < nDigits; i++) {
            sb.append(r.nextInt(10));
        }
        return new BigInteger(sb.toString());
    }

}