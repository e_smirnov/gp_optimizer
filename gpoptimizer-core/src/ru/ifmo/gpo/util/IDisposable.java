/**
 * User: jedi-philosopher
 * Date: 06.01.2011
 * Time: 20:20:07
 */
package ru.ifmo.gpo.util;

/**
 * Interface for classes that may contain native resources that must be closed explicitly
 */
public interface IDisposable {

    public void dispose();

}
