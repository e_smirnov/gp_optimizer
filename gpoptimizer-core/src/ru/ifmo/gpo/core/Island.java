/**
 * User: jedi-philosopher
 * Date: 15.09.2010
 * Time: 22:31:37
 */
package ru.ifmo.gpo.core;

import org.jgap.Genotype;
import org.jgap.IChromosome;
import org.jgap.InvalidConfigurationException;
import org.jgap.Population;
import ru.ifmo.gpo.core.history.ChromosomeOrigin;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.operators.CodeCrossoverOperation;
import ru.ifmo.gpo.core.population.IPopulationGenerator;
import ru.ifmo.gpo.core.population.PopulationGenerationFailedException;
import ru.ifmo.gpo.core.population.generators.ClonePopulationGenerator;
import ru.ifmo.gpo.core.population.generators.RandomPopulationGenerator;
import ru.ifmo.gpo.core.statistics.StatsManager;

import java.util.LinkedList;
import java.util.List;

/**
 * Genetic process uses 'island model', where several evolutions are run in parallel independently, each of them on a separate
 * 'island'. Different islands may have different configurations. Islands may exchange chromosomes from time to time.
 */
public class Island {
    /**
     * Unique island name
     */
    private String name;

    /**
     * Configuration used on this island
     */
    private GPConfiguration conf;

    /**
     * Island population
     */
    private Genotype population;

    private StatsManager statistics = new StatsManager();

    private IPopulationGenerator populationGenerator;

    public Island(GPConfiguration conf, String name) {
        this.name = name;
        this.conf = conf;
    }

    public String getName() {
        return name;
    }

    /**
     * Sets up random population for this island - every chromosome is unique and random. Most common case.
     */
    public void setUpRandomPopulation() throws InvalidConfigurationException, PopulationGenerationFailedException {
        populationGenerator = new RandomPopulationGenerator(conf);
        population = new Genotype(conf, new Population(conf, populationGenerator.generatePopulation()));
    }

    /**
     * Sets up population where all individuals are copies of a reference instruction sequence
     */
    public void setUpPopulationWithAllEqualChromosomes() throws InvalidConfigurationException, PopulationGenerationFailedException {
        populationGenerator = new ClonePopulationGenerator(conf);
        population = new Genotype(conf, new Population(conf, populationGenerator.generatePopulation()));
    }

    /**
     * Sets up random population and adds to it ONE copy of a reference sequence
     */
    public void setUpPopulationWithOneRefCopy() throws InvalidConfigurationException, PopulationGenerationFailedException {
        populationGenerator = new RandomPopulationGenerator(conf);
        IChromosome[] randomPopulation = populationGenerator.generatePopulation();
        randomPopulation[0] = conf.getRefChromosome().toChromosome(conf);
        ChromosomeOrigin.fromInitialGeneration(randomPopulation[0]);
        population = new Genotype(conf, new Population(conf, randomPopulation));
    }

    public IPopulationGenerator getPopulationGenerator() {
        return populationGenerator;
    }

    /**
     * Performs one iteration of an evolution
     */
    public void processGeneration(int generationNmb) {
        population.evolve();
        statistics.processGeneration(population, conf);

        for (IGenerationEventListener listener : conf.getListeners()) {
            listener.generationProcessed(this, generationNmb);
        }

        conf.getStagnationMonitor().iterationProcessed(conf, population.getPopulation(), conf.getFitnessFunc().getFitnessValue(population.getFittestChromosome()));
    }

    public Genotype getPopulation() {
        return population;
    }

    /**
     * Exchanges single random chromosome with another island
     */
    public void exchangeSingleChromosome(Island otherIsland) {
        int ourCandidateIndex = (int) (Math.random() * population.getPopulation().size());
        int theirCandidateIndex = (int) (Math.random() * otherIsland.getPopulation().getPopulation().size());

        IChromosome ourCandidate = population.getPopulation().getChromosome(ourCandidateIndex);

        population.getPopulation().setChromosome(ourCandidateIndex, otherIsland.getPopulation().getPopulation().getChromosome(theirCandidateIndex));
        otherIsland.getPopulation().getPopulation().setChromosome(theirCandidateIndex, ourCandidate);
    }

    /**
     * Crossovers single chromosome with mate from another island
     */
    public void crossoverSingleChromosome(Island otherIsland) {
        try {
            CodeCrossoverOperation operation = new CodeCrossoverOperation(conf, 1);

            int ourCandidateIndex = (int) (Math.random() * population.getPopulation().size());
            int theirCandidateIndex = (int) (Math.random() * otherIsland.getPopulation().getPopulation().size());

            IChromosome ourCandidate = population.getPopulation().getChromosome(ourCandidateIndex);
            IChromosome theirCandidate = otherIsland.getPopulation().getPopulation().getChromosome(theirCandidateIndex);

            List rz = new LinkedList();
            operation.doCrossover(ourCandidate, theirCandidate, rz, conf.getRandomGenerator());
            if (rz.isEmpty()) {
                return;
            }
            population.getPopulation().setChromosome(ourCandidateIndex, (IChromosome) rz.get(0));
            otherIsland.getPopulation().getPopulation().setChromosome(theirCandidateIndex, (IChromosome) rz.get(1));

        } catch (InvalidConfigurationException ignore) {
        }
    }

    public StatsManager getStats() {
        return statistics;
    }

    public SolutionStorage getSolutions() {
        return conf.getSolutions();
    }

    public InstructionSequence getRefSequence() {
        return conf.getRefChromosome();
    }

    public GPConfiguration getConfiguration() {
        return conf;
    }
}
