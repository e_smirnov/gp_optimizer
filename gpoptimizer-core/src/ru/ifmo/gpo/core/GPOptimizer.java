package ru.ifmo.gpo.core;

import org.apache.commons.lang.time.StopWatch;
import org.jgap.IChromosome;
import org.jgap.InvalidConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.population.PopulationGenerationFailedException;
import ru.ifmo.gpo.core.statistics.OptimizationReport;
import ru.ifmo.gpo.verify.BaseVerifier;
import ru.ifmo.gpo.verify.IChromosomeVerifier;
import ru.ifmo.gpo.verify.VerificationResult;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * User: jedi-philosopher
 * Date: 02.02.2011
 * Time: 22:18:59
 * <p/>
 * All-in-one optimizer. Takes target architecture provider and source instruction sequence and performs all steps of optimization and
 * verification, returning single best result.
 */
public class GPOptimizer {

    private Logger logger = LoggerFactory.getLogger(GPOptimizer.class);

    private ITargetArchitecture arch;

    // optimizer uses island model with 2 islands that have slightly different configurations
    private Island firstIsland;

    private Island secondIsland;

    private boolean saveStatistics = false;

    /**
     * Date format used in names of statistic files
     */
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy_mm_dd_HH-mm-ss");

    public GPOptimizer(ITargetArchitecture targetArchitecture) {
        this.arch = targetArchitecture;
    }

    private void setUpConfiguration(InstructionSequence source) throws InvalidConfigurationException, PopulationGenerationFailedException {

        final GPConfiguration mainIslandConf = new MainIslandGPConfiguration(arch);
        mainIslandConf.setReferenceChromosome(source);
        mainIslandConf.setUpConfiguration();
        firstIsland = new Island(mainIslandConf, "Main island");

        final GPConfiguration secondIslandConf = new SupportIslandGPConfiguration(arch);
        secondIslandConf.setReferenceChromosome(source);
        secondIslandConf.setUpConfiguration();
        secondIsland = new Island(secondIslandConf, "Second island");

        logger.info("Islands created, generating initial population");

        firstIsland.setUpRandomPopulation();
        secondIsland.setUpRandomPopulation();

        logger.info("Population generated");
    }

    private void processIteration(int iterationIdx) {
        logger.info("Processing generation #" + iterationIdx + "...");

        StopWatch iterationTime = new StopWatch();
        iterationTime.start();

        firstIsland.processGeneration(iterationIdx);
        secondIsland.processGeneration(iterationIdx);

        IChromosome bestFromFirst = firstIsland.getPopulation().getFittestChromosome();
        IChromosome bestFromSecond = secondIsland.getPopulation().getFittestChromosome();
        double d1 = firstIsland.getConfiguration().getFitnessFunc().getFitnessValue(bestFromFirst);
        double d2 = secondIsland.getConfiguration().getFitnessFunc().getFitnessValue(bestFromSecond);

        iterationTime.stop();

        logger.info("Best fitness value for generation is {} in first island, {} in second", d1, d2);
        logger.debug("Iteration completed in {} ms", iterationTime.getTime());
    }


    private void saveStatistics() {
        logger.info("Saving statistics");
        try {
            final String timeSuffix = dateFormat.format(new Date());
            firstIsland.getStats().saveToCSV("first_island_" + timeSuffix + ".csv");
            secondIsland.getStats().saveToCSV("second_island_" + timeSuffix + ".csv");
        } catch (IOException ex) {
            logger.warn("Failed to save stats: ", ex);
        }
    }

    /**
     * Enables or disables statistics saving. Statistics contains various data about genetic process - avg fitness values, chromosome count,
     * clone number per generation etc. Statistics is saved into .csv files. Statistics is disabled by default
     *
     * @param saveStatistics true to enable stats, false to disable
     */
    public void setSaveStatistics(boolean saveStatistics) {
        this.saveStatistics = saveStatistics;
    }

    /**
     * Performs optimization of a given instruction sequence, using given limit of iterations. Limit of 100-200 is suitable for small sequences
     * of 3-4 instructions, 300-500 is average for long sequences of 9-10 instructions.
     *
     * @param source         Instruction sequence to optimize
     * @param maxGenerations Iteration limit
     * @return Report for optimization process. Best result is stored in 'bestResult' field, if it is null then optimization has failed
     */
    public OptimizationReport optimizeAndGetReport(InstructionSequence source, int maxGenerations) {
        logger.info("Optimization process started");

        try {
            setUpConfiguration(source);
        } catch (InvalidConfigurationException ex) {
            logger.error("Failed to set up configuration", ex);
            return null;
        } catch (PopulationGenerationFailedException e) {
            logger.error("Failed to set up initial population", e);
            return null;
        }

        logger.info("Genetic process started");

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        int i;
        for (i = 0; i < maxGenerations; ++i) {
            processIteration(i);
        }

        if (saveStatistics) {
            saveStatistics();
        }

        logger.info("Genetic process finished in " + (stopWatch.getTime() / 1000) + " seconds");

        logger.info("Verifying results...");

        IChromosomeVerifier verifier = BaseVerifier.createVerifierFromConfig(this.arch);

        SolutionStorage resultStorage = firstIsland.getSolutions();
        resultStorage.merge(secondIsland.getSolutions());

        List<InstructionSequence> verificationFails = new LinkedList<InstructionSequence>();

        InstructionSequence bestSolution = null;
        for (Iterator<InstructionSequence> iter = resultStorage.iterator(); iter.hasNext(); ) {
            InstructionSequence candidate = iter.next();
            VerificationResult rz = verifier.verifyEquivalence(candidate, new InstructionSequence(firstIsland.getRefSequence()), firstIsland.getConfiguration().getStateSchema());
            if (rz != VerificationResult.RESULT_SUCCESS) {
                iter.remove();
                verificationFails.add(candidate);
            } else if (bestSolution == null) {
                bestSolution = candidate;
            }
        }
        if (resultStorage.isEmpty()) {
            logger.warn("No optimized solution found");
            final int solutionsFound = verificationFails.size();
            if (solutionsFound > 0) {
                logger.info(solutionsFound + " solutions were found, but failed to pass verification");
            }
        } else {
            logger.info("Verification is successful");
        }

        return new OptimizationReport(source, i, resultStorage, bestSolution, verificationFails);
    }

    public InstructionSequence optimize(InstructionSequence source, int maxGenerations) {
        return optimizeAndGetReport(source, maxGenerations).bestResult;
    }

}
