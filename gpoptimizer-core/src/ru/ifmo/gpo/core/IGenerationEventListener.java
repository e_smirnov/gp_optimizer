package ru.ifmo.gpo.core;

/**
 * User: jedi-philosopher
 * Date: 11.09.11
 * Time: 22:16
 */
public interface IGenerationEventListener {

    public void generationProcessed(Island island, int generationNumber);

}
