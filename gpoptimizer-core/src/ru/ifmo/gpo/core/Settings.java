package ru.ifmo.gpo.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Properties;

/**
 * User: jedi-philosopher
 * Date: 04.11.2010
 * Time: 17:31:53
 */
public class Settings {

    private static Properties properties;

    private static Logger logger;

    private static boolean debugCrossover;

    private static Integer maxSequenceLengthMultiplier;

    private static Integer retryCount;

    static {
        logger = LoggerFactory.getLogger(Settings.class);
        properties = new Properties();
        try {
            properties.load(Settings.class.getResourceAsStream("gpoptimizer.properties"));
        } catch (IOException e) {
            logger.error("Failed to read properties: " + e);
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        debugCrossover = properties.getProperty("core.debug.debugCrossover").equals("true");
        maxSequenceLengthMultiplier = Integer.parseInt(properties.getProperty("core.genetics.maxSequenceLengthMultiplier"));
        retryCount = Integer.parseInt(properties.getProperty("core.genetics.retryCount"));
    }

    public static boolean debugCrossover() {
        return debugCrossover;
    }

    public static boolean debugMutation() {
        return properties.getProperty("core.debug.debugMutation").equals("true");
    }

    public static boolean trackOrigin() {
        return properties.getProperty("core.debug.trackChromosomeOrigin").equals("true");
    }

    public static boolean useDebugNativeLibraries() {
        return properties.getProperty("core.debug.useDebugNativeLibraries").equals("true");
    }

    public static boolean dumpResult() {
        return properties.getProperty("test.dumpResult").equals("true");
    }

    public static String getLogLevel() {
        return properties.getProperty("core.logging.logLevel");
    }

    public static boolean verifyGeneratedClasses() {
        return properties.getProperty("core.debug.verifyClasses").equals("true");
    }

    public static Integer getMaxSequenceLengthMultiplier() {
        return maxSequenceLengthMultiplier;
    }

    public static String getProperty(String key) {
        return properties.getProperty(key);
    }

    public static Integer getRetryCount() {
        return retryCount;
    }

    public static String getOutputDirName() {
        return properties.getProperty("core.outputDir");
    }

    public static Integer getMaxFailures() {
        return Integer.parseInt(properties.getProperty("core.genetics.maxFailureCount", "1"));
    }

    public static Properties getProperties() {
        return properties;
    }
}
