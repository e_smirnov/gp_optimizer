/**
 * User: e_smirnov
 * Date: 02.12.2010
 * Time: 16:18:02
 */
package ru.ifmo.gpo.core.history;

import org.jgap.IChromosome;
import ru.ifmo.gpo.core.operators.BaseCodeCrossoverOperation;

/**
 * Information about crossover origin of a chromosome. Contains both parents and crossover point.
 */
public class CrossoverOrigin extends ChromosomeOrigin {
    private IChromosome firstParent;
    private IChromosome secondParent;

    private BaseCodeCrossoverOperation.CrossoverPoint crossoverPoint;

    public CrossoverOrigin(IChromosome chromosome, IChromosome firstParent, IChromosome secondParent, BaseCodeCrossoverOperation.CrossoverPoint crossoverPoint) {
        super(OriginType.ORIGIN_CROSSOVER_RESULT, chromosome);
        this.firstParent = firstParent;
        this.secondParent = secondParent;
        this.crossoverPoint = crossoverPoint;
    }

    public IChromosome getFirstParent() {
        return firstParent;
    }

    public IChromosome getSecondParent() {
        return secondParent;
    }

    public BaseCodeCrossoverOperation.CrossoverPoint getCrossoverPoint() {
        return crossoverPoint;
    }
}
