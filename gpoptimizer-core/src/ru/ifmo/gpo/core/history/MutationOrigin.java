/**
 * User: e_smirnov
 * Date: 02.12.2010
 * Time: 16:19:31
 */
package ru.ifmo.gpo.core.history;

import org.jgap.IChromosome;

/**
 * Origin data for chromosome created by single-instruction mutation. Contains parent and index of mutated instruction.
 */
public class MutationOrigin extends ChromosomeOrigin {
    IChromosome parent;
    private int mutatedIdx;

    public MutationOrigin(IChromosome chromosome, IChromosome parent, int mutatedIdx) {
        super(OriginType.ORIGIN_MUTATION_RESULT, chromosome);
        this.parent = parent;
        this.mutatedIdx = mutatedIdx;
    }

    public int getMutatedIdx() {
        return mutatedIdx;
    }

    public IChromosome getParent() {
        return parent;
    }
}
