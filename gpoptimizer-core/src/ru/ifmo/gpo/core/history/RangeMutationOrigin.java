/**
 * User: e_smirnov
 * Date: 02.12.2010
 * Time: 16:53:33
 */
package ru.ifmo.gpo.core.history;

import org.jgap.IChromosome;

/**
 * Contains information about chromosome origin from range mutation. Contains parent and range that was mutated.
 */
public class RangeMutationOrigin extends ChromosomeOrigin {
    private IChromosome source;
    int rangeStart;
    int rangeLength;

    public RangeMutationOrigin(IChromosome chromosome, IChromosome source, int rangeStart, int rangeLength) {
        super(OriginType.ORIGIN_MUTATION_RESULT, chromosome);
        this.source = source;
        this.rangeStart = rangeStart;
        this.rangeLength = rangeLength;
    }

    public IChromosome getSource() {
        return source;
    }

    public int getRangeStart() {
        return rangeStart;
    }

    public int getRangeLength() {
        return rangeLength;
    }
}
