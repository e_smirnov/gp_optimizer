/**
 * User: e_smirnov
 * Date: 02.12.2010
 * Time: 16:16:27
 */
package ru.ifmo.gpo.core.history;

import org.jgap.IChromosome;
import ru.ifmo.gpo.core.Settings;
import ru.ifmo.gpo.core.operators.BaseCodeCrossoverOperation;

/**
 * Class used for debugging of invalid chromosomes. Stores information about how this chromosome was generated - by initial generation,
 * by crossover, mutation etc.
 */
public abstract class ChromosomeOrigin {
    private OriginType type;
    private IChromosome chromosome;

    protected ChromosomeOrigin(OriginType type, IChromosome chromosome) {
        this.type = type;
        this.chromosome = chromosome;
    }

    public OriginType getType() {
        return type;
    }

    public IChromosome getChromosome() {
        return chromosome;
    }

    public static void fromCrossover(IChromosome rz, IChromosome first, IChromosome second, BaseCodeCrossoverOperation.CrossoverPoint cp) {
        if (Settings.trackOrigin()) {
            rz.setApplicationData(new CrossoverOrigin(rz, first, second, cp));
        }
    }

    public static void fromMutation(IChromosome rz, IChromosome parent, int rangeStart, int rangeLength) {
        if (Settings.trackOrigin()) {
            rz.setApplicationData(new RangeMutationOrigin(rz, parent, rangeStart, rangeLength));
        }
    }

    public static void fromInitialGeneration(IChromosome rz) {
        if (Settings.trackOrigin()) {
            rz.setApplicationData(new InitialGenerationOrigin(rz));
        }
    }
}
