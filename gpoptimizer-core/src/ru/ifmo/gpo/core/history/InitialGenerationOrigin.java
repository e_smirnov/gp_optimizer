/**
 * User: e_smirnov
 * Date: 02.12.2010
 * Time: 16:17:33
 */
package ru.ifmo.gpo.core.history;

import org.jgap.IChromosome;

/**
 * Origin for chromosomes created in initial generation of population
 */
public class InitialGenerationOrigin extends ChromosomeOrigin {

    public InitialGenerationOrigin(IChromosome chromosome) {
        super(OriginType.ORIGIN_INITIAL_POPULATION, chromosome);
    }

}
