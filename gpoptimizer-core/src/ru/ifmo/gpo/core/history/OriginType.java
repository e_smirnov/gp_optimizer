package ru.ifmo.gpo.core.history;

/**
 * User: e_smirnov
 * Date: 02.12.2010
 * Time: 16:14:11
 */
public enum OriginType {
    ORIGIN_INITIAL_POPULATION,
    ORIGIN_CROSSOVER_RESULT,
    ORIGIN_MUTATION_RESULT
}
