package ru.ifmo.gpo.core;

/**
 * Executes code, generated from chromosomes by corresponding ICodeGenerator implementation
 */
public interface ICodeExecutor {
    /**
     * Returns run time of a code
     *
     * @param code Generated code
     * @param env  Input data for code
     * @return Code run time in some time units
     */
    public long profileCode(ICodeGenerationResult code, ICodeExecutionEnvironment env);

    /**
     * Returns values computed by code (contents of stack, registers etc after code execution)
     *
     * @param code Generated code
     * @param env  Input data for code
     * @return Set of computed values
     */
    public IExecutionResult executeCode(ICodeGenerationResult code, ICodeExecutionEnvironment env);
}