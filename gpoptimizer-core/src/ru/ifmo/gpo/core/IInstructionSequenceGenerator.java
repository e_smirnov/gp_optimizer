package ru.ifmo.gpo.core;

import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.SequenceGenerationFailedException;
import ru.ifmo.gpo.core.vmstate.IMachineState;
import ru.ifmo.gpo.core.vmstate.IStateDelta;

/**
 * User: e_smirnov
 * Date: 20.01.2011
 * Time: 16:09:11
 * <p/>
 * This class is used by genetics when it is necessary to create sequence of instructions that satisfy given restrictions on VM state
 */
public interface IInstructionSequenceGenerator<StateClass extends IMachineState, DeltaClass extends IStateDelta> {
    /**
     * Generates instruction sequence that takes source state as input and changes it in a way specified by desiredDelta. Generated sequence should be
     * in range of 0.5 to 1.5 lengths
     *
     * @param source       Source state
     * @param desiredDelta Operations that should be done by generated sequence
     * @param length       Average expected sequence length
     * @return Generated instruction sequence
     * @throws SequenceGenerationFailedException
     *          if failed to generate instruction sequence after some retries. In most cases generation is still possible, even in case of
     */
    public InstructionSequence generateInstructionSequence(StateClass source, DeltaClass desiredDelta, int length) throws SequenceGenerationFailedException;

    public InstructionSequence generateInstructionSequence(StateClass source, StateClass desiredState, int length) throws SequenceGenerationFailedException;

}
