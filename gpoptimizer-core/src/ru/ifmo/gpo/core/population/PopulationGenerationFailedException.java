/**
 * Created with IntelliJ IDEA.
 * User: Egor.Smirnov
 * Date: 04.07.12
 * Time: 13:38
 */

package ru.ifmo.gpo.core.population;

/**
 * Thrown by Population Generators in case of error while creating chromosomes
 */
public class PopulationGenerationFailedException extends Exception {

    private static final long serialVersionUID = -7771813964898413084L;

    public PopulationGenerationFailedException() {
    }

    public PopulationGenerationFailedException(String message) {
        super(message);
    }

    public PopulationGenerationFailedException(String message, Throwable cause) {
        super(message, cause);
    }

    public PopulationGenerationFailedException(Throwable cause) {
        super(cause);
    }
}
