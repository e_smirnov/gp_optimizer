/**
 * User: jedi-philosopher
 * Date: 05.12.2010
 * Time: 18:08:36
 */
package ru.ifmo.gpo.core.population;

import org.jgap.IChromosome;

/**
 * In case of generation failure, fills the gap with arbitrary already generated chromosome.
 * If population is empty (this was the first one), retries generation for 100 times and if still no results are achieved - throws an exception
 */
public class CloneRandomRetryStrategy implements IGenerationRetryStrategy {
    private IPopulationGenerator owner;

    private static final int retryCount = 100;

    @Override
    public void setPopulationGenerator(IPopulationGenerator gen) {
        owner = gen;
    }

    @Override
    public IChromosome generateChromosomeAfterFailedAttempt(IChromosome[] population, int failedIdx) throws PopulationGenerationFailedException {
        if (failedIdx == 0) {
            // no chromosomes were generated yet, retry untill one is generated
            IChromosome rz;
            int tryIdx = 0;
            do {
                rz = owner.generateSingleChromosome();
                if (tryIdx++ >= retryCount && rz == null) {
                    throw new PopulationGenerationFailedException("Failed to create initial chromosome");
                }
            } while (rz == null);
            return rz;
        }

        return (IChromosome) population[(int) (Math.random() * failedIdx)].clone();
    }
}
