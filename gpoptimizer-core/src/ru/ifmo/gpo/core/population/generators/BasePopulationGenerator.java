package ru.ifmo.gpo.core.population.generators;

import org.jgap.Chromosome;
import org.jgap.Gene;
import org.jgap.IChromosome;
import org.jgap.InvalidConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ifmo.gpo.core.CodeGene;
import ru.ifmo.gpo.core.GPConfiguration;
import ru.ifmo.gpo.core.Settings;
import ru.ifmo.gpo.core.history.ChromosomeOrigin;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.SequenceGenerationFailedException;
import ru.ifmo.gpo.core.instructions.generic.IGenericInstruction;
import ru.ifmo.gpo.core.population.IGenerationRetryStrategy;
import ru.ifmo.gpo.core.population.IPopulationGenerator;
import ru.ifmo.gpo.core.population.InfiniteRetryStrategy;
import ru.ifmo.gpo.core.population.PopulationGenerationFailedException;
import ru.ifmo.gpo.core.vmstate.IInstructionSequenceAnalyzer;
import ru.ifmo.gpo.core.vmstate.IMachineState;

import java.util.LinkedList;
import java.util.List;


/**
 * User: jedi-philosopher
 * Date: 26.01.2010
 * Time: 18:56:25
 */
public abstract class BasePopulationGenerator implements IPopulationGenerator {
    protected InstructionSequence initialSequence;
    protected IInstructionSequenceAnalyzer _initialChromosomeAnalyzer;
    protected GPConfiguration _conf;

    protected IGenerationRetryStrategy retryStrategy;

    protected static final int generationAttempts = 100;
    private static Logger logger = LoggerFactory.getLogger(BasePopulationGenerator.class);

    protected BasePopulationGenerator(GPConfiguration conf) {
        this._conf = conf;
        this.initialSequence = conf.getRefChromosome();
        this._initialChromosomeAnalyzer = conf.getTargetArchitecture().analyzeCode(new InstructionSequence(initialSequence), _conf.getStateSchema());
        setRetryStrategyFromConfig();
    }

    @Override
    public GPConfiguration getConfiguration() {
        return _conf;
    }

    private void setRetryStrategyFromConfig() {
        String retryStrategyName = Settings.getProperty("core.population.retryStrategy");
        if (retryStrategyName == null) {
            retryStrategy = new InfiniteRetryStrategy();
        } else {
            try {
                Class<? extends IGenerationRetryStrategy> c = (Class<? extends IGenerationRetryStrategy>) Class.forName(retryStrategyName);
                retryStrategy = c.newInstance();
            } catch (Exception e) {
                logger.warn("Failed to load class for retry strategy {} ({})using default InfiniteRetryStrategy", retryStrategyName, e);
                retryStrategy = new InfiniteRetryStrategy();
            }
        }
        retryStrategy.setPopulationGenerator(this);
    }

    public void setRetryStrategy(IGenerationRetryStrategy retryStrategy) {
        this.retryStrategy = retryStrategy;
    }

    protected void setOrigin(IChromosome chromosome) {
        ChromosomeOrigin.fromInitialGeneration(chromosome);
    }

    @Override
    public IChromosome generateSingleChromosome() throws PopulationGenerationFailedException {
        try {
            Chromosome res = new Chromosome(_conf);
            IMachineState context = _conf.getTargetArchitecture().createInitialState(_conf.getStateSchema());

            List<Gene> genes = new LinkedList<Gene>();
            final int initialChromosomeLength = initialSequence.size();
            InstructionSequence sequence = _conf.getTargetArchitecture().getInstructionSequenceGenerator().generateInstructionSequence(context, _initialChromosomeAnalyzer.getStateChange(), initialChromosomeLength);

            for (IGenericInstruction instr : sequence) {
                genes.add(new CodeGene(_conf, instr, res));
            }
            res.setGenes(genes.toArray(new Gene[genes.size()]));
            setOrigin(res);
            return res;

        } catch (SequenceGenerationFailedException ex) {
            return null;
        } catch (InvalidConfigurationException e) {
            throw new PopulationGenerationFailedException("Failed to generate initial population due to invalid JPPF configuration", e);
        }
    }
}
