package ru.ifmo.gpo.core.population.generators;

import org.jgap.IChromosome;
import ru.ifmo.gpo.core.GPConfiguration;

/**
 * User: jedi-philosopher
 * Date: 05.12.2010
 * Time: 18:01:24
 */
public class ClonePopulationGenerator extends BasePopulationGenerator {

    public ClonePopulationGenerator(GPConfiguration conf) {
        super(conf);
    }

    @Override
    public IChromosome[] generatePopulation() {
        IChromosome[] result = new IChromosome[_conf.getPopulationSize()];
        for (int i = 0; i < _conf.getPopulationSize(); ++i) {
            result[i] = (IChromosome) initialSequence.clone();
            setOrigin(result[i]);
        }
        return result;
    }
}
