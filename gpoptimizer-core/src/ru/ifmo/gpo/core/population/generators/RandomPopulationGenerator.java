/**
 * User: jedi-philosopher
 * Date: 05.12.2010
 * Time: 17:59:44
 */

package ru.ifmo.gpo.core.population.generators;

import org.jgap.IChromosome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ifmo.gpo.core.GPConfiguration;
import ru.ifmo.gpo.core.population.PopulationGenerationFailedException;

/**
 * Generates a random population.
 * If a chromosome can not be created after some retries, retryStrategy.generateChromosomeAfterfailedAttempt is called
 */
public class RandomPopulationGenerator extends BasePopulationGenerator {

    private static Logger logger = LoggerFactory.getLogger(RandomPopulationGenerator.class);

    public RandomPopulationGenerator(GPConfiguration conf) {
        super(conf);
    }

    private void generateRandomToArray(IChromosome[] targetArray, int size) throws PopulationGenerationFailedException {
        for (int i = 0; i < size; ++i) {
            int attempt = 0;
            do {
                targetArray[i] = generateSingleChromosome();
            } while (attempt++ < generationAttempts && targetArray[i] == null);

            if (targetArray[i] == null) {
                targetArray[i] = retryStrategy.generateChromosomeAfterFailedAttempt(targetArray, i);

                logger.debug("Failed to generate initial chromosome after {} retries", generationAttempts);
            }
        }
    }

    @Override
    public IChromosome[] generatePopulation() throws PopulationGenerationFailedException {
        IChromosome[] result = new IChromosome[_conf.getPopulationSize()];
        generateRandomToArray(result, _conf.getPopulationSize());
        return result;
    }
}
