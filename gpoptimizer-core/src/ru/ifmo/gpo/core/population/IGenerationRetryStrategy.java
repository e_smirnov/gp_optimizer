package ru.ifmo.gpo.core.population;

import org.jgap.IChromosome;

/**
 * User: jedi-philosopher
 * Date: 05.12.2010
 * Time: 18:03:33
 * <p/>
 * Sometimes it is possible that population generator fails to generate a chromosome, even after some retries. In this case,
 * method of this strategy is used to fill the gap in population.
 */
public interface IGenerationRetryStrategy {

    public void setPopulationGenerator(IPopulationGenerator gen);

    public IChromosome generateChromosomeAfterFailedAttempt(IChromosome[] population, int failedIdx) throws PopulationGenerationFailedException;

}
