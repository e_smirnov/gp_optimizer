/**
 * User: jedi-philosopher
 * Date: 05.12.2010
 * Time: 18:15:14
 */
package ru.ifmo.gpo.core.population;

import org.jgap.IChromosome;

/**
 * Infinitely retries chromosome generation until it succeeds
 */
public class InfiniteRetryStrategy implements IGenerationRetryStrategy {
    private IPopulationGenerator owner;

    @Override
    public void setPopulationGenerator(IPopulationGenerator gen) {
        owner = gen;
    }

    @Override
    public IChromosome generateChromosomeAfterFailedAttempt(IChromosome[] population, int failedIdx) throws PopulationGenerationFailedException {
        IChromosome rz;
        do {
            rz = owner.generateSingleChromosome();
        } while (rz == null);
        return rz;
    }
}
