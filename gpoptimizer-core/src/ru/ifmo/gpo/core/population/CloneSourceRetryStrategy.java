/**
 * User: jedi-philosopher
 * Date: 05.12.2010
 * Time: 18:07:03
 */
package ru.ifmo.gpo.core.population;

import org.jgap.IChromosome;
import org.jgap.InvalidConfigurationException;
import ru.ifmo.gpo.core.GPConfiguration;
import ru.ifmo.gpo.core.instructions.InstructionSequence;

/**
 * If chromosome generation is failed, fills the gap with copy of reference code sequence
 */
public class CloneSourceRetryStrategy implements IGenerationRetryStrategy {
    private GPConfiguration conf;

    private InstructionSequence original;

    @Override
    public void setPopulationGenerator(IPopulationGenerator gen) {
        conf = gen.getConfiguration();
        original = gen.getConfiguration().getRefChromosome();
    }

    @Override
    public IChromosome generateChromosomeAfterFailedAttempt(IChromosome[] population, int failedIdx) throws PopulationGenerationFailedException {
        try {
            return original.toChromosome(conf);
        } catch (InvalidConfigurationException e) {
            throw new PopulationGenerationFailedException(e);
        }
    }
}
