/**
 * User: jedi-philosopher
 * Date: 05.12.2010
 * Time: 17:58:03
 */

package ru.ifmo.gpo.core.population;

import org.jgap.IChromosome;
import ru.ifmo.gpo.core.GPConfiguration;

/**
 * Interface for initial population generation strategy
 */
public interface IPopulationGenerator {

    /**
     * Generate whole population.
     * Resulting array should have size as stored in GPConfiguration.getPopulationSize() and contain no null elements
     *
     * @return population
     */
    IChromosome[] generatePopulation() throws PopulationGenerationFailedException;

    /**
     * Generate single member of a population
     *
     * @return chromosome
     */
    IChromosome generateSingleChromosome() throws PopulationGenerationFailedException;

    /**
     * Retrieve current configuration
     *
     * @return configuration
     */
    GPConfiguration getConfiguration();
}
