package ru.ifmo.gpo.core;

import ru.ifmo.gpo.util.IDisposable;

import java.math.BigDecimal;

/**
 * Represents set of values computed by code, generated from chromosome.
 */
public interface IExecutionResult extends IDisposable {
    /**
     * Returns difference of results computed by this code and by reference. It may be a distance in some multe-dimensional space, where
     * each point is a result of code execution.
     *
     * @param reference Another execution result to check results with
     * @return Difference value
     */
    public BigDecimal computeDifference(IExecutionResult reference);

    /**
     * Maximum possible difference. Used when code failed to run.
     *
     * @return Maximum possible difference.
     */
    public BigDecimal maxPossibleDifference();

    /**
     * Get exception (if any) that happened during code execution
     *
     * @return Exception that happened during code execution, or null if code completed successfully
     */
    public Exception getException();
}