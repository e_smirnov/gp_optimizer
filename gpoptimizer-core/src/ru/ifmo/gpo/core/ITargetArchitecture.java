package ru.ifmo.gpo.core;

import org.jgap.InvalidConfigurationException;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.IGenericInstruction;
import ru.ifmo.gpo.core.vmstate.IInstructionSequenceAnalyzer;
import ru.ifmo.gpo.core.vmstate.IMachineState;
import ru.ifmo.gpo.core.vmstate.IStateDelta;
import ru.ifmo.gpo.core.vmstate.IStateSchema;

import java.io.Serializable;

/**
 * User: e_smirnov
 * Date: 20.01.2011
 * Time: 16:22:24
 * <p/>
 * Common interface, used by platform-independent code to perform platform-dependent actions, such as
 * code generation/execution, analysis etc. Subclasses should register themself in SupportedArchitectures class
 * in order to be available in server application,
 */
public interface ITargetArchitecture extends Serializable {

    /**
     * Is called once at the beginning of genetic process, so that some adjustments could be done on source instruction sequence.
     * Resulting sequence is used in genetics after that.
     * E.g. in case of Pixel Shader optimizations it strips setup instructions that should not be modified by genetics.
     *
     * @param source IS to be optimized
     * @return Modified source that will be used in genetics
     */
    public InstructionSequence preprocessSource(InstructionSequence source);

    /**
     * Performs code analysis, returns object containing all necessary information about provided instruction sequence
     *
     * @param code        Code to analyze
     * @param stateSchema Context for code execution - available registers, memory areas etc
     * @return Code information
     */
    public IInstructionSequenceAnalyzer analyzeCode(InstructionSequence code, IStateSchema stateSchema);

    /**
     * Deeply copies machine state. Returned object should be of same real type as parameter.
     *
     * @param state State to clone
     * @return state deep copy
     */
    public IMachineState cloneState(IMachineState state);

    /**
     * Computes difference between two processor states - set of actions that should be done to get
     * second state from first.
     *
     * @param first  Source state
     * @param second Dest state
     * @return Delta between two states
     */
    public IStateDelta getStateDelta(IMachineState first, IMachineState second);

    /**
     * Special case of delta check. Should be optimized if possible by custom implementations. Is generally faster
     * than getStateDelta(first, second).getDelta()==0
     *
     * @param first  Source state
     * @param second Dest state
     * @return true if delta between first and second is zero, false otherwise
     */
    public boolean isDeltaZero(IMachineState first, IMachineState second);

    /**
     * Creates state schema from given instruction sequence. State schema includes data about available registers, memory
     * areas etc and is used to select operands for instructions.
     *
     * @param code Code to build schema from
     * @return State schema
     */
    public IStateSchema buildStateSchema(InstructionSequence code);

    /**
     * Creates state that satisfies a given schema (and so can serve as initial state)
     *
     * @param schema Source schema
     * @return State created by that schema
     */
    public IMachineState createInitialState(IStateSchema schema);

    /**
     * Returns VM state that is a result of execution of instruction insn on a state sourceState
     *
     * @param sourceState Source VM state for instruction evaluation
     * @param insn        Instruction to evaluate
     * @return State of VM after instruction evaluation
     */
    public IMachineState evaluateInstruction(IMachineState sourceState, IGenericInstruction insn);

    /**
     * Return code generator for this platform
     *
     * @return Code generator for this platform
     */
    public ICodeGenerator getCodeGenerator();

    /**
     * Get code executor for this platform
     *
     * @return Code executor
     */
    public ICodeExecutor getCodeExecutor();

    /**
     * Return instruction sequence generator for this platform
     *
     * @return Instruction sequence generator
     */
    public IInstructionSequenceGenerator getInstructionSequenceGenerator();

    /**
     * Deeply copies instruction. Returned object should be of same subclass of IGenericInstruction as parameter
     *
     * @param instruction Instruction to clone
     * @return Deep copy of instruction
     */
    public IGenericInstruction cloneInstruction(IGenericInstruction instruction);

    /**
     * Returns NOP instruction or its analog for this architecture
     *
     * @return NOP instruction, correctly set up for usage in instruction sequences
     */
    public IGenericInstruction createNOPInstruction();

    /**
     * Checks if sequence satisfies platform-specific restrictions (like total instruction count, instruction order etc)
     * Used as additional filter in genetic operators
     *
     * @param seq InstructionSequence to check
     * @return true if sequence satisfies all platform restrictions and can be succesfully executed
     */
    public boolean isInstructionSequenceValid(InstructionSequence seq);

    /**
     * Returns unique id of architecture, used to find it in SupportedArchitectures collection
     *
     * @return Architecture id
     */
    public String getId();

    /**
     * Is called when instance of this interface is used to create a gpconfiguration
     *
     * @param gpConfiguration Configuration created
     * @throws org.jgap.InvalidConfigurationException
     *          In case of JGAP error
     */
    void configurationCreated(GPConfiguration gpConfiguration) throws InvalidConfigurationException;
}
