/**
 * User: e_smirnov
 * Date: 25.07.2011
 * Time: 16:42:06
 */
package ru.ifmo.gpo.core;

import ru.ifmo.gpo.core.instructions.InstructionSequence;

/**
 * Base class for code generation results
 */
public abstract class BaseCodeGenerationResult implements ICodeGenerationResult {

    /**
     * ID of this code sequence, must be unique for a single run of optimizer
     */
    protected String name;

    protected InstructionSequence source;

    protected BaseCodeGenerationResult() {
    }

    protected BaseCodeGenerationResult(String name, InstructionSequence source) {
        this.name = name;
        this.source = source;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public InstructionSequence getSourceChromosome() {
        return source;
    }
}
