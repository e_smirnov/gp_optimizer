package ru.ifmo.gpo.core.vmstate;

import ru.ifmo.gpo.core.instructions.generic.GenericOperand;

import java.util.Map;
import java.util.Set;

/**
 * User: e_smirnov
 * Date: 17.08.2011
 * Time: 15:22:38
 */
public abstract class BaseStateDelta<StateClass extends IMachineState> implements IStateDelta {

    protected StateClass source;
    protected StateClass target;

    protected BaseStateDelta(StateClass source, StateClass target) {
        this.source = source;
        this.target = target;
    }

    @Override
    public IMachineState getSource() {
        return source;
    }

    @Override
    public IMachineState getTarget() {
        return target;
    }

    /**
     * Fills targetSet with operands that exist in targetOperands, but not exist, or have weaker access in sourceOperands
     *
     * @param sourceOperands All resources from source state
     * @param targetOperands All resources from same type from target state
     * @param targetSet      Where to put results
     */
    protected <T extends GenericOperand> void processArea(Map<String, T> sourceOperands, Map<String, T> targetOperands, Set<T> targetSet) {
        for (T operand : targetOperands.values()) {
            GenericOperand sourceVal = sourceOperands.get(operand.getId());
            if (sourceVal == null && !operand.isWriteable()) {
                // it is possible that one of states has more input, but same output. in this case
                // they should be considered equal
                continue;
            }
            if ((sourceVal == null)
                    || (sourceVal.isWriteable() ^ operand.isWriteable())
                    || (sourceVal.hasValue() ^ operand.hasValue())) {
                targetSet.add(operand);
            }
        }
    }
}
