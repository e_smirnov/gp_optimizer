/**
 * User: e_smirnov
 * Date: 17.08.2011
 * Time: 13:54:49
 */
package ru.ifmo.gpo.core.vmstate;

import ru.ifmo.gpo.core.instructions.InstructionSequence;

import java.util.LinkedList;
import java.util.List;

/**
 * Base class for analysis of an instruction sequence. Calculates changes this sequence causes in CPU state.
 */
public abstract class BaseInstructionSequenceAnalyzer<StateClass extends IMachineState, StateDeltaClass extends IStateDelta> implements IInstructionSequenceAnalyzer {
    /**
     * Sequence of instructions to be analyzed
     */
    protected InstructionSequence source;

    /**
     * Index of first instruction in source to be analyzed
     */
    protected int from;

    /**
     * Number of instructions (starting at index from) to be analyzed
     */
    protected int rangeLength;

    /**
     * States of CPU after execution of instructions.
     * states[0] is state before execution
     * states[1] is state after execution instruction #1
     * ...
     */
    protected List<StateClass> states = new LinkedList<StateClass>();

    /**
     * State before execution of instructions
     */
    protected StateClass initialState;

    /**
     * State after execution
     */
    protected StateClass stateAfter;

    /**
     * Diff between initialState and stateAfter
     */
    protected StateDeltaClass stateDelta;

    protected BaseInstructionSequenceAnalyzer() {
    }

    public BaseInstructionSequenceAnalyzer(InstructionSequence sequence) {
        this(sequence, 0, sequence.size());
    }

    public BaseInstructionSequenceAnalyzer(InstructionSequence sequence, int from, int len) {
        this.source = sequence;
        this.from = from;
        this.rangeLength = len;
    }


    /**
     * Performs actual analysis of code
     * Should be called from constructor of as subclass just after call to super()
     */
    protected abstract void analyze();

    public abstract boolean checkStateEqual(IInstructionSequenceAnalyzer second);

    @Override
    public StateClass getStateBefore() {
        return initialState;
    }

    @Override
    public StateClass getStateAfter() {
        return stateAfter;
    }

    public StateDeltaClass getStateChange() {
        return stateDelta;
    }

    @Override
    public List<? extends IMachineState> getStates() {
        return states;
    }
}
