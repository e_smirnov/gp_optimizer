package ru.ifmo.gpo.core.vmstate;

/**
 * User: e_smirnov
 * Date: 14.12.2010
 * Time: 16:04:12
 * <p/>
 * Shows that for given VM state, no instruction can be found that can satisfy applied restrictions
 */
public class NoSuitableInstructionFoundException extends FailedToSatisfyRestrictionsException {
    private static final long serialVersionUID = 387926303773970034L;

    public NoSuitableInstructionFoundException() {
        super("No suitable instruction can be found with provided VM state");
    }
}
