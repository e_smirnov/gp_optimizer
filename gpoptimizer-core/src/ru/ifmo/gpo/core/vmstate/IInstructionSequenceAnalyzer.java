package ru.ifmo.gpo.core.vmstate;

import java.util.List;

/**
 * User: e_smirnov
 * Date: 13.01.2011
 * Time: 16:12:45
 * <p/>
 * Performs analysis of an instruction sequence. Determines resources used by that sequence (registers, memory) and its
 * influence on VM state.
 */
public interface IInstructionSequenceAnalyzer {
    /**
     * Acquire state of VM that can serve as initial state for running instruction sequence. i.e. it has all necessary
     * resource slots set.
     *
     * @return State that IS can operate on
     */
    IMachineState getStateBefore();

    /**
     * Acquire state of VM after execution of instruction sequence.
     *
     * @return State after execution
     */
    IMachineState getStateAfter();

    /**
     * Computes delta between initial and last state
     *
     * @return state delta between states returned by getStateBefore() and getStateAfter()
     */
    IStateDelta getStateChange();

    /**
     * Returns list of intermediate states. List element #i corresponds to state of VM after execution of i-th instruction from sequence
     *
     * @return Intermediate VM states
     */
    List<? extends IMachineState> getStates();

    /**
     * Checks that instruction sequences from this and other analyzers perform same change on vm state
     *
     * @param second Analyzer created from some other instruction sequence
     * @return true if both sequences perform same actions on vm states
     */
    boolean checkStateEqual(IInstructionSequenceAnalyzer second);
}
