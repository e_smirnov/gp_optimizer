package ru.ifmo.gpo.core.vmstate;

import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;

/**
 * User: jedi-philosopher
 * Date: 30.03.11
 * Time: 14:55
 * <p/>
 * Represents sets of available operands for instructinos - available registers, memory areas etc
 */
public interface IStateSchema {

    /**
     * Adds new slot to this schema. All slots must be filled with values before instruction sequence evaluation
     *
     * @param slot        Slot to add (e.g. register id)
     * @param initialType Type of value that is expected to be in this slot (for typed VMs)
     * @param accessType  Access for this slot at the beginning of sequence execution
     * @return Previous value if schema already contained this slot
     */
    public GenericOperand addOrUpdateSlot(GenericOperand slot, Type initialType, GenericOperand.AccessType accessType);


    /**
     * Retrieve all available slots
     *
     * @return List of slots
     */
    public Iterable<? extends GenericOperand> getSlots();
}
