package ru.ifmo.gpo.core.vmstate;

import ru.ifmo.gpo.core.instructions.generic.IGenericInstruction;


/**
 * User: jedi-philosopher
 * Date: 11.12.2010
 * Time: 16:35:33
 * <p/>
 * Thrown if IOperandSelector subclass can not find any suitable operand for an instruction using given VM state and
 * restrictions.
 */
public class NoSuitableOperandFoundException extends FailedToSatisfyRestrictionsException {
    private static final long serialVersionUID = 7211985581651063780L;

    public NoSuitableOperandFoundException(IGenericInstruction instruction) {
        super("No suitable operand can be found for " + instruction);
    }

    public NoSuitableOperandFoundException(int instruction) {
        super("No suitable operand can be found for instruction with opcode " + instruction);
    }

}
