package ru.ifmo.gpo.core.vmstate;

/**
 * User: e_smirnov
 * Date: 13.01.2011
 * Time: 16:10:19
 * <p/>
 * Performs a difference between two states of VM.
 * Difference is a size of a minimum set of actions (like register or memory writes) that are needed to turn
 * source state into target state.
 * For example, ILOAD instruction modifies VM state in such way, that delta has a size of 1 and
 * contains of a single action 'put int on stack'
 */
public interface IStateDelta {

    public IMachineState getSource();

    public IMachineState getTarget();

    public int getDelta();

}
