package ru.ifmo.gpo.core.vmstate;

/**
 * User: e_smirnov
 * Date: 14.12.2010
 * Time: 16:10:44
 * <p/>
 * Base class for exceptions
 */
public class FailedToSatisfyRestrictionsException extends Exception {
    private static final long serialVersionUID = 5762751733153321973L;

    public FailedToSatisfyRestrictionsException() {
    }

    public FailedToSatisfyRestrictionsException(String message) {
        super(message);
    }
}
