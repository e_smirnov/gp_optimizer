/**
 * User: jedi-philosopher
 * Date: 23.04.2010
 * Time: 22:09:09
 */

package ru.ifmo.gpo.core.vmstate;

import ru.ifmo.gpo.util.CollectionUtils;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Contains constants that are used in some code sequense. As this sequense may compute some non-trivial formula containing some huge
 * constants, it would be virtually impossible to evolve code that implements this formula without any knowledge about at least order, and most often about exact values of these constants.
 * So all constants are preserved and can emerge as operands.
 * Also, contains some default constants (like 0, 1, 2, -1), that can become usefull in almost any computation
 * Uses lazy initialization for performance
 */
public class ConstantStorage {

    private static final Integer[] defaultIntConstants = {-1, 0, 1, 2, 3, 4, 5};
    private static final Float[] defaultFloatConstants = {-1.0f, 0.0f, 1.0f, 2.0f};

    // default constants are added lazily
    private boolean defaultConstantsAdded = false;

    private Set<Integer> integerConstants;
    private Set<Float> floatConstants;

    public ConstantStorage() {
        // do not create set untill it is needed
    }

    /**
     * Copy constructor
     *
     * @param proto sets this as a copy of proto
     */
    public ConstantStorage(ConstantStorage proto) {
        integerConstants = proto.getIntConstants() != null ? new HashSet<Integer>(proto.getIntConstants()) : null;
        floatConstants = proto.getFloatConstants() != null ? new HashSet<Float>(proto.getFloatConstants()) : null;
        defaultConstantsAdded = proto.defaultConstantsAdded;
    }

    /**
     * Lazily creates sets and adds default constants to them
     */
    private void lazyInit() {
        if (integerConstants == null) {
            integerConstants = new HashSet<Integer>();
        }
        if (floatConstants == null) {
            floatConstants = new HashSet<Float>();
        }
        if (!defaultConstantsAdded) {
            Collections.addAll(integerConstants, defaultIntConstants);
            Collections.addAll(floatConstants, defaultFloatConstants);
            defaultConstantsAdded = true;
        }
    }

    /**
     * Adds all constants from other to this
     *
     * @param other Other storage
     */
    public void merge(ConstantStorage other) {
        lazyInit();
        integerConstants.addAll(other.integerConstants);
        floatConstants.addAll(other.floatConstants);
    }

    /**
     * Adds single new integer constant to storage
     *
     * @param i New constant value
     */
    public void addIntConstant(int i) {
        lazyInit();
        integerConstants.add(i);
    }

    /**
     * Adds single new floting point constant to storage
     *
     * @param f New constant value
     */
    public void addFloatConstant(float f) {
        lazyInit();
        floatConstants.add(f);
    }

    /**
     * Returns arbitrarty floating point number from set of available constants
     *
     * @return One of constants contined in this storage
     */
    public float getFloatConstant() {
        lazyInit();
        if (floatConstants.isEmpty()) {
            throw new IllegalStateException("Attempt to get float constant when no such constants are available");
        }
        return CollectionUtils.selectRandomElement(floatConstants);
    }

    /**
     * Returns arbitrarty integer from set of available constants
     *
     * @return One of constants contined in this storage
     */
    public int getIntConstant() {
        lazyInit();
        if (integerConstants.isEmpty()) {
            throw new IllegalStateException("Attempt to get int constant when no such constants are available");
        }
        return CollectionUtils.selectRandomElement(integerConstants);
    }

    public Set<Integer> getIntConstants() {
        lazyInit();
        return integerConstants;
    }

    public Set<Float> getFloatConstants() {
        lazyInit();
        return floatConstants;
    }

    public int getIntConstantNumber() {
        int rz = integerConstants != null ? integerConstants.size() : 0;
        if (!defaultConstantsAdded) {
            rz += defaultIntConstants.length;
        }
        return rz;
    }

    public int getFloatConstantNumber() {
        int rz = floatConstants != null ? floatConstants.size() : 0;
        if (!defaultConstantsAdded) {
            rz += defaultFloatConstants.length;
        }
        return rz;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final ConstantStorage that = (ConstantStorage) o;
        return !(integerConstants != null ? !integerConstants.equals(that.integerConstants) : that.integerConstants != null);
    }

    @Override
    public int hashCode() {
        return integerConstants != null ? integerConstants.hashCode() : 0;
    }
}
