/**
 * User: jedi-philosopher
 * Date: 17.01.2011
 * Time: 23:49:40
 */
package ru.ifmo.gpo.core.vmstate;

import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.instructions.generic.GenericStackItem;

import java.util.List;
import java.util.Map;

/**
 * State of CPU at some point of code execution - with all values of memory, registers and stack.
 * If some architecture does not have one of areas (stack for example), it must return empty collection.
 */
public interface IMachineState {

    /**
     * Retrieve register contents (map is from register ID to register value)
     */
    public Map<String, ? extends GenericOperand> getRegisters();

    /**
     * Retrieve stack
     */
    public List<? extends GenericStackItem> getStack();

    /**
     * Retrieve memory areas (map from memory address to memory value)
     */
    public Map<String, ? extends GenericOperand> getMemory();

    /**
     * Retrieve constants
     */
    public ConstantStorage getConstants();
}
