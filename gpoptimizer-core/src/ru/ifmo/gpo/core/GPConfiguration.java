/**
 * User: jedi-philosopher
 * Date: 29.11.2009
 * Time: 15:05:17
 */
package ru.ifmo.gpo.core;

import org.jgap.Configuration;
import org.jgap.InvalidConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.optimizations.PrematureConvergenceFixer;
import ru.ifmo.gpo.core.vmstate.IStateSchema;

import java.util.ArrayList;
import java.util.List;

/**
 * Contains all necessary configuration for a single optimization task. Contains both platform-dependent data and also task-dependent.
 */
public abstract class GPConfiguration extends Configuration {

    private static final long serialVersionUID = 1L;

    /**
     * Candidates for solution (chromosomes with fitness > 1.0)
     */
    private SolutionStorage solutions = new SolutionStorage();

    /**
     * Target platform information
     */
    private ITargetArchitecture targetArchitecture;

    private Logger logger = LoggerFactory.getLogger(GPConfiguration.class);

    /**
     * If enabled, genetic process is stopped after a couple of exceptions during population evaluation.
     * If disabled, process is only stopped after reaching iteration limit
     */
    private boolean enableFailureCounting = true;

    private List<IGenerationEventListener> listeners = new ArrayList<IGenerationEventListener>();

    /**
     * Amount of chromosomes that cause failures and exceptions. If this count exceeds some max value, and enableFailureCounting is true, genetic process is stopped
     */
    private int failureCount = 0;

    /**
     * Fitness function
     */
    private CodeFitnessFunction fitnessFunc;

    /**
     * CPU State schema for original code sequence
     */
    protected IStateSchema stateSchema;

    /**
     * Original code sequence that should be optimized
     */
    protected InstructionSequence reference;

    /**
     * Helps to resolve stagnation problems in population
     */
    protected StagnationMonitor stagnationMonitor = new StagnationMonitor();

    public GPConfiguration(ITargetArchitecture targetArchitecture, String uniqueKey, String description) throws InvalidConfigurationException {
        super(uniqueKey, description);
        this.targetArchitecture = targetArchitecture;
        this.targetArchitecture.configurationCreated(this);

        addListener(new PrematureConvergenceFixer(targetArchitecture));
    }

    public void addListener(IGenerationEventListener listener) {
        listeners.add(listener);
    }

    public List<IGenerationEventListener> getListeners() {
        return listeners;
    }

    public CodeFitnessFunction getFitnessFunc() {
        return fitnessFunc;
    }

    public InstructionSequence getRefChromosome() {
        return reference;
    }

    public SolutionStorage getSolutions() {
        return solutions;
    }

    public IStateSchema getStateSchema() {
        return stateSchema;
    }

    public void setStateSchema(IStateSchema stateSchema) {
        this.stateSchema = stateSchema;
    }

    public void setTargetArchitecture(ITargetArchitecture targetArchitecture) {
        this.targetArchitecture = targetArchitecture;
    }

    public ITargetArchitecture getTargetArchitecture() {
        return targetArchitecture;
    }

    public StagnationMonitor getStagnationMonitor() {
        return stagnationMonitor;
    }

    public void setReferenceChromosome(InstructionSequence reference) throws InvalidConfigurationException {
        stateSchema = targetArchitecture.buildStateSchema(reference);
        this.reference = getTargetArchitecture().preprocessSource(reference); // modify reference, strip unneded instructions
        setSampleChromosome(reference.toChromosome(this));
        fitnessFunc = new CodeFitnessFunction(this);
        setFitnessFunction(fitnessFunc);
    }

    public void onException() {
        if (enableFailureCounting && (++failureCount > Settings.getMaxFailures())) {
            throw new RuntimeException("Exceeded failure count of " + failureCount + ", stopping genetic process");
        }
    }

    public void setEnableFailureCounting(boolean enableFailureCounting) {
        this.enableFailureCounting = enableFailureCounting;
    }

    public abstract void setUpConfiguration() throws InvalidConfigurationException;

}
