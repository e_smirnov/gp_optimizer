/**
 * Created by IntelliJ IDEA.
 * User: jedi-philosopher
 * Date: 29.11.2009
 * Time: 15:50:02
 * <p/>
 *
 */
package ru.ifmo.gpo.core;

import org.jgap.FitnessFunction;
import org.jgap.IChromosome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.vmstate.IInstructionSequenceAnalyzer;
import ru.ifmo.gpo.util.StatUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;


/**
 * Implements fitness function for code sequences.
 * Fitness of a code is based on 3 metrics: values that it calculated, number of instructions and speed of execution.
 * First, accuracy of values is measured. For that purpose, both tested and reference code sequences are run over a fixed set of tests
 * (sets of input values), after that modified values in all registers, memory, stack areas etc are compared.
 * Only if these values are close enough, code size and execution speed is taken into account.
 * Fitness function has following properties:
 * - it is in range (0, 1) for bad solutions (starting at something like 10^-9 for first random generations)
 * - it is exactly 1.0 for reference code
 * - it is > 1.0 for solutions (that do the same computations, but do them faster or use less instructions)
 */
public class CodeFitnessFunction extends FitnessFunction {

    /**
     * Number of test sets
     */
    static final int EVALUATION_PASSES = 10;

    /**
     * Input values for code test runs.
     */
    private ICodeExecutionEnvironment[] testEnvs;

    /**
     * Data about reference code sequence
     */
    private IInstructionSequenceAnalyzer referenceAnalyzer;

    private GPConfiguration conf;

    /**
     * Code generated from reference sequence, used in value computation
     */
    private ICodeGenerationResult sandboxedRefCode;

    /**
     * Code generated from reference sequence, used in performance measuring
     */
    private ICodeGenerationResult profileRefCode;

    /**
     * Reference sequence
     */
    private InstructionSequence chromosome;

    private Logger logger = LoggerFactory.getLogger(getClass());

    private static final long serialVersionUID = 1180401815451358432L;

    /**
     * Results of execution of reference sequence over a test set, computed only once
     */
    private IExecutionResult[] refCodeExecResults;

    /**
     * Calculated average run time of reference code
     */
    private long refCodeRunTime;

    public CodeFitnessFunction(GPConfiguration conf) {
        this.conf = conf;
        try {
            chromosome = conf.getRefChromosome();

            referenceAnalyzer = conf.getTargetArchitecture().analyzeCode(chromosome, conf.getStateSchema());

            ICodeGenerator gen = conf.getTargetArchitecture().getCodeGenerator();
            sandboxedRefCode = gen.generateSandboxedCode(chromosome, conf.getStateSchema(), null);
            profileRefCode = gen.generateProfileCode(chromosome, conf.getStateSchema(), null);

            long[] refCodeRunTimes = new long[EVALUATION_PASSES];
            refCodeExecResults = new IExecutionResult[EVALUATION_PASSES];
            testEnvs = new ICodeExecutionEnvironment[EVALUATION_PASSES];

            testEnvs[0] = sandboxedRefCode.createExecutionEnvironment();
            testEnvs[0].setToZero();

            testEnvs[1] = sandboxedRefCode.createExecutionEnvironment();
            testEnvs[1].setToFF();

            for (int i = 2; i < EVALUATION_PASSES; ++i) {
                testEnvs[i] = sandboxedRefCode.createExecutionEnvironment();
                testEnvs[i].setRandomValues();
            }

            // computing run times and values for ref code
            for (int i = 0; i < EVALUATION_PASSES; ++i) {
                refCodeRunTimes[i] = conf.getTargetArchitecture().getCodeExecutor().profileCode(profileRefCode, testEnvs[i]);
                refCodeExecResults[i] = conf.getTargetArchitecture().getCodeExecutor().executeCode(sandboxedRefCode, testEnvs[i]);
            }
            refCodeRunTime = StatUtils.avgWithErrorCorrection(refCodeRunTimes, 0);

        } catch (Exception ex) {
            logger.error("Error while creating fitness function", ex);
            throw new RuntimeException(ex);
        }
        logger.debug("Fitness function created successfully");
    }

    /**
     * Calculates how many times this candidate code sequence is faster/slower than reference one
     *
     * @param codeForProfile Candidate sequence
     * @return Reference code execution time divided by candidate execution time.
     */
    private double calculateSpeedup(ICodeGenerationResult codeForProfile) {
        long[] codeProfiletimes = new long[EVALUATION_PASSES];
        for (int i = 0; i < EVALUATION_PASSES; ++i) {
            try {
                long codeRunTime = conf.getTargetArchitecture().getCodeExecutor().profileCode(codeForProfile, testEnvs[i]);

                codeProfiletimes[i] = codeRunTime;
            } catch (Exception ex) {
                // run failed
                codeProfiletimes[i] = -1;
            }
        }
        long codeRunTime = StatUtils.avgWithErrorCorrection(codeProfiletimes, 0);
        if (codeRunTime > 0 && refCodeRunTime > 0) {
            return Math.abs((double) refCodeRunTime / (double) codeRunTime);
        } else {
            return 1;
        }
    }

    @Override
    protected double evaluate(IChromosome iChromosome) {
        ICodeGenerationResult codeForProfile;
        ICodeGenerationResult codeForCompare;

        InstructionSequence refSequence = new InstructionSequence(chromosome);
        InstructionSequence testedSequence = new InstructionSequence(iChromosome);

        if (refSequence.equals(testedSequence)) {
            // found chromosome equals source chromosome, for such case fitness value is always 1.0 and no solution is added to solution storage
            return 1.0;
        }

        if (iChromosome.getFitnessValueDirectly() > 0) {
            // do not recalculate if already calculated
            return iChromosome.getFitnessValueDirectly();
        }

        try {

            // reference code is generated here, because if same instance is always used, JIT compiler optimizes it much better
            // than code generated from chromosomes and profiling result comparison
            codeForProfile = conf.getTargetArchitecture().getCodeGenerator().generateProfileCode(testedSequence, conf.getStateSchema(), referenceAnalyzer);
            codeForCompare = conf.getTargetArchitecture().getCodeGenerator().generateSandboxedCode(testedSequence, conf.getStateSchema(), referenceAnalyzer);

            BigDecimal difference = BigDecimal.valueOf(0);
            BigDecimal maxPossibleDifference = BigDecimal.valueOf(0);

            int successfulRuns = 0;

            // execute candidate over given sets of input data and compare results with results of reference code
            for (int i = 0; i < EVALUATION_PASSES; ++i) {

                IExecutionResult res1 = conf.getTargetArchitecture().getCodeExecutor().executeCode(codeForCompare, testEnvs[i]);
                IExecutionResult res2 = refCodeExecResults[i];

                Exception ex1 = res1.getException();
                Exception ex2 = res2.getException();


                if (ex1 != null) {
                    if (ex2 == null || !ex1.getClass().equals(ex2.getClass())) {
                        //  difference += res1.maxPossibleDifference();
                    }
                } else if (ex2 != null) {
                    //difference += res1.maxPossibleDifference();
                } else {
                    successfulRuns++;
                    difference = difference.add(res1.computeDifference(res2));
                    // it is possible that getNanoTinme will return negative vales
                }

                maxPossibleDifference = res2.maxPossibleDifference();
                res1.dispose();
            }
            final int failedRuns = EVALUATION_PASSES - successfulRuns;
            if (failedRuns > 0) {
                difference = difference.add(maxPossibleDifference.multiply(BigDecimal.valueOf(failedRuns)));
            }
            difference = difference.divide(BigDecimal.valueOf(EVALUATION_PASSES));


            double speedUpMultiplier = 1;

            // length multiplier is only taken into account when candidate is longer or when difference is close to 0
            // this prevents emerging of extremely short chromosomes during initial convergence process, and also
            // helps to throw away sequences that contain a lot of unnecessary instructions, that however have small influence on performance
            double lengthMultiplier = (difference.doubleValue() < 0.0001
                    || iChromosome.getGenes().length > conf.getRefChromosome().size()) ? Math.sqrt((double) iChromosome.getGenes().length / conf.getRefChromosome().size()) : 1;

            // speedup is only calculated if candidate performs same calculations as original code
            // adding speedup to fitness on early stages leads to overfilling population with incorrect but very fast chromosomes
            if (difference.doubleValue() < 0.0001) {
                speedUpMultiplier = calculateSpeedup(codeForProfile);
            }

            BigDecimal rz = BigDecimal.valueOf(speedUpMultiplier / lengthMultiplier).divide(difference.add(BigDecimal.valueOf(1)), 32, RoundingMode.HALF_UP);
            double resultFitness = rz.doubleValue();

            // all chromosomes that have fitness > 1 are potentially solutions of optimization task, save them for future analysis
            if (resultFitness > 1.0) {
                conf.getSolutions().addSolution(testedSequence, rz.doubleValue());
            }

            if (resultFitness < 0) {
                return 0;
            }
            return resultFitness;
        } catch (RuntimeException ex) {
            logger.error("Error while trying to compute fitness function", ex);
            logger.error("Chromosome code: {}", Arrays.toString(iChromosome.getGenes()));
            conf.onException();
            return 0;
        } catch (Exception ex) {
            logger.error("Error while trying to compute fitness function", ex);
            logger.error("Chromosome code: {}", Arrays.toString(iChromosome.getGenes()));
            conf.onException();
            return 0;
        }
    }

    @Override
    public java.lang.Object clone() {
        CodeFitnessFunction f = (CodeFitnessFunction) super.clone();
        f.conf = conf;
        f.sandboxedRefCode = sandboxedRefCode;
        f.profileRefCode = profileRefCode;
        return f;
    }

}
