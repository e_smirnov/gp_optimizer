package ru.ifmo.gpo.core;

import java.util.HashMap;
import java.util.Map;

public class SupportedArchitectures {
    private static Map<String, Class<? extends ITargetArchitecture>> supportedArchitectures = new HashMap<String, Class<? extends ITargetArchitecture>>();
    private static Map<String, ITargetArchitecture> cachedInstances = new HashMap<String, ITargetArchitecture>();

    public static void register(String id, Class<? extends ITargetArchitecture> arch) {
        supportedArchitectures.put(id, arch);
    }

    public static Class<? extends ITargetArchitecture> get(String id) {
        return supportedArchitectures.get(id);
    }

    public static ITargetArchitecture createInstance(String id) {
        if (cachedInstances.containsKey(id)) {
            return cachedInstances.get(id);
        }
        Class<? extends ITargetArchitecture> archClass = get(id);
        if (archClass == null) {
            throw new IllegalArgumentException("Target architecture " + id + " is not registered in supported targets list");
        }
        try {
            ITargetArchitecture rz = archClass.newInstance();
            cachedInstances.put(id, rz);
            return rz;
        } catch (Exception e) {
            throw new IllegalStateException("Failed to instantiate ITargetArchitecture instance with id " + id + ": " + e);
        }
    }
}
