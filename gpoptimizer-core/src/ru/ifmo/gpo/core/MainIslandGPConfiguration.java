package ru.ifmo.gpo.core;

import org.jgap.DefaultFitnessEvaluator;
import org.jgap.InvalidConfigurationException;
import org.jgap.event.EventManager;
import org.jgap.impl.StandardPostSelector;
import org.jgap.impl.StockRandomGenerator;
import ru.ifmo.gpo.core.operators.CodeCrossoverOperation;
import ru.ifmo.gpo.core.operators.CodeMergeWithSourceOperator;
import ru.ifmo.gpo.core.operators.CodeRangeMutationOperator;

/**
 * User: jedi-philosopher Date: 15.09.2010 Time: 21:58:42
 */
public class MainIslandGPConfiguration extends GPConfiguration {

    public static final String NAME = "mainIslandConf";
    private static final long serialVersionUID = -6459044365313620072L;

    public MainIslandGPConfiguration(ITargetArchitecture targetArchitecture) throws InvalidConfigurationException {
        super(targetArchitecture, NAME, "Configuration for main island");
    }

    @Override
    public void setUpConfiguration() throws InvalidConfigurationException {
        if (reference == null) {
            throw new IllegalStateException(
                    "setUpConfiguration() must be called aftger setReferenceChromosome()");
        }
        setPopulationSize(100);
        setAlwaysCaculateFitness(false);
        setKeepPopulationSizeConstant(false);
        setPreservFittestIndividual(true);
        addGeneticOperator(new CodeRangeMutationOperator(this, 6));
        addGeneticOperator(new CodeCrossoverOperation(this, 4));
        addGeneticOperator(new CodeMergeWithSourceOperator(this, 50, true));
        setFitnessEvaluator(new DefaultFitnessEvaluator());
        setRandomGenerator(new StockRandomGenerator());
        addNaturalSelector(new StandardPostSelector(this), false);
        EventManager eventManager = new EventManager();
        setEventManager(eventManager);
    }
}
