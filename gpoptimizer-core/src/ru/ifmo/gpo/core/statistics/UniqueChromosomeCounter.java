package ru.ifmo.gpo.core.statistics;

import org.jgap.IChromosome;
import org.jgap.Population;
import ru.ifmo.gpo.core.ITargetArchitecture;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.IGenericInstruction;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Calculates percent of unique chromosomes in a population
 * Ignores NOP instructions (e.g. [add, nop] will be equal to [nop, add, nop]
 */
public class UniqueChromosomeCounter {

    private Population population;

    private Map<InstructionSequence, Integer> uniqueChromosomes = new HashMap<InstructionSequence, Integer>();

    private IGenericInstruction nopInstruction;

    public UniqueChromosomeCounter(ITargetArchitecture arch, Population population) {
        this.population = population;
        this.nopInstruction = arch.createNOPInstruction();
        recalculate();
    }

    /**
     * Recalculates statistics (in case population has changed)
     */
    public void recalculate() {
        uniqueChromosomes.clear();
        for (IChromosome chr : (List<IChromosome>) population.getChromosomes()) {
            uniqueChromosomes.put(convertToISstripNOPs(chr), getCopyCount(chr) + 1);
        }
    }

    /**
     * Converts IChromosome to InstructionSequence with all NOP instructions removed
     * @param source source chromosome
     * @return InstructionSequence with no NOPs
     */
    private InstructionSequence convertToISstripNOPs(IChromosome source)
    {
        InstructionSequence seq = new InstructionSequence(source);
        for (Iterator<IGenericInstruction> iter = seq.iterator(); iter.hasNext();) {
            IGenericInstruction insn = iter.next();
            if (insn.equals(nopInstruction)) {
                iter.remove();
            }
        }
        return seq;
    }

    public int getUniqueCount() {
        return uniqueChromosomes.size();
    }

    public double getUniquePercent() {
        return ((double) getUniqueCount()) / population.size();
    }

    public int getCopyCount(IChromosome chr) {
        Integer amount = uniqueChromosomes.get(convertToISstripNOPs(chr));
        if (amount == null) {
            amount = 0;
        }
        return amount;
    }

    /**
     * Call when a copy of chromosome is removed from population, updates stats
     * @param chr Copy to be killed
     */
    public void killCopy(IChromosome chr) {
        final int copyCount = getCopyCount(chr);
        if (copyCount == 0) {
            return;
        }
        final int newCopyCount = copyCount - 1;
        InstructionSequence seq = convertToISstripNOPs(chr);
        if (newCopyCount <= 0) {
            uniqueChromosomes.remove(seq);
        } else {
            uniqueChromosomes.put(seq, newCopyCount);
        }
    }
}
