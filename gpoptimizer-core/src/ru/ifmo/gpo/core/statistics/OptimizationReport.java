/**
 * Created with IntelliJ IDEA.
 * User: Egor.Smirnov
 * Date: 28.09.12
 * Time: 13:21
 */
package ru.ifmo.gpo.core.statistics;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import ru.ifmo.gpo.core.SolutionStorage;
import ru.ifmo.gpo.core.instructions.InstructionSequence;

import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Structure that contains full info about one run of an optimization process
 */
public class OptimizationReport {
    /**
     * Reference instruction sequence that was optimized
     */
    public final String referenceSequence;

    /**
     * Number of optimization steps
     */
    public final int iterationCount;

    public final transient InstructionSequence bestResult;

    /**
     * Single best optimization result. If null - optimization failed
     */
    public final String bestResultString;

    /**
     * Optimized solutions that successfully passed verification
     */
    public final List<String> verifiedResults;

    /**
     * Solutions that were reported by optimizer but failed to pass verification
     */
    public final List<String> verificationFails;

    public OptimizationReport(InstructionSequence referenceSequence, int iterationCount, SolutionStorage verifiedResults, InstructionSequence bestResult, List<InstructionSequence> verificationFails) {
        this.referenceSequence = referenceSequence.toString();
        this.iterationCount = iterationCount;
        this.verifiedResults = new LinkedList<String>();
        for (InstructionSequence is : verifiedResults) {
            this.verifiedResults.add(is.toString());
        }
        this.bestResult = bestResult;
        this.bestResultString = bestResult != null ? bestResult.toString() : "";
        this.verificationFails = new LinkedList<String>();
        for (InstructionSequence is : verificationFails) {
            this.verificationFails.add(is.toString());
        }
    }

    public void dumpToJSONFile(String filePath) throws IOException {
        final FileWriter writer = new FileWriter(filePath);
        final Gson gson = new GsonBuilder().setPrettyPrinting().create();
        writer.write(gson.toJson(this));
        writer.close();
    }
}
