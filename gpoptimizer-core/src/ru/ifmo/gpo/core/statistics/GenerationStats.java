/**
 * User: jedi-philosopher
 * Date: 14.09.2010
 * Time: 18:27:46
 */
package ru.ifmo.gpo.core.statistics;

import org.jgap.Genotype;
import org.jgap.IChromosome;
import ru.ifmo.gpo.core.GPConfiguration;
import ru.ifmo.gpo.core.instructions.InstructionSequence;

import java.util.List;

/**
 * Statistics for single generation of a population
 */
public class GenerationStats {
    /**
     * Best fitness value for population
     */
    public double maxFitnessFunctionValue;

    /**
     * Number of chromosomes in population that are equal to reference one
     */
    public int referenceCloneCount;

    /**
     * Average length of chromosomes
     */
    public int avgLen;

    /**
     * Number of individuals that have low fitness value (<0.6, which means that they calculate invalid values)
     */
    public int chromosomesWithLowVal;
    /**
     * Number of chromosomes that have fitness value close to 1. This means that they compute correct values, but may
     * work slower or have more instructions
     */
    public int chromosomesWithCloseVal;
    /**
     * Number of solutions in population - individuals with fitness > 1
     */
    public int chromosomesWithHigherVal;

    /**
     * Number of chromosomes that have no clones in population
     */
    public int uniqueChromosomes;

    public GenerationStats(Genotype genotype, GPConfiguration conf) {
        maxFitnessFunctionValue = genotype.getFittestChromosome().getFitnessValue();
        InstructionSequence ref = new InstructionSequence(conf.getRefChromosome());
        avgLen = 0;
        for (IChromosome chr : (List<IChromosome>) genotype.getPopulation().getChromosomes()) {
            if (ref.equals(new InstructionSequence(chr))) {
                referenceCloneCount++;
            }
            avgLen += chr.getGenes().length;
            if (chr.getFitnessValue() < 0.6) {
                chromosomesWithLowVal++;
            } else if (chr.getFitnessValue() < 1.03) {
                chromosomesWithCloseVal++;
            } else {
                chromosomesWithHigherVal++;
            }

        }
        UniqueChromosomeCounter uniqueCounter = new UniqueChromosomeCounter(conf.getTargetArchitecture(), genotype.getPopulation());
        uniqueChromosomes = uniqueCounter.getUniqueCount();
        avgLen /= genotype.getPopulation().size();
    }
}
