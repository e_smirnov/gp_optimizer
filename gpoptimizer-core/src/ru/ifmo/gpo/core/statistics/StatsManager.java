/**
 * User: jedi-philosopher
 * Date: 14.09.2010
 * Time: 18:43:19
 */
package ru.ifmo.gpo.core.statistics;

import org.jgap.Genotype;
import ru.ifmo.gpo.core.GPConfiguration;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Collects various metrics for a population. Can save results to csv file for later analysis.
 */
public class StatsManager {
    private LinkedList<GenerationStats> stats = new LinkedList<GenerationStats>();

    public List<GenerationStats> getStats() {
        return stats;
    }

    public List<GenerationStats> getLastNStats(int n) {
        if (stats.size() <= n) {
            return stats;
        }
        return stats.subList(stats.size() - n, stats.size() - 1);
    }

    public void processGeneration(Genotype generation, GPConfiguration conf) {
        stats.addLast(new GenerationStats(generation, conf));
    }

    public void saveToCSV(String fileName) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
        int idx = 0;
        writer.write("nmb;fitness;lowCount;closeCount;higherCount;refCloneCount;uniqueCount;avgLength\n");
        for (GenerationStats stat : stats) {
            writer.write(String.format("%d;%f;%d;%d;%d;%d;%d;%d\n", idx++, stat.maxFitnessFunctionValue, stat.chromosomesWithLowVal, stat.chromosomesWithCloseVal, stat.chromosomesWithHigherVal, stat.referenceCloneCount, stat.uniqueChromosomes, stat.avgLen));
        }
        writer.close();
    }
}
