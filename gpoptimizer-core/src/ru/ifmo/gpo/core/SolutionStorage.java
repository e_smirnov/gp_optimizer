/**
 * User: jedi-philosopher
 * Date: 01.02.2010
 * Time: 14:20:30
 */
package ru.ifmo.gpo.core;

import ru.ifmo.gpo.core.instructions.InstructionSequence;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Storage for solutions found in genetic process (solution is a chromosome with fitness > 1.0)
 * Solutions are sorted by fitness in descending order.
 */
public class SolutionStorage implements Iterable<InstructionSequence>, Serializable {

    private static final long serialVersionUID = 6260359181812208948L;

    /**
     * Solution themselves, sorted by fitness
     */
    private SortedSet<Solution> solutions = new TreeSet<Solution>(new ChromosomeComparator());

    /**
     * Compares chromosomes based on fitness
     */
    private static class ChromosomeComparator implements Comparator<Solution>, Serializable {
        private static final long serialVersionUID = -1625323650383353336L;

        public int compare(Solution o1, Solution o2) {
            if (o1.equals(o2)) {
                return 0;
            }
            if (o1.fitness > o2.fitness) {
                return -1;
            } else if (o1.fitness < o2.fitness) {
                return 1;
            }
            return 0;
        }
    }

    /**
     * Solution of optimization task
     */
    private static class Solution implements Serializable {
        /**
         * Solution itself
         */
        public InstructionSequence solution;
        /**
         * Solution fitness value
         */
        public double fitness;

        private static final long serialVersionUID = 5096889973074115716L;

        private Solution(InstructionSequence solution, double fitness) {
            this.solution = solution;
            this.fitness = fitness;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Solution solution1 = (Solution) o;

            return !(solution != null ? !solution.equals(solution1.solution) : solution1.solution != null);
        }

        @Override
        public int hashCode() {
            return solution != null ? solution.hashCode() : 0;
        }

        @Override
        public String toString() {
            return "Solution{" +
                    "solution=" + solution.toString() +
                    ", fitness=" + fitness +
                    '}';
        }
    }

    /**
     * Wrapper for solution iterator, adapter between Iterator<Solution> and Iterator<InstructionSequence>
     */
    private static class SolutionIterator implements Iterator<InstructionSequence>, Serializable {

        private Iterator<Solution> _myIterator;

        private static final long serialVersionUID = 9083957702449677207L;

        public SolutionIterator(Iterator<Solution> iter) {
            _myIterator = iter;
        }

        public boolean hasNext() {
            return _myIterator.hasNext();
        }

        public InstructionSequence next() {
            return _myIterator.next().solution;
        }

        public void remove() {
            _myIterator.remove();
        }
    }

    public void addSolution(InstructionSequence solution, double fitness) {
        // if storage already has a solution but with different fitness value, update fitness with middle value
        for (Iterator<Solution> iter = solutions.iterator(); iter.hasNext(); ) {
            Solution sol = iter.next();
            if (sol.solution.equals(solution)) {
                iter.remove();
                sol.fitness = (sol.fitness + fitness) / 2;
                solutions.add(sol); // readding solution as we can't modify set contents inplace
                return;
            }
        }
        solutions.add(new Solution(solution, fitness));
    }

    /**
     * Adds all values from storage to this
     */
    public void merge(SolutionStorage storage) {
        for (Solution sol : storage.solutions) {
            addSolution(sol.solution, sol.fitness);
        }
    }

    public Iterator<InstructionSequence> iterator() {
        return new SolutionIterator(solutions.iterator());
    }

    public int size() {
        return solutions.size();
    }

    public boolean isEmpty() {
        return solutions.isEmpty();
    }

    @Override
    public String toString() {
        return "SolutionStorage [solutions=" + solutions + "]";
    }


}
