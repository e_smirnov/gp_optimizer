/**
 *
 */
package ru.ifmo.gpo.core.optimizations;

import org.jgap.IChromosome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ifmo.gpo.core.IGenerationEventListener;
import ru.ifmo.gpo.core.ITargetArchitecture;
import ru.ifmo.gpo.core.Island;
import ru.ifmo.gpo.core.population.PopulationGenerationFailedException;
import ru.ifmo.gpo.core.statistics.UniqueChromosomeCounter;

/**
 * @author jedi-philosopher
 *         This class attempts to fix 'Premature Convergence' problem, when genetic process tends to some sub-optimal solution and
 *         most of population turns into clones of single most successive chromosome.
 */
public class PrematureConvergenceFixer implements IGenerationEventListener {

    private static Logger logger = LoggerFactory.getLogger(PrematureConvergenceFixer.class);

    private final int iterationsBetweenRemoval = Integer.parseInt(System.getProperty("core.optimizations.iterationsBetweenCloneRemoval", "10"));

    private final int maxCloneCount = Integer.parseInt(System.getProperty("core.optimizations.allowedCloneCount", "1"));

    private final float minimumUniqueShare = Float.parseFloat(System.getProperty("core.optimizations.minimumUniqueShare", "0.3"));

    private ITargetArchitecture targetArch;

    public PrematureConvergenceFixer(ITargetArchitecture targetArch) {
        this.targetArch = targetArch;
    }

    public void generationProcessed(Island island, int genIdx) {

        if ((genIdx + 1) % iterationsBetweenRemoval != 0) {
            return;
        }

        UniqueChromosomeCounter counter = new UniqueChromosomeCounter(targetArch, island.getPopulation().getPopulation());
        double uniquePercent = counter.getUniquePercent();
        if (uniquePercent > minimumUniqueShare) {
            // enough unique chromosomes in population, no clean needed
            logger.debug("Share of unique chromosomes on island '{}' is {}", island.getName(), uniquePercent);
            return;
        }
        logger.debug("Share of unique chromosomes on island '{}' is only {}, perfoming clone cleaning", island.getName(), uniquePercent);
        int replacedCount = 0;
        for (int i = 0; i < island.getPopulation().getPopulation().getChromosomes().size(); ++i) {
            IChromosome chr = island.getPopulation().getPopulation().getChromosome(i);
            if (counter.getCopyCount(chr) > maxCloneCount) {
                replaceCopy(island, i);
                counter.killCopy(chr);
                ++replacedCount;
            }
        }
        counter.recalculate();
        logger.debug("Clone cleaning completed, cleaned {} clones, unique percent is now {}", replacedCount, counter.getUniquePercent());
    }

    private void replaceCopy(Island island, int idx) {
        IChromosome newChromosome = null;
        do {
            try {
                newChromosome = island.getPopulationGenerator().generateSingleChromosome();
            } catch (PopulationGenerationFailedException e) {
                logger.warn("Exception while generating new chromosome while fixing premature convergence", e);
            }
        } while (newChromosome == null);

        island.getPopulation().getPopulation().setChromosome(idx, newChromosome);
    }

}
