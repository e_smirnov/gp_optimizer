package ru.ifmo.gpo.core;

import org.jgap.Population;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Monitors state of a population.
 * Detects stagnation - when fitness value does not increase for a long time, because population becomes flooded with clones of single
 * most successful chromosome.
 * Can be used to conditionally enable/disable genetic operators that can fix this
 */
public class StagnationMonitor {
    /**
     * Listener interface for stagnation effects
     */
    public interface StagnationListener {
        /**
         * Will be called once when stagnation is detected (not for every iteration!)
         *
         * @param conf       Configuration
         * @param population Population
         */
        public void stagnationDetected(GPConfiguration conf, Population population);

        /**
         * Will be called when stagnation is fixed (best fitness function value changes in any way)
         *
         * @param conf       Configuration
         * @param population Population
         */
        public void stagnationFixed(GPConfiguration conf, Population population);
    }

    private List<StagnationListener> listeners = new LinkedList<StagnationListener>();

    /**
     * For how many iterations population must have same highest fitness function value so that it will be counted as stagnation
     * Suitable value is 15-20
     */
    private int stagnationThreshold;

    /**
     * Values of best fitness for last generations
     */
    private Queue<Double> fitnessHistory = new LinkedList<Double>();

    private boolean isInStagnation = false;

    public StagnationMonitor() {
        stagnationThreshold = Integer.parseInt(Settings.getProperties().getProperty("core.genetics.stagnationThreshold", "15"));
    }

    public void addStagnationListener(StagnationListener listener) {
        listeners.add(listener);
    }

    public void iterationProcessed(GPConfiguration conf, Population pop, double bestFitnessValue) {
        fitnessHistory.add(bestFitnessValue);
        if (fitnessHistory.size() < stagnationThreshold) {
            return;
        }

        if (fitnessHistory.size() > stagnationThreshold) {
            fitnessHistory.remove();
        }

        for (Double val : fitnessHistory) {
            if (Math.abs(val - bestFitnessValue) > 0.0001) {
                // found some different value. if were in stagnation mode - stop it and notify listeners
                if (isInStagnation) {
                    for (StagnationListener listener : listeners) {
                        listener.stagnationFixed(conf, pop);
                    }
                    isInStagnation = false;
                }
                return;
            }
        }

        if (isInStagnation) {
            // listeners already notified
            return;
        }

        isInStagnation = true;
        for (StagnationListener listener : listeners) {
            listener.stagnationDetected(conf, pop);
        }

    }

}
