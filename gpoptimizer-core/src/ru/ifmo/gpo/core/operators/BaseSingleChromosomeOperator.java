package ru.ifmo.gpo.core.operators;

import org.jgap.*;
import ru.ifmo.gpo.core.GPConfiguration;

import java.util.List;

/**
 * Base class for genetic operators that process one cromosome at a time (like mutation)
 * Will perform fixed number of mutations = population.size / mutation rate
 */
public abstract class BaseSingleChromosomeOperator extends BaseGeneticOperator {

    private static final long serialVersionUID = -4296122336580143566L;

    protected int mutationRate;

    protected BaseSingleChromosomeOperator(GPConfiguration conf, int mutationRate) throws InvalidConfigurationException {
        super(conf);
        this.mutationRate = mutationRate;
    }

    @Override
    public void operate(Population population, List list) {
        if (population == null || list == null) {
            // Population or candidate chromosomes list empty:
            // nothing to do.
            // -----------------------------------------------
            return;
        }
        if (mutationRate == 0) {
            // If the mutation rate is set to zero, we don't perform any mutation.
            // ----------------------------------------------------------------
            return;
        }

        RandomGenerator generator = getConfiguration().getRandomGenerator();
        // It would be inefficient to create copies of each Chromosome just
        // to decide whether to mutate them. Instead, we only make a copy
        // once we've positively decided to perform a mutation.
        // ----------------------------------------------------------------
        int size = Math.min(getConfiguration().getPopulationSize(), population.size());
        int mutatedNum = size / mutationRate;

        for (int i = 0; i < mutatedNum; i++) {
            IChromosome candidate = population.getChromosome(generator.nextInt(size));
            if (candidate.getAge() < 1) {
                continue;
            }
            operate(list, candidate, generator);
        }

    }

    /**
     * Perform all actions on a chromosome
     *
     * @param population List containing population. Processed candidate should be added to it
     * @param candidate Chromosome to process
     * @param generator Random generator that should be used whenever random number is required
     */
    public abstract void operate(List population, IChromosome candidate, RandomGenerator generator);

    @Override
    public int compareTo(Object o) {
        if (o == null) {
            return 1;
        }
        if (!(o instanceof CodeRangeMutationOperator)) {
            return 1;
        }
        CodeRangeMutationOperator p = (CodeRangeMutationOperator) o;

        if (mutationRate != p.mutationRate) {
            if (mutationRate > p.mutationRate) {
                return 1;
            } else {
                return -1;
            }
        }
        if (getConfiguration() != p.getConfiguration()) {
            if (getConfiguration()!= null) {
                return 1;
            } else {
                return -1;
            }
        }

        // Everything is equal. Return zero.
        // ---------------------------------
        return 0;
    }
}
