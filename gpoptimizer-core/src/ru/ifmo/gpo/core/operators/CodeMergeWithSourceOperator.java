package ru.ifmo.gpo.core.operators;

import org.jgap.IChromosome;
import org.jgap.InvalidConfigurationException;
import org.jgap.Population;
import org.jgap.RandomGenerator;
import ru.ifmo.gpo.core.GPConfiguration;
import ru.ifmo.gpo.core.instructions.InstructionSequence;

import java.util.LinkedList;
import java.util.List;

/**
 * User: jedi-philosopher
 * Date: 14.09.2010
 * Time: 15:20:36
 * <p/>
 * Performs crossover between arbitrary chromosome from current generation and reference chromosome
 */
public class CodeMergeWithSourceOperator extends BaseCodeCrossoverOperation {

    private InstructionSequence refSequence;

    private static final long serialVersionUID = 3015115898494207281L;

    public CodeMergeWithSourceOperator(GPConfiguration conf, int crossoverRate, boolean enabled) throws InvalidConfigurationException {
        super(conf, crossoverRate);
        refSequence = conf.getRefChromosome();
    }

    public void operate(Population a_population, List list) {

        int size = Math.min(getConfiguration().getPopulationSize(),
                a_population.size());
        int numCrossovers = size / m_crossoverRate;

        RandomGenerator generator = getConfiguration().getRandomGenerator();
        int index;
        for (int i = 0; i < numCrossovers; i++) {
            index = generator.nextInt(size);
            IChromosome chrom = a_population.getChromosome(index);
            // Verify that crossover is allowed.
            // ---------------------------------
            if (chrom.getAge() < 1) {
                // Crossing over two newly created chromosomes is not seen as helpful
                // here.
                // ------------------------------------------------------------------
                continue;
            }

            // Clone the chromosomes.
            // ----------------------
            // perform crossover. We need only one out of two generated chromosomes
            List rz = new LinkedList();
            try {
                IChromosome firstMate = (IChromosome) chrom.clone();
                IChromosome secondMate = refSequence.toChromosome(_conf);
                doCrossover(firstMate, secondMate, rz, generator);
            } catch (InvalidConfigurationException e) {
                throw new RuntimeException(e);
            }
            if (!rz.isEmpty()) {
                list.add(rz.get(0));
            }
        }
    }
}
