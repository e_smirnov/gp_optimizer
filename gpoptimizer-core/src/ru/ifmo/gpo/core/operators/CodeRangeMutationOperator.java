package ru.ifmo.gpo.core.operators;

import org.jgap.*;
import org.slf4j.LoggerFactory;
import ru.ifmo.gpo.core.CodeGene;
import ru.ifmo.gpo.core.GPConfiguration;
import ru.ifmo.gpo.core.Settings;
import ru.ifmo.gpo.core.history.ChromosomeOrigin;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.SequenceGenerationFailedException;
import ru.ifmo.gpo.core.instructions.generic.IGenericInstruction;
import ru.ifmo.gpo.core.vmstate.IInstructionSequenceAnalyzer;
import ru.ifmo.gpo.core.vmstate.IMachineState;
import ru.ifmo.gpo.core.vmstate.IStateDelta;

import java.util.LinkedList;
import java.util.List;

/**
 * Performs a mutation of a single code sequence, randomly generating its subsequence of arbitrary length
 */
public class CodeRangeMutationOperator extends BaseSingleChromosomeOperator {

    private static final long serialVersionUID = -5371694895604254910L;

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(CodeRangeMutationOperator.class);

    public CodeRangeMutationOperator(GPConfiguration conf, int mutationRate) throws InvalidConfigurationException {
        super(conf, mutationRate);
    }

    @Override
    public void operate(List list, IChromosome candidate, RandomGenerator generator) {
        if (candidate.getGenes().length <= 3) {
            return; // too short
        }
        int geneIdx = generator.nextInt(candidate.getGenes().length - 2);
        int rangeLength = 2 + generator.nextInt(candidate.getGenes().length - geneIdx - 2); // shortest possible range is 2
        doMutation(candidate, geneIdx, rangeLength, list, generator);
    }


    @SuppressWarnings("unchecked")
    public void doMutation(IChromosome chromosome, int geneIdx, int rangeLength, List list, RandomGenerator generator) {
        try {
            final GPConfiguration conf = (GPConfiguration) getConfiguration();
            IInstructionSequenceAnalyzer analyzer = conf.getTargetArchitecture().analyzeCode(new InstructionSequence(chromosome), conf.getStateSchema());

            IMachineState state;

            if (geneIdx == 0) {
                state = conf.getTargetArchitecture().cloneState(analyzer.getStateBefore());
            } else {
                state = conf.getTargetArchitecture().cloneState(analyzer.getStates().get(geneIdx - 1));
            }

            IStateDelta stateDelta;

            if (geneIdx + rangeLength - 1 < chromosome.getGenes().length) {
                stateDelta = conf.getTargetArchitecture().getStateDelta(state, analyzer.getStates().get(geneIdx + rangeLength - 1));
            } else {
                stateDelta = conf.getTargetArchitecture().getStateDelta(state, analyzer.getStateAfter());
            }

            LinkedList<Gene> newGenes = new LinkedList<Gene>();
            for (int i = 0; i < geneIdx; ++i) {
                newGenes.addLast(chromosome.getGene(i));
            }

            InstructionSequence mutated = null;
            int count = 0;
            do {
                try {
                    mutated = conf.getTargetArchitecture().getInstructionSequenceGenerator().generateInstructionSequence(state, stateDelta, rangeLength);
                } catch (SequenceGenerationFailedException ignore) {
                    if (count++ > 10) {
                        logger.info("Failed to mutate after 10 retries");
                        return;
                    }
                }
            } while (mutated == null);

            IChromosome rz = new Chromosome(conf);

            for (IGenericInstruction instr : mutated) {
                newGenes.add(new CodeGene(conf, instr, rz));
            }
            for (int i = geneIdx + rangeLength; i < chromosome.getGenes().length; ++i) {
                newGenes.addLast(chromosome.getGene(i));
            }

            rz.setGenes(newGenes.toArray(new Gene[newGenes.size()]));

            if (Settings.debugMutation()) {
                IInstructionSequenceAnalyzer mutatedAnalyzer = conf.getTargetArchitecture().analyzeCode(new InstructionSequence(rz), conf.getStateSchema());
                IInstructionSequenceAnalyzer sourceAnalyzer = conf.getTargetArchitecture().analyzeCode(new InstructionSequence(chromosome), conf.getStateSchema());
                if (!mutatedAnalyzer.checkStateEqual(sourceAnalyzer)) {
                    throw new IllegalStateException("Error in mutation: resulting chromosome has invalid or different VM states");
                }
            }
            ChromosomeOrigin.fromMutation(rz, chromosome, geneIdx, rangeLength);
            if (conf.getTargetArchitecture().isInstructionSequenceValid(new InstructionSequence(rz))) {
                list.add(rz);
                logger.debug("Mutation, source length " + chromosome.getGenes().length + ", target length " + rz.getGenes().length);
            }

        } catch (InvalidConfigurationException ex) {
            logger.error("Mutation failed due to invalid configuration:", ex);
        }
    }

}