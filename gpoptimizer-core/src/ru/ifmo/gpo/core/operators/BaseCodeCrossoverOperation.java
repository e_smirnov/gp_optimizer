package ru.ifmo.gpo.core.operators;

import org.jgap.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ifmo.gpo.core.CodeGene;
import ru.ifmo.gpo.core.GPConfiguration;
import ru.ifmo.gpo.core.Settings;
import ru.ifmo.gpo.core.history.ChromosomeOrigin;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.vmstate.IInstructionSequenceAnalyzer;
import ru.ifmo.gpo.core.vmstate.IMachineState;
import ru.ifmo.gpo.util.ProbabilitySet;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * User: jedi-philosopher
 * Date: 28.01.2010
 * Time: 15:42:13
 */
public abstract class BaseCodeCrossoverOperation extends BaseGeneticOperator {

    private static final long serialVersionUID = -3377278783049724503L;

    protected GPConfiguration _conf;
    protected int m_crossoverRate;
    private Logger logger = LoggerFactory.getLogger(getClass());

    public BaseCodeCrossoverOperation(GPConfiguration conf, int crossoverRate) throws InvalidConfigurationException {
        super(conf);
        this._conf = conf;
        m_crossoverRate = crossoverRate;
    }

    /**
     * Defines chromosome selection logic, must be implemented in subclass
     *
     * @param a_population Population
     * @param list         Next population generation
     */
    public abstract void operate(Population a_population, List list);


    public static class CrossoverPoint {
        public int first;
        public int second;

        public double chance;

        private CrossoverPoint(int first, int second, double chance) {
            this.first = first;
            this.second = second;
            this.chance = chance;
        }

        @Override
        public String toString() {
            return String.format("{%d, %d}:%f", first, second, chance);
        }
    }

    public List<CrossoverPoint> getPossibleCrossoverPoints(IChromosome first, IChromosome second) {
        List<CrossoverPoint> result = new ArrayList<CrossoverPoint>();
        IInstructionSequenceAnalyzer firstAnalyzer = _conf.getTargetArchitecture().analyzeCode(new InstructionSequence(first), _conf.getStateSchema());
        IInstructionSequenceAnalyzer secondAnalyzer = _conf.getTargetArchitecture().analyzeCode(new InstructionSequence(second), _conf.getStateSchema());

        List<? extends IMachineState> firstStackStates = firstAnalyzer.getStates();
        List<? extends IMachineState> secondStackStates = secondAnalyzer.getStates();

        for (int i = 0; i < firstStackStates.size() - 1; ++i) { // -1 because crossover point after end of chromosome is not valid
            for (int j = 0; j < secondStackStates.size() - 1; ++j) {
                if (_conf.getTargetArchitecture().isDeltaZero(firstStackStates.get(i), secondStackStates.get(j))
                        && _conf.getTargetArchitecture().isDeltaZero(secondStackStates.get(j), firstStackStates.get(i))) { // two checks as delta is not reflexive
                    final int newFirstLen = i + (second.getGenes().length - j);
                    final int newSecondLen = j + (first.getGenes().length - i);

                    final double chance = 1.0 / (1 + Math.abs(newFirstLen - newSecondLen));
                    if (Math.abs(chance) < 0.00001) {
                        logger.error("Crossover point probability should not be zero");
                    }
                    result.add(new CrossoverPoint(i, j, chance));
                }
            }
        }
        return result;
    }

    protected CrossoverPoint getCrossoverPoint(IChromosome first, IChromosome second, RandomGenerator rand) {
        List<CrossoverPoint> result = getPossibleCrossoverPoints(first, second);
        if (result.isEmpty()) {
            return null;
        }
        if (result.size() == 1) {
            return result.get(0);
        }

        ProbabilitySet<CrossoverPoint> selectionSet = new ProbabilitySet<CrossoverPoint>(new Random(rand.nextLong()));
        for (CrossoverPoint point : result) {
            selectionSet.put(point, point.chance);
        }
        return selectionSet.getRandom();
    }

    @SuppressWarnings("unchecked")
    public void doCrossover(IChromosome first, IChromosome second, List list, RandomGenerator generator) throws InvalidConfigurationException {
        CrossoverPoint point = getCrossoverPoint(first, second, generator);
        if (point == null) {
            logger.debug("Failed to find any suitable crossover points");
            return;
        }
        IChromosome newFirst = new Chromosome(_conf);
        IChromosome newSecond = new Chromosome(_conf);

        Gene[] firstGeneSet = new CodeGene[point.first + second.getGenes().length - point.second];
        Gene[] secondGeneSet = new CodeGene[point.second + first.getGenes().length - point.first];

        for (int i = 0; i <= point.first; ++i) {
            firstGeneSet[i] = first.getGene(i);
        }
        for (int i = point.second + 1; i < second.getGenes().length; ++i) {
            firstGeneSet[point.first + 1 + (i - point.second - 1)] = second.getGene(i);
        }

        for (int i = 0; i <= point.second; ++i) {
            secondGeneSet[i] = second.getGene(i);
        }
        for (int i = point.first + 1; i < first.getGenes().length; ++i) {
            secondGeneSet[point.second + 1 + (i - point.first - 1)] = first.getGene(i);
        }

        newFirst.setGenes(firstGeneSet);
        newSecond.setGenes(secondGeneSet);

        InstructionSequence firstIS = new InstructionSequence(newFirst);
        InstructionSequence secondIS = new InstructionSequence(newSecond);

        if (Settings.debugCrossover()) {
            IInstructionSequenceAnalyzer firstAnalyzer = _conf.getTargetArchitecture().analyzeCode(firstIS, _conf.getStateSchema());
            IInstructionSequenceAnalyzer secondAnalyzer = _conf.getTargetArchitecture().analyzeCode(secondIS, _conf.getStateSchema());
            if (!firstAnalyzer.checkStateEqual(secondAnalyzer)
                    || !secondAnalyzer.checkStateEqual(firstAnalyzer)) {
                throw new IllegalStateException("Error in crossover: resulting chromosomes have invalid or different VM states");
            }
        }
        ChromosomeOrigin.fromCrossover(newFirst, first, second, point);
        ChromosomeOrigin.fromCrossover(newSecond, first, second, point);

        logger.debug("Crossover: input lengths " + first.getGenes().length + ", " + second.getGenes().length + "; result lengths: " + newFirst.getGenes().length + ", " + newSecond.getGenes().length);
        boolean atLeastOneValid = false;
        if (_conf.getTargetArchitecture().isInstructionSequenceValid(firstIS)) {
            list.add(newFirst);
            atLeastOneValid = true;
        }
        if (_conf.getTargetArchitecture().isInstructionSequenceValid(secondIS)) {
            list.add(newSecond);
            atLeastOneValid = true;
        }

        if (!atLeastOneValid) {
            logger.info("Crossover failed because both results are not valid");
        }
    }


    public int compareTo(Object o) {
        if (o == null) {
            return 1;
        }
        if (!(o instanceof BaseCodeCrossoverOperation)) {
            return 1;
        }
        BaseCodeCrossoverOperation op = (BaseCodeCrossoverOperation) o;

        if (m_crossoverRate != op.m_crossoverRate) {
            if (m_crossoverRate > op.m_crossoverRate) {
                return 1;
            } else {
                return -1;
            }
        }
        if (_conf != op._conf) {
            if (_conf != null) {
                return 1;
            } else {
                return -1;
            }
        }

        // Everything is equal. Return zero.
        // ---------------------------------
        return 0;

    }
}

