/**
 * User: jedi-philosopher
 * Date: 28.01.2010
 * Time: 15:42:13
 */
package ru.ifmo.gpo.core.operators;

import org.jgap.IChromosome;
import org.jgap.InvalidConfigurationException;
import org.jgap.Population;
import org.jgap.RandomGenerator;
import ru.ifmo.gpo.core.GPConfiguration;

import java.util.List;

/**
 * Performs N crossovers between random pairs of individuals.
 */
public class CodeCrossoverOperation extends BaseCodeCrossoverOperation {
    private static final long serialVersionUID = -4384231848656526200L;

    public CodeCrossoverOperation(GPConfiguration conf, int crossoverRate) throws InvalidConfigurationException {
        super(conf, crossoverRate);
    }

    public void operate(Population a_population, List list) {
        // Work out the number of crossovers that should be performed.
        // -----------------------------------------------------------
        int size = Math.min(getConfiguration().getPopulationSize(),
                a_population.size());
        int numCrossovers = 0;

        numCrossovers = size / m_crossoverRate;

        RandomGenerator generator = getConfiguration().getRandomGenerator();
        // For each crossover, grab two random chromosomes, pick a random
        // locus (gene location), and then swap that gene and all genes
        // to the "right" (those with greater loci) of that gene between
        // the two chromosomes.
        // --------------------------------------------------------------
        int index1, index2;
        for (int i = 0; i < numCrossovers; i++) {
            index1 = generator.nextInt(size);
            index2 = generator.nextInt(size);
            IChromosome chrom1 = a_population.getChromosome(index1);
            IChromosome chrom2 = a_population.getChromosome(index2);
            // Verify that crossover is allowed.
            // ---------------------------------
            if (chrom1.getAge() < 1 && chrom2.getAge() < 1) {
                // Crossing over two newly created chromosomes is not seen as helpful
                // here.
                // ------------------------------------------------------------------
                continue;
            }

            // Clone the chromosomes.
            // ----------------------
            IChromosome firstMate = (IChromosome) chrom1.clone();
            IChromosome secondMate = (IChromosome) chrom2.clone();
            // Cross over the chromosomes.
            // ---------------------------
            try {
                doCrossover(firstMate, secondMate, list, generator);
            } catch (InvalidConfigurationException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
