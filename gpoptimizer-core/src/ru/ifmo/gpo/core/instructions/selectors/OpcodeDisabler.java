package ru.ifmo.gpo.core.instructions.selectors;

import ru.ifmo.gpo.core.instructions.IOperandSelector;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.vmstate.IMachineState;
import ru.ifmo.gpo.core.vmstate.IStateDelta;
import ru.ifmo.gpo.core.vmstate.NoSuitableOperandFoundException;

import java.util.Collections;
import java.util.List;

/**
 * User: e_smirnov
 * Date: 15.09.2011
 * Time: 18:09:41
 * <p/>
 * Selector that always throws exceptions. Can be used to disable specific opcode (InstructionSequenceGenerators won't be able to select it, because
 * will always fail to select operands for it)
 */
public class OpcodeDisabler implements IOperandSelector {

    private static OpcodeDisabler instance = new OpcodeDisabler();

    public static IOperandSelector getInstance() {
        return instance;
    }

    @Override
    public List<GenericOperand> selectOperandsRandom(Object opcode, IMachineState state) throws NoSuitableOperandFoundException {
        throw new NoSuitableOperandFoundException(-1);
    }

    @Override
    public List<GenericOperand> selectOparandsSatisfyRestrictions(Object opcode, IMachineState stateBefore, IMachineState desiredState, IStateDelta diffWithDesiredState) throws NoSuitableOperandFoundException {
        throw new NoSuitableOperandFoundException(-1);
    }

    @Override
    public List<List<GenericOperand>> getAllPossibleOperandSets(Object opcode, IMachineState state) throws NoSuitableOperandFoundException {
        return Collections.emptyList();
    }
}
