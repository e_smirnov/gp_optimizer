package ru.ifmo.gpo.core.instructions.selectors;

import ru.ifmo.gpo.core.instructions.IOperandSelector;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.vmstate.IMachineState;
import ru.ifmo.gpo.core.vmstate.IStateDelta;
import ru.ifmo.gpo.core.vmstate.NoSuitableOperandFoundException;

import java.util.Collections;
import java.util.List;


/**
 * User: jedi-philosopher
 * Date: 11.12.2010
 * Time: 16:24:40
 *
 * Operand selector that is suitable for any target architecture. Always successfully returns empty list of operands. Never throws exceptions.
 */
public class DummyOperandSelector<OpcodeClass, StateClass extends IMachineState, StateDeltaClass extends IStateDelta> implements IOperandSelector<OpcodeClass, StateClass, StateDeltaClass> {

    private static final DummyOperandSelector instance = new DummyOperandSelector();

    public static IOperandSelector getInstance() {
        return instance;
    }

    private DummyOperandSelector() {
    }

    @Override
    public List<GenericOperand> selectOperandsRandom(OpcodeClass opcode, StateClass state) {
        return Collections.emptyList();
    }

    @Override
    public List<GenericOperand> selectOparandsSatisfyRestrictions(OpcodeClass opcode, StateClass stateBefore, StateClass desiredState, StateDeltaClass diffWithDesiredState) {
        return Collections.emptyList();
    }

    @Override
    public List<List<GenericOperand>> getAllPossibleOperandSets(
            OpcodeClass opcode, StateClass state)
            throws NoSuitableOperandFoundException {
        return Collections.emptyList();
    }
}
