package ru.ifmo.gpo.core.instructions.generic;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * User: jedi-philosopher
 * Date: 12.07.11
 * Time: 20:30
 * <p/>
 * Base class for a generic instruction, suitable for most common cases
 */
public class BaseGenericInstruction implements IGenericInstruction {

    private int opcode;

    private List<GenericOperand> parameters = new LinkedList<GenericOperand>();

    public BaseGenericInstruction(int opcode, GenericOperand... params) {
        this.opcode = opcode;
        if (params.length > 0) {
            Collections.addAll(this.parameters, params);
        }
    }

    public BaseGenericInstruction(int opcode, Collection<GenericOperand> params) {
        this.opcode = opcode;
        if (!params.isEmpty()) {
            this.parameters.addAll(params);
        }
    }

    @Override
    public int getOpcode() {
        return opcode;
    }

    @Override
    public List<GenericOperand> getParameters() {
        return parameters;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseGenericInstruction that = (BaseGenericInstruction) o;

        if (opcode != that.opcode) return false;
        if (parameters != null ? !parameters.equals(that.parameters) : that.parameters != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = opcode;
        result = 31 * result + (parameters != null ? parameters.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "BaseGenericInstruction{" +
                "opcode=" + opcode +
                ", parameters=" + parameters +
                '}';
    }
}
