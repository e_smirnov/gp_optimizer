package ru.ifmo.gpo.core.instructions.generic;

import org.objectweb.asm.Type;
import ru.ifmo.gpo.verify.IArithmeticTreeNode;

/**
 * User: jedi-philosopher
 * Date: 31.12.2010
 * Time: 15:21:48
 */
public abstract class GenericStackItem extends GenericOperand {

    public GenericStackItem(IArithmeticTreeNode expression, Type elementType) {
        super(expression, elementType);
    }
}
