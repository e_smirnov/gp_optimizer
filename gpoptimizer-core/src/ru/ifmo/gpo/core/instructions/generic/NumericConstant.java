package ru.ifmo.gpo.core.instructions.generic;

import org.objectweb.asm.Type;
import ru.ifmo.gpo.verify.NumericConstantTreeNode;

public class NumericConstant extends GenericOperand {

    private Number constantValue;
    private static final long serialVersionUID = 9212071283683760833L;

    public NumericConstant() {
        this(0);
    }

    public NumericConstant(Number val) {
        this(val, Type.getType(val.getClass()));
    }

    public NumericConstant(Number val, Type t) {
        super(new NumericConstantTreeNode(t, val), t);
        this.constantValue = val;
    }

    public Number getValue() {
        return constantValue;
    }

    public int intValue() {
        return constantValue.intValue();
    }

    public double doubleValue() {
        return constantValue.doubleValue();
    }


    @Override
    public String getId() {
        // TODO Auto-generated method stub
        return constantValue.toString();
    }

    @Override
    public String toString() {
        return constantValue.toString();
    }


}
