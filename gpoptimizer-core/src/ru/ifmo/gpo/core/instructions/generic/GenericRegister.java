package ru.ifmo.gpo.core.instructions.generic;

import org.objectweb.asm.Type;
import ru.ifmo.gpo.verify.IArithmeticTreeNode;

/**
 * User: jedi-philosopher
 * Date: 31.12.2010
 * Time: 15:20:26
 */
public abstract class GenericRegister extends GenericOperand {

    protected GenericRegister() {
    }

    protected GenericRegister(IArithmeticTreeNode expression, Type type) {
        super(expression, type);
    }

    protected GenericRegister(IArithmeticTreeNode expression, Type type, AccessType accessType) {
        super(expression, type, accessType);
    }
}
