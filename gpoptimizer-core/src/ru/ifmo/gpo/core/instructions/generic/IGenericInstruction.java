/**
 * User: jedi-philosopher
 * Date: 31.12.2010
 * Time: 15:18:02
 */
package ru.ifmo.gpo.core.instructions.generic;

import java.util.List;

/**
 * Interface for instructions, that form code sequences that must be optimized.
 */
public interface IGenericInstruction {

    /**
     * Returns unique instruction opcode, platform-dependent
     */
    public int getOpcode();

    /**
     * Returns list of instruction arguments
     */
    public List<GenericOperand> getParameters();
}
