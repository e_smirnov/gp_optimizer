package ru.ifmo.gpo.core.instructions.generic;

import org.objectweb.asm.Type;
import ru.ifmo.gpo.verify.IArithmeticTreeNode;
import ru.ifmo.gpo.verify.UnknownValueNode;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * User: jedi-philosopher
 * Date: 31.12.2010
 * Time: 15:18:30
 * <p/>
 * Base class for operands of instructions. Operands serve for two purposes: as keys and as values.
 * VM state usually has methods like 'get/set value for operand'. So implementation of an instruction looks like:
 * 1) extract GenericOperands from instruction
 * 2) get actual state of resources referenced by these operands
 * 3) update state if necessary
 * <p/>
 * Generic operand serves both as key and as value. So method signature is smth like
 * GenericOperand getValue(GenericOperand key);
 * Key may have expression/type members not set, only its id() matters
 * Object returned from method will have them all.
 */
public abstract class GenericOperand implements Serializable {

    public enum AccessType {
        READ,
        WRITE,
        ANY,
        NONE
    }

    protected transient IArithmeticTreeNode expression;
    protected transient Type type;
    protected transient AccessType accessType;

    public abstract String getId();

    public boolean isWriteable() {
        return accessType == AccessType.WRITE || accessType == AccessType.ANY;
    }

    public boolean isReadable() {
        return accessType == AccessType.READ || accessType == AccessType.ANY;
    }

    /**
     * Elevates acces type for resource referenced by this operand. ANY>WRITE>READ>NONE
     * Write access can be added to read to produce ANY access
     *
     * @param type Access type to add
     */
    public void addAccessType(AccessType type) {
        if (type == AccessType.ANY
                || (isReadable() && type == AccessType.WRITE)
                || (isWriteable() && type == AccessType.READ)) {
            accessType = AccessType.ANY;
        } else if (accessType == AccessType.NONE) {
            accessType = type;
        }
    }

    /**
     * Complex operands can contain multiple expressions and multiple nodes (like 4 component shader registers)
     * They can override this method to provide full list of expressions that should be verified.
     * Operands of same type should be returned in same order, as they are verified one-to-one
     *
     * @return List of all arithmetic expressions contained in this operand. Used for equality verification purposes.
     */
    public IArithmeticTreeNode[] getExpressions() {
        IArithmeticTreeNode[] rz = new IArithmeticTreeNode[1];
        rz[0] = expression;
        return rz;
    }

    public boolean hasAccessType(AccessType type) {
        switch (type) {
            case NONE:
                return true;
            case READ:
                return isReadable();
            case WRITE:
                return isWriteable();
            case ANY:
                return accessType == AccessType.ANY;
            default:
                throw new IllegalArgumentException("Unsupported value for AccessType " + type);
        }
    }

    /**
     * Checks if this instance contains actual state of a resource.
     *
     * @return true if actual state is stored inside. Otherwise this instance is just a key to get this value
     */
    public boolean hasValue() {
        return !(expression instanceof UnknownValueNode);
    }

    public static List<GenericOperand> filterWriteable(Collection<GenericOperand> operands) {
        List<GenericOperand> rz = new LinkedList<GenericOperand>();
        for (GenericOperand op : operands) {
            if (op.isWriteable()) {
                rz.add(op);
            }
        }

        return rz;
    }

    public static List<GenericOperand> filterReadable(Collection<GenericOperand> operands) {
        List<GenericOperand> rz = new LinkedList<GenericOperand>();
        for (GenericOperand op : operands) {
            if (op.isReadable()) {
                rz.add(op);
            }
        }

        return rz;
    }

    protected GenericOperand() {
        this.expression = new UnknownValueNode();
        this.type = Type.VOID_TYPE;
        this.accessType = AccessType.NONE;
    }

    protected GenericOperand(IArithmeticTreeNode expression, Type type) {
        this.expression = expression;
        this.type = type;
        this.accessType = AccessType.NONE;
    }

    protected GenericOperand(IArithmeticTreeNode expression, Type type, AccessType accessType) {
        this.expression = expression;
        this.type = type;
        this.accessType = accessType;
    }

    public IArithmeticTreeNode getExpression() {
        return expression;
    }

    public void setAccessType(AccessType accessType) {
        this.accessType = accessType;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public AccessType getAccessType() {
        return accessType;
    }

    public void setExpression(IArithmeticTreeNode expression) {
        this.expression = expression;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        GenericOperand other = (GenericOperand) obj;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "GenericOperand [expression=" + expression + ", type=" + type
                + ", accessType=" + accessType + "]";
    }


}
