package ru.ifmo.gpo.core.instructions;

/**
 * User: jedi-philosopher
 * Date: 12.12.2010
 * Time: 18:40:31
 */
public class SequenceGenerationFailedException extends Exception {
    private static final long serialVersionUID = 6439760833092591460L;

    public static enum Cause {
        NO_INSTRUCTION_POSSIBLE,
        TOO_MANY_INSTRUCTIONS,
        ERROR_DURING_GENERATION
    }

    private InstructionSequence generatedSequence;

    public SequenceGenerationFailedException(Cause cause) {
        super("Instruction sequence generation failed: " + cause.toString());
    }

    public SequenceGenerationFailedException(Cause cause, InstructionSequence generatedSequence) {
        this(cause);
        this.generatedSequence = generatedSequence;
    }

    public SequenceGenerationFailedException(Cause cause, Throwable ex, InstructionSequence generatedSequence) {
        super(cause.toString(), ex);
        this.generatedSequence = generatedSequence;
    }

    public InstructionSequence getGeneratedSequence() {
        return generatedSequence;
    }
}
