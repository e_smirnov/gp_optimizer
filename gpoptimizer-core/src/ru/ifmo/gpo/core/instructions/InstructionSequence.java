package ru.ifmo.gpo.core.instructions;

import org.jgap.Chromosome;
import org.jgap.Gene;
import org.jgap.IChromosome;
import org.jgap.InvalidConfigurationException;
import ru.ifmo.gpo.core.CodeGene;
import ru.ifmo.gpo.core.GPConfiguration;
import ru.ifmo.gpo.core.instructions.generic.IGenericInstruction;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * User: jedi-philosopher
 * Date: 29.05.2010
 * Time: 23:41:51
 * <p/>
 * Adapter class that represents sequence of instructions. Contains helper functions to convert sequence to other
 * representations.
 */

public class InstructionSequence extends ArrayList<IGenericInstruction> implements Serializable {
    private static final long serialVersionUID = 5680812744413094512L;

    public InstructionSequence() {
        super();
    }

    public InstructionSequence(Collection<? extends IGenericInstruction> source) {
        super(source);
    }

    public InstructionSequence(IGenericInstruction[] instructions) {
        for (IGenericInstruction instr : instructions) {
            add(instr);
        }
    }

    public InstructionSequence(IChromosome chromosome) {
        for (Gene g : chromosome.getGenes()) {
            if (g == null) {
                continue;
            }
            if (!(g instanceof CodeGene)) {
                throw new IllegalArgumentException("InstructionSequence can be created only from Chromosome that contains CodeGenes");
            }
            CodeGene codeGene = (CodeGene) g;
            add(((GPConfiguration) codeGene.getConfiguration()).getTargetArchitecture().cloneInstruction(codeGene.getInstruction()));
        }
    }

    public IChromosome toChromosome(GPConfiguration conf) throws InvalidConfigurationException {
        IChromosome rz = new Chromosome(conf);
        Gene[] genes = new Gene[size()];

        for (int i = 0; i < size(); ++i) {
            genes[i] = new CodeGene(conf, get(i), rz);
        }
        rz.setGenes(genes);
        return rz;
    }

    public String outputFormatted() {
        StringBuilder builder = new StringBuilder();
        for (IGenericInstruction instr : this) {
            builder.append(instr.toString()).append("\n");
        }
        return builder.toString();
    }

}
