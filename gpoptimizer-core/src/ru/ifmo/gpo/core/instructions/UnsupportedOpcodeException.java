package ru.ifmo.gpo.core.instructions;

/**
 * User: e_smirnov
 * Date: 19.01.2011
 * Time: 16:07:13
 */
public class UnsupportedOpcodeException extends RuntimeException {
    private static final long serialVersionUID = 2821187174496116228L;

    public UnsupportedOpcodeException(int opcode) {
        super("Unsupported opcode value: " + opcode);
    }

    public UnsupportedOpcodeException(String mnemonics) {
        super("Unsupported opcode: " + mnemonics);
    }
}
