package ru.ifmo.gpo.core.instructions;

import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.vmstate.IMachineState;
import ru.ifmo.gpo.core.vmstate.IStateDelta;
import ru.ifmo.gpo.core.vmstate.NoSuitableOperandFoundException;

import java.util.List;


/**
 * User: jedi-philosopher
 * Date: 11.12.2010
 * Time: 0:33:04
 * <p/>
 * Class that performs selection of suitable operand for an instruction, considering current and desired VM state
 */
public interface IOperandSelector<OpcodeClass, StateClass extends IMachineState, StateDeltaClass extends IStateDelta> {

    /**
     * Returns random suitable operand. For example, for write store instruction will return arbitrary var idx, taken from list
     * of variables, available for writing.
     *
     * @param opcode Instruction to select operand for
     * @param state  State of VM prior to instruction execution
     * @return Operand for an instruction (may be null if instruction really does not need operand)
     * @throws ru.ifmo.gpo.core.vmstate.NoSuitableOperandFoundException
     *          If failed to find any suitable operand with given VM state
     */
    public List<GenericOperand> selectOperandsRandom(OpcodeClass opcode, StateClass state) throws NoSuitableOperandFoundException;

    /**
     * Returns operand selected in such way, that it will make VM state closer to desiredState. i.e. if desired state needs INT variable
     * in slot 0 (and it is not there in stateBefore yet), this method may return 0 as operand for ISTORE opcode.
     *
     * @param opcode               Instruction to select operand for
     * @param stateBefore          State of VM prior to instruction execution
     * @param desiredState         State of VM, that is desired at some moment after instruction execution. This method must return operand in such way,
     *                             that state immediately after opcode execution will be closer to desiredState than stateBefore
     * @param diffWithDesiredState Difference between current and desired states.
     * @return Operand for an instruction
     * @throws NoSuitableOperandFoundException
     *          If failed to find any suitable operand, or if all possible operands lead to increase of delta between current
     *          and desired states.
     */
    public List<GenericOperand> selectOparandsSatisfyRestrictions(OpcodeClass opcode, StateClass stateBefore, StateClass desiredState, StateDeltaClass diffWithDesiredState) throws NoSuitableOperandFoundException;

    /**
     * Returns all sets of operands that are possible given current vm state
     * Used only for testing
     *
     * @param opcode
     * @param state
     * @return
     */
    public List<List<GenericOperand>> getAllPossibleOperandSets(OpcodeClass opcode, StateClass state) throws NoSuitableOperandFoundException;
}
