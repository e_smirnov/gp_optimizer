package ru.ifmo.gpo.core;

import org.jgap.*;
import ru.ifmo.gpo.core.instructions.generic.IGenericInstruction;

/**
 * Created by IntelliJ IDEA.
 * User: jedi-philosopher
 * Date: 08.11.2009
 * Time: 15:52:31
 * <p/>
 * Representation of a gene used in genetic process. Contains a single instruction.
 * Extends internal JGAP class
 */
public class CodeGene extends BaseGene implements Gene {
    /**
     * Instruction stored in this gene
     */
    private IGenericInstruction instr;

    /**
     * Chromosome to which this gene belongs
     */
    private IChromosome owner;

    private static final long serialVersionUID = 533759937675699281L;

    @Override
    public String toString() {
        if (instr != null) {
            return instr.toString();
        }
        return "NOP";
    }


    public CodeGene(Configuration configuration) throws InvalidConfigurationException {
        super(configuration);
        instr = ((GPConfiguration) configuration).getTargetArchitecture().createNOPInstruction();
    }

    public CodeGene(Configuration conf, IChromosome owner) throws InvalidConfigurationException {
        super(conf);
        this.owner = owner;
        instr = ((GPConfiguration) conf).getTargetArchitecture().createNOPInstruction();
    }

    public CodeGene(Configuration conf, IGenericInstruction ins, IChromosome owner) throws InvalidConfigurationException {
        super(conf);
        this.owner = owner;
        instr = ins;
    }

    public IGenericInstruction getInstruction() {
        return instr;
    }

    protected Object getInternalValue() {
        return instr;
    }

    protected Gene newGeneInternal() {
        try {
            return new CodeGene(getConfiguration(), instr, owner);
        } catch (InvalidConfigurationException e) {
            throw new IllegalStateException(e);
        }
    }

    public void setAllele(Object o) {
        if (o instanceof IGenericInstruction) {
            instr = (IGenericInstruction) o;
        }
    }

    public String getPersistentRepresentation() throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    public void setValueFromPersistentRepresentation(String s) throws UnsupportedOperationException, UnsupportedRepresentationException {
        throw new UnsupportedOperationException();

    }

    public void setToRandomValue(RandomGenerator randomGenerator) {
        throw new UnsupportedOperationException("setToRandomValue is not supported by CodeGene, use InitialPopulationGenerator instead of manual creation of genes");
    }

    public void applyMutation(int i, double v) {
        throw new UnsupportedOperationException("CodeGene cannot be used with standart mutation operations ");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        CodeGene codeGene = (CodeGene) o;

        return !(instr != null ? !instr.equals(codeGene.instr) : codeGene.instr != null);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (instr != null ? instr.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof CodeGene) {
            CodeGene aGene = (CodeGene) o;
            if (instr.equals(aGene.instr)) {
                return 0;
            }
            return instr.getOpcode() - aGene.instr.getOpcode();
        } else {
            throw new ClassCastException();
        }
    }

    public int size() {
        return 2;
    }
}
