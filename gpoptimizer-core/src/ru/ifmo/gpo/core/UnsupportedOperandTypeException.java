package ru.ifmo.gpo.core;

import org.objectweb.asm.Type;

/**
 * User: jedi-philosopher
 * Date: 13.09.2010
 * Time: 22:05:41
 */
public class UnsupportedOperandTypeException extends RuntimeException {
    private transient Type type;
    private transient Class clazz;
    private static final long serialVersionUID = -1941281868910989858L;

    public UnsupportedOperandTypeException(Type type) {
        super("Unsupported operand type: " + type);
        this.type = type;
    }

    public UnsupportedOperandTypeException(Class clazz) {
        super("Unsupported operand type: " + clazz.getSimpleName());
        this.clazz = clazz;
    }

    public Class getClazz() {
        return clazz;
    }

    public Type getType() {
        return type;
    }
}
