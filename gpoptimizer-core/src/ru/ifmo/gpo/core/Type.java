package ru.ifmo.gpo.core;

/**
 * User: e_smirnov
 * Date: 26.08.2011
 * Time: 14:49:17
 * <p/>
 * Set of type IDs that are used in code analysis. Contains some general types, that exist in most every programming language
 * If implementation of target architecture needs that, it can extend list of type constants
 */
public interface Type {
    public final static int INT_TYPE = 0;
    public final static int FLOAT_TYPE = 1;
    public final static int LONG_TYPE = 2;
    public final static int DOUBLE_TYPE = 3;
    public final static int POINTER_TYPE = 4;

    // for cases when variable is used only with untyped instructions, so that its type can not be deduced
    public final static int UNKNOWN_TYPE = 100;
    // for some special cases, where variable is actually not a variable
    public final static int NO_TYPE = -1;
}
