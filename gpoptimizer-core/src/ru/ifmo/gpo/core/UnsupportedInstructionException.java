package ru.ifmo.gpo.core;

import ru.ifmo.gpo.core.instructions.generic.IGenericInstruction;

/**
 * User: jedi-philosopher
 * Date: 02.09.2010
 * Time: 23:31:58
 */
public class UnsupportedInstructionException extends RuntimeException {

    private IGenericInstruction instr;
    private static final long serialVersionUID = 1538603098449676052L;

    public UnsupportedInstructionException(IGenericInstruction instr) {
        super("Unsupported instruction with opcode " + instr.getOpcode());
        this.instr = instr;
    }

    public IGenericInstruction getInstr() {
        return instr;
    }
}
