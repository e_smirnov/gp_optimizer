package ru.ifmo.gpo.core;

import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.vmstate.IStateSchema;

/**
 * Interface for generating runnable platform-dependent code from given chromosome. There are 2 possible code generation modes.
 * In first mode, 'sandboxed' code is generated. This code is desired for obtaining computational results from chromosome. Such results are later used to check
 * correctnes and fitness. This code can contain any amount of supporting instructions.
 * In second mode, profile code is generated. This code should be as close as possible to chromosome instruction set and is used to measure chromosome performance.
 */
public interface ICodeGenerator {
    /**
     * Generates code from given chromosome, that is used to measure computation speed. Should be as close to its source as possible.
     *
     * @param chromosome     Chromosome to be checked for performance
     * @param stateSchema    Information about computational resources (registers, memory, etc) used by chromosome
     * @param additionalData Platform-dependent additional data
     * @return Generated code that can be executed by corresponding ICodeExecutor.profileCode() implementation
     */
    public ICodeGenerationResult generateProfileCode(InstructionSequence chromosome, IStateSchema stateSchema, Object additionalData);

    /**
     * Generates 'sandboxeed' code for given chromosome. This code should be used to retrieve values, computed by chromosome contents and may contain any overhead
     * needed for that purpose.
     *
     * @param chromosome     Chromosome for which code should be generated
     * @param stateSchema    Information about computational resources (registers, memory, etc) used by chromosome
     * @param additionalData Platform-dependent additional data
     * @return Generated code that can be executed by corresponding ICodeExecutor.executeCode() implementation
     */
    public ICodeGenerationResult generateSandboxedCode(InstructionSequence chromosome, IStateSchema stateSchema, Object additionalData);
}