package ru.ifmo.gpo.core;

import org.jgap.DefaultFitnessEvaluator;
import org.jgap.InvalidConfigurationException;
import org.jgap.event.EventManager;
import org.jgap.impl.StandardPostSelector;
import org.jgap.impl.StockRandomGenerator;
import ru.ifmo.gpo.core.operators.CodeCrossoverOperation;
import ru.ifmo.gpo.core.operators.CodeRangeMutationOperator;

/**
 * User: jedi-philosopher Date: 15.09.2010 Time: 21:59:29
 */
public class SupportIslandGPConfiguration extends GPConfiguration {

    public static final String NAME = "supportIslandConf";
    private static final long serialVersionUID = 2674378266290038980L;

    public SupportIslandGPConfiguration(ITargetArchitecture targetArchitecture) throws InvalidConfigurationException {
        super(targetArchitecture, NAME, "Configuration for second island");
    }

    @Override
    public void setUpConfiguration() throws InvalidConfigurationException {
        if (reference == null) {
            throw new IllegalStateException(
                    "setUpConfiguration() must be called aftger setReferenceChromosome()");
        }

        setPopulationSize(100);
        setAlwaysCaculateFitness(false);
        setKeepPopulationSizeConstant(false);
        setPreservFittestIndividual(true);
        addGeneticOperator(new CodeRangeMutationOperator(this, 6));
        addGeneticOperator(new CodeCrossoverOperation(this, 4));
        setFitnessEvaluator(new DefaultFitnessEvaluator());
        setRandomGenerator(new StockRandomGenerator());
        addNaturalSelector(new StandardPostSelector(this), false);
        EventManager eventManager = new EventManager();
        setEventManager(eventManager);
    }
}
