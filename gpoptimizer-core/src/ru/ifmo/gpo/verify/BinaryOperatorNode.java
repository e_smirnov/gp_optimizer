package ru.ifmo.gpo.verify;

/**
 * Created by IntelliJ IDEA.
 * User: jedi-philosopher
 * Date: 06.03.2010
 * Time: 19:14:34
 */
public class BinaryOperatorNode implements IArithmeticTreeNode {

    private static final long serialVersionUID = -7774285936593385900L;

    private IArithmeticTreeNode left;

    private IArithmeticTreeNode right;

    private GenericOperator operator;


    public void accept(IArithmeticTreeVisitor visitor) {
        visitor.visitBinaryTreeNode(this);
    }

    public IArithmeticTreeNode getLeft() {
        return left;
    }

    public IArithmeticTreeNode getRight() {
        return right;
    }

    public GenericOperator getOperator() {
        return operator;
    }

    public BinaryOperatorNode(IArithmeticTreeNode left, IArithmeticTreeNode right, GenericOperator operator) {
        this.left = left;
        this.right = right;
        this.operator = operator;
    }

    @Override
    public String toString() {
        return "(" + left + " " + operator + " " + right + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BinaryOperatorNode that = (BinaryOperatorNode) o;

        if (left != null ? !left.equals(that.left) : that.left != null) return false;
        if (operator != that.operator) return false;
        if (right != null ? !right.equals(that.right) : that.right != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = left != null ? left.hashCode() : 0;
        result = 31 * result + (right != null ? right.hashCode() : 0);
        result = 31 * result + (operator != null ? operator.hashCode() : 0);
        return result;
    }
}
