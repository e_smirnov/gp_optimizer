/**
 * User: jedi-philosopher
 * Date: 06.03.2010
 * Time: 20:31:13
 */
package ru.ifmo.gpo.verify;

import org.objectweb.asm.Type;

/**
 * Node for input value of a code sequence. Its exact value is unknown and depends on code usage context.
 */
public class VariableNode implements IArithmeticTreeNode {
    private static final long serialVersionUID = -2387413754115817566L;

    private String name;

    private Type type;

    public void accept(IArithmeticTreeVisitor visitor) {
        visitor.visitVariable(this);
    }

    public VariableNode(String name, Type type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public Type get_type() {
        return type;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VariableNode that = (VariableNode) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

}
