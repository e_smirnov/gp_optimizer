package ru.ifmo.gpo.verify;

/**
 * User: e_smirnov
 * Date: 19.01.2011
 * Time: 16:14:08
 */
public enum GenericOperator {
    PLUS,
    MINUS,
    MUL,
    DIV,
    REM,
    SHL,
    SHR,
    AND,
    OR,
    XOR,
    NOT, NEG, FADD, FSUB, FMUL, FDIV, FNEG, FREM,
}
