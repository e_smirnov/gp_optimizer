package ru.ifmo.gpo.verify;

/**
 * User: jedi-philosopher
 * Date: 06.03.2010
 * Time: 20:28:36
 */

public class IntegerConstantTreeNode implements IArithmeticTreeNode {

    private static final long serialVersionUID = 5372867252230382829L;

    private int value;

    public void accept(IArithmeticTreeVisitor visitor) {
        visitor.visitIntConstant(this);
    }

    public IntegerConstantTreeNode(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "" + value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IntegerConstantTreeNode that = (IntegerConstantTreeNode) o;

        if (value != that.value) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return value;
    }
}
