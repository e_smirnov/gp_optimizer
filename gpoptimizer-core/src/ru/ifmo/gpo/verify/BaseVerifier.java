/**
 * User: jedi-philosopher
 * Date: 19.12.2010
 * Time: 16:40:16
 */

package ru.ifmo.gpo.verify;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ifmo.gpo.core.ITargetArchitecture;
import ru.ifmo.gpo.core.Settings;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.instructions.generic.GenericStackItem;
import ru.ifmo.gpo.core.vmstate.IInstructionSequenceAnalyzer;
import ru.ifmo.gpo.core.vmstate.IMachineState;
import ru.ifmo.gpo.core.vmstate.IStateSchema;

import java.lang.reflect.Constructor;
import java.util.Iterator;
import java.util.Map;

public abstract class BaseVerifier implements IChromosomeVerifier {

    private static final long serialVersionUID = 671832173687541934L;

    protected abstract VerificationResult verifyEquivalence(IArithmeticTreeNode first, IArithmeticTreeNode second);

    protected static Logger logger = LoggerFactory.getLogger(BaseVerifier.class);

    protected ITargetArchitecture arch;

    protected BaseVerifier(ITargetArchitecture arch) {
        this.arch = arch;
    }

    /**
     * Verifies that expression stored in two instruction operands (registers for example) are equal
     */
    private VerificationResult processTwoOperands(GenericOperand firstItem, GenericOperand secondItem) {
        if (!firstItem.getType().equals(secondItem.getType())) {
            return VerificationResult.RESULT_FAIL;
        }

        final IArithmeticTreeNode[] firstExpressions = firstItem.getExpressions();
        final IArithmeticTreeNode[] secondExpressions = secondItem.getExpressions();

        if (firstExpressions.length != secondExpressions.length) {
            logger.error("Items of type {} and {} returned different amount of subexpressions ({} and {}), their equivalence can not be verified. This can happen if you try to compare multidimensional register with a single cell of a register");
            throw new IllegalStateException("Items of type '" + firstItem.getClass() + "' and '" + secondItem.getClass() + "' returned different amount of subexpressions");
        }
        for (int i = 0; i < firstExpressions.length; ++i) {
            VerificationResult rz = verifyEquivalence(firstExpressions[i], secondExpressions[i]);
            if (rz != VerificationResult.RESULT_SUCCESS) {
                return rz;
            }
        }
        return VerificationResult.RESULT_SUCCESS;
    }

    /**
     * Verifies that expression stored in i-th element of first iterable is equal to that in i-th element of second iterable
     */
    private VerificationResult processSequence(Iterator<? extends GenericOperand> firstIterator, Iterator<? extends GenericOperand> secondIterator) {
        // verifying stack state
        while (firstIterator.hasNext() && secondIterator.hasNext()) {
            GenericOperand firstItem = firstIterator.next();
            GenericOperand secondItem = secondIterator.next();

            VerificationResult vr = processTwoOperands(firstItem, secondItem);
            if (vr != VerificationResult.RESULT_SUCCESS) {
                return vr;
            }
        }
        return VerificationResult.RESULT_SUCCESS;
    }

    /**
     * Verifies that expressions are equal for values that have same keys in both maps
     */
    private VerificationResult processMap(Map<String, ? extends GenericOperand> first, Map<String, ? extends GenericOperand> second) {
        if (!first.keySet().equals(second.keySet())) {
            return VerificationResult.RESULT_FAIL;
        }

        for (Map.Entry<String, ? extends GenericOperand> entry : first.entrySet()) {
            GenericOperand firstItem = entry.getValue();
            GenericOperand secondItem = second.get(entry.getKey());
            if (secondItem == null) {
                return VerificationResult.RESULT_FAIL;
            }

            if (!firstItem.isWriteable() && !secondItem.isWriteable()) {
                // no need to check this pair as they are read-only
                continue;
            }

            VerificationResult rz = processTwoOperands(firstItem, secondItem);
            if (rz != VerificationResult.RESULT_SUCCESS) {
                return rz;
            }
        }

        return VerificationResult.RESULT_SUCCESS;
    }

    @Override
    public VerificationResult verifyEquivalence(InstructionSequence first, InstructionSequence second, IStateSchema stateSchema) {
        try {
            IInstructionSequenceAnalyzer firstAnalyzer = arch.analyzeCode(first, stateSchema);
            IInstructionSequenceAnalyzer secondAnalyzer = arch.analyzeCode(second, stateSchema);

            IMachineState firstState = firstAnalyzer.getStateAfter();
            IMachineState secondState = secondAnalyzer.getStateAfter();

            if (firstState.getStack().size() != secondState.getStack().size()) {
                return VerificationResult.RESULT_FAIL;
            }

            Iterator<? extends GenericStackItem> firstIterator = firstState.getStack().iterator();
            Iterator<? extends GenericStackItem> secondIterator = secondState.getStack().iterator();

            VerificationResult rz = processSequence(firstIterator, secondIterator);
            if (rz != VerificationResult.RESULT_SUCCESS) {
                return rz;
            }

            rz = processMap(firstState.getRegisters(), secondState.getRegisters());
            if (rz != VerificationResult.RESULT_SUCCESS) {
                return rz;
            }

            rz = processMap(firstState.getMemory(), secondState.getMemory());
            if (rz != VerificationResult.RESULT_SUCCESS) {
                return rz;
            }

            return VerificationResult.RESULT_SUCCESS;
        } catch (Exception ex) {
            logger.error("Unexpected error running verification: ", ex);
            return VerificationResult.RESULT_INTERNAL_ERROR;
        }
    }

    /**
     * Factory method, creates subclass specified in config file
     */
    public static IChromosomeVerifier createVerifierFromConfig(ITargetArchitecture target) {
        String verifierClassName = Settings.getProperty("core.verification.engine");
        if (verifierClassName == null) {
            throw new IllegalStateException("No verification engine specified, please define 'core.verification.engine' property");
        } else {
            try {
                Class<? extends IChromosomeVerifier> c = (Class<? extends IChromosomeVerifier>) Class.forName(verifierClassName);
                Constructor<? extends IChromosomeVerifier> ctor = c.getConstructor(ITargetArchitecture.class);
                if (ctor == null) {
                    return c.newInstance();
                }
                return ctor.newInstance(target);
            } catch (Exception e) {
                logger.warn("Failed to load verifier class " + verifierClassName + " :" + e);
            }
        }
        return null;
    }
}
