package ru.ifmo.gpo.verify;

import java.io.Serializable;

/**
 * Node of arithmetic tree that represent expression stored in some register-memory-stack item.
 * These trees are created and compared during verification
 */
public interface IArithmeticTreeNode extends Serializable {

    public void accept(IArithmeticTreeVisitor visitor);

}