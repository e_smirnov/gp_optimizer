package ru.ifmo.gpo.verify;

import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.vmstate.IStateSchema;

import java.io.Serializable;

/**
 * Interface for chromosome verifiers. Verifier should check that 2 given instruction sequences produce exactly same results.
 * Classes that implement this interface should have no-arg constructor, or constructor that takes ITargetArchitecture instance.
 * Serializable, because verifier instances are sent across network in VerificationTask
 *
 * @author jedi-philosopher
 */
public interface IChromosomeVerifier extends Serializable {
    public VerificationResult verifyEquivalence(InstructionSequence first, InstructionSequence second, IStateSchema stateSchema);
}