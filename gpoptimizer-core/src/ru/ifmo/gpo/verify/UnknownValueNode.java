package ru.ifmo.gpo.verify;

/**
 * User: jedi-philosopher
 * Date: 05.09.2010
 * Time: 21:20:47
 */
public class UnknownValueNode implements IArithmeticTreeNode {
    private static final long serialVersionUID = 1252061424671298218L;

    public void accept(IArithmeticTreeVisitor visitor) {
        visitor.visitUnknown(this);
    }

    @Override
    public String toString() {
        return "<unknown>";
    }

    @Override
    public boolean equals(Object o) {
        return this == o || !(o == null || getClass() != o.getClass());
    }
}
