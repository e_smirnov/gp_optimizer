package ru.ifmo.gpo.verify;

import org.objectweb.asm.Type;

/**
 * User: e_smirnov
 * Date: 26.11.2010
 * Time: 16:14:22
 */
public class NumericConstantTreeNode implements IArithmeticTreeNode {

    private static final long serialVersionUID = 9222686487450312670L;

    private Type constantType;

    private Number constantValue;

    public NumericConstantTreeNode(Type constantType, Number constantValue) {
        this.constantType = constantType;
        this.constantValue = constantValue;
    }

    public Type getConstantType() {
        return constantType;
    }

    public Number getConstantValue() {
        return constantValue;
    }

    @Override
    public String toString() {
        return constantValue.toString();
    }

    @Override
    public void accept(IArithmeticTreeVisitor visitor) {
        visitor.visitNumericConstant(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NumericConstantTreeNode that = (NumericConstantTreeNode) o;

        if (constantType != null ? !constantType.equals(that.constantType) : that.constantType != null) return false;
        if (constantValue != null ? !constantValue.equals(that.constantValue) : that.constantValue != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = constantType != null ? constantType.hashCode() : 0;
        result = 31 * result + (constantValue != null ? constantValue.hashCode() : 0);
        return result;
    }
}
