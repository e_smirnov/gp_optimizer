/**
 * Created by IntelliJ IDEA.
 * User: Egor.Smirnov
 * Date: 21.02.12
 * Time: 15:10
 */

package ru.ifmo.gpo.verify;

/**
 * Node for computation of min or max of two node values
 */
public class MinMaxNode implements IArithmeticTreeNode {

    private static final long serialVersionUID = 6287268358274810885L;

    private IArithmeticTreeNode left;

    private IArithmeticTreeNode right;

    private String op;

    public static final String MIN = "<";

    public static final String MAX = ">";

    public MinMaxNode(IArithmeticTreeNode left, IArithmeticTreeNode right, String op) {
        this.left = left;
        this.right = right;
        this.op = op;
    }

    public IArithmeticTreeNode getLeft() {
        return left;
    }

    public IArithmeticTreeNode getRight() {
        return right;
    }

    public String getOp() {
        return op;
    }

    @Override
    public void accept(IArithmeticTreeVisitor visitor) {
        visitor.visitMinMaxNode(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MinMaxNode that = (MinMaxNode) o;

        if (left != null ? !left.equals(that.left) : that.left != null) return false;
        if (op != null ? !op.equals(that.op) : that.op != null) return false;
        if (right != null ? !right.equals(that.right) : that.right != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = left != null ? left.hashCode() : 0;
        result = 31 * result + (right != null ? right.hashCode() : 0);
        result = 31 * result + (op != null ? op.hashCode() : 0);
        return result;
    }
}
