/**
 * User: e_smirnov
 * Date: 11.02.2011
 * Time: 19:50:17
 */
package ru.ifmo.gpo.verify;

import org.objectweb.asm.Type;

/**
 * Node for operation of packing two small values as high and low part of a bigger one (e.g. packing 2 int32 to single int64)
 */
public class ValuePairNode implements IArithmeticTreeNode {

    private IArithmeticTreeNode value;

    private IArithmeticTreeNode lowPart;
    private IArithmeticTreeNode highPart;
    private static final long serialVersionUID = -7824602218376615276L;

    public ValuePairNode(IArithmeticTreeNode value) {
        this.value = value;

        this.highPart = new BinaryOperatorNode(value, new NumericConstantTreeNode(Type.LONG_TYPE, 0x00000000ffffffffL), GenericOperator.AND);
        this.lowPart = new BinaryOperatorNode(value, new NumericConstantTreeNode(Type.LONG_TYPE, 0xffffffff00000000L), GenericOperator.AND);
    }

    public ValuePairNode(IArithmeticTreeNode high, IArithmeticTreeNode low) {
        this.highPart = high;
        this.lowPart = low;

        IArithmeticTreeNode shifted = new BinaryOperatorNode(new TypeCastNode(high, Type.LONG_TYPE), new NumericConstantTreeNode(Type.INT_TYPE, 32), GenericOperator.SHL);
        this.value = new BinaryOperatorNode(shifted, low, GenericOperator.OR);
    }

    public IArithmeticTreeNode getHighPart() {
        return highPart;
    }

    public IArithmeticTreeNode getLowPart() {
        return lowPart;
    }

    public IArithmeticTreeNode getFullValue() {
        return value;
    }

    @Override
    public void accept(IArithmeticTreeVisitor visitor) {
        visitor.visitValuePair(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ValuePairNode that = (ValuePairNode) o;

        if (highPart != null ? !highPart.equals(that.highPart) : that.highPart != null) return false;
        if (lowPart != null ? !lowPart.equals(that.lowPart) : that.lowPart != null) return false;
        if (value != null ? !value.equals(that.value) : that.value != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = value != null ? value.hashCode() : 0;
        result = 31 * result + (lowPart != null ? lowPart.hashCode() : 0);
        result = 31 * result + (highPart != null ? highPart.hashCode() : 0);
        return result;
    }
}
