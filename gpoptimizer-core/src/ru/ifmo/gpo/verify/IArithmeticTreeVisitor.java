package ru.ifmo.gpo.verify;

/**
 * User: jedi-philosopher
 * Date: 06.03.2010
 * Time: 19:13:36
 */
public interface IArithmeticTreeVisitor {
    public void visitBinaryTreeNode(BinaryOperatorNode node);

    public void visitIntConstant(IntegerConstantTreeNode node);

    public void visitNumericConstant(NumericConstantTreeNode node);

    public void visitVariable(VariableNode node);

    public void visitUnaryOperator(UnaryOperatorNode node);

    public void visitValuePair(ValuePairNode node);

    public void visitTypeCastNode(TypeCastNode node);

    public void visitMinMaxNode(MinMaxNode node);

    public void visitUnknown(UnknownValueNode node);
}
