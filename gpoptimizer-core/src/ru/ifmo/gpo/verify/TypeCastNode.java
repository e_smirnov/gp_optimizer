package ru.ifmo.gpo.verify;

import org.objectweb.asm.Type;

public class TypeCastNode implements IArithmeticTreeNode {

    private IArithmeticTreeNode value;
    private Type targetType;
    private static final long serialVersionUID = -4814642180710879683L;

    public TypeCastNode(IArithmeticTreeNode value, Type targetType) {
        this.value = value;
        this.targetType = targetType;
    }

    public IArithmeticTreeNode getValue() {
        return value;
    }

    public Type getTargetType() {
        return targetType;
    }

    @Override
    public void accept(IArithmeticTreeVisitor visitor) {
        visitor.visitTypeCastNode(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TypeCastNode that = (TypeCastNode) o;

        if (targetType != null ? !targetType.equals(that.targetType) : that.targetType != null) return false;
        if (value != null ? !value.equals(that.value) : that.value != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = value != null ? value.hashCode() : 0;
        result = 31 * result + (targetType != null ? targetType.hashCode() : 0);
        return result;
    }
}
