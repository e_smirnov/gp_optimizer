package ru.ifmo.gpo.verify;


import org.objectweb.asm.Type;

/**
 * Contains static methods for most common tree node creation scenarios
 */
public class TreeNodeFactory {

    // creates 'original op constant' node
    public static IArithmeticTreeNode createBinaryWithConstant(IArithmeticTreeNode original, GenericOperator op, double constant)
    {
        return new BinaryOperatorNode(
                original
                , new NumericConstantTreeNode(Type.DOUBLE_TYPE, constant)
                , op
        );
    }

    // creates 'constant op original' node
    public static IArithmeticTreeNode createBinaryWithConstant( double constant, GenericOperator op, IArithmeticTreeNode original)
    {
        return new BinaryOperatorNode(
                original
                , new NumericConstantTreeNode(Type.DOUBLE_TYPE, constant)
                , op
        );
    }

}
