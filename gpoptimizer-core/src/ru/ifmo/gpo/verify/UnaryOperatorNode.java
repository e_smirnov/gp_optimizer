package ru.ifmo.gpo.verify;

/**
 * Created by IntelliJ IDEA.
 * User: jedi-philosopher
 * Date: 07.03.2010
 * Time: 20:02:33
 */
public class UnaryOperatorNode implements IArithmeticTreeNode {
    private static final long serialVersionUID = 760599333973719340L;

    public void accept(IArithmeticTreeVisitor visitor) {
        visitor.visitUnaryOperator(this);
    }

    private IArithmeticTreeNode operand;
    private GenericOperator operator;

    public enum OperatorType {
        TYPE_PREFIX,
        TYPE_POSTFIX
    }

    private OperatorType _type;

    public IArithmeticTreeNode getOperand() {
        return operand;
    }

    @Override
    public String toString() {
        if (_type == OperatorType.TYPE_PREFIX) {
            return operator + " " + operand;
        } else {
            return operand + " " + operator;
        }
    }

    public GenericOperator getOperator() {
        return operator;
    }

    public OperatorType get_type() {
        return _type;
    }

    public UnaryOperatorNode(IArithmeticTreeNode operand, GenericOperator operator, OperatorType _type) {
        this.operand = operand;
        this.operator = operator;
        this._type = _type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UnaryOperatorNode that = (UnaryOperatorNode) o;

        if (_type != that._type) return false;
        if (operand != null ? !operand.equals(that.operand) : that.operand != null) return false;
        if (operator != that.operator) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = operand != null ? operand.hashCode() : 0;
        result = 31 * result + (operator != null ? operator.hashCode() : 0);
        result = 31 * result + (_type != null ? _type.hashCode() : 0);
        return result;
    }
}
