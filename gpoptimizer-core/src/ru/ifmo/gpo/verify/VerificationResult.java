/**
 * Created by IntelliJ IDEA.
 * User: jedi-philosopher
 * Date: 27.03.2010
 * Time: 22:09:57
 */
package ru.ifmo.gpo.verify;

/**
 * Result of a verification process
 */
public enum VerificationResult {
    /**
     * Expressions calculated are proven to be equivalent
     */
    RESULT_SUCCESS,
    /**
     * Counter-example is found, when using given specific inputs expressions produce different results.
     */
    RESULT_FAIL,
    /**
     * Something strange has happened, expressions can not be compared
     */
    RESULT_UNDEFINED,

    /**
     * Unexpected error happened while processing the results
     */
    RESULT_INTERNAL_ERROR
}
