package ru.ifmo.gpo.verify;

/**
 * User: jedi-philosopher
 * Date: 18.12.2010
 * Time: 17:12:17
 */
public class VariableNameGenerator {

    private static VariableNameGenerator instance = new VariableNameGenerator();

    public static VariableNameGenerator getInstance() {
        return instance;
    }

    public String getUnderflowStackVarName(int idx) {
        return "stack" + idx;
    }

    public String getLocalVarName(int idx) {
        return "localVar" + idx;
    }


}
