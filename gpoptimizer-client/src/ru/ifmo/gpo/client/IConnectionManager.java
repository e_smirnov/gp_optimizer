package ru.ifmo.gpo.client;

import ru.ifmo.gpo.protocol.PatternItem;
import ru.ifmo.gpo.protocol.ServerVersionInfo;

import java.io.IOException;
import java.net.InetAddress;

/**
 * Created by IntelliJ IDEA.
 * User: jedi-philosopher
 * Date: 27.03.2010
 * Time: 22:15:45
 * <p/>
 * Interface for sending optimization tasks to server.
 */
public interface IConnectionManager {
    /**
     * Initialize connectino to server
     *
     * @param hostName Server host name
     * @return true if connection is successfully established
     */
    public boolean connect(String hostName);

    /**
     * Initialize connectino to server
     *
     * @param address Server address
     * @return true if connection is successfully established
     */

    public boolean connect(InetAddress address);

    /**
     * Closes connection to server
     */
    public void disconnect();

    /**
     * Checks that connection is alive
     *
     * @return true if connection is active and tasks can be submitted
     */
    public boolean isConnected();

    /**
     * Queries server for its version
     *
     * @return Server version information
     */
    public ServerVersionInfo getServerVersion();

    /**
     * Sends optimization request to server
     *
     * @param item Code pattern to be optimized
     * @return Optimized pattern (if server already had it), or null if task was submitted for future optimization.
     * @throws IOException If problem with connection occurs
     */
    public PatternItem sendRequest(PatternItem item) throws IOException;
}
