package ru.ifmo.gpo.client;


import ru.ifmo.gpo.protocol.PatternItem;
import ru.ifmo.gpo.protocol.ServerVersionInfo;
import ru.ifmo.gpo.protocol.conn_events.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * User: jedi-philosopher Date: 27.03.2010 Time: 22:18:52
 */
public class ClientConnectionManager implements IConnectionManager {

    private InetAddress _hostAddress;
    private Socket _socket;

    private static final int _port = 13999;

    private ServerVersionInfo _versionInfo;

    private static final int _version = 3;

    private ObjectOutputStream outputStream;
    private ObjectInputStream inputStream;

    @Override
    public boolean connect(InetAddress address) {
        try {
            System.out.println("Connecting to " + address.toString() + ":"
                    + _port + "...");
            _hostAddress = address;
            _socket = new Socket(_hostAddress, _port);

            System.out.println("Handshaking...");

            outputStream = new ObjectOutputStream(_socket.getOutputStream());
            inputStream = new ObjectInputStream(_socket.getInputStream());

            outputStream.writeObject(new SessionStartedEvent());

            try {
                _versionInfo = (ServerVersionInfo) inputStream.readObject();
            } catch (ClassNotFoundException ex) {
                SessionEndedEvent cause = new SessionEndedEvent(
                        "ClassNotFoundExcepion on client: possibly outdated version");
                outputStream.writeObject(cause);
                _socket.close();
                _socket = null;
                System.err.println(ex.getMessage());
                return false;
            }

            if (_versionInfo.protocolVersion != _version) {
                SessionEndedEvent cause = new SessionEndedEvent(
                        "Protocol verisons mismatch: server reported version "
                                + _versionInfo.protocolVersion
                                + ", but this client supports only " + _version);
                outputStream.writeObject(cause);
                _socket.close();
                _socket = null;
                System.err.println(cause.getCause());
                return false;
            }
            System.out.println("Connection established");
            return true;
        } catch (IOException ex) {
            System.err.println("Failed to establish connection to server: "
                    + ex);
            return false;
        }
    }

    @Override
    public boolean connect(String hostName) {
        try {
            return connect(InetAddress.getByName(hostName));
        } catch (UnknownHostException e) {
            System.err.println(e.getMessage());
            return false;
        }
    }

    @Override
    public void disconnect() {
        try {
            if (_socket != null) {
                if (_socket.isConnected()) {
                    System.out.println("Closing connection...");
                    outputStream.writeObject(new SessionEndedEvent("Disconnected by client"));
                }
                outputStream.close();
                inputStream.close();
                _socket.close();
                outputStream = null;
                inputStream = null;
                _socket = null;
            }
        } catch (Exception ex) {
            _socket = null;
        }
        _hostAddress = null;
        _versionInfo = null;
        System.out.println("Connection terminated");
    }

    @Override
    public boolean isConnected() {
        return _socket != null && _socket.isConnected();
    }

    @Override
    public ServerVersionInfo getServerVersion() {
        return _versionInfo;
    }

    @Override
    public PatternItem sendRequest(PatternItem item) throws IOException {
        if (!isConnected()) {
            throw new IOException("Client is not connected to server");
        }
        outputStream.writeObject(new RequestEvent(item));
        Object obj;
        try {
            obj = inputStream.readObject();
        } catch (ClassNotFoundException ex) {
            disconnect();
            throw new IOException("Failed to read message from channel:" + ex);
        }

        if (obj instanceof SessionEndedEvent) {
            System.out.println("Server unexpectedly terminated connection");
            return null;
        } else if (obj instanceof DataResponseEvent) {
            return (PatternItem) ((DataResponseEvent) obj).getData();
        } else if (obj instanceof SimpleResponseEvent) {
            int result = ((SimpleResponseEvent) obj).getResult();
            switch (result) {
                case SimpleResponseEvent.SCHEDULED:
                    break;
                case SimpleResponseEvent.REJECTED:
                    break;
                case SimpleResponseEvent.FAILED:
                    System.out.println("Server failed to process request");
                    break;
                case SimpleResponseEvent.OK:
                    break;
                default:
                    System.err.println("Unrecognized response from server: " + result);

            }
            return null;
        } else {
            System.out.println("Unrecognized response from server: " + obj.getClass().getSimpleName());
            return null;
        }
    }
}
