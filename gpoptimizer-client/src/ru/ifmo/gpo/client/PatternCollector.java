package ru.ifmo.gpo.client;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.tree.*;
import org.objectweb.asm.util.AbstractVisitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.instructions.generic.IGenericInstruction;
import ru.ifmo.gpo.core.instructions.generic.NumericConstant;
import ru.ifmo.gpo.java.instructions.JVMInstruction;
import ru.ifmo.gpo.java.vmstate.LocalVariableSlot;
import ru.ifmo.gpo.protocol.PatternItem;
import ru.ifmo.gpo.protocol.SupportedOperationsInfo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * User: jedi-philosopher
 * Date: 26.03.2010
 * Time: 23:50:06
 */
public class PatternCollector {
    private LinkedList<PatternItem> _foundPatterns = new LinkedList<PatternItem>();
    private SupportedOperationsInfo _supportedOperations;

    private static Logger logger = LoggerFactory.getLogger(PatternCollector.class);

    private int _minLength;
    private int _maxLength;

    public PatternCollector(SupportedOperationsInfo supportedOperations, int minLength, int maxLength) {
        this._supportedOperations = supportedOperations;
        this._minLength = minLength;
        this._maxLength = maxLength;
    }

    public void collectPatternsFromClass(String className) throws IOException {
        ClassReader cr = new ClassReader(className);
        _collectPatternsImpl(cr);

    }

    public void collectPatternsFromFile(String pathToClassFile) throws IOException {
        collectPatternsFromFile(new File(pathToClassFile));
    }

    public void collectPatternsFromFile(File classFile) throws IOException {
        ClassReader cr = new ClassReader(new FileInputStream(classFile));
        _collectPatternsImpl(cr);
    }

    public List<PatternItem> getPatterns() {
        return _foundPatterns;
    }

    ////////////////////////////////////////////////////////////////////////////////

    private void _collectPatternsImpl(ClassReader cr) {
        ClassNode cn = new ClassNode();
        cr.accept(cn, ClassReader.SKIP_DEBUG);

        List methods = cn.methods;
        for (Object method1 : methods) {
            MethodNode method = (MethodNode) method1;
            _collectPatternsFromMethod(cr.getClassName(), method);
        }
    }

    private void _collectPatternsFromMethod(String className, MethodNode method) {
        if (method.instructions.size() == 0) {
            return;
        }

        LinkedList<IGenericInstruction> instructions = new LinkedList<IGenericInstruction>();

        ListIterator instructionIterator = method.instructions.iterator();
        int instructionIdx = 0;

        while (instructionIterator.hasNext()) {
            AbstractInsnNode instruction = (AbstractInsnNode) instructionIterator.next();
            int opcode = instruction.getOpcode();
            PatternItem.SourceFileInfo info = new PatternItem.SourceFileInfo(className, method.name, method.desc, instructionIdx - instructions.size());
            ++instructionIdx;
            if (!_supportedOperations.isSupportedInstruction(opcode)) {
                _createPattern(instructions, info);
                final String opcodeMnemonics = opcode >= 0 ? AbstractVisitor.OPCODES[opcode] : "Label";
                if (instructions.size() >= _minLength) {
                    logger.debug("Unsupported opcode found:{}", opcodeMnemonics);
                }
                continue;
            }

            if (instructions.size() >= _maxLength) {
                _createPattern(instructions, info);
            }

            switch (instruction.getType()) {
                case AbstractInsnNode.IINC_INSN:
                    IincInsnNode iincNode = (IincInsnNode) instruction;
                    instructions.add(new JVMInstruction(opcode, ((GenericOperand) new LocalVariableSlot(iincNode.var)), (GenericOperand) (new NumericConstant(iincNode.incr)))); //TODO: third arg
                    break;
                case AbstractInsnNode.INSN:
                    instructions.add(new JVMInstruction(opcode));
                    break;
                case AbstractInsnNode.INT_INSN:
                    instructions.add(new JVMInstruction(opcode, new NumericConstant(((IntInsnNode) instruction).operand)));
                    break;
                case AbstractInsnNode.VAR_INSN:
                    instructions.add(new JVMInstruction(opcode, new LocalVariableSlot(((VarInsnNode) instruction).var)));
                    break;
                case AbstractInsnNode.LDC_INSN: {
                    Object constant = ((LdcInsnNode) instruction).cst;
                    if (!_supportedOperations.isSupportedConstantType(constant.getClass())) {
                        _createPattern(instructions, info);
                        break;
                    }
                    //TODO: fixe possible type
                    instructions.add(new JVMInstruction(opcode, new NumericConstant((Number) constant)));
                }
                default:
                    _createPattern(instructions, info);
            }
        }
    }

    private void _createPattern(LinkedList<IGenericInstruction> instructions, PatternItem.SourceFileInfo info) {
        if (instructions.size() < _minLength) {
            instructions.clear();
            return;
        }

        InstructionSequence temp = new InstructionSequence();
        for (IGenericInstruction instr : instructions) {
            temp.add(instr);
        }
        _foundPatterns.add(new PatternItem(info, temp));
        instructions.clear();
    }
}
