package ru.ifmo.gpo.client;

import org.objectweb.asm.*;
import org.objectweb.asm.tree.*;
import org.objectweb.asm.util.AbstractVisitor;
import org.objectweb.asm.util.CheckClassAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ifmo.gpo.core.instructions.generic.IGenericInstruction;
import ru.ifmo.gpo.java.instructions.JVMInstruction;
import ru.ifmo.gpo.java.vmstate.LocalVariableSlot;
import ru.ifmo.gpo.protocol.PatternItem;
import ru.ifmo.gpo.util.DummyOutputStream;

import java.io.*;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

public class Patcher {

    private final static Logger logger = LoggerFactory.getLogger(Patcher.class);

    public void patchClass(String inputPath, String outputClasspath) {
        logger.debug("Starting patching class " + inputPath);

        try {
            ClassReader cr = new ClassReader(new FileInputStream(inputPath));

            List<PatternItem> patternsForClass = PatternItemStorage.instance().getItemsForClass(cr.getClassName());

            if (patternsForClass.size() == 0) {
                logger.info("No suitable patterns found for class " + inputPath + "," +
                        " copying it to destination folder without modification");
                _copyWithoutModifications(inputPath, cr.getClassName(), outputClasspath);
                return;
            }

            ClassWriter resultClass = new ClassWriter(ClassWriter.COMPUTE_MAXS);

            ClassNode cn = new ClassNode();
            cr.accept(cn, ClassReader.SKIP_DEBUG);

            String[] arr = new String[cn.interfaces.size()];
            resultClass.visit(cn.version, cn.access, cn.name, cn.signature, cn.superName, (String[]) cn.interfaces.toArray(arr)); // wtf with the compiler??

            for (Object methodObject : cn.methods) {
                MethodNode method = (MethodNode) methodObject;
                _patchMethod(resultClass, method, _buildMethodPatternMap(method.name, method.desc, patternsForClass));
            }

            resultClass.visitEnd();

            CheckClassAdapter.verify(new ClassReader(resultClass.toByteArray()), true, new PrintWriter(new DummyOutputStream())); // todo: think about where to put output

            File outputFile = new File(outputClasspath + "\\" + cr.getClassName() + ".class");
            outputFile.getParentFile().mkdirs();

            FileOutputStream outputStream = new FileOutputStream(outputFile);
            outputStream.write(resultClass.toByteArray());
            outputStream.close();

        } catch (IOException ex) {
            logger.error("Failed to process class " + inputPath, ex);
        }

    }

    private Map<Integer, PatternItem> _buildMethodPatternMap(String methodName, String methodDescr, List<PatternItem> items) {
        Map<Integer, PatternItem> rz = new HashMap<Integer, PatternItem>();

        for (PatternItem item : items) {
            if (item.getInfo().methodName.equals(methodName) && item.getInfo().methodDescriptor.equals(methodDescr)) {
                rz.put(item.getInfo().instructionIdx, item);
            }
        }
        return rz;
    }

    private void _copyInstruction(MethodVisitor resultMethod, AbstractInsnNode instruction) {
        switch (instruction.getType()) {
            case AbstractInsnNode.IINC_INSN:
                resultMethod.visitIincInsn(((IincInsnNode) instruction).var, ((IincInsnNode) instruction).incr);
                break;
            case AbstractInsnNode.INSN:
                resultMethod.visitInsn(instruction.getOpcode());
                break;
            case AbstractInsnNode.INT_INSN:
                resultMethod.visitIntInsn(instruction.getOpcode(), ((IntInsnNode) instruction).operand);
                break;
            case AbstractInsnNode.VAR_INSN:
                resultMethod.visitIntInsn(instruction.getOpcode(), ((VarInsnNode) instruction).var);
                break;
            case AbstractInsnNode.LDC_INSN:
                resultMethod.visitLdcInsn(((LdcInsnNode) instruction).cst);
                break;
            case AbstractInsnNode.TYPE_INSN:
                resultMethod.visitTypeInsn(instruction.getOpcode(), ((TypeInsnNode) instruction).desc);
                break;
            case AbstractInsnNode.FIELD_INSN:
                resultMethod.visitFieldInsn(instruction.getOpcode(), ((FieldInsnNode) instruction).owner, ((FieldInsnNode) instruction).name, ((FieldInsnNode) instruction).desc);
                break;
            case AbstractInsnNode.METHOD_INSN:
                resultMethod.visitMethodInsn(instruction.getOpcode(), ((MethodInsnNode) instruction).owner, ((MethodInsnNode) instruction).name, ((MethodInsnNode) instruction).desc);
                break;
            case AbstractInsnNode.JUMP_INSN:
                resultMethod.visitJumpInsn(instruction.getOpcode(), ((JumpInsnNode) instruction).label.getLabel());
                break;
            case AbstractInsnNode.LABEL:
                resultMethod.visitLabel(((LabelNode) instruction).getLabel());
                break;
            case AbstractInsnNode.TABLESWITCH_INSN: {
                TableSwitchInsnNode tableswitchNode = (TableSwitchInsnNode) instruction;
                Label[] labelArray = new Label[tableswitchNode.labels.size()];
                for (int i = 0; i < labelArray.length; ++i) {
                    labelArray[i] = ((LabelNode) tableswitchNode.labels.get(i)).getLabel();
                }
                resultMethod.visitTableSwitchInsn(tableswitchNode.min, tableswitchNode.max, tableswitchNode.dflt.getLabel(), labelArray);
                break;
            }
            case AbstractInsnNode.LOOKUPSWITCH_INSN:
                LookupSwitchInsnNode lookupSwitchNode = (LookupSwitchInsnNode) instruction;
                int[] array = new int[lookupSwitchNode.keys.size()]; // I HATE HATE HATE java toArray() problems
                for (int i = 0; i < array.length; ++i) {
                    array[i] = (Integer) lookupSwitchNode.keys.get(i);
                }
                Label[] labelArray = new Label[lookupSwitchNode.labels.size()];
                for (int i = 0; i < labelArray.length; ++i) {
                    labelArray[i] = ((LabelNode) lookupSwitchNode.labels.get(i)).getLabel();
                }
                resultMethod.visitLookupSwitchInsn(lookupSwitchNode.dflt.getLabel(), array, labelArray);
                break;
            case AbstractInsnNode.LINE:
                resultMethod.visitLineNumber(((LineNumberNode) instruction).line, ((LineNumberNode) instruction).start.getLabel());
                break;
            case AbstractInsnNode.MULTIANEWARRAY_INSN: {
                MultiANewArrayInsnNode node = (MultiANewArrayInsnNode) instruction;
                resultMethod.visitMultiANewArrayInsn(node.desc, node.dims);
                break;
            }
            case AbstractInsnNode.FRAME: {
                FrameNode frameNode = (FrameNode) instruction;
                int localSize = 0;
                Object[] localArray = null;
                if (frameNode.local != null) {
                    localSize = frameNode.local.size();
                    localArray = frameNode.local.toArray();
                }

                int stackSize = 0;
                Object[] stack = null;
                if (frameNode.stack != null) {
                    stackSize = frameNode.stack.size();
                    stack = frameNode.stack.toArray();
                }

                resultMethod.visitFrame(frameNode.type, localSize, localArray, stackSize, stack);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown node type " + AbstractVisitor.OPCODES[instruction.getOpcode()]);
        }
    }

    private void _addInstruction(MethodVisitor resultMethod, JVMInstruction instr) {
        // todo: do something here!!!
        if (instr.getParameters().isEmpty()) {
            resultMethod.visitInsn(instr.getOpcode());
        } else if (instr.getOpcode() == Opcodes.BIPUSH && instr.getParameters().size() == 1) {
            resultMethod.visitIntInsn(instr.getOpcode(), instr.getIntParameter(0));
        } else if (instr.getOpcode() == Opcodes.IINC) {
            resultMethod.visitIincInsn(((LocalVariableSlot) instr.getParameter(0)).getIdx(), instr.getIntParameter(1));
        } else {
            resultMethod.visitVarInsn(instr.getOpcode(), ((LocalVariableSlot) instr.getParameter(0)).getIdx());
        }
    }


    private void _patchMethod(ClassWriter resultClass, MethodNode method, Map<Integer, PatternItem> patterns) {
        ListIterator instructionIterator = method.instructions.iterator();

        String[] arr = new String[method.exceptions.size()];
        MethodVisitor resultMethod = resultClass.visitMethod(method.access, method.name, method.desc, method.signature, (String[]) method.exceptions.toArray(arr));

        for (int idx = 0; instructionIterator.hasNext(); ++idx) {

            if (!patterns.containsKey(idx)) {
                AbstractInsnNode instruction = (AbstractInsnNode) instructionIterator.next();
                _copyInstruction(resultMethod, instruction);
                continue;
            }

            PatternItem item = patterns.get(idx);

            int instructionsToSkip = item.getSource().size();
            // skipping instructions in source sequence
            for (int i = 0; i < instructionsToSkip; ++i) {
                instructionIterator.next();
            }
            idx += instructionsToSkip - 1;

            // applying new instructions
            for (IGenericInstruction instr : item.getOptimized()) {
                _addInstruction(resultMethod, (JVMInstruction) instr);
            }
        }

        resultMethod.visitMaxs(0, 0); // arguments will be ignored because of  COMPUTE_MAXS flag in ClassWriter
        resultMethod.visitEnd();
    }

    private void _copyWithoutModifications(String inputPath, String fullClassName, String outputDirName) {
        // poor java has no good api for file copying

        FileChannel in = null, out = null;
        try {
            in = new FileInputStream(inputPath).getChannel();

            File outputFile = new File(outputDirName + "\\" + fullClassName + ".class");
            outputFile.getParentFile().mkdirs();
            out = new FileOutputStream(outputFile).getChannel();

            long size = in.size();
            MappedByteBuffer buf = in.map(FileChannel.MapMode.READ_ONLY, 0, size);

            out.write(buf);

        } catch (Exception ex) {
            logger.error("Failed to copy output file", ex);
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
            } catch (Exception ignore) {

            }
        }
    }
}