package ru.ifmo.gpo.client;

import ru.ifmo.gpo.protocol.PatternItem;

import java.util.*;

public class PatternItemStorage {

    private Map<String, LinkedList<PatternItem>> _items = new HashMap<String, LinkedList<PatternItem>>();

    public void addItem(PatternItem item) {
        String className = item.getInfo().className;

        if (_items.containsKey(className)) {
            _items.get(className).add(item);
        } else {
            LinkedList<PatternItem> val = new LinkedList<PatternItem>();
            val.add(item);
            _items.put(className, val);
        }
    }

    public void addAll(Collection<PatternItem> itemsToAdd) {
        for (PatternItem item : itemsToAdd) {
            if (item != null) {
                addItem(item);
            }
        }
    }

    public List<PatternItem> getItemsForClass(String className) {
        if (_items.containsKey(className)) {
            return _items.get(className);
        }

        return new LinkedList<PatternItem>();
    }

/////////////////////Singleton functions/////////////////////////////

    public static void init() {
        assert _instance == null;
        _instance = new PatternItemStorage();
    }

    public static void term() {
        _instance = null;
    }

    public static PatternItemStorage instance() {
        assert _instance != null;
        return _instance;
    }

////////////////////////////////////////////////////////////

    private static PatternItemStorage _instance = null;

    private PatternItemStorage() {
    }
}