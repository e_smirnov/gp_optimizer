package ru.ifmo.gpo.client;

import jargs.gnu.CmdLineParser;
import org.apache.log4j.BasicConfigurator;
import ru.ifmo.gpo.protocol.PatternItem;
import ru.ifmo.gpo.util.ClassFileFilter;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * User: jedi-philosopher
 * Date: 03.04.2010
 * Time: 1:45:46
 * <p/>
 * Main class for client application.
 */
public class ClientMain {

    private CmdLineParser cmdParser = new CmdLineParser();
    private CmdLineParser.Option classpath;
    private CmdLineParser.Option recursive;
    private CmdLineParser.Option output;
    private CmdLineParser.Option minLength;
    private CmdLineParser.Option maxLength;

    private IConnectionManager connection = new ClientConnectionManager();
    private PatternCollector patternCollector;

    private Set<File> processedFiles = new HashSet<File>();
    private Set<PatternItem> serverResponse = new HashSet<PatternItem>();

    private boolean isRecursive = false;

    private static void printUsage() {
        System.out.println("Usage: ClientMain [--classpath][{-r --recursive}] [--output -o] [--min] [--max] [optional class name list]");
        System.out.println("--classpath: folder containing .class files for optimization, defaults to current working directory");
        System.out.println("--recursive: scan subdirectories");
        System.out.println("--min: minimum length of instruction sequence, default 3");
        System.out.println("--max: maximum length of instruction sequence, default 10");
        System.out.println("--output: path to folder where optimized files should be stored, defaults to ./out");
        System.out.println("Class name list - list of classes");
    }

    private void parseArgs(String[] args) {
        classpath = cmdParser.addStringOption("classpath");
        recursive = cmdParser.addBooleanOption('r', "recursive");
        output = cmdParser.addStringOption('o', "output");
        minLength = cmdParser.addIntegerOption("min");
        maxLength = cmdParser.addIntegerOption("max");

        try {
            cmdParser.parse(args);
        } catch (CmdLineParser.OptionException e) {
            System.err.println(e.getMessage());
            printUsage();
            System.exit(2);
        }
    }

    private void connect() {
        try {
            final boolean rz = connection.connect(InetAddress.getLocalHost());
            if (!rz) {
                System.err.println("Failed to connect to server");
                System.exit(1);
            }
        } catch (IOException ex) {
            System.err.println("Failed to connect to server: " + ex);
            System.exit(1);
        }
    }

    private void collectFromFile(File file) {
        if (!file.exists()) {
            System.err.println("File " + file.getName() + " can not be found");
            System.exit(3);
        }

        try {
            patternCollector.collectPatternsFromFile(file);
        } catch (IOException e) {
            System.err.println("Error while collecting patterns from file " + file.getName() + ": " + e);
            System.exit(3);
        }
        processedFiles.add(file);
    }

    private void collectFromFolder(File folder) {
        if (!folder.exists() || !folder.isDirectory()) {
            System.err.println("Folder " + folder.getName() + " can not be found");
            System.exit(3);
        }
        ClassFileFilter filter = new ClassFileFilter();
        if (!isRecursive) {
            for (File f : folder.listFiles(filter)) {
                collectFromFile(f);
            }
        } else {
            for (File f : folder.listFiles()) {
                if (f.isFile() && filter.accept(folder, f.getName())) {
                    collectFromFile(f);
                } else if (f.isDirectory()) {
                    collectFromFolder(f);
                }
            }
        }
    }

    private void collectPatterns() {
        System.out.println("Collecting patterns...");

        patternCollector = new PatternCollector(
                connection.getServerVersion().supportedOperations
                , (Integer) cmdParser.getOptionValue(minLength, 3)
                , (Integer) cmdParser.getOptionValue(maxLength, 10));
        final String classPath = (String) cmdParser.getOptionValue(classpath, "");
        isRecursive = (Boolean) cmdParser.getOptionValue(recursive, Boolean.FALSE);
        for (String cp : classPath.split(";")) {
            if (cp.isEmpty()) {
                continue;
            }
            collectFromFolder(new File(cp));
        }

        for (String file : cmdParser.getRemainingArgs()) {
            collectFromFile(new File(file));
        }

        System.out.println(patternCollector.getPatterns().size() + " patterns collected from " + processedFiles.size() + " files");
    }

    private void sendToServer() {
        System.out.println("Sending patterns to server...");
        for (PatternItem item : patternCollector.getPatterns()) {
            PatternItem optimized = null;
            try {
                optimized = connection.sendRequest(item);
            } catch (IOException e) {
                System.err.println("Error while communicating with server: " + e);
                disconnect();
                System.exit(3);
            }
            if (optimized != null) {
                serverResponse.add(optimized);
            }
        }

        if (serverResponse.isEmpty()) {
            System.out.println("All patterns scheduled for optimization on server, no optimized results available yet. Repeat your request after some time");
        } else {
            System.out.println("Received " + serverResponse.size() + " optimized versions from server");
            PatternItemStorage.instance().addAll(serverResponse);
        }
    }

    private void patch() {
        Patcher patcher = new Patcher();
        String outputDirName = (String) cmdParser.getOptionValue(output, "./out");
        for (File f : processedFiles) {
            patcher.patchClass(f.getPath(), outputDirName);
        }
    }

    private void disconnect() {
        if (connection.isConnected()) {
            connection.disconnect();
        }
    }

    public static void main(String[] args) {
        BasicConfigurator.configure();
        PatternItemStorage.init();
        ClientMain app = new ClientMain();
        app.parseArgs(args);
        app.connect();
        app.collectPatterns();
        app.sendToServer();
        app.patch();
        app.disconnect();
        PatternItemStorage.term();
    }
}
