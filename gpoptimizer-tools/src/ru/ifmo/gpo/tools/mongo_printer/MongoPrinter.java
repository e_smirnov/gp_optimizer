package ru.ifmo.gpo.tools.mongo_printer;

import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.server.storage.MongoDBStorageManager;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

/**
 * User: e_smirnov
 * Date: 06.04.2011
 * Time: 16:34:20
 */
public class MongoPrinter {
    public static void main(String[] args) {
        if (args.length < 2) {
            System.err.println("Usage: MongoPrinter dbname outfile");
            System.exit(1);
        }
        MongoDBStorageManager storageManager = new MongoDBStorageManager();
        storageManager.init(args[0]);
        Map<InstructionSequence, InstructionSequence> records = storageManager.getAllResults();

        try {
            File outputFile = new File(args[1]);
            File parent = outputFile.getParentFile();
            if (!parent.exists() && !parent.mkdirs()) {
                System.out.println("Failed to build directory path to output file");
                return;
            }

            BufferedWriter writer = new BufferedWriter(new FileWriter(args[1]));
            for (Map.Entry<InstructionSequence, InstructionSequence> record : records.entrySet()) {
                writer.write("==============================================================\n");
                writer.write(">>>Source:\n");
                writer.write(record.getKey().outputFormatted());
                writer.write(">>>Optimized:\n");
                writer.write(record.getValue().outputFormatted());
            }
            writer.close();
        } catch (IOException e) {
            System.out.println("Failed to save contents to ouput file");
            System.out.println(e);
        }
        System.out.println(records.size() + " patterns found and stored in " + args[1]);
    }
}
