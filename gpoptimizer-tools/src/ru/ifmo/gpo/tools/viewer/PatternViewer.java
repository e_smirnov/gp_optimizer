package ru.ifmo.gpo.tools.viewer;


import com.mongodb.*;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeSelectionModel;
import java.util.Set;

public class PatternViewer implements Runnable {

    private static final String _version = "0.1";

    private JFrame frame;

    private PatternPanel patternPanel;

    private TreeModel treeModel;

    private DB mongoDb;

    public static void main(String[] args) {
        if (args.length != 1) {
            return;
        }

        SwingUtilities.invokeLater(new PatternViewer(args[0]));
    }

    public PatternViewer(String dbName) {
        try {
            Mongo m = new Mongo();
            mongoDb = m.getDB("gp_optimizer");
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(frame, "Failed to connect to database", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void run() {
        _initGui();
    }


    private void _initGui() {
        frame = new JFrame("Pattern viewer v" + _version);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.LINE_AXIS));
        frame.getContentPane().add(createLeftPanel());
        frame.getContentPane().add(createRightPanel());
        frame.pack();
        frame.setVisible(true);
    }

    private JPanel createLeftPanel() {
        JPanel result = new JPanel();
        result.setLayout(new BoxLayout(result, BoxLayout.PAGE_AXIS));
        result.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(), BorderFactory.createEmptyBorder(10, 10, 10, 5)));
        result.add(new JLabel("Database contents:"));

        DefaultMutableTreeNode topNode = new DefaultMutableTreeNode("gp_optimizer");
        treeModel = new DefaultTreeModel(topNode);
        loadDb(topNode);
        JTree tree = new JTree(treeModel);
        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);

        result.add(new JScrollPane(tree));
        return result;
    }

    private void loadDb(DefaultMutableTreeNode root) {
        if (mongoDb == null) {
            return;
        }

        try {
            Set<String> collections = mongoDb.getCollectionNames();
            for (String s : collections) {
                DefaultMutableTreeNode category = new DefaultMutableTreeNode(s);
                root.add(category);

                DBCollection coll = mongoDb.getCollection(s);
                DBCursor cur = coll.find();
                while (cur.hasNext()) {
                    DefaultMutableTreeNode pattern = new DefaultMutableTreeNode(cur.next());
                    category.add(pattern);
                }
            }
        } catch (MongoException ex) {
            JOptionPane.showMessageDialog(frame, "Failed to connect to database", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private JPanel createRightPanel() {
        JPanel result = new JPanel();
        result.setLayout(new BoxLayout(result, BoxLayout.PAGE_AXIS));
        result.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(), BorderFactory.createEmptyBorder(10, 10, 10, 5)));
        result.add(new JLabel("Pattern view:"));

        JPanel pPanel = new JPanel();
        pPanel.setLayout(new BoxLayout(pPanel, BoxLayout.LINE_AXIS));
        patternPanel = new PatternPanel();

        patternPanel.sourceSequence = new JTextArea(15, 20);
        patternPanel.optimizedSequence = new JTextArea(15, 20);
        pPanel.add(patternPanel.sourceSequence);
        pPanel.add(patternPanel.optimizedSequence);
        result.add(pPanel);

        JPanel infoPanel = new JPanel();
        infoPanel.add(new JLabel("Info:"));
        result.add(infoPanel);

        return result;
    }

}