package ru.ifmo.gpo.tools.viewer;

import javax.swing.*;

/**
 * User: e_smirnov
 * Date: 30.09.2010
 * Time: 15:29:57
 */
public class PatternPanel {
    public JTextArea sourceSequence;
    public JTextArea optimizedSequence;
}
