package ru.ifmo.gpo.tools.viewer;

import com.mongodb.DB;

import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;

/**
 * User: e_smirnov
 * Date: 28.09.2010
 * Time: 14:47:31
 */
public class PatternSelectionListener implements TreeSelectionListener {

    private DB mongoDb;

    public PatternSelectionListener(DB mongoDb) {
        this.mongoDb = mongoDb;
    }

    public void valueChanged(TreeSelectionEvent e) {
        TreePath path = e.getPath();
        // path contains of 3 elements: root element, collection of patterns of same size, pattern itself
    }
}
