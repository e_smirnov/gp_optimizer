package ru.ifmo.gpo.tools.perfomance;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.java.InstructionSequenceEnumerator;
import ru.ifmo.gpo.java.instructions.JVMInstruction;
import ru.ifmo.gpo.java.instructions.JavaOpcodes;
import ru.ifmo.gpo.java.vmstate.LocalVariableSlot;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Checks that genetics is actually ever better than exhaustive search
 *
 * @author jedi-philosopher
 */
public class PerformaceComparator {

    public static void main(String[] args) {
        BasicConfigurator.configure();

        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_M1));
        sequence.add(new JVMInstruction(JavaOpcodes.IXOR));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(10)));
        sequence.add(new JVMInstruction(JavaOpcodes.IAND));

        Logger.getLogger("ru.ifmo.gpo.java.JavaBytecodeGenerator").setLevel(Level.OFF);
        Logger.getLogger("ru.ifmo.gpo.java.JavaBytecodeExecutor").setLevel(Level.OFF);
        Logger.getLogger("ru.ifmo.gpo.core.CodeFitnessFunction").setLevel(Level.OFF);

        InstructionSequenceEnumerator enumerator = new InstructionSequenceEnumerator(sequence, 4);
        enumerator.process(false);

        File outputFile = new File("./out/solutions.txt");
        File parent = outputFile.getParentFile();
        if (!parent.exists() && !parent.mkdirs()) {
            System.out.println("Failed to build directory path to output file");
            return;
        }

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));
            for (InstructionSequence solution : enumerator.getSolutions()) {
                writer.write("======================");
                writer.write(solution.outputFormatted());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
