package ru.ifmo.gpo.tools.collector;

import jargs.gnu.CmdLineParser;
import jargs.gnu.CmdLineParser.OptionException;
import ru.ifmo.gpo.client.PatternCollector;
import ru.ifmo.gpo.java.instructions.JavaSimpleOpcodeFactory;
import ru.ifmo.gpo.protocol.PatternItem;
import ru.ifmo.gpo.util.ClassPathProcessor;

import java.io.*;

/**
 * User: jedi-philosopher
 * Date: 05.12.2010
 * Time: 21:49:30
 * <p/>
 * Simple application that collects all suitable patterns from directory
 */
public class CollectorMain {

    public static final String usage = "Usage: CollectorMain --classpath=<folder with .class files> <output file>";

    private static void printPattern(Writer w, PatternItem pattern) throws IOException {
        w.write("=================================\n");
        w.write("From " + pattern.getInfo().className + "." + pattern.getInfo().methodName + "\n");
        w.write(pattern.getSource().outputFormatted());
    }

    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println(usage);
            System.exit(1);
        }
        CmdLineParser cmdParser = new CmdLineParser();
        ClassPathProcessor cpProcessor = new ClassPathProcessor(cmdParser);

        try {
            cmdParser.parse(args);
        } catch (OptionException e1) {
            System.err.println(e1);
            System.out.println(usage);
            System.exit(1);
        }

        if (cmdParser.getRemainingArgs().length != 1) {
            System.out.println("No output file name specified");
            System.out.println(usage);
            System.exit(1);
        }

        PatternCollector collector = new PatternCollector(JavaSimpleOpcodeFactory.getSupportedOperations(), 4, 15);

        System.out.println("Processing class files in " + cpProcessor.getClassPath() + "...");
        for (File f : cpProcessor.getFiles()) {
            try {
                collector.collectPatternsFromFile(f);
            } catch (IOException e) {
                System.err.println("Failed to process file " + f.getName());
                System.err.println(e);
            }
        }

        System.out.println("Saving output...");
        try {
            File outputFile = new File(cmdParser.getRemainingArgs()[0]);
            File parent = outputFile.getParentFile();
            if (!parent.exists() && !parent.mkdirs()) {
                System.out.println("Failed to build directory path to output file");
                return;
            }

            BufferedWriter writer = new BufferedWriter(new FileWriter(args[1]));
            for (PatternItem pattern : collector.getPatterns()) {
                printPattern(writer, pattern);
            }
            writer.close();
        } catch (IOException e) {
            System.out.println("Failed to save contents to ouput file");
            System.out.println(e);
        }
        System.out.println(collector.getPatterns().size() + " patterns found and stored in " + args[1]);
    }
}
