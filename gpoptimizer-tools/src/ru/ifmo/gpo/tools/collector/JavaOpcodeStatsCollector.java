package ru.ifmo.gpo.tools.collector;

import jargs.gnu.CmdLineParser;
import jargs.gnu.CmdLineParser.IllegalOptionValueException;
import jargs.gnu.CmdLineParser.UnknownOptionException;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.util.AbstractVisitor;
import ru.ifmo.gpo.java.instructions.JavaOpcodes;
import ru.ifmo.gpo.util.ClassPathProcessor;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

public class JavaOpcodeStatsCollector {

    private ClassPathProcessor cpProcessor;
    private CmdLineParser cmdProcessor;

    private Map<Integer, Integer> stats = new HashMap<Integer, Integer>();

    public JavaOpcodeStatsCollector(String[] args) {
        cmdProcessor = new CmdLineParser();
        cpProcessor = new ClassPathProcessor(cmdProcessor);

        try {
            cmdProcessor.parse(args);
        } catch (IllegalOptionValueException e) {
            System.err.println(e);
            return;
        } catch (UnknownOptionException e) {
            System.err.println(e);
            return;
        }
    }

    public void collectStats() throws IOException {
        for (File f : cpProcessor.getFiles()) {
            ClassReader cr = new ClassReader(new FileInputStream(f));
            ClassNode cn = new ClassNode();
            cr.accept(cn, ClassReader.SKIP_DEBUG);

            List methods = cn.methods;
            for (Object method1 : methods) {
                MethodNode method = (MethodNode) method1;
                ListIterator insnIter = method.instructions.iterator();
                for (; insnIter.hasNext();) {
                    AbstractInsnNode instruction = (AbstractInsnNode) insnIter.next();
                    Integer amount = stats.get(instruction.getOpcode());
                    if (amount == null) {
                        amount = 0;
                    }
                    stats.put(instruction.getOpcode(), amount + 1);
                }
            }
        }
    }

    public void printStats() {
        int supportedCount = 0;
        int totalCount = 0;
        for (Map.Entry<Integer, Integer> entry : stats.entrySet()) {
            if (entry.getKey() < 0 || entry.getKey() >= AbstractVisitor.OPCODES.length) {
                continue;
            }
            System.out.println(AbstractVisitor.OPCODES[entry.getKey()] + ": " + entry.getValue());
            totalCount += entry.getValue();
            try {
                JavaOpcodes javaOpcode = JavaOpcodes.valueOf(entry.getKey());
                supportedCount += entry.getValue();
            } catch (IllegalArgumentException ex) {

            }
        }

        System.out.println("Currently supported " + supportedCount + " out of " + totalCount + " instructions (" + ((double) supportedCount / totalCount) + ")");
    }

    public static void main(String[] args) {
        JavaOpcodeStatsCollector coll = new JavaOpcodeStatsCollector(args);
        try {
            coll.collectStats();
        } catch (IOException e) {
            System.err.println(e);
        }
        coll.printStats();
    }

}
