package ru.ifmo.gpo.server.storage;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.java.instructions.JVMInstruction;
import ru.ifmo.gpo.java.instructions.JavaOpcodes;

import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;

/**
 * User: e_smirnov
 * Date: 08.04.2011
 * Time: 15:22:17
 */
public class JVMInstructionSerializer implements JsonSerializer<JVMInstruction>, JsonDeserializer<JVMInstruction> {
    @Override
    public JsonElement serialize(JVMInstruction instr, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonObject result = new JsonObject();
        result.addProperty("opcode", instr.getOpcodeInternal().toString());
        if (instr.getParameters().isEmpty()) {
            return result;
        }
        JsonArray paramsArray = new JsonArray();
        for (GenericOperand op : instr.getParameters()) {
            JsonObject paramObject = new JsonObject();
            paramObject.addProperty("class", op.getClass().getCanonicalName());
            paramObject.add("value", jsonSerializationContext.serialize(op));
            paramsArray.add(paramObject);
        }
        result.add("operands", paramsArray);
        return result;
    }

    @Override
    public JVMInstruction deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject obj = json.getAsJsonObject();
        String opcodeVal = obj.get("opcode").getAsString();
        JsonElement operands = obj.get("operands");
        if (operands == null) {
            return new JVMInstruction(JavaOpcodes.valueOf(opcodeVal));
        }
        List<GenericOperand> operandValues = new LinkedList<GenericOperand>();
        for (JsonElement elem : operands.getAsJsonArray()) {
            JsonObject parameter = elem.getAsJsonObject();
            String className = parameter.get("class").getAsString();
            try {
                Class paramClass = Class.forName(className);
                GenericOperand op = context.deserialize(parameter.get("value"), TypeToken.get(paramClass).getType());
                operandValues.add(op);
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
        return new JVMInstruction(JavaOpcodes.valueOf(opcodeVal), operandValues);
    }
}
