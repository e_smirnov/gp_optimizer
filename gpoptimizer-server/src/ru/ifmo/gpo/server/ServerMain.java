package ru.ifmo.gpo.server;

import org.apache.log4j.PropertyConfigurator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * User: jedi-philosopher
 * Date: 03.04.2010
 * Time: 1:09:50
 */
public class ServerMain {

    private static void processInput(GPOServer server) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String command = "";
        do {
            try {
                command = br.readLine();
            } catch (IOException ex) {
                command = "exit";
            }

            if (command.equals("exit") || command.equals("stop")) {
                server.stopServer();
                return;
            }
        } while (true);

    }

    public static void main(String args[]) {
        PropertyConfigurator.configure(System.getProperty("log4j.configuration"));

        GPOServer server = new GPOServer();
        server.start();

        processInput(server);
    }
}
