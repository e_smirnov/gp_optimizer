package ru.ifmo.gpo.server;


import org.jppf.JPPFException;
import org.jppf.client.JPPFJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.java.JavaTargetArchitecture;
import ru.ifmo.gpo.server.tasks.OptimizationTask;

import java.util.concurrent.TimeUnit;

/**
 * User: e_smirnov
 * Date: 13.04.2011
 * Time: 14:47:11
 * <p/>
 * Thread that runs in background, and if sees that server load is low - attempts to re-optimize code that was
 * previously processed without success.
 */
public class OptimizationRetryThread extends Thread {

    private GPOServer myServer;

    private static final Logger logger = LoggerFactory.getLogger(OptimizationRetryThread.class);

    private volatile boolean isStopping = false;

    public OptimizationRetryThread(GPOServer myServer) {
        super("optimization_retry");
        this.myServer = myServer;
    }

    public void terminate() {
        isStopping = true;
    }

    @Override
    public void run() {
        logger.info("Retry thread started");
        while (!isStopping) {

            try {
                Thread.sleep(TimeUnit.MINUTES.toMillis(1));
            } catch (InterruptedException ignore) {
                // its ok to try to submit earlier
            }

            final float serverLoad = myServer.getServerLoad();
            if (serverLoad > 0.1) {
                logger.debug("Server load is too high ({}), sleeping", serverLoad);
                continue;
            }
            submitTask();
        }
    }

    private void submitTask() {
        InstructionSequence previouslyFailed = myServer.getStorageManager().getRandomRecordWithoutSolution();
        InstructionSequence previouslySucceeded = myServer.getStorageManager().getRandomRecordWithSolution();
        InstructionSequence candidate;

        if (previouslyFailed != null && previouslySucceeded != null) {
            candidate = (Math.random() < 0.5 ? previouslyFailed : previouslySucceeded);
        } else if (previouslyFailed != null) {
            candidate = previouslyFailed;
        } else {
            candidate = previouslySucceeded;
        }

        if (candidate == null) {
            logger.debug("Database is empty, can not submit reoptimization task");
            return;
        }

        OptimizationTask task = new OptimizationTask(candidate, JavaTargetArchitecture.id);
        JPPFJob job = new JPPFJob();
        try {
            job.addTask(task);
        } catch (JPPFException e) {
            logger.error("Failed to set up reoptimization job for source " + candidate, e);
            return;
        }
        job.getJobSLA().setPriority(GPOServer.OPTIMIZATION_JOB_PRIORITY);
        if (!myServer.submitJob(job)) {
            logger.warn("Failed to submit reoptimization task, submitJob() returned false");
        }
    }

}
