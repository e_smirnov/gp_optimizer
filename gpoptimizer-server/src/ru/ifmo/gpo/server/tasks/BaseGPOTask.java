package ru.ifmo.gpo.server.tasks;

import org.jppf.server.protocol.JPPFTask;
import ru.ifmo.gpo.core.ITargetArchitecture;
import ru.ifmo.gpo.core.SupportedArchitectures;
import ru.ifmo.gpo.core.instructions.InstructionSequence;

/**
 * User: e_smirnov
 * Date: 09.04.2011
 * Time: 16:10:28
 */
public abstract class BaseGPOTask extends JPPFTask {
    protected InstructionSequence source;
    protected String targetArchitectureID;

    protected BaseGPOTask(InstructionSequence source, String targetArchitectureID) {
        this.source = source;
        this.targetArchitectureID = targetArchitectureID;
    }

    public InstructionSequence getSource() {
        return source;
    }

    public String getTargetArchitectureID() {
        return targetArchitectureID;
    }

    protected ITargetArchitecture createTargetArchitecture() {
        return SupportedArchitectures.createInstance(targetArchitectureID);
    }
}
