package ru.ifmo.gpo.server.tasks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ifmo.gpo.core.*;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.java.JavaBytecodeAnalyzer;
import ru.ifmo.gpo.java.JavaBytecodeExecutor;
import ru.ifmo.gpo.java.JavaBytecodeGenerator;
import ru.ifmo.gpo.java.vmstate.JVMStateSchema;
import ru.ifmo.gpo.util.StatUtils;


/**
 * Used to filter out solutions that are actually not faster than the source one. Performs complex
 * profiling with a lot of passes (much more than used in genetic process). Filters out results that have no
 * or very little speedup (speedup below 1-2% is usually just an accidental inaccuracy
 */

public class ProfilingTask extends BaseGPOTask {
    private SolutionStorage candidates;

    private static final int EVALUATION_PASSES = 10;

    private static final int WARMUP_PASSES = 1000;
    private static final int PROFILE_PASSES = 1000;

    private transient ICodeExecutionEnvironment[] testEnvs;
    private transient JavaBytecodeAnalyzer refAnalyzer;
    private transient ICodeGenerator generator;
    private transient JavaBytecodeExecutor executor;
    private transient Logger logger;
    private static final long serialVersionUID = 7839166059781229835L;

    public ProfilingTask(SolutionStorage candidates, InstructionSequence source, String targetArch) {
        super(source, targetArch);
        this.candidates = candidates;
    }

    public InstructionSequence getSource() {
        return source;
    }

    @Override
    public String toString() {
        return "ProfilingTask [source=" + source + ", candidates=" + candidates
                + ", targetArchitectureId=" + targetArchitectureID + "]";
    }

    private double processCandidate(InstructionSequence candidate, JVMStateSchema schema) {
        ICodeGenerationResult ref;
        ICodeGenerationResult cand;

        try {
            ref = generator.generateProfileCode(source, schema, null);
            cand = generator.generateProfileCode(candidate, schema, refAnalyzer);
        } catch (Exception ex) {
            logger.error("Failed to generate code for source {} and candidate {}", source, candidate);
            throw new RuntimeException(ex);
        }

        testEnvs = new ICodeExecutionEnvironment[EVALUATION_PASSES];

        testEnvs[0] = ref.createExecutionEnvironment();
        testEnvs[0].setToZero();

        testEnvs[1] = ref.createExecutionEnvironment();
        testEnvs[1].setToFF();
        for (int i = 2; i < EVALUATION_PASSES; ++i) {
            testEnvs[i] = ref.createExecutionEnvironment();
            testEnvs[i].setRandomValues();
        }


        IExecutionResult res1 = null;
        IExecutionResult res2 = null;

        int succesfullRuns = 0;

        long[] refCodeProfiletimes = new long[EVALUATION_PASSES];
        long[] codeProfiletimes = new long[EVALUATION_PASSES];

        for (int i = 0; i < EVALUATION_PASSES; ++i) {

            res1 = executor.executeCode(cand, testEnvs[i]);
            res2 = executor.executeCode(ref, testEnvs[i]);

            Exception ex1 = res1.getException();
            Exception ex2 = res2.getException();


            if (ex1 != null) {
                if (ex2 == null || !ex1.getClass().equals(ex2.getClass())) {

                }
            } else if (ex2 != null) {

            } else {
                succesfullRuns++;
                // it is possible that getNanoTinme will return negative vales
                long codeRunTime = executor.profileCode(cand, testEnvs[i], WARMUP_PASSES, PROFILE_PASSES);
                long refCodeRunTime = executor.profileCode(ref, testEnvs[i], WARMUP_PASSES, PROFILE_PASSES);

                codeProfiletimes[i] = codeRunTime;
                refCodeProfiletimes[i] = refCodeRunTime;
            }
            res1.dispose();
            res2.dispose();
        }

        final long codeRunTime = StatUtils.avgWithErrorCorrection(codeProfiletimes, 0);
        final long refCodeRunTime = StatUtils.avgWithErrorCorrection(refCodeProfiletimes, 0);

        return Math.abs((double) refCodeRunTime / (double) codeRunTime);
    }

    public void run() {
        logger = LoggerFactory.getLogger(ProfilingTask.class);
        logger.debug("Started profiling task for source {}", source);
        JVMStateSchema schema = new JVMStateSchema(source);
        refAnalyzer = new JavaBytecodeAnalyzer(source, schema);
        generator = new JavaBytecodeGenerator();
        executor = new JavaBytecodeExecutor();
        SolutionStorage resultStorage = new SolutionStorage();
        for (InstructionSequence seq : candidates) {
            if (seq == null) {
                logger.warn("SolutionStorage should not contain null elements");
                continue;
            }
            double speedup = processCandidate(seq, schema);
            if (speedup > 1.01) {
                resultStorage.addSolution(seq, speedup);
            } else {
                logger.debug("Candidate {} recalculated speedup is only {}, removing", seq, speedup);
            }
        }
        setResult(resultStorage);
    }
}