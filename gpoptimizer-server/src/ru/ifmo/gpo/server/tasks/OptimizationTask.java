package ru.ifmo.gpo.server.tasks;

import org.jgap.Configuration;
import org.jgap.IChromosome;
import org.jgap.InvalidConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ifmo.gpo.core.*;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.population.PopulationGenerationFailedException;
import ru.ifmo.gpo.java.JavaTargetArchitecture;

import java.util.LinkedList;

public class OptimizationTask extends BaseGPOTask {
    private static final long serialVersionUID = 8733555514947334812L;

    public OptimizationTask(InstructionSequence source, String targetArchitectureID) {
        super(source, targetArchitectureID);
    }

    @Override
    public String toString() {
        return "OptimizationTask [source=" + source + ", targetArchitectureID="
                + targetArchitectureID + "]";
    }

    public void run() {
        Logger logger = LoggerFactory.getLogger(getClass());
        logger.info("Started optimization task");
        logger.debug("Source chromosome is {}", source);

        Island firstIsland;
        Island secondIsland;
        int maxGenerations = 75 * (source.size() / 2);

        try {
            ITargetArchitecture arch = new JavaTargetArchitecture();
            this.targetArchitectureID = arch.getId();

            Configuration.reset(MainIslandGPConfiguration.NAME);
            Configuration.reset(SupportIslandGPConfiguration.NAME);

            GPConfiguration conf = new MainIslandGPConfiguration(arch);
            conf.setReferenceChromosome(source);
            conf.setUpConfiguration();
            firstIsland = new Island(conf, "First island");
            logger.debug("Generating initial population");
            firstIsland.setUpRandomPopulation();

            conf = new SupportIslandGPConfiguration(arch);
            conf.setReferenceChromosome(source);
            conf.setUpConfiguration();
            secondIsland = new Island(conf, "Second island");
            secondIsland.setUpPopulationWithAllEqualChromosomes();
        } catch (InvalidConfigurationException ex) {
            logger.error("Failed to set up GP configuration", ex);
            setException(ex);
            return;
        } catch (PopulationGenerationFailedException e) {
            logger.error("Failed to set up initial population", e);
            setException(e);
            return;
        }


        logger.debug("Genetic process started with {} iterations", maxGenerations);

        LinkedList<Double> fitnessVals = new LinkedList<Double>();

        for (int i = 0; i < maxGenerations; ++i) {
            long iterationStartTime = System.currentTimeMillis();

            logger.debug("Processing generation #" + i + "...");
            firstIsland.processGeneration(i);
            secondIsland.processGeneration(i);


            IChromosome best = firstIsland.getPopulation().getFittestChromosome();
            double d = firstIsland.getConfiguration().getFitnessFunc().getFitnessValue(best);
            logger.debug("Best fitness value for generation is " + d);
            logger.debug("Best chromosome length: " + best.getGenes().length);
            fitnessVals.addLast(d);
            if (fitnessVals.size() > 10) {
                fitnessVals.removeFirst();
            }

            logger.debug("Iteration completed in " + (System.currentTimeMillis() - iterationStartTime));
            if ((i + 1) % 20 == 0) {
                firstIsland.crossoverSingleChromosome(secondIsland);
            }
        }
        SolutionStorage solutions = firstIsland.getConfiguration().getSolutions();
        solutions.merge(secondIsland.getConfiguration().getSolutions());
        if (solutions.size() > 0) {
            logger.debug("{} optimized solutions found for pattern {}", solutions.size(), source);

        } else {
            logger.debug("Failed to find optimized solution for pattern {}", source);
        }
        setResult(solutions);
    }

    public InstructionSequence getSource() {
        return source;
    }
}