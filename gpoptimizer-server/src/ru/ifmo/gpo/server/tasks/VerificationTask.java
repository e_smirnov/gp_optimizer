package ru.ifmo.gpo.server.tasks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ifmo.gpo.core.ITargetArchitecture;
import ru.ifmo.gpo.core.SolutionStorage;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.verify.IChromosomeVerifier;
import ru.ifmo.gpo.verify.VerificationResult;
import ru.ifmo.gpo.verify.verifiers.Z3Verifier;

/**
 * User: jedi-philosopher
 * Date: 27.03.2010
 * Time: 22:08:27
 * <p/>
 * Verifies that shource and candidate code sequences do same thing
 */

public class VerificationTask extends BaseGPOTask {
    private SolutionStorage candidates;
    private static final long serialVersionUID = -5958413515108783420L;

    public VerificationTask(SolutionStorage candidates, InstructionSequence reference, String archName) {
        super(reference, archName);
        this.candidates = candidates;
    }

    @Override
    public String toString() {
        return "VerificationTask{" +
                "candidates=" + candidates +
                "} " + super.toString();
    }

    public void run() {
        Logger logger = LoggerFactory.getLogger(VerificationTask.class);
        logger.info("Started verification task");
        ITargetArchitecture arch = createTargetArchitecture();
        IChromosomeVerifier myVerifier = new Z3Verifier(arch); //todo: somehow read from config
        for (InstructionSequence candidate : candidates) {
            VerificationResult rz = myVerifier.verifyEquivalence(candidate, source, arch.buildStateSchema(source));
            if (rz == VerificationResult.RESULT_SUCCESS) {
                logger.debug("Successfully verified candidate {} for source {}", candidate, source);
                setResult(candidate);
                return;
            }
            logger.debug("Verification failed for candidate {} and reference {}", candidate, source);
        }
        logger.debug("All solutions failed to pass verification for source {}", source);
        setResult(null);
    }
}
