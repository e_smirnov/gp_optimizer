package ru.ifmo.gpo.server;

import org.jppf.JPPFException;
import org.jppf.client.JPPFClient;
import org.jppf.client.JPPFJob;
import org.jppf.client.event.TaskResultEvent;
import org.jppf.client.event.TaskResultListener;
import org.jppf.server.protocol.JPPFTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ifmo.gpo.core.SolutionStorage;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.java.instructions.JavaSimpleOpcodeFactory;
import ru.ifmo.gpo.protocol.ServerVersionInfo;
import ru.ifmo.gpo.server.storage.MongoDBStorageManager;
import ru.ifmo.gpo.server.tasks.BaseGPOTask;
import ru.ifmo.gpo.server.tasks.OptimizationTask;
import ru.ifmo.gpo.server.tasks.ProfilingTask;
import ru.ifmo.gpo.server.tasks.VerificationTask;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Main class for optimization server. Listens for client connections. Submits jobs to JPPF grid.
 *
 * @author jedi-philosopher
 */
public class GPOServer extends Thread implements TaskResultListener {

    /**
     * Version information, is sent to clients when they connect
     */
    private ServerVersionInfo versionInfo;

    /**
     * Port for incoming connections
     */
    private static final int portNumber = 13999;

    private static final int maxConcurrentConnections = 10;

    /**
     * Stopping flag
     */
    private volatile boolean isStoping = false;

    /**
     * Just a logger
     */
    private Logger logger = LoggerFactory.getLogger(getClass());

    private ExecutorService clientConnectionsPool = Executors.newFixedThreadPool(maxConcurrentConnections);

    private OptimizationRetryThread retryThread = new OptimizationRetryThread(this);

    /**
     * Interface for JPPF grid
     */
    private JPPFClient jppfClient;

    /**
     * Interface for persisting optimization results
     */
    private IStorageManager storageManager;

    /**
     * Sorted queue of jobs that are waiting for submission into JPPF grid. JPPF has strange mechanism of submission,
     * when jobs with higher priorities are not executed before jobs with lower ones if they were submitted later.
     * This queue performs manual sorting of jobs according to their priorities
     */
    private final PriorityQueue<JPPFJob> taskQueue = new PriorityQueue<JPPFJob>(50, new JobComparator());

    /**
     * Number of tasks that were submitted to grid (by jppfClient.submit() and are now being processed)
     */
    private int submittedJobsCount = 0;

    /**
     * Maximum number of simultaneously submitted tasks. Is restricted by JPPF connection pool size
     */
    private static final int MAX_SUBMITTED_COUNT = 2;

    private static final int MAX_QUEUED_COUNT = 1000;

    /**
     * Used to sort jobs in a priority queue
     */
    private static class JobComparator implements Comparator<JPPFJob> {
        @Override
        public int compare(JPPFJob o1, JPPFJob o2) {
            return o2.getJobSLA().getPriority() - o1.getJobSLA().getPriority(); // reversed order, as PriorityQueue's head has lowest priority, and we want highest
        }
    }

    /**
     * Priority for JPPF optimization jobs that actually perform genetic process
     */
    public static final int OPTIMIZATION_JOB_PRIORITY = 5;

    /**
     * Priority for JPPF jobs that check optimization result speed
     */
    public static final int PROFILING_JOB_PRIORITY = 6;

    /**
     * Priority for JPPF jobs that verify results of optimizations. Should be highest between all other job priorities, so that
     * it is not required to wait for all previously submitted optimization tasks in order to verify and store final results.
     */
    public static final int VERIFICATION_JOB_PRIORITY = 8;

    public GPOServer() {
        versionInfo = new ServerVersionInfo(ServerVersion.protocolVersion, JavaSimpleOpcodeFactory.getLanguageId(), JavaSimpleOpcodeFactory.getSupportedOperations());
        jppfClient = new JPPFClient();
        storageManager = new MongoDBStorageManager();

    }

    public void stopServer() {
        logger.info("Received stop signal, initiating shutdown");
        isStoping = true;
    }

    public float getServerLoad() {
        return taskQueue.size() / MAX_QUEUED_COUNT;
    }

    /**
     * Submits job for processing. If number of currently active jobs is below MAX_SUBMITTED_COUNT, job is instantly
     * sent to grid, otherwise it is put into a queue
     *
     * @param job Job to process
     * @return true if submitted succesfully, false if error occured
     */
    public boolean submitJob(JPPFJob job) {
        if (taskQueue.size() > MAX_QUEUED_COUNT) {
            // sever is overloaded
            return false;
        }
        try {
            job.setBlocking(false);
            job.setResultListener(this);
            if (submittedJobsCount < MAX_SUBMITTED_COUNT) {
                logger.debug("Job submitted:{}", job.getTasks().get(0));
                jppfClient.submit(job);
                ++submittedJobsCount;
            } else {
                logger.debug("Job queued");
                synchronized (taskQueue) {
                    taskQueue.add(job);
                }
            }
            return true;
        } catch (Exception ex) {
            logger.error("Error while submitting job " + job + ": ", ex);
            return false;
        }
    }

    /**
     * Checks if this instruction sequence is already queued for optimization
     *
     * @param source Instruction sequence to check
     * @return true if task of any type with this sequence as source is found in queue
     */
    public boolean isSourceAlreadyQueued(InstructionSequence source) {
        synchronized (taskQueue) {
            for (JPPFJob job : taskQueue) {
                for (JPPFTask task : job.getTasks()) {
                    if (source.equals(((BaseGPOTask) task).getSource())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public IStorageManager getStorageManager() {
        return storageManager;
    }

    public void run() {
        logger.info("Server started");

        if (!storageManager.init("gpoptimizer")) {
            logger.error("Cannot start server without active Storage Manager, terminating");
            return;
        }
        retryThread.start();
        ServerSocket socket;
        try {
            socket = new ServerSocket(portNumber);
            socket.setSoTimeout(4000);
        } catch (IOException ex) {
            logger.error("Failed to open socket " + portNumber + ": ", ex);
            return;
        }

        logger.info("Waiting for connections on " + socket.toString());

        do {
            try {
                Socket clientSocket = socket.accept();
                logger.info("Client connected from " + clientSocket.getInetAddress().getHostAddress());
                ClientConnectionThread clientConnectionThread = new ClientConnectionThread(this, versionInfo, clientSocket);
                clientConnectionsPool.execute(clientConnectionThread);
            } catch (SocketTimeoutException ex) {
                // We must sometimes exit accept state and check if we should terminate this thread
            } catch (IOException ex) {
                logger.error("Exception while communicating with client");
            }

        } while (!isStoping);

        logger.info("Terminating client communication...");

        for (Runnable thread : ((ThreadPoolExecutor) clientConnectionsPool).getQueue()) {
            if (thread instanceof ClientConnectionThread) {
                ((ClientConnectionThread) thread).terminateConnection();
            }
        }
        retryThread.terminate();
        jppfClient.close();
        storageManager.term();
        logger.info("Server stopped");
    }

    public void resultsReceived(TaskResultEvent taskResultEvent) {
        logger.debug("Received results for " + taskResultEvent.getTaskList().size() + " job(s)");
        --submittedJobsCount;
        for (JPPFTask job : taskResultEvent.getTaskList()) {
            if (job.getException() != null) {
                logger.warn("Received response with exception inside:", job.getException());
                logger.warn("Job is {} ", job);
                continue;
            }
            if (job instanceof VerificationTask) {
                VerificationTask completedTask = (VerificationTask) job;
                processCompletedVerificationTask(completedTask);
            } else if (job instanceof OptimizationTask) {
                OptimizationTask completedTask = (OptimizationTask) job;
                processCompletedOptimizationTask(completedTask);
            } else if (job instanceof ProfilingTask) {
                processCompletedProfilingTask((ProfilingTask) job);
            }
        }
        if (!taskQueue.isEmpty() && submittedJobsCount < MAX_SUBMITTED_COUNT) {
            JPPFJob jobToSubmit = taskQueue.poll();
            logger.debug("Submitting job {}", jobToSubmit.getTasks().get(0));
            try {
                jppfClient.submit(jobToSubmit);
                ++submittedJobsCount;
            } catch (Exception e) {
                logger.error("Failed to submit job", e);
            }
        }
    }

    private void processCompletedOptimizationTask(OptimizationTask completedTask) {
        SolutionStorage solutions = (SolutionStorage) completedTask.getResult();
        if (solutions.size() == 0) {
            logger.debug("Received optimization task response with 0 results");
            storageManager.addFailedOptimizationRecord(new InstructionSequence(completedTask.getSource()));
        } else {
            logger.debug("Received succesful optimization task response, submitting profiling task");
            ProfilingTask newTask = new ProfilingTask(solutions, completedTask.getSource(), completedTask.getTargetArchitectureID());
            JPPFJob job = new JPPFJob();
            try {
                job.addTask(newTask);
                job.getJobSLA().setPriority(PROFILING_JOB_PRIORITY); // verification tasks have maximum priority to receive results as fast as possible
            } catch (JPPFException ex) {
                logger.error("Failed to create verification task: ", ex);
                //todo: do smth
                return;
            }
            submitJob(job);
        }
    }

    private void processCompletedProfilingTask(ProfilingTask completedTask) {
        SolutionStorage solutions = (SolutionStorage) completedTask.getResult();
        if (solutions.size() == 0) {
            logger.debug("Profiling task returned zero results");
            storageManager.addFailedOptimizationRecord(new InstructionSequence(completedTask.getSource()));
        } else {
            logger.debug("Received profiling tasks with successfull results, submitting verification task");
            VerificationTask newTask = new VerificationTask(solutions, completedTask.getSource(), completedTask.getTargetArchitectureID());
            JPPFJob job = new JPPFJob();
            try {
                job.addTask(newTask);
                job.getJobSLA().setPriority(VERIFICATION_JOB_PRIORITY); // verification tasks have maximum priority to receive results as fast as possible
            } catch (JPPFException ex) {
                logger.error("Failed to create verification task: ", ex);
                //todo: do smth
                return;
            }
            submitJob(job);
        }
    }

    private void processCompletedVerificationTask(VerificationTask task) {
        if (task.getResult() != null) {
            // we have succesfully completed optimization
            logger.debug("Received positive verification response");
            storageManager.addOptimizationRecord(task.getSource(), (InstructionSequence) task.getResult());
        } else {
            logger.debug("Received negative verification response");
            storageManager.addFailedOptimizationRecord(new InstructionSequence(task.getSource()));
        }
    }
}

