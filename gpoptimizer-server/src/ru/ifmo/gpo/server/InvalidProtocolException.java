package ru.ifmo.gpo.server;

/**
 * User: e_smirnov
 * Date: 01.09.2010
 * Time: 14:39:38
 */
public class InvalidProtocolException extends RuntimeException {
    private int serverProtocolVersion;
    private int clientProtocolVersion;
    private static final long serialVersionUID = -9068031988845564379L;

    public InvalidProtocolException(String message, int serverProtocolVersion, int clientProtocolVersion) {
        super("Protocol error: " + message + ", server version " + serverProtocolVersion + ", client version " + clientProtocolVersion);
        this.serverProtocolVersion = serverProtocolVersion;
        this.clientProtocolVersion = clientProtocolVersion;
    }

    public int getServerProtocolVersion() {
        return serverProtocolVersion;
    }

    public int getClientProtocolVersion() {
        return clientProtocolVersion;
    }
}
