package ru.ifmo.gpo.server;

/**
 * User: e_smirnov
 * Date: 01.09.2010
 * Time: 14:50:46
 */
public class ServerVersion {

    public static final int serverMajorVersion = 0;
    public static final int serverMinorVersion = 2;

    public static final int protocolVersion = 3;
}
