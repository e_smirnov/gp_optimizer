package ru.ifmo.gpo.server;

import ru.ifmo.gpo.core.instructions.InstructionSequence;

/**
 * Interface for persisting optimization results.
 *
 * @author jedi-philosopher
 */
public interface IStorageManager {
    /**
     * Add record about successfull optimization
     *
     * @param source    Source instruction sequence
     * @param optimized Optimized version
     */
    public void addOptimizationRecord(InstructionSequence source, InstructionSequence optimized);

    /**
     * Add record about failed optimization (so that it will not be sceduled again on client request).
     * If already exists optimized version, added earlier by addOptimizationRecord(), this method should not
     * overwrite it.
     *
     * @param source Instruction sequence for which no better variant was found
     */
    public void addFailedOptimizationRecord(InstructionSequence source);

    /**
     * Returns optimized version for sequence, or null if database contains either no record, or a failed optimization record
     *
     * @param source Instruction sequence for which to retrieve better version
     * @return Optimized sequence or null if it was not found before
     */
    public InstructionSequence getOptimized(InstructionSequence source);

    /**
     * Checks if previously addFailedOptimizationRecord() was called with same source, and no successfull optimizations were ever made
     *
     * @param source Instruction sequence to check
     * @return true if db contains only failed optimization record, false if there is no record or a successfull record
     */
    public boolean containsFailedOptimizationRecord(InstructionSequence source);

    /**
     * Returns random instruction sequence from list of available sequences that were successfully optimized before. May be used to
     * run re-optimization in attempt to find even better variant
     *
     * @return One of optimized instruction sequences or null if database is empty
     */
    public InstructionSequence getRandomRecordWithSolution();

    /**
     * Returns random sequence, which was added before by addFailedOptimizationRecord(), so that new attempt to find solution could be done.
     *
     * @return Sequence that has no optimized version, or null if database contains no such sequences
     */
    public InstructionSequence getRandomRecordWithoutSolution();

    /**
     * Get total count of all sequences that were successfully optimized
     *
     * @return Record count
     */
    public int getRecordsNum();

    /**
     * Initialize storage manager, using given database
     *
     * @param dbName Database name
     * @return true if successfully inited, false otherwise
     */
    public boolean init(String dbName);

    /**
     * Terminate storage manager
     */
    public void term();

    /**
     * Force save all data to database
     */
    public void flush();

    /**
     * Clear database. Used mainly for test purposes.
     */
    public void clear();
}