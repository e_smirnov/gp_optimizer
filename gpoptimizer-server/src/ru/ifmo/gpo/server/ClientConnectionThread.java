package ru.ifmo.gpo.server;

import org.jppf.client.JPPFJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.java.JavaTargetArchitecture;
import ru.ifmo.gpo.protocol.PatternItem;
import ru.ifmo.gpo.protocol.ServerVersionInfo;
import ru.ifmo.gpo.protocol.conn_events.*;
import ru.ifmo.gpo.server.tasks.OptimizationTask;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * User: jedi-philosopher
 * Date: 28.03.2010
 * Time: 0:52:51
 */


class ClientConnectionThread implements Runnable {

    private Logger logger = LoggerFactory.getLogger(getClass());

    private ServerVersionInfo _versionInfo;

    private Socket clientSocket;

    private volatile boolean isStopping;

    private GPOServer server;

    public ClientConnectionThread(GPOServer server, ServerVersionInfo versionInfo, Socket clientSocket) {
        this._versionInfo = versionInfo;
        this.clientSocket = clientSocket;
        this.isStopping = false;
        this.server = server;
    }

    public void terminateConnection() {
        isStopping = true;
    }

    @Override
    public void run() {
        try {

            ObjectInputStream inputFromClient = new ObjectInputStream(clientSocket.getInputStream());
            ObjectOutputStream outputToClient = new ObjectOutputStream(clientSocket.getOutputStream());

            outputToClient.writeObject(_versionInfo);

            Object event = inputFromClient.readObject();

            if (!(event instanceof SessionStartedEvent)) {
                // something wrong with client
                logger.warn("Client {} has failed to provide SessionStartedEvent, disconnecting", clientSocket.getInetAddress());
                // socket will be closed later
                throw new InvalidProtocolException("Session must start with SessionStartedEvent", _versionInfo.protocolVersion, -1);
            }

            do {
                event = inputFromClient.readObject();
                processEvent(outputToClient, event);
            } while (!(event instanceof SessionEndedEvent) && !isStopping);

            if (event != null && event instanceof SessionEndedEvent) {
                logger.info("Client disconnected ({}) ", ((SessionEndedEvent) event).getCause());
            }

        } catch (Exception ex) {
            logger.error("Exception has happened while communicating with client", ex);
        } finally {
            try {
                clientSocket.close();
            } catch (IOException ignore) {
                // nothing
            }
            logger.info("Connection terminated");
        }
    }

    private void processEvent(ObjectOutputStream linkToClient, Object event) throws IOException {
        if (event instanceof RequestEvent) {
            RequestEvent request = (RequestEvent) event;

            IStorageManager storage = server.getStorageManager();
            final InstructionSequence source = request.getRequestData().getSource();
            InstructionSequence rz = storage.getOptimized(source);

            if (rz != null) {
                // pattern found
                logger.debug("Optimized pattern found in database");
                PatternItem pattern = request.getRequestData();
                pattern.setOptimized(rz);
                DataResponseEvent response = new DataResponseEvent(SimpleResponseEvent.OK, pattern);
                linkToClient.writeObject(response);
                return;
            }

            if (storage.containsFailedOptimizationRecord(source)) {
                logger.debug("Received request for record that was previosly processed without success. Skipping.");
                linkToClient.writeObject(new SimpleResponseEvent(SimpleResponseEvent.SCHEDULED));
                return;
            }

            try {
                if (server.isSourceAlreadyQueued(source)) {
                    // do not queue for second time, just wait untill completed
                    linkToClient.writeObject(new SimpleResponseEvent(SimpleResponseEvent.SCHEDULED));
                    return;
                }
                if (server.getServerLoad() > 0.9999) {
                    linkToClient.writeObject(new SimpleResponseEvent(SimpleResponseEvent.REJECTED));
                    return;
                }
                OptimizationTask task = new OptimizationTask(source, JavaTargetArchitecture.id);
                JPPFJob job = new JPPFJob();
                job.addTask(task);
                job.getJobSLA().setPriority(GPOServer.OPTIMIZATION_JOB_PRIORITY);
                if (server.submitJob(job)) {
                    linkToClient.writeObject(new SimpleResponseEvent(SimpleResponseEvent.SCHEDULED));
                } else {
                    linkToClient.writeObject(new SimpleResponseEvent(SimpleResponseEvent.FAILED));
                }
            } catch (Exception ex) {
                logger.error("Exception while submitting task", ex);
                SimpleResponseEvent response = new SimpleResponseEvent(SimpleResponseEvent.FAILED);
                linkToClient.writeObject(response);
            }
        }
    }


}
