package ru.ifmo.gpo.tests.x86;

import org.junit.Assert;
import org.junit.Test;
import ru.ifmo.gpo.core.ICodeGenerationResult;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.x86.state.x86ConstantOperand;
import ru.ifmo.gpo.x86.state.x86MemoryOperand;
import ru.ifmo.gpo.x86.state.x86Register;
import ru.ifmo.gpo.x86.x86CodeGenerator;
import ru.ifmo.gpo.x86.x86Instruction;
import ru.ifmo.gpo.x86.x86Opcodes;

/**
 * User: jedi-philosopher
 * Date: 21.01.2011
 * Time: 15:42:27
 */
public class x86CodeGeneratorTest {

    @Test
    public void codegenSimple() {
        x86CodeGenerator generator = new x86CodeGenerator();
        InstructionSequence seq = new InstructionSequence();
        seq.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.eax), new x86Register(x86Opcodes.ebx)));
        ICodeGenerationResult res = generator.generateSandboxedCode(seq, null, null);
        Assert.assertNotNull(res);
    }

    @Test
    public void codegenSimpleArithmetic() {
        x86CodeGenerator generator = new x86CodeGenerator();
        InstructionSequence seq = new InstructionSequence();
        seq.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.eax), new x86MemoryOperand("0x00ff", 4)));
        seq.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.ebx), new x86MemoryOperand("0x00ff", 4)));
        seq.add(new x86Instruction(x86Opcodes.ADD, new x86Register(x86Opcodes.eax), new x86Register(x86Opcodes.ebx)));
        seq.add(new x86Instruction(x86Opcodes.MOV, new x86MemoryOperand("0x00ff", 4), new x86Register(x86Opcodes.eax)));

        ICodeGenerationResult res = generator.generateSandboxedCode(seq, null, null);
        Assert.assertNotNull(res);
    }

    @Test
    public void testImmediateOperand() {
        x86CodeGenerator generator = new x86CodeGenerator();
        InstructionSequence seq = new InstructionSequence();
        seq.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.eax), new x86ConstantOperand(0)));
        ICodeGenerationResult res = generator.generateSandboxedCode(seq, null, null);
        Assert.assertNotNull(res);
    }

}
