package ru.ifmo.gpo.tests.x86;

import org.junit.Assert;
import org.junit.Test;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.vmstate.IMachineState;
import ru.ifmo.gpo.verify.BinaryOperatorNode;
import ru.ifmo.gpo.verify.GenericOperator;
import ru.ifmo.gpo.verify.IArithmeticTreeNode;
import ru.ifmo.gpo.x86.state.*;
import ru.ifmo.gpo.x86.x86Instruction;
import ru.ifmo.gpo.x86.x86Opcodes;

/**
 * User: jedi-philosopher
 * Date: 25.01.2011
 * Time: 17:30:13
 */
public class AnalyzerTest {
    @Test
    public void testSimple() {
        InstructionSequence seq = new InstructionSequence();
        seq.add(new x86Instruction(x86Opcodes.MOV, new x86MemoryOperand("0xabc", 4), new x86Register(x86Opcodes.eax)));
        StateAnalyzer analyzer = new StateAnalyzer(seq);

        Assert.assertEquals(1, analyzer.getStateChange().getDelta());
        Assert.assertEquals(1, analyzer.getStateBefore().getRegisters().size());
        Assert.assertEquals(x86Opcodes.eax, analyzer.getStateBefore().getRegisters().values().iterator().next().getId());
    }

    @Test
    public void testSimpleArithmetics() {
        InstructionSequence seq = new InstructionSequence();
        seq.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.eax), new x86MemoryOperand("0xff", 4)));
        seq.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.ebx), new x86MemoryOperand("0xff", 4)));
        seq.add(new x86Instruction(x86Opcodes.MUL, new x86Register(x86Opcodes.eax), new x86Register(x86Opcodes.ebx)));
        seq.add(new x86Instruction(x86Opcodes.MOV, new x86MemoryOperand("0xabc", 4), new x86Register(x86Opcodes.eax)));

        StateAnalyzer analyzer = new StateAnalyzer(seq);
        Assert.assertEquals(3, analyzer.getStateChange().getDelta());
        Assert.assertEquals(2, analyzer.getStateBefore().getRegisters().size());
        Assert.assertEquals(2, analyzer.getStateBefore().getMemory().size());

        Assert.assertFalse(analyzer.getStateBefore().getRegisters().get(x86Opcodes.eax).hasValue());
        Assert.assertFalse(analyzer.getStateBefore().getRegisters().get(x86Opcodes.ebx).hasValue());
        Assert.assertTrue(analyzer.getStateBefore().getMemory().get("0xff").hasValue());

        IMachineState stateAfter = analyzer.getStateAfter();
        Assert.assertEquals(2, stateAfter.getRegisters().size());

        GenericOperand op = stateAfter.getRegisters().get(x86Opcodes.eax);
        IArithmeticTreeNode rzNode = op.getExpression();
        Assert.assertTrue(rzNode instanceof BinaryOperatorNode);
        Assert.assertTrue(((BinaryOperatorNode) rzNode).getOperator().equals(GenericOperator.MUL));
    }

    @Test
    public void testStatesEqualFromSameCode() {
        InstructionSequence seq = new InstructionSequence();
        seq.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.eax), new x86MemoryOperand("0xff", 4)));
        StateAnalyzer analyzer1 = new StateAnalyzer(seq);
        StateAnalyzer analyzer2 = new StateAnalyzer(seq);

        Assert.assertTrue(analyzer1.checkStateEqual(analyzer2));
    }

    @Test
    public void testOperandAccessBeforeCode() {
        InstructionSequence seq = new InstructionSequence();
        seq.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.eax), new x86MemoryOperand("0xff", 4)));
        seq.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.ebx), new x86Register(x86Opcodes.ecx)));
        seq.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.ecx), new x86Register(x86Opcodes.eax)));

        StateAnalyzer analyzer = new StateAnalyzer(seq);
        x86ProcessorState state = analyzer.getStateBefore();

        GenericOperand ecx = state.getRegisters().get(x86Opcodes.ecx);
        GenericOperand mem = state.getMemory().get("0xff");

        Assert.assertTrue(ecx.isWriteable());
        Assert.assertTrue(ecx.isReadable());

        Assert.assertFalse(mem.isWriteable());
        Assert.assertTrue(mem.isReadable());
    }

    @Test
    public void testOperandAccessAfterCode() {
        InstructionSequence seq = new InstructionSequence();
        seq.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.eax), new x86MemoryOperand("0xff", 4)));
        seq.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.ebx), new x86Register(x86Opcodes.ecx)));
        seq.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.eax), new x86Register(x86Opcodes.ecx)));

        StateAnalyzer analyzer = new StateAnalyzer(seq);
        x86ProcessorState state = analyzer.getStateAfter();

        GenericOperand eax = state.getRegisters().get(x86Opcodes.eax);
        GenericOperand ebx = state.getRegisters().get(x86Opcodes.ebx);
        GenericOperand ecx = state.getRegisters().get(x86Opcodes.ecx);
        GenericOperand mem = state.getMemory().get("0xff");

        Assert.assertTrue(eax.isWriteable());
        Assert.assertTrue(eax.isReadable());

        Assert.assertTrue(ebx.isWriteable());
        Assert.assertTrue(ebx.isReadable());

        Assert.assertFalse(ecx.isWriteable());
        Assert.assertTrue(ecx.isReadable());

        Assert.assertFalse(mem.isWriteable());
        Assert.assertTrue(mem.isReadable());
    }

    @Test
    public void testConstants() {
        InstructionSequence seq = new InstructionSequence();
        seq.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.eax), new x86ConstantOperand(100500)));
        seq.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.ebx), new x86ConstantOperand(-2)));
        seq.add(new x86Instruction(x86Opcodes.ADD, new x86Register(x86Opcodes.eax), new x86Register(x86Opcodes.ebx)));

        StateAnalyzer analyzer = new StateAnalyzer(seq);

        for (IMachineState state : analyzer.getStates()) {
            Assert.assertTrue(state.getConstants().getIntConstants().contains(100500));
            Assert.assertTrue(state.getConstants().getIntConstants().contains(-2));
        }
    }

    @Test
    public void testEqualStatesMovAndAdd() {
        InstructionSequence first = new InstructionSequence();
        first.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.eax), new x86MemoryOperand("0xff", 4)));

        StateAnalyzer firstAnalyzer = new StateAnalyzer(first);

        InstructionSequence second = new InstructionSequence();
        second.add(new x86Instruction(x86Opcodes.ADD, new x86Register(x86Opcodes.eax), new x86MemoryOperand("0xff", 4)));
        second.add(new x86Instruction(x86Opcodes.OR, new x86Register(x86Opcodes.eax), new x86MemoryOperand("0xff", 4)));

        StateAnalyzer secondAnalyzer = new StateAnalyzer(second);

        Assert.assertTrue(firstAnalyzer.checkStateEqual(secondAnalyzer));
    }

    @Test
    public void testEqualsArithAndSimpleConstantMov() {
        InstructionSequence first = new InstructionSequence();
        first.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.eax), new x86MemoryOperand("0x1000a", 4)));
        first.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.ebx), new x86MemoryOperand("0x1000f", 4)));
        first.add(new x86Instruction(x86Opcodes.ADD, new x86Register(x86Opcodes.eax), new x86Register(x86Opcodes.ebx)));

        InstructionSequence second = new InstructionSequence();
        second.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.eax), new x86ConstantOperand(-1)));
        second.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.ebx), new x86ConstantOperand(2)));

        StateAnalyzer firstAnalyzer = new StateAnalyzer(first);
        StateAnalyzer secondAnalyzer = new StateAnalyzer(second);

        Assert.assertTrue(firstAnalyzer.checkStateEqual(secondAnalyzer));
    }

    @Test
    public void testMovEqualsXorWithNop() {
        InstructionSequence first = new InstructionSequence();
        first.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.eax), new x86MemoryOperand("0x1000a", 4)));
        first.add(new x86Instruction(x86Opcodes.NOP));

        InstructionSequence second = new InstructionSequence();
        second.add(new x86Instruction(x86Opcodes.NOP));
        second.add(new x86Instruction(x86Opcodes.XOR, new x86Register(x86Opcodes.eax), new x86Register(x86Opcodes.eax)));

        StateAnalyzer firstAnalyzer = new StateAnalyzer(first);
        StateAnalyzer secondAnalyzer = new StateAnalyzer(second);

        Assert.assertTrue(firstAnalyzer.checkStateEqual(secondAnalyzer));
        Assert.assertTrue(secondAnalyzer.checkStateEqual(firstAnalyzer));
    }

    @Test
    public void testDestOperandsAreAddedToNecessaryWrites() {
        InstructionSequence first = new InstructionSequence();
        first.add(new x86Instruction(x86Opcodes.ADD, new x86Register(x86Opcodes.eax), new x86Register(x86Opcodes.ebx)));
        StateAnalyzer firstAnalyzer = new StateAnalyzer(first);

        GenericOperand eax = firstAnalyzer.getStateBefore().getRegisters().get(x86Opcodes.eax);
        Assert.assertTrue(eax.isReadable());
        Assert.assertTrue(eax.isWriteable());
        Assert.assertTrue(eax.hasValue());
    }
}
