package ru.ifmo.gpo.tests.x86;

import org.junit.Assert;
import org.junit.Test;
import ru.ifmo.gpo.core.ICodeExecutionEnvironment;
import ru.ifmo.gpo.core.ICodeGenerationResult;
import ru.ifmo.gpo.core.IExecutionResult;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.x86.*;
import ru.ifmo.gpo.x86.state.x86ConstantOperand;
import ru.ifmo.gpo.x86.state.x86Register;

import java.math.BigDecimal;

/**
 * User: jedi-philosopher
 * Date: 29.01.2011
 * Time: 19:00:12
 */
public class x86CodeExecutorTest {

    @Test
    public void testExecuteSimple() {
        x86CodeGenerator generator = new x86CodeGenerator();
        InstructionSequence seq = new InstructionSequence();
        seq.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.eax), new x86Register(x86Opcodes.ebx)));
        ICodeGenerationResult res = generator.generateProfileCode(seq, null, null);

        x86CodeExecutor executor = new x86CodeExecutor();
        ICodeExecutionEnvironment env = res.createExecutionEnvironment();
        env.setRandomValues();
        long result = executor.profileCode(res, env);

        Assert.assertTrue(result != 0);
    }

    @Test
    public void testWriteOneToEaxHasVectorOfLength1() {
        x86CodeGenerator generator = new x86CodeGenerator();
        x86CodeExecutor executor = new x86CodeExecutor();

        InstructionSequence first = new InstructionSequence();
        first.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.eax), new x86ConstantOperand(1)));
        ICodeGenerationResult res = generator.generateSandboxedCode(first, null, null);
        x86ExecutionResult execRes = (x86ExecutionResult) executor.executeCode(res, res.createExecutionEnvironment());

        Assert.assertTrue(Math.abs(execRes.getExecutionResultVectorLength() - 1) < 0.01);

        execRes.dispose();
    }

    @Test
    public void testZeroToEaxTwoWaysGiveSameResult() {
        x86CodeGenerator generator = new x86CodeGenerator();
        x86CodeExecutor executor = new x86CodeExecutor();

        InstructionSequence first = new InstructionSequence();
        first.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.eax), new x86ConstantOperand(0)));
        ICodeGenerationResult res = generator.generateSandboxedCode(first, null, null);

        InstructionSequence second = new InstructionSequence();
        second.add(new x86Instruction(x86Opcodes.XOR, new x86Register(x86Opcodes.eax), new x86Register(x86Opcodes.eax)));
        ICodeGenerationResult secondRes = generator.generateSandboxedCode(second, null, null);

        IExecutionResult execRes1 = executor.executeCode(res, res.createExecutionEnvironment());
        IExecutionResult execRes2 = executor.executeCode(secondRes, secondRes.createExecutionEnvironment());

        Assert.assertTrue(execRes1.computeDifference(execRes2).abs().compareTo(BigDecimal.valueOf(0.001)) < 0);
        Assert.assertTrue(Math.abs(((x86ExecutionResult) execRes1).getExecutionResultVectorLength()) < 0.001);
        Assert.assertTrue(Math.abs(((x86ExecutionResult) execRes2).getExecutionResultVectorLength()) < 0.001);
    }

    @Test
    public void testZeroAndOneGiveDifferenceOfOne() {
        x86CodeGenerator generator = new x86CodeGenerator();
        x86CodeExecutor executor = new x86CodeExecutor();

        InstructionSequence first = new InstructionSequence();
        first.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.eax), new x86ConstantOperand(0)));
        ICodeGenerationResult res = generator.generateSandboxedCode(first, null, null);

        InstructionSequence second = new InstructionSequence();
        second.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.eax), new x86ConstantOperand(1)));
        ICodeGenerationResult secondRes = generator.generateSandboxedCode(second, null, null);

        IExecutionResult execRes1 = executor.executeCode(res, res.createExecutionEnvironment());
        IExecutionResult execRes2 = executor.executeCode(secondRes, secondRes.createExecutionEnvironment());

        Assert.assertTrue(execRes1.computeDifference(execRes2).subtract(BigDecimal.valueOf(1)).abs().compareTo(BigDecimal.valueOf(0.001)) < 0);
    }

    @Test(expected = NativeCodeException.class)
    public void testNativeExceptionOnInvalidCodegenResult() {
        x86CodeExecutor executor = new x86CodeExecutor();
        executor.executeCode(new x86CodeGenerationResult(null, "name", 100500), new x86CodeExecutionEnvironment(100500));
    }

    @Test(expected = ArithmeticException.class)
    public void testIntegerDivisionByZeroCausesArithmeticException() {
        x86CodeGenerator generator = new x86CodeGenerator();
        x86CodeExecutor executor = new x86CodeExecutor();

        InstructionSequence first = new InstructionSequence();
        first.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.ebx), new x86ConstantOperand(0)));
        first.add(new x86Instruction(x86Opcodes.DIV, new x86Register(x86Opcodes.ebx)));
        ICodeGenerationResult res = generator.generateSandboxedCode(first, null, null);

        executor.executeCode(res, res.createExecutionEnvironment());

    }

    @Test
    public void testMultipleSequenceExecutionDoesNotCauseNativeException() {
        x86CodeGenerator generator = new x86CodeGenerator();
        x86CodeExecutor executor = new x86CodeExecutor();

        InstructionSequence first = new InstructionSequence();
        first.add(new x86Instruction(x86Opcodes.AND, new x86Register(x86Opcodes.eax), new x86Register(x86Opcodes.ebx)));
        first.add(new x86Instruction(x86Opcodes.SUB, new x86Register(x86Opcodes.eax), new x86Register(x86Opcodes.eax)));
        first.add(new x86Instruction(x86Opcodes.SUB, new x86Register(x86Opcodes.eax), new x86Register(x86Opcodes.ebx)));
        ICodeGenerationResult res = generator.generateSandboxedCode(first, null, null);

        for (int i = 0; i < 10; ++i) {
            IExecutionResult execRes = executor.executeCode(res, res.createExecutionEnvironment());
            Assert.assertNotNull(execRes);
            execRes.dispose();
        }
    }
}
