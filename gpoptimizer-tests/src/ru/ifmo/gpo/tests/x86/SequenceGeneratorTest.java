package ru.ifmo.gpo.tests.x86;

import org.junit.Assert;
import org.junit.Test;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.SequenceGenerationFailedException;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.vmstate.NoSuitableOperandFoundException;
import ru.ifmo.gpo.x86.state.StateAnalyzer;
import ru.ifmo.gpo.x86.state.x86MemoryOperand;
import ru.ifmo.gpo.x86.state.x86Register;
import ru.ifmo.gpo.x86.state.x86StateDelta;
import ru.ifmo.gpo.x86.x86Instruction;
import ru.ifmo.gpo.x86.x86OpcodeWrapper;
import ru.ifmo.gpo.x86.x86Opcodes;
import ru.ifmo.gpo.x86.x86TargetArchitecture;

import java.util.List;

/**
 * User: e_smirnov
 * Date: 27.01.2011
 * Time: 15:39:53
 */
public class SequenceGeneratorTest {


    @Test
    public void testGenerateWriteToEax() throws SequenceGenerationFailedException {
        x86TargetArchitecture arch = new x86TargetArchitecture();
        InstructionSequence seq = new InstructionSequence();
        seq.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.eax), new x86MemoryOperand("0x1000a", 4)));
        seq.add(new x86Instruction(x86Opcodes.NOP));

        StateAnalyzer analyzer = new StateAnalyzer(seq);

        InstructionSequence rz = arch.getInstructionSequenceGenerator().generateInstructionSequence(analyzer.getStateBefore(), (x86StateDelta) analyzer.getStateChange(), 2);

        StateAnalyzer rzAnalyzer = new StateAnalyzer(rz);

        x86StateDelta stateDelta = new x86StateDelta(rzAnalyzer.getStateAfter(), analyzer.getStateAfter());
        if (stateDelta.getDelta() != 0) {
            System.out.println(seq.toString());
            System.out.println(rz.toString());
        }
        Assert.assertTrue(rzAnalyzer.checkStateEqual(analyzer));
        Assert.assertTrue(analyzer.checkStateEqual(rzAnalyzer));
    }

    @Test
    public void testOperandSelectorCanSelectAnything() throws NoSuitableOperandFoundException {
        InstructionSequence seq = new InstructionSequence();
        seq.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.eax), new x86MemoryOperand("0x1000a", 4)));
        seq.add(new x86Instruction(x86Opcodes.NOP));

        StateAnalyzer analyzer = new StateAnalyzer(seq);

        List<GenericOperand> rz = x86OpcodeWrapper.MOV.getOperandSelector().selectOperandsRandom(x86OpcodeWrapper.MOV, analyzer.getStateBefore());
        Assert.assertEquals(2, rz.size());
    }

    @Test
    public void testGenerateSimpleArithmetics() throws SequenceGenerationFailedException {
        x86TargetArchitecture arch = new x86TargetArchitecture();
        InstructionSequence seq = new InstructionSequence();
        seq.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.eax), new x86MemoryOperand("0x1000a", 4)));
        seq.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.ebx), new x86MemoryOperand("0x1000f", 4)));
        seq.add(new x86Instruction(x86Opcodes.ADD, new x86Register(x86Opcodes.eax), new x86Register(x86Opcodes.ebx)));

        StateAnalyzer analyzer = new StateAnalyzer(seq);

        InstructionSequence rz = arch.getInstructionSequenceGenerator().generateInstructionSequence(analyzer.getStateBefore(), (x86StateDelta) analyzer.getStateChange(), 3);

        StateAnalyzer rzAnalyzer = new StateAnalyzer(rz);

        x86StateDelta stateDelta = new x86StateDelta(rzAnalyzer.getStateAfter(), analyzer.getStateAfter());
        if (stateDelta.getDelta() != 0) {
            System.out.println(seq.toString());
            System.out.println(rz.toString());
        }

        Assert.assertTrue(rzAnalyzer.checkStateEqual(analyzer));
        Assert.assertTrue(analyzer.checkStateEqual(rzAnalyzer));
    }
}
