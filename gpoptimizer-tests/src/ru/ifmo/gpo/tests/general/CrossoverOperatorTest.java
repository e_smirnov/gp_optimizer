package ru.ifmo.gpo.tests.general;

import org.jgap.Configuration;
import org.jgap.IChromosome;
import org.jgap.InvalidConfigurationException;
import org.jgap.impl.StockRandomGenerator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ru.ifmo.gpo.core.GPConfiguration;
import ru.ifmo.gpo.core.MainIslandGPConfiguration;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.operators.BaseCodeCrossoverOperation;
import ru.ifmo.gpo.core.operators.CodeCrossoverOperation;
import ru.ifmo.gpo.core.vmstate.IInstructionSequenceAnalyzer;
import ru.ifmo.gpo.java.JavaTargetArchitecture;
import ru.ifmo.gpo.java.instructions.JVMInstruction;
import ru.ifmo.gpo.java.instructions.JavaOpcodes;
import ru.ifmo.gpo.java.vmstate.JVMStateSchema;
import ru.ifmo.gpo.java.vmstate.LocalVariableSlot;
import ru.ifmo.gpo.x86.state.x86MemoryOperand;
import ru.ifmo.gpo.x86.state.x86Register;
import ru.ifmo.gpo.x86.x86Instruction;
import ru.ifmo.gpo.x86.x86Opcodes;
import ru.ifmo.gpo.x86.x86TargetArchitecture;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * User: jedi-philosopher
 * Date: 02.11.2010
 * Time: 22:43:30
 */
public class CrossoverOperatorTest {


    static GPConfiguration conf;

    @Before
    public void setConf() throws InvalidConfigurationException {
        Configuration.reset();
        conf = new MainIslandGPConfiguration(new JavaTargetArchitecture());
    }

    @After
    public void resetConf() {
        conf = null;
    }

    public void checkResults(IChromosome first, IChromosome second, List<IChromosome> results) {
        InstructionSequence firstSeq = new InstructionSequence(first);
        InstructionSequence secondSeq = new InstructionSequence(second);
        IInstructionSequenceAnalyzer firstAnalyzer = conf.getTargetArchitecture().analyzeCode(firstSeq, conf.getStateSchema());
        IInstructionSequenceAnalyzer secondAnalyzer = conf.getTargetArchitecture().analyzeCode(secondSeq, conf.getStateSchema());


        for (IChromosome result : results) {
            try {
                IInstructionSequenceAnalyzer resultAnalyzer = conf.getTargetArchitecture().analyzeCode(new InstructionSequence(result), conf.getStateSchema());

                assert resultAnalyzer.checkStateEqual(firstAnalyzer);
                assert resultAnalyzer.checkStateEqual(secondAnalyzer);
            } catch (AssertionError error) {
                System.out.println(Arrays.toString(result.getGenes()));
                throw error;
            }
        }

    }

    public void performCrossover(InstructionSequence first, InstructionSequence second) throws InvalidConfigurationException {
        CodeCrossoverOperation crossoverOperation = new CodeCrossoverOperation(conf, 100);

        List<IChromosome> results = new LinkedList<IChromosome>();

        IChromosome firstChromosome = first.toChromosome(conf);
        IChromosome secondChromosome = second.toChromosome(conf);

        crossoverOperation.doCrossover(firstChromosome, secondChromosome, results, new StockRandomGenerator());

        checkResults(firstChromosome, secondChromosome, results);
    }

    @Test
    public void testCrossoverSimple() throws InvalidConfigurationException {
        InstructionSequence first = new InstructionSequence();
        first.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        first.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        first.add(new JVMInstruction(JavaOpcodes.IADD));

        InstructionSequence second = new InstructionSequence();
        second.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        second.add(new JVMInstruction(JavaOpcodes.ICONST_2));
        second.add(new JVMInstruction(JavaOpcodes.IADD));

        conf.setStateSchema(new JVMStateSchema(first));

        performCrossover(first, second);
    }

    @Test
    public void testCrossoverWithStackDraining() throws InvalidConfigurationException {
        InstructionSequence first = new InstructionSequence();
        first.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        first.add(new JVMInstruction(JavaOpcodes.ISHL));
        first.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(0)));

        InstructionSequence second = new InstructionSequence();
        second.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        second.add(new JVMInstruction(JavaOpcodes.DUP));
        second.add(new JVMInstruction(JavaOpcodes.IADD));
        second.add(new JVMInstruction(JavaOpcodes.IADD));
        second.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(0)));

        conf.setStateSchema(new JVMStateSchema(first));

        performCrossover(first, second);
    }

    @Test
    public void testCrossoverPointsWithStackDraining() throws InvalidConfigurationException {
        InstructionSequence first = new InstructionSequence();
        first.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        first.add(new JVMInstruction(JavaOpcodes.ISHL));
        first.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(0)));

        InstructionSequence second = new InstructionSequence();
        second.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        second.add(new JVMInstruction(JavaOpcodes.DUP));
        second.add(new JVMInstruction(JavaOpcodes.IADD));
        second.add(new JVMInstruction(JavaOpcodes.IADD));
        second.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(0)));

        conf.setStateSchema(new JVMStateSchema(first));

        CodeCrossoverOperation crossoverOperation = new CodeCrossoverOperation(conf, 100);

        IChromosome firstChromosome = first.toChromosome(conf);
        IChromosome secondChromosome = second.toChromosome(conf);

        List<BaseCodeCrossoverOperation.CrossoverPoint> points = crossoverOperation.getPossibleCrossoverPoints(firstChromosome, secondChromosome);

        assert points.size() == 3;
    }

    @Test
    public void testCrossoverTwoTypes() throws InvalidConfigurationException {
        InstructionSequence first = new InstructionSequence();
        first.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        first.add(new JVMInstruction(JavaOpcodes.I2F));
        first.add(new JVMInstruction(JavaOpcodes.DUP));
        first.add(new JVMInstruction(JavaOpcodes.FMUL));

        InstructionSequence second = new InstructionSequence();
        second.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        second.add(new JVMInstruction(JavaOpcodes.FCONST_2));
        second.add(new JVMInstruction(JavaOpcodes.F2I));
        second.add(new JVMInstruction(JavaOpcodes.IMUL));
        second.add(new JVMInstruction(JavaOpcodes.I2F));

        conf.setStateSchema(new JVMStateSchema(first));

        performCrossover(first, second);
    }

    @Test
    public void testCrossoverTwoTypes2() throws InvalidConfigurationException {
        InstructionSequence first = new InstructionSequence();
        first.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        first.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(1)));
        first.add(new JVMInstruction(JavaOpcodes.DUP));
        first.add(new JVMInstruction(JavaOpcodes.I2F));
        first.add(new JVMInstruction(JavaOpcodes.FCONST_2));
        first.add(new JVMInstruction(JavaOpcodes.FDIV));
        first.add(new JVMInstruction(JavaOpcodes.FSTORE, new LocalVariableSlot(1)));
        first.add(new JVMInstruction(JavaOpcodes.IMUL));
        first.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(0)));

        InstructionSequence second = new InstructionSequence();
        second.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        second.add(new JVMInstruction(JavaOpcodes.ICONST_2));
        second.add(new JVMInstruction(JavaOpcodes.I2F));
        second.add(new JVMInstruction(JavaOpcodes.FCONST_2));
        second.add(new JVMInstruction(JavaOpcodes.FADD));
        second.add(new JVMInstruction(JavaOpcodes.FSTORE, new LocalVariableSlot(1)));
        second.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(0)));

        conf.setStateSchema(new JVMStateSchema(first));

        performCrossover(first, second);
    }

    @Test
    public void testCrossoverx86simple() throws InvalidConfigurationException {
        conf.setTargetArchitecture(new x86TargetArchitecture());
        InstructionSequence first = new InstructionSequence();
        first.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.eax), new x86MemoryOperand("0xff", 4)));
        first.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.ebx), new x86MemoryOperand("0xff", 4)));
        first.add(new x86Instruction(x86Opcodes.MUL, new x86Register(x86Opcodes.eax), new x86Register(x86Opcodes.ebx)));
        first.add(new x86Instruction(x86Opcodes.MOV, new x86MemoryOperand("0xabc", 4), new x86Register(x86Opcodes.eax)));

        InstructionSequence second = new InstructionSequence();
        second.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.eax), new x86MemoryOperand("0xff", 4)));
        second.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.ebx), new x86Register(x86Opcodes.eax)));
        second.add(new x86Instruction(x86Opcodes.ADD, new x86Register(x86Opcodes.eax), new x86Register(x86Opcodes.ebx)));
        second.add(new x86Instruction(x86Opcodes.MOV, new x86MemoryOperand("0xabc", 4), new x86Register(x86Opcodes.eax)));

        performCrossover(first, second);
    }

    @Test
    public void testCrossoverX86WithUnfilledRegisters() throws InvalidConfigurationException {

        conf.setTargetArchitecture(new x86TargetArchitecture());
        InstructionSequence first = new InstructionSequence();
        first.add(new x86Instruction(x86Opcodes.MUL, new x86Register(x86Opcodes.eax), new x86Register(x86Opcodes.ebx)));
        first.add(new x86Instruction(x86Opcodes.ADD, new x86Register(x86Opcodes.ecx), new x86Register(x86Opcodes.edx)));
        first.add(new x86Instruction(x86Opcodes.SHL, new x86Register(x86Opcodes.eax)));
        first.add(new x86Instruction(x86Opcodes.ADD, new x86Register(x86Opcodes.eax), new x86Register(x86Opcodes.ecx)));
        first.add(new x86Instruction(x86Opcodes.MOV, new x86MemoryOperand("0xabc", 4), new x86Register(x86Opcodes.eax)));

        InstructionSequence second = new InstructionSequence();

        second.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.eax), new x86Register(x86Opcodes.ebx)));
        second.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.ecx), new x86Register(x86Opcodes.edx)));
        second.add(new x86Instruction(x86Opcodes.ADD, new x86Register(x86Opcodes.eax), new x86Register(x86Opcodes.ecx)));
        second.add(new x86Instruction(x86Opcodes.MOV, new x86MemoryOperand("0xabc", 4), new x86Register(x86Opcodes.ecx)));

        performCrossover(first, second);
    }
}
