package ru.ifmo.gpo.tests.general;

import org.jgap.*;
import org.jgap.impl.StringGene;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.objectweb.asm.Opcodes;
import ru.ifmo.gpo.core.CodeGene;
import ru.ifmo.gpo.core.GPConfiguration;
import ru.ifmo.gpo.core.MainIslandGPConfiguration;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.IGenericInstruction;
import ru.ifmo.gpo.java.JavaTargetArchitecture;
import ru.ifmo.gpo.java.instructions.JVMInstruction;
import ru.ifmo.gpo.java.instructions.JavaOpcodes;
import ru.ifmo.gpo.java.vmstate.LocalVariableSlot;

import java.util.LinkedList;
import java.util.List;

/**
 * User: e_smirnov
 * Date: 01.11.2010
 * Time: 14:46:42
 */
public class InstructionSequenceTest {

    static GPConfiguration conf;

    @Before
    public void setConf() throws InvalidConfigurationException {
        Configuration.reset();
        conf = new MainIslandGPConfiguration(new JavaTargetArchitecture());
    }

    @After
    public void resetConf() {
        conf = null;
    }

    @Test
    public void testCreateFromCollection() {
        List<IGenericInstruction> list = new LinkedList<IGenericInstruction>();
        list.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        list.add(new JVMInstruction(JavaOpcodes.DUP));

        InstructionSequence sequence = new InstructionSequence(list);

        Assert.assertEquals(list.size(), sequence.size());
        Assert.assertEquals(Opcodes.ILOAD, sequence.get(0).getOpcode());
        Assert.assertEquals(0, ((LocalVariableSlot) ((JVMInstruction) sequence.get(0)).getParameter(0)).getIdx());
        Assert.assertEquals(Opcodes.DUP, sequence.get(1).getOpcode());
        Assert.assertNull(((JVMInstruction) sequence.get(1)).getParameter(0));
    }

    @Test
    public void testCreateFromArray() {
        JVMInstruction[] instrs = {new JVMInstruction(JavaOpcodes.IADD), new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(5))};

        InstructionSequence sequence = new InstructionSequence(instrs);
        Assert.assertEquals(2, sequence.size());
        Assert.assertEquals(Opcodes.IADD, sequence.get(0).getOpcode());
        Assert.assertNull(((JVMInstruction) sequence.get(0)).getParameter(0));
        Assert.assertEquals(Opcodes.ISTORE, sequence.get(1).getOpcode());
        Assert.assertEquals(new LocalVariableSlot(5), ((JVMInstruction) sequence.get(1)).getParameter(0));
    }

    @Test
    public void testCreateFromChromosome() throws InvalidConfigurationException, UnsupportedRepresentationException {
        Chromosome chromosome = new Chromosome(conf);

        CodeGene codeGenes[] = new CodeGene[3];
        codeGenes[0] = new CodeGene(conf);
        codeGenes[0].setAllele(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(2)));

        codeGenes[1] = new CodeGene(conf);
        codeGenes[1].setAllele(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(2)));

        codeGenes[2] = new CodeGene(conf);
        codeGenes[2].setAllele(new JVMInstruction(JavaOpcodes.IMUL));
        chromosome.setGenes(codeGenes);

        InstructionSequence sequence = new InstructionSequence(chromosome);

        Assert.assertEquals(3, sequence.size());
        Assert.assertEquals(Opcodes.ILOAD, sequence.get(0).getOpcode());
        Assert.assertEquals(new LocalVariableSlot(2), ((JVMInstruction) sequence.get(0)).getParameter(0));
        Assert.assertEquals(Opcodes.ILOAD, sequence.get(1).getOpcode());
        Assert.assertEquals(new LocalVariableSlot(2), ((JVMInstruction) sequence.get(1)).getParameter(0));
        Assert.assertNull(((JVMInstruction) sequence.get(2)).getParameter(0));
        Assert.assertEquals(Opcodes.IMUL, sequence.get(2).getOpcode());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateFromNotCodeChromosome() throws UnsupportedRepresentationException, InvalidConfigurationException {
        Chromosome chromosome = new Chromosome(conf);

        Gene codeGenes[] = new Gene[3];
        codeGenes[0] = new CodeGene(conf);
        codeGenes[0].setAllele(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));

        codeGenes[1] = new StringGene(conf);

        codeGenes[2] = new CodeGene(conf);
        codeGenes[2].setAllele(new JVMInstruction(JavaOpcodes.IMUL));
        chromosome.setGenes(codeGenes);

        InstructionSequence sequence = new InstructionSequence(chromosome);
        Assert.assertNull(sequence);
    }

    @Test
    public void testToChromosome() throws InvalidConfigurationException {
        JVMInstruction[] instrs = {new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(1)), new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(5))};
        InstructionSequence sequence = new InstructionSequence(instrs);
        IChromosome chromosome = sequence.toChromosome(conf);

        Assert.assertEquals(2, chromosome.getGenes().length);
        if (!((chromosome.getGene(0) instanceof CodeGene) && (chromosome.getGene(1) instanceof CodeGene))) {
            throw new ClassCastException("Chromosome generated from instruction sequence should contain only CodeGenes");
        }

        Assert.assertEquals(Opcodes.ILOAD, ((CodeGene) chromosome.getGene(0)).getInstruction().getOpcode());
        Assert.assertEquals(new LocalVariableSlot(1), ((JVMInstruction) ((CodeGene) chromosome.getGene(0)).getInstruction()).getParameter(0));
        Assert.assertEquals(Opcodes.ISTORE, ((CodeGene) chromosome.getGene(1)).getInstruction().getOpcode());
        Assert.assertEquals(new LocalVariableSlot(5), ((JVMInstruction) ((CodeGene) chromosome.getGene(1)).getInstruction()).getParameter(0));
    }

    @Test
    public void testEquals() {
        InstructionSequence first = new InstructionSequence();
        first.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        first.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        first.add(new JVMInstruction(JavaOpcodes.ISHL));
        first.add(new JVMInstruction(JavaOpcodes.DUP));

        InstructionSequence second = new InstructionSequence();
        second.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        second.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        second.add(new JVMInstruction(JavaOpcodes.ISHL));
        second.add(new JVMInstruction(JavaOpcodes.DUP));

        Assert.assertEquals(first, second);
    }

}
