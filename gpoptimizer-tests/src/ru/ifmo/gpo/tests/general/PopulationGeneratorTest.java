package ru.ifmo.gpo.tests.general;

import org.jgap.Configuration;
import org.jgap.IChromosome;
import org.jgap.InvalidConfigurationException;
import org.jgap.UnsupportedRepresentationException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.ifmo.gpo.core.GPConfiguration;
import ru.ifmo.gpo.core.MainIslandGPConfiguration;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.population.IPopulationGenerator;
import ru.ifmo.gpo.core.population.PopulationGenerationFailedException;
import ru.ifmo.gpo.core.population.generators.RandomPopulationGenerator;
import ru.ifmo.gpo.core.vmstate.IInstructionSequenceAnalyzer;
import ru.ifmo.gpo.java.JavaTargetArchitecture;
import ru.ifmo.gpo.java.instructions.JVMInstruction;
import ru.ifmo.gpo.java.instructions.JavaOpcodes;
import ru.ifmo.gpo.java.vmstate.LocalVariableSlot;
import ru.ifmo.gpo.shader.ps_1_1.PixelShaderTargetArchitecture;
import ru.ifmo.gpo.shader.ps_1_1.parser.ShaderParseException;
import ru.ifmo.gpo.shader.ps_1_1.parser.ShaderParser;
import ru.ifmo.gpo.x86.state.x86ConstantOperand;
import ru.ifmo.gpo.x86.state.x86Register;
import ru.ifmo.gpo.x86.x86Instruction;
import ru.ifmo.gpo.x86.x86Opcodes;
import ru.ifmo.gpo.x86.x86TargetArchitecture;

import java.util.Arrays;

/**
 * User: jedi-philosopher
 * Date: 27.01.2010
 * Time: 17:56:05
 */
public class PopulationGeneratorTest {
    static GPConfiguration conf;

    @Before
    public void setConf() throws UnsupportedRepresentationException, InvalidConfigurationException {
        Configuration.reset(MainIslandGPConfiguration.NAME);
        conf = new MainIslandGPConfiguration(new JavaTargetArchitecture());
    }

    @After
    public void resetConf() {
        conf = null;
    }

    private void verifyPopulation(IChromosome[] population) {
        Assert.assertNotNull(population);
        Assert.assertEquals(conf.getPopulationSize(), population.length);

        IInstructionSequenceAnalyzer initialChromosomeAnalyzer = conf.getTargetArchitecture().analyzeCode(new InstructionSequence(conf.getSampleChromosome()), conf.getStateSchema());

        for (IChromosome candidate : population) {
            IInstructionSequenceAnalyzer analyzer = conf.getTargetArchitecture().analyzeCode(new InstructionSequence(candidate), conf.getStateSchema());
            // checking that stack state after generated chromosomes is the same as after reference one
            try {
                Assert.assertTrue(analyzer.checkStateEqual(initialChromosomeAnalyzer));
            } catch (AssertionError e) {
                conf.getTargetArchitecture().getStateDelta(initialChromosomeAnalyzer.getStateAfter(), analyzer.getStateAfter());
                System.out.println("Assertion failed for candidate " + Arrays.toString(candidate.getGenes()));
                throw e;
            }
        }
    }

    @Test
    public void testEvaluation() throws InvalidConfigurationException, UnsupportedRepresentationException, PopulationGenerationFailedException {

        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.IMUL));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.IMUL));

        conf.setReferenceChromosome(sequence);
        conf.setUpConfiguration();

        IPopulationGenerator gen = new RandomPopulationGenerator(conf);

        verifyPopulation(gen.generatePopulation());
    }

    @Test
    public void testEvaluationWithRefCode() throws InvalidConfigurationException, UnsupportedRepresentationException, PopulationGenerationFailedException {
        InstructionSequence ref = new InstructionSequence();
        ref.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        ref.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        ref.add(new JVMInstruction(JavaOpcodes.IMUL));
        ref.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        ref.add(new JVMInstruction(JavaOpcodes.IMUL));
        ref.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        ref.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        ref.add(new JVMInstruction(JavaOpcodes.IMUL));
        ref.add(new JVMInstruction(JavaOpcodes.IADD));
        ref.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        ref.add(new JVMInstruction(JavaOpcodes.IADD));

        conf.setReferenceChromosome(ref);
        conf.setUpConfiguration();

        IPopulationGenerator gen = new RandomPopulationGenerator(conf);

        verifyPopulation(gen.generatePopulation());
    }

    @Test
    public void testGenerationMultipleTypes() throws InvalidConfigurationException, PopulationGenerationFailedException {
        InstructionSequence ref = new InstructionSequence();
        ref.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        ref.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(1)));
        ref.add(new JVMInstruction(JavaOpcodes.FCONST_2));
        ref.add(new JVMInstruction(JavaOpcodes.FDIV));
        ref.add(new JVMInstruction(JavaOpcodes.F2I));
        ref.add(new JVMInstruction(JavaOpcodes.IADD));
        ref.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(0)));

        conf.setReferenceChromosome(ref);
        conf.setUpConfiguration();

        IPopulationGenerator gen = new RandomPopulationGenerator(conf);
        verifyPopulation(gen.generatePopulation());
    }

    @Test
    public void testReference2() throws InvalidConfigurationException, PopulationGenerationFailedException {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        sequence.add(new JVMInstruction(JavaOpcodes.ISHR));
        sequence.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(3)));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(3)));
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        sequence.add(new JVMInstruction(JavaOpcodes.ISHR));
        sequence.add(new JVMInstruction(JavaOpcodes.IXOR));
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        sequence.add(new JVMInstruction(JavaOpcodes.IAND));

        conf.setReferenceChromosome(sequence);
        conf.setUpConfiguration();

        IPopulationGenerator gen = new RandomPopulationGenerator(conf);
        verifyPopulation(gen.generatePopulation());
    }

    @Test
    public void testFloat() throws InvalidConfigurationException, PopulationGenerationFailedException {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.FMUL));
        sequence.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.FMUL));

        conf.setReferenceChromosome(sequence);
        conf.setUpConfiguration();

        IPopulationGenerator gen = new RandomPopulationGenerator(conf);

        verifyPopulation(gen.generatePopulation());
    }

    @Test
    public void testInfiniteGeneration() throws InvalidConfigurationException, PopulationGenerationFailedException {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.DUP));
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_2));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(3)));

        conf.setReferenceChromosome(sequence);
        conf.setUpConfiguration();

        IPopulationGenerator gen = new RandomPopulationGenerator(conf);

        verifyPopulation(gen.generatePopulation());
    }

    @Test
    public void testX86() throws InvalidConfigurationException, PopulationGenerationFailedException {
        conf.setTargetArchitecture(new x86TargetArchitecture());
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new x86Instruction(x86Opcodes.XOR, new x86Register(x86Opcodes.eax), new x86ConstantOperand(-1)));
        sequence.add(new x86Instruction(x86Opcodes.AND, new x86Register(x86Opcodes.eax), new x86Register(x86Opcodes.ebx)));
        conf.setReferenceChromosome(sequence);
        conf.setUpConfiguration();

        IPopulationGenerator gen = new RandomPopulationGenerator(conf);
        verifyPopulation(gen.generatePopulation());
    }

    @Test
    public void testPixelShder() throws InvalidConfigurationException, PopulationGenerationFailedException, ShaderParseException {
        conf.setTargetArchitecture(new PixelShaderTargetArchitecture());
        InstructionSequence cand = new ShaderParser().parse("ps_2_0\n" +
                "def c0, 1.0, 1.0, 1.0, 1.0\n" +
                "def c1, 2.0, 2.0, 2.0, 2.0\n" +
                "add r0, c0, c0\n" +
                "mov oC0, r0"
        );
        conf.setReferenceChromosome(cand);
        conf.setUpConfiguration();

        IPopulationGenerator gen = new RandomPopulationGenerator(conf);
        verifyPopulation(gen.generatePopulation());
    }

}
