package ru.ifmo.gpo.tests.general;

import org.jgap.Configuration;
import org.jgap.IChromosome;
import org.jgap.InvalidConfigurationException;
import org.jgap.UnsupportedRepresentationException;
import org.jgap.impl.StockRandomGenerator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ru.ifmo.gpo.core.GPConfiguration;
import ru.ifmo.gpo.core.MainIslandGPConfiguration;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.operators.CodeRangeMutationOperator;
import ru.ifmo.gpo.core.vmstate.IInstructionSequenceAnalyzer;
import ru.ifmo.gpo.java.JavaTargetArchitecture;
import ru.ifmo.gpo.java.instructions.JVMInstruction;
import ru.ifmo.gpo.java.instructions.JavaOpcodes;
import ru.ifmo.gpo.java.vmstate.JVMStateSchema;
import ru.ifmo.gpo.java.vmstate.LocalVariableSlot;
import ru.ifmo.gpo.x86.state.x86MemoryOperand;
import ru.ifmo.gpo.x86.state.x86Register;
import ru.ifmo.gpo.x86.x86Instruction;
import ru.ifmo.gpo.x86.x86Opcodes;
import ru.ifmo.gpo.x86.x86TargetArchitecture;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * User: jedi-philosopher
 * Date: 31.10.2010
 * Time: 19:19:07
 */
public class MutationOperatorTest {

    static GPConfiguration conf;

    @Before
    public void setConf() throws InvalidConfigurationException {
        Configuration.reset(MainIslandGPConfiguration.NAME);
        conf = new MainIslandGPConfiguration(new JavaTargetArchitecture());
    }

    @After
    public void resetConf() {
        conf = null;
    }

    public void compareChromosomes(IChromosome ref, IChromosome result) {
        IInstructionSequenceAnalyzer refAnalyzer = conf.getTargetArchitecture().analyzeCode(new InstructionSequence(ref), conf.getStateSchema());
        IInstructionSequenceAnalyzer resultAnalyzer = conf.getTargetArchitecture().analyzeCode(new InstructionSequence(result), conf.getStateSchema());

        try {
            assert refAnalyzer.checkStateEqual(resultAnalyzer);
        } catch (AssertionError err) {
            System.out.println("Ref:" + Arrays.toString(ref.getGenes()));
            System.out.println("Mutated:" + Arrays.toString(result.getGenes()));
            throw err;
        }
    }

    @Test
    public void testReplaceSingleIntArithmeticInstruction() throws InvalidConfigurationException, UnsupportedRepresentationException {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.IMUL));

        conf.setStateSchema(new JVMStateSchema(sequence));
        CodeRangeMutationOperator mutationOperator = new CodeRangeMutationOperator(conf, 100);
        List list = new LinkedList();
        IChromosome chromosome = sequence.toChromosome(conf);
        mutationOperator.doMutation(chromosome, 2, 1, list, new StockRandomGenerator());

        IChromosome result = (IChromosome) list.get(0);

        compareChromosomes(chromosome, result);
    }

    @Test
    public void testReplaceIntLoadInstruction() throws InvalidConfigurationException, UnsupportedRepresentationException {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.IMUL));

        conf.setStateSchema(new JVMStateSchema(sequence));

        IChromosome chromosome = sequence.toChromosome(conf);

        CodeRangeMutationOperator mutationOperator = new CodeRangeMutationOperator(conf, 100);
        List list = new LinkedList();
        do {
            mutationOperator.doMutation(chromosome, 1, 1, list, new StockRandomGenerator());
        } while (list.isEmpty());

        IChromosome result = (IChromosome) list.get(0);

        compareChromosomes(chromosome, result);

    }

    @Test
    public void testReplaceIntStoreInstruction() throws InvalidConfigurationException, UnsupportedRepresentationException {

        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.IMUL));
        sequence.add(new JVMInstruction(JavaOpcodes.DUP));
        sequence.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(0)));

        conf.setStateSchema(new JVMStateSchema(sequence));

        IChromosome chromosome = sequence.toChromosome(conf);
        CodeRangeMutationOperator mutationOperator = new CodeRangeMutationOperator(conf, 100);
        List list = new LinkedList();
        mutationOperator.doMutation(chromosome, 4, 1, list, new StockRandomGenerator());

        IChromosome result = (IChromosome) list.get(0);

        compareChromosomes(chromosome, result);

    }

    @Test
    public void testReplaceRangeOfIntArithmeticInstructions() throws InvalidConfigurationException, UnsupportedRepresentationException {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.DUP));
        sequence.add(new JVMInstruction(JavaOpcodes.IMUL));
        sequence.add(new JVMInstruction(JavaOpcodes.IADD));

        conf.setStateSchema(new JVMStateSchema(sequence));

        IChromosome chromosome = sequence.toChromosome(conf);

        CodeRangeMutationOperator mutationOperator = new CodeRangeMutationOperator(conf, 100);
        List list = new LinkedList();
        mutationOperator.doMutation(chromosome, 3, 2, list, new StockRandomGenerator());

        IChromosome result = (IChromosome) list.get(0);

        compareChromosomes(chromosome, result);
    }

    @Test
    public void testReplaceRangeComplex() throws InvalidConfigurationException, UnsupportedRepresentationException {

        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.IDIV));
        sequence.add(new JVMInstruction(JavaOpcodes.DUP));
        sequence.add(new JVMInstruction(JavaOpcodes.IADD));
        sequence.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_1));

        conf.setStateSchema(new JVMStateSchema(sequence));

        IChromosome chromosome = sequence.toChromosome(conf);

        CodeRangeMutationOperator mutationOperator = new CodeRangeMutationOperator(conf, 100);
        List list = new LinkedList();
        mutationOperator.doMutation(chromosome, 3, 2, list, new StockRandomGenerator());

        IChromosome result = (IChromosome) list.get(0);

        compareChromosomes(chromosome, result);
    }

    @Test
    public void testReplaceINEG() throws InvalidConfigurationException, UnsupportedRepresentationException {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_0));
        sequence.add(new JVMInstruction(JavaOpcodes.INEG));

        conf.setStateSchema(new JVMStateSchema(sequence));
        IChromosome chromosome = sequence.toChromosome(conf);

        CodeRangeMutationOperator mutationOperator = new CodeRangeMutationOperator(conf, 100);
        List list = new LinkedList();
        mutationOperator.doMutation(chromosome, 1, 1, list, new StockRandomGenerator());

        IChromosome result = (IChromosome) list.get(0);

        compareChromosomes(chromosome, result);
    }

    @Test
    public void testReplaceNOPRange() throws InvalidConfigurationException, UnsupportedRepresentationException {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.NOP));
        sequence.add(new JVMInstruction(JavaOpcodes.INEG));

        conf.setStateSchema(new JVMStateSchema(sequence));
        IChromosome chromosome = sequence.toChromosome(conf);

        CodeRangeMutationOperator mutationOperator = new CodeRangeMutationOperator(conf, 100);
        List list = new LinkedList();
        mutationOperator.doMutation(chromosome, 1, 1, list, new StockRandomGenerator());

        IChromosome result = (IChromosome) list.get(0);

        compareChromosomes(chromosome, result);
    }

    @Test
    public void testReplaceF2I() throws InvalidConfigurationException {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        sequence.add(new JVMInstruction(JavaOpcodes.FCONST_1));
        sequence.add(new JVMInstruction(JavaOpcodes.F2I));
        sequence.add(new JVMInstruction(JavaOpcodes.IADD));

        conf.setStateSchema(new JVMStateSchema(sequence));
        IChromosome chromosome = sequence.toChromosome(conf);

        CodeRangeMutationOperator mutationOperator = new CodeRangeMutationOperator(conf, 100);
        List list = new LinkedList();
        mutationOperator.doMutation(chromosome, 2, 1, list, new StockRandomGenerator());

        IChromosome result = (IChromosome) list.get(0);
        compareChromosomes(chromosome, result);
    }

    @Test
    public void testReplaceFloat() throws InvalidConfigurationException {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(1)));
        sequence.add(new JVMInstruction(JavaOpcodes.FADD));

        conf.setStateSchema(new JVMStateSchema(sequence));
        IChromosome chromosome = sequence.toChromosome(conf);

        CodeRangeMutationOperator mutationOperator = new CodeRangeMutationOperator(conf, 100);
        List list = new LinkedList();
        mutationOperator.doMutation(chromosome, 2, 1, list, new StockRandomGenerator());

        IChromosome result = (IChromosome) list.get(0);
        compareChromosomes(chromosome, result);
    }

    @Test
    public void testReplaceRangeWithConversionsAtEdges() throws InvalidConfigurationException {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.I2F));
        sequence.add(new JVMInstruction(JavaOpcodes.FCONST_2));
        sequence.add(new JVMInstruction(JavaOpcodes.FDIV));
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_2));
        sequence.add(new JVMInstruction(JavaOpcodes.I2F));
        sequence.add(new JVMInstruction(JavaOpcodes.FADD));
        sequence.add(new JVMInstruction(JavaOpcodes.F2I));
        sequence.add(new JVMInstruction(JavaOpcodes.IMUL));

        conf.setStateSchema(new JVMStateSchema(sequence));
        IChromosome chromosome = sequence.toChromosome(conf);

        CodeRangeMutationOperator mutationOperator = new CodeRangeMutationOperator(conf, 100);
        List list = new LinkedList();
        mutationOperator.doMutation(chromosome, 3, 5, list, new StockRandomGenerator());

        IChromosome result = (IChromosome) list.get(0);

        compareChromosomes(chromosome, result);
    }

    @Test
    public void testReplaceWithConversionAtEdge2() throws InvalidConfigurationException {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.FCONST_2));
        sequence.add(new JVMInstruction(JavaOpcodes.F2I));
        sequence.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.FCONST_1));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        sequence.add(new JVMInstruction(JavaOpcodes.IOR));
        sequence.add(new JVMInstruction(JavaOpcodes.I2F));
        sequence.add(new JVMInstruction(JavaOpcodes.FDIV));
        sequence.add(new JVMInstruction(JavaOpcodes.F2I));

        conf.setStateSchema(new JVMStateSchema(sequence));
        IChromosome chromosome = sequence.toChromosome(conf);

        CodeRangeMutationOperator mutationOperator = new CodeRangeMutationOperator(conf, 100);
        List list = new LinkedList();
        mutationOperator.doMutation(chromosome, 0, 7, list, new StockRandomGenerator());

        IChromosome result = (IChromosome) list.get(0);

        compareChromosomes(chromosome, result);

    }

    @Test
    public void testReplaceLongFloatRange() throws InvalidConfigurationException {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.FCONST_1));
        sequence.add(new JVMInstruction(JavaOpcodes.F2I));
        sequence.add(new JVMInstruction(JavaOpcodes.FCONST_2));
        sequence.add(new JVMInstruction(JavaOpcodes.NOP));
        sequence.add(new JVMInstruction(JavaOpcodes.FCONST_2));
        sequence.add(new JVMInstruction(JavaOpcodes.FDIV));
        sequence.add(new JVMInstruction(JavaOpcodes.F2I));
        sequence.add(new JVMInstruction(JavaOpcodes.ISHR));

        conf.setStateSchema(new JVMStateSchema(sequence));
        IChromosome chromosome = sequence.toChromosome(conf);

        CodeRangeMutationOperator mutationOperator = new CodeRangeMutationOperator(conf, 100);
        List list = new LinkedList();
        mutationOperator.doMutation(chromosome, 0, 6, list, new StockRandomGenerator());

        IChromosome result = (IChromosome) list.get(0);

        compareChromosomes(chromosome, result);
    }

    @Test
    public void testMutatex86() throws InvalidConfigurationException {
        conf.setTargetArchitecture(new x86TargetArchitecture());

        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.eax), new x86MemoryOperand("0xff", 4)));
        sequence.add(new x86Instruction(x86Opcodes.MOV, new x86Register(x86Opcodes.ebx), new x86Register(x86Opcodes.eax)));
        sequence.add(new x86Instruction(x86Opcodes.MUL, new x86Register(x86Opcodes.eax), new x86Register(x86Opcodes.ebx)));
        sequence.add(new x86Instruction(x86Opcodes.ADD, new x86Register(x86Opcodes.eax), new x86Register(x86Opcodes.ebx)));
        sequence.add(new x86Instruction(x86Opcodes.MUL, new x86Register(x86Opcodes.eax), new x86Register(x86Opcodes.ebx)));
        sequence.add(new x86Instruction(x86Opcodes.MOV, new x86MemoryOperand("0xf0", 4), new x86Register(x86Opcodes.eax)));

        IChromosome chromosome = sequence.toChromosome(conf);

        CodeRangeMutationOperator mutationOperator = new CodeRangeMutationOperator(conf, 100);
        List list = new LinkedList();
        mutationOperator.doMutation(chromosome, 0, 6, list, new StockRandomGenerator());

        IChromosome result = (IChromosome) list.get(0);

        compareChromosomes(chromosome, result);
    }
}
