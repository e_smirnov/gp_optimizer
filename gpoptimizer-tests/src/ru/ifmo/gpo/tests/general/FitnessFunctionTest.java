package ru.ifmo.gpo.tests.general;

import junit.framework.Assert;
import org.jgap.Configuration;
import org.jgap.InvalidConfigurationException;
import org.jgap.UnsupportedRepresentationException;
import org.junit.Test;
import ru.ifmo.gpo.core.GPConfiguration;
import ru.ifmo.gpo.core.MainIslandGPConfiguration;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.NumericConstant;
import ru.ifmo.gpo.java.JavaTargetArchitecture;
import ru.ifmo.gpo.java.instructions.JVMInstruction;
import ru.ifmo.gpo.java.instructions.JavaOpcodes;
import ru.ifmo.gpo.java.vmstate.LocalVariableSlot;
import ru.ifmo.gpo.shader.ps_1_1.PixelShaderTargetArchitecture;
import ru.ifmo.gpo.shader.ps_1_1.parser.ShaderParseException;
import ru.ifmo.gpo.shader.ps_1_1.parser.ShaderParser;

/**
 * User: jedi-philosopher
 * Date: 19.01.2010
 * Time: 18:57:29
 */
public class FitnessFunctionTest {


    @Test
    public void testEvaluation() throws InvalidConfigurationException, UnsupportedRepresentationException {

        Configuration.reset(MainIslandGPConfiguration.NAME);
        GPConfiguration conf = new MainIslandGPConfiguration(new JavaTargetArchitecture());

        InstructionSequence sample = new InstructionSequence();
        sample.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sample.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sample.add(new JVMInstruction(JavaOpcodes.IMUL));
        sample.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sample.add(new JVMInstruction(JavaOpcodes.IMUL));

        conf.setReferenceChromosome(sample);
        conf.setUpConfiguration();


        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.IMUL));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.IMUL));

        double val = conf.getFitnessFunc().getFitnessValue(sequence.toChromosome(conf));

        System.out.println("Fitness value: " + val);
    }

    @Test
    public void testEqualGuessWhy() throws InvalidConfigurationException {
        Configuration.reset(MainIslandGPConfiguration.NAME);
        GPConfiguration conf = new MainIslandGPConfiguration(new JavaTargetArchitecture());

        InstructionSequence sample = new InstructionSequence();
        sample.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        sample.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sample.add(new JVMInstruction(JavaOpcodes.BIPUSH, new NumericConstant(31)));
        sample.add(new JVMInstruction(JavaOpcodes.IAND));
        sample.add(new JVMInstruction(JavaOpcodes.ISHL));
        sample.add(new JVMInstruction(JavaOpcodes.IOR));

        conf.setReferenceChromosome(sample);
        conf.setUpConfiguration();


        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ISHL));
        sequence.add(new JVMInstruction(JavaOpcodes.IOR));

        double val = conf.getFitnessFunc().getFitnessValue(sequence.toChromosome(conf));
        Assert.assertTrue(val > 0.8);
    }

    @Test
    public void equalCodeHasFitness1_0() throws InvalidConfigurationException {
        Configuration.reset(MainIslandGPConfiguration.NAME);
        GPConfiguration conf = new MainIslandGPConfiguration(new JavaTargetArchitecture());

        InstructionSequence sample = new InstructionSequence();
        sample.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        sample.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sample.add(new JVMInstruction(JavaOpcodes.IAND));
        sample.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(2)));

        conf.setReferenceChromosome(sample);
        conf.setUpConfiguration();

        InstructionSequence sampleCopy = new InstructionSequence();
        sampleCopy.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        sampleCopy.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sampleCopy.add(new JVMInstruction(JavaOpcodes.IAND));
        sampleCopy.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(2)));

        double val = conf.getFitnessFunc().getFitnessValue(sampleCopy.toChromosome(conf));
        Assert.assertTrue(Math.abs(val - 1.0) < 0.0001);
    }

    @Test
    public void equalCodeHasFitness1_0_PS() throws InvalidConfigurationException, ShaderParseException {
        Configuration.reset(MainIslandGPConfiguration.NAME);
        GPConfiguration conf = new MainIslandGPConfiguration(new PixelShaderTargetArchitecture());

        ShaderParser p = new ShaderParser();

        InstructionSequence sample = p.parse("ps_2_0\n" +
                "def c0, 1.0, 1.0, 1.0, 1.0\n" +
                "def c1, 2.0, 2.0, 2.0, 2.0\n" +
                "mov r0, c0\n" +
                "mul r0, r0, c1\n" +
                "mov oC0, r0");


        conf.setReferenceChromosome(sample);
        conf.setUpConfiguration();

        InstructionSequence cand = p.parse("def c0, 1.0, 1.0, 1.0, 1.0\n" +
                "def c1, 2.0, 2.0, 2.0, 2.0\n" +
                "add r0, c0, c0\n" +
                "mov r0, r0\n" + // to make both shaders have same length
                "mov oC0, r0"
        );

        double val = conf.getFitnessFunc().getFitnessValue(cand.toChromosome(conf));
        Assert.assertTrue(Math.abs(val - 1.0) < 0.0001);
    }
}
