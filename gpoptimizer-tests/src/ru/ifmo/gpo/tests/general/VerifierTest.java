package ru.ifmo.gpo.tests.general;

import org.apache.log4j.BasicConfigurator;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.ifmo.gpo.core.ITargetArchitecture;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.NumericConstant;
import ru.ifmo.gpo.java.JavaTargetArchitecture;
import ru.ifmo.gpo.java.instructions.JVMInstruction;
import ru.ifmo.gpo.java.instructions.JavaOpcodes;
import ru.ifmo.gpo.java.vmstate.JVMStateSchema;
import ru.ifmo.gpo.java.vmstate.LocalVariableSlot;
import ru.ifmo.gpo.shader.ps_1_1.PixelShaderTargetArchitecture;
import ru.ifmo.gpo.shader.ps_1_1.emulator.ShaderEmulator;
import ru.ifmo.gpo.shader.ps_1_1.parser.ShaderParseException;
import ru.ifmo.gpo.shader.ps_1_1.parser.ShaderParser;
import ru.ifmo.gpo.verify.IChromosomeVerifier;
import ru.ifmo.gpo.verify.VerificationResult;
import ru.ifmo.gpo.verify.verifiers.SPEARVerifier;
import ru.ifmo.gpo.verify.verifiers.Z3Verifier;

/**
 * User: e_smirnov
 * Date: 24.11.2010
 * Time: 13:43:54
 */
public class VerifierTest {

    private ITargetArchitecture javaTarget = new JavaTargetArchitecture();

    @BeforeClass
    public static void beforeAll() {
        BasicConfigurator.configure();
    }

    public void testWithAllVerificationEngines(InstructionSequence first, InstructionSequence second, VerificationResult expected, ITargetArchitecture target) {
        IChromosomeVerifier spear = new SPEARVerifier(target);
        Assert.assertEquals(expected, spear.verifyEquivalence(first, second, target.buildStateSchema(first)));

        IChromosomeVerifier z3 = new Z3Verifier(target);
        Assert.assertEquals(expected, z3.verifyEquivalence(first, second, target.buildStateSchema(first)));
    }

    @Test
    public void testSingleLoad() {
        InstructionSequence first = new InstructionSequence();
        first.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));

        InstructionSequence second = new InstructionSequence();
        second.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        second.add(new JVMInstruction(JavaOpcodes.ICONST_0));
        second.add(new JVMInstruction(JavaOpcodes.POP));

        testWithAllVerificationEngines(first, second, VerificationResult.RESULT_SUCCESS, javaTarget);
    }

    @Test
    public void testShiftAndMultiplication() {
        InstructionSequence first = new InstructionSequence();
        first.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        first.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        first.add(new JVMInstruction(JavaOpcodes.IADD));

        InstructionSequence second = new InstructionSequence();
        second.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        second.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        second.add(new JVMInstruction(JavaOpcodes.ISHL));

        testWithAllVerificationEngines(first, second, VerificationResult.RESULT_SUCCESS, javaTarget);
    }

    @Test
    public void testIINC() {
        InstructionSequence first = new InstructionSequence();
        first.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        first.add(new JVMInstruction(JavaOpcodes.ICONST_2));
        first.add(new JVMInstruction(JavaOpcodes.IADD));
        first.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(0)));

        InstructionSequence second = new InstructionSequence();
        second.add(new JVMInstruction(JavaOpcodes.IINC, new LocalVariableSlot(0), new NumericConstant(2)));

        testWithAllVerificationEngines(first, second, VerificationResult.RESULT_SUCCESS, javaTarget);
    }

    @Test
    public void testReferenceArithmetics() {
        InstructionSequence first = new InstructionSequence();
        first.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        first.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        first.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        first.add(new JVMInstruction(JavaOpcodes.IMUL));
        first.add(new JVMInstruction(JavaOpcodes.IMUL));
        first.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        first.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        first.add(new JVMInstruction(JavaOpcodes.IMUL));
        first.add(new JVMInstruction(JavaOpcodes.IADD));
        first.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        first.add(new JVMInstruction(JavaOpcodes.IADD));

        InstructionSequence second = new InstructionSequence();

        second.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        second.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        second.add(new JVMInstruction(JavaOpcodes.IADD));
        second.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        second.add(new JVMInstruction(JavaOpcodes.IMUL));
        second.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        second.add(new JVMInstruction(JavaOpcodes.IXOR));
        second.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        second.add(new JVMInstruction(JavaOpcodes.IMUL));

        testWithAllVerificationEngines(first, second, VerificationResult.RESULT_SUCCESS, javaTarget);
    }

    @Test
    public void testFailedVerification() {
        InstructionSequence first = new InstructionSequence();
        first.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        first.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        first.add(new JVMInstruction(JavaOpcodes.IADD));

        InstructionSequence second = new InstructionSequence();
        second.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        second.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        second.add(new JVMInstruction(JavaOpcodes.ISUB));

        testWithAllVerificationEngines(first, second, VerificationResult.RESULT_FAIL, javaTarget);
    }

    @Test
    public void testWithUnderflowAndAndWithSelf() {
        InstructionSequence first = new InstructionSequence();
        first.add(new JVMInstruction(JavaOpcodes.ICONST_M1));
        first.add(new JVMInstruction(JavaOpcodes.IXOR));
        first.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(1)));
        first.add(new JVMInstruction(JavaOpcodes.IAND));

        InstructionSequence second = new InstructionSequence();
        second.add(new JVMInstruction(JavaOpcodes.ICONST_M1));
        second.add(new JVMInstruction(JavaOpcodes.IXOR));
        second.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(1)));
        second.add(new JVMInstruction(JavaOpcodes.IAND));
        second.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(1)));
        second.add(new JVMInstruction(JavaOpcodes.IAND));

        IChromosomeVerifier verifier = new Z3Verifier(javaTarget);// SPEAR engine will fail this test because it does not allow negative constants
        Assert.assertEquals(VerificationResult.RESULT_SUCCESS, verifier.verifyEquivalence(first, second, new JVMStateSchema(first)));
    }

    @Test
    public void testFloatExpression() {
        InstructionSequence first = new InstructionSequence();
        first.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(0)));
        first.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(0)));
        first.add(new JVMInstruction(JavaOpcodes.FADD));

        InstructionSequence second = new InstructionSequence();
        second.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(0)));
        second.add(new JVMInstruction(JavaOpcodes.FCONST_2));
        second.add(new JVMInstruction(JavaOpcodes.FMUL));

        IChromosomeVerifier verifier = new Z3Verifier(javaTarget);// SPEAR engine will fail this test because it does not support float arithmetics
        Assert.assertEquals(VerificationResult.RESULT_SUCCESS, verifier.verifyEquivalence(first, second, new JVMStateSchema(first)));
    }

    @Test
    public void testFloatExpressionFail() {
        InstructionSequence first = new InstructionSequence();
        first.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(0)));
        first.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(1)));
        first.add(new JVMInstruction(JavaOpcodes.FDIV));

        InstructionSequence second = new InstructionSequence();
        second.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(1)));
        second.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(0)));
        second.add(new JVMInstruction(JavaOpcodes.FDIV));

        IChromosomeVerifier verifier = new Z3Verifier(javaTarget);// SPEAR engine will fail this test because it does not support float arithmetics
        Assert.assertEquals(VerificationResult.RESULT_FAIL, verifier.verifyEquivalence(first, second, new JVMStateSchema(first)));
    }


    @Test
    public void testShaderVerifyFail() throws ShaderParseException {

        ITargetArchitecture psTarget = new PixelShaderTargetArchitecture();

        ShaderParser sp = new ShaderParser();
        InstructionSequence first = sp.parse("ps_2_0\n" +
                "dcl t0.xyzw\n" +
                "dcl t1.xyzw\n" +
                "dcl t2.xyzw\n" +
                "dcl t3.xyzw\n" +
                "dcl t4.xyzw\n" +
                "dcl t5.xyzw\n" +
                "dcl t6.xyzw\n" +
                "dcl t7.xyzw\n" +
                "dcl_2d s0\n" +
                "texld r0, t0, s0\n" +
                "texld r1, t1, s0\n" +
                "mul   r0, r0, c0\n" +
                "mad   r0, r1, c1, r0\n" +
                "texld r1, t2, s0\n" +
                "texld r2, t3, s0\n" +
                "mad   r0, r1, c2, r0\n" +
                "mad   r0, r2, c3, r0\n" +
                "texld r1, t4, s0\n" +
                "texld r2, t5, s0\n" +
                "mad   r0, r1, c4, r0\n" +
                "mad   r0, r2, c5, r0\n" +
                "texld r1, t6, s0\n" +
                "texld r2, t7, s0\n" +
                "mad   r0, r1, c6, r0\n" +
                "mad   r0, r2, c7, r0\n" +
                "mov oC0, r0");
        InstructionSequence second = sp.parse(
                "texld r0, t1, s0\n" +
                        "texld r1, t1, s0\n" +
                        "mul r0, r0, c0\n" +
                        "mad r0, r1, c1, r0\n" +
                        "texld r2, t3, s0\n" +
                        "mad r0, r1, c2, r0\n" +
                        "mad r0, r2, c3, r0\n" +
                        "mad r0, r1, c4, r0\n" +
                        "mad r0, r2, c5, r0\n" +
                        "texld r2, t7, s0\n" +
                        "mad r0, r1, c6, r0\n" +
                        "mad r0, r2, c7, r0\n" +
                        "mov oC0, r0");

        IChromosomeVerifier verifier = new Z3Verifier(psTarget);
        VerificationResult vr = verifier.verifyEquivalence(first, second, ShaderEmulator.buildSchema(first));

        Assert.assertEquals(VerificationResult.RESULT_FAIL, vr);
    }
}
