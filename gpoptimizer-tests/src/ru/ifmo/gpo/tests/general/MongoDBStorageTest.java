package ru.ifmo.gpo.tests.general;

import org.junit.*;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.java.instructions.JVMInstruction;
import ru.ifmo.gpo.java.instructions.JavaOpcodes;
import ru.ifmo.gpo.java.vmstate.LocalVariableSlot;
import ru.ifmo.gpo.server.IStorageManager;
import ru.ifmo.gpo.server.storage.MongoDBStorageManager;

/**
 * User: e_smirnov
 * Date: 11.09.2010
 * Time: 17:33:33
 */
public class MongoDBStorageTest {
    IStorageManager storage;

    @Before
    public void setUp() {
        storage = new MongoDBStorageManager();
        storage.init("test_db");
    }

    @After
    public void tearDown() {
        storage.clear();
    }

    @Test(timeout = 2000)
    @Ignore("MongoDB tests disabled as they require running mongo demon process")
    public void testInsert() {
        InstructionSequence source = new InstructionSequence();
        source.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));

        InstructionSequence target = new InstructionSequence();
        target.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(0)));

        storage.addOptimizationRecord(source, target);

        InstructionSequence targetFromDb = storage.getOptimized(source);

        Assert.assertNotNull(targetFromDb);
        Assert.assertEquals(target, targetFromDb);
    }

    @Test(timeout = 2000)
    @Ignore("MongoDB tests disabled as they require running mongo demon process")
    public void testClear() {
        InstructionSequence source = new InstructionSequence();
        source.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));

        InstructionSequence target = new InstructionSequence();
        target.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(0)));

        storage.addOptimizationRecord(source, target);
        Assert.assertNotNull(storage.getOptimized(source));
        storage.clear();
        Assert.assertNull(storage.getOptimized(source));
    }

    @Test(timeout = 2000)
    @Ignore("MongoDB tests disabled as they require running mongo demon process")
    public void testInsertFailed() {
        InstructionSequence source = new InstructionSequence();
        source.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));

        storage.addFailedOptimizationRecord(source);
        Assert.assertNull(storage.getOptimized(source));
    }

    @Test(timeout = 2000)
    @Ignore("MongoDB tests disabled as they require running mongo demon process")
    public void testFailedOptimizationDoesNotRewriteResult() {
        InstructionSequence source = new InstructionSequence();
        source.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));

        InstructionSequence target = new InstructionSequence();
        target.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(0)));

        storage.addOptimizationRecord(source, target);
        storage.addFailedOptimizationRecord(source);

        InstructionSequence targetFromDb = storage.getOptimized(source);

        Assert.assertNotNull(targetFromDb);
        Assert.assertEquals(target, targetFromDb);
    }
}
