package ru.ifmo.gpo.tests.shader;

import org.junit.Assert;
import org.junit.Test;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.shader.ps_1_1.emulator.ShaderEmulator;
import ru.ifmo.gpo.shader.ps_1_1.parser.ShaderParseException;
import ru.ifmo.gpo.shader.ps_1_1.parser.ShaderParser;
import ru.ifmo.gpo.shader.ps_1_1.state.PSInstructionSequenceAnalyzer;
import ru.ifmo.gpo.shader.ps_1_1.state.PSRegister;
import ru.ifmo.gpo.shader.ps_1_1.state.PSStateDelta;
import ru.ifmo.gpo.shader.ps_1_1.state.PSStateSchema;

/**
 * User: jedi-philosopher
 * Date: 15.01.12
 * Time: 19:13
 */
public class PSAnalyzerTest {

    private ShaderParser p = new ShaderParser();

    @Test
    public void testDeltaTwoWrites() throws ShaderParseException {

        InstructionSequence seq = p.parse("texld r1, t0, s0\nmov r0,t0");
        PSInstructionSequenceAnalyzer analyzer = new PSInstructionSequenceAnalyzer(seq, ShaderEmulator.buildSchema(seq));
        PSStateDelta delta = new PSStateDelta(analyzer.getStateBefore(), analyzer.getStateAfter());

        Assert.assertEquals(8, delta.getDelta());
        Assert.assertEquals(2, delta.getFullRegisters().size());
    }

    @Test
    public void testDeltaBug() throws ShaderParseException {
        InstructionSequence seq = p.parse("add t2, t1, c1\n" +
                "mov t0, t3\n" +
                "mad t2.xyz, c2, t1, -r0\n" +
                "mad t2, -t1, t0, c0");
        PSInstructionSequenceAnalyzer analyzer = new PSInstructionSequenceAnalyzer(seq, ShaderEmulator.buildSchema(seq));
        PSStateDelta delta = new PSStateDelta(analyzer.getStateBefore(), analyzer.getStateAfter());

        Assert.assertEquals(8, delta.getDelta());
        Assert.assertEquals(2, delta.getFullRegisters().size());
    }

    @Test
    public void testIsWritableAfter() throws ShaderParseException {
        InstructionSequence seq = p.parse("texld r0, t0, s0");
        PSInstructionSequenceAnalyzer analyzer = new PSInstructionSequenceAnalyzer(seq, ShaderEmulator.buildSchema(seq));

        Assert.assertTrue(analyzer.getStateAfter().getTempRegisters().get("r0").isWriteable());
        Assert.assertTrue(analyzer.getStateAfter().getTempRegisters().get("r0").isReadable());
    }

    @Test
    public void testIsWritableAfter2() throws ShaderParseException {
        InstructionSequence seq = p.parse("add r0,t0,c0");
        PSInstructionSequenceAnalyzer analyzer = new PSInstructionSequenceAnalyzer(seq, ShaderEmulator.buildSchema(seq));

        Assert.assertTrue(analyzer.getStateAfter().getTextureRegisters().get("t0").isReadable());
        Assert.assertFalse(analyzer.getStateAfter().getTextureRegisters().get("t0").isWriteable());

        Assert.assertTrue(analyzer.getStateAfter().getTempRegisters().get("r0").isReadable());
        Assert.assertTrue(analyzer.getStateAfter().getTempRegisters().get("r0").isWriteable());

        Assert.assertTrue(analyzer.getStateAfter().getConstantRegisters().get("c0").isReadable());
        Assert.assertFalse(analyzer.getStateAfter().getConstantRegisters().get("c0").isWriteable());
    }

    @Test
    public void testConstantRegAccessType() throws ShaderParseException {
        InstructionSequence seq = p.parse("def c0,1,1,1,1\nmov r0,c0");
        PSStateSchema schema = ShaderEmulator.buildSchema(seq);
        PSInstructionSequenceAnalyzer analyzer = new PSInstructionSequenceAnalyzer(seq, schema);

        Assert.assertEquals(GenericOperand.AccessType.READ, analyzer.getStateAfter().getConstantRegisters().get("c0").getAccessType());
    }

    @Test
    public void testCalculateTexOpSeqLen() throws ShaderParseException {
        InstructionSequence seq = p.parse("sub_sat r1.xy, c7, t3\n" +
                "texld r1, t1, s0\n" +
                "mul r0, r0, c0\n" +
                "mad r0, r1, c1, r0\n" +
                "texld r1, t2, s0\n" +
                "texld r2, t3, s0\n" +
                "mad r0, r1, c2, r0\n" +
                "mad r0, r2, c3, r0\n" +
                "texld r1, t4, s0\n" +
                "texld r2, t5, s0\n" +
                "mad r0, r1, c4, r0\n" +
                "mad r0, r2, c5, r0\n" +
                "texld r1, t6, s0");
        PSInstructionSequenceAnalyzer analyzer = new PSInstructionSequenceAnalyzer(seq, ShaderEmulator.buildSchema(seq));
        Assert.assertEquals(4, analyzer.getStateAfter().getValue(new PSRegister("r1")).getDependentTexOpRead());
    }
}
