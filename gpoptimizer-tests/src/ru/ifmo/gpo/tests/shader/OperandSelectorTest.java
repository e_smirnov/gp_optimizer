package ru.ifmo.gpo.tests.shader;

import junitx.framework.Assert;
import org.junit.Test;
import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.vmstate.NoSuitableOperandFoundException;
import ru.ifmo.gpo.shader.ps_1_1.emulator.ShaderEmulator;
import ru.ifmo.gpo.shader.ps_1_1.instructions.PSOpcodes;
import ru.ifmo.gpo.shader.ps_1_1.instructions.PixelShaderInstruction;
import ru.ifmo.gpo.shader.ps_1_1.parser.ShaderParseException;
import ru.ifmo.gpo.shader.ps_1_1.parser.ShaderParser;
import ru.ifmo.gpo.shader.ps_1_1.state.*;

import java.util.List;

/**
 * User: jedi-philosopher
 * Date: 27.12.11
 * Time: 23:13
 */
public class OperandSelectorTest {

    @Test
    public void testSelectSimple() throws NoSuitableOperandFoundException {
        PSStateSchema schema = new PSStateSchema();
        schema.addOrUpdateSlot(new PSRegister(RegisterType.CONSTANT, 0), Type.DOUBLE_TYPE, GenericOperand.AccessType.ANY);
        schema.addOrUpdateSlot(new PSRegister(RegisterType.TEMP, 0), Type.DOUBLE_TYPE, GenericOperand.AccessType.ANY);
        PSState state = new PSState(schema);
        PSOpcodes.MOV.getOperandSelector().selectOperandsRandom(PSOpcodes.MOV, state);
    }


    @Test(expected = NoSuitableOperandFoundException.class)
    public void testCantSelectDestNonWritable() throws NoSuitableOperandFoundException {
        PSStateSchema schema = new PSStateSchema();
        schema.addOrUpdateSlot(new PSRegister(RegisterType.COLOR, 0), Type.DOUBLE_TYPE, GenericOperand.AccessType.READ);
        schema.addOrUpdateSlot(new PSRegister(RegisterType.COLOR, 1), Type.DOUBLE_TYPE, GenericOperand.AccessType.READ);
        PSState state = new PSState(schema);
        PSOpcodes.MOV.getOperandSelector().selectOperandsRandom(PSOpcodes.MOV, state);
    }


    @Test
    public void testSelectWithAnalyzer() throws ShaderParseException, NoSuitableOperandFoundException {
        ShaderParser p = new ShaderParser();
        InstructionSequence seq = p.parse("def c0,1,1,1,1\ntexld r0, t0, s0\n");
        final PSStateSchema schema = ShaderEmulator.buildSchema(seq);
        PSInstructionSequenceAnalyzer analyzer = new PSInstructionSequenceAnalyzer(seq, schema);
        List<GenericOperand> rz = PSOpcodes.ADD.getOperandSelector().selectOperandsRandom(PSOpcodes.ADD, analyzer.getStateAfter());
        Assert.assertEquals(3, rz.size());
    }

    @Test(expected = NoSuitableOperandFoundException.class)
    public void canNotSelectFromUninitializedConst() throws ShaderParseException, NoSuitableOperandFoundException {
        ShaderParser p = new ShaderParser();
        InstructionSequence ref = p.parse("def c0,5,6,7,8\n");
        PSStateSchema schema = ShaderEmulator.buildSchema(ref);

        PSInstructionSequenceAnalyzer analyzer = new PSInstructionSequenceAnalyzer(ref, schema);

        PSOpcodes.ADD.getOperandSelector().selectOperandsRandom(PSOpcodes.ADD, analyzer.getStateAfter());
    }

    @Test(expected = NoSuitableOperandFoundException.class)
    public void canNotSelectoC0AsDestForNonMOVInstruction() throws NoSuitableOperandFoundException {

        PSStateSchema schema = new PSStateSchema();
        schema.addOrUpdateSlot(new PSRegister(RegisterType.CONSTANT, 0), Type.DOUBLE_TYPE, GenericOperand.AccessType.READ);
        schema.addOrUpdateSlot(new PSRegister(RegisterType.TEMP, 0), Type.DOUBLE_TYPE, GenericOperand.AccessType.READ);
        schema.addOrUpdateSlot(new PSRegister(RegisterType.OUTPUT_COLOR, 0), Type.DOUBLE_TYPE, GenericOperand.AccessType.WRITE);
        PSState state = new PSState(schema);
        PSOpcodes.ADD.getOperandSelector().selectOperandsRandom(PSOpcodes.ADD, state);
    }

    @Test(expected = NoSuitableOperandFoundException.class)
    public void testCanNotSelectForTEXLDbecauseOfDependentSequenceLen() throws NoSuitableOperandFoundException {
        PSStateSchema schema = new PSStateSchema();
        PSRegister r0 = new PSRegister(RegisterType.TEMP, 0);
        PSRegister t0 = new PSRegister(RegisterType.TEXTURE, 0);
        PSRegister s0 = new PSRegister(RegisterType.SAMPLER, 0);

        schema.addOrUpdateSlot(r0, Type.DOUBLE_TYPE, GenericOperand.AccessType.ANY);
        schema.addOrUpdateSlot(t0, Type.DOUBLE_TYPE, GenericOperand.AccessType.READ);
        schema.addOrUpdateSlot(s0, Type.DOUBLE_TYPE, GenericOperand.AccessType.READ);
        PSState state = new PSState(schema);

        state = ShaderEmulator.emulateInstruction(state, new PixelShaderInstruction(PSOpcodes.MOV, r0, t0));
        state = ShaderEmulator.emulateInstruction(state, new PixelShaderInstruction(PSOpcodes.TEXLD, r0, t0, s0));
        state = ShaderEmulator.emulateInstruction(state, new PixelShaderInstruction(PSOpcodes.TEXLD, r0, t0, s0));
        state = ShaderEmulator.emulateInstruction(state, new PixelShaderInstruction(PSOpcodes.TEXLD, r0, t0, s0));

        PSOpcodes.TEXLD.getOperandSelector().selectOperandsRandom(PSOpcodes.TEXLD, state);
    }
}
