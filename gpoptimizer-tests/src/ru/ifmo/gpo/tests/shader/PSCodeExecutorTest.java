package ru.ifmo.gpo.tests.shader;

import org.junit.Assert;
import org.junit.Test;
import ru.ifmo.gpo.core.ICodeExecutionEnvironment;
import ru.ifmo.gpo.core.ICodeGenerationResult;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.instructions.generic.NumericConstant;
import ru.ifmo.gpo.shader.ps_1_1.PixelShaderCodeExecutionEnvironment;
import ru.ifmo.gpo.shader.ps_1_1.PixelShaderCodeExecutor;
import ru.ifmo.gpo.shader.ps_1_1.PixelShaderCodeGenerator;
import ru.ifmo.gpo.shader.ps_1_1.PixelShaderExecutionResult;
import ru.ifmo.gpo.shader.ps_1_1.emulator.ShaderEmulator;
import ru.ifmo.gpo.shader.ps_1_1.instructions.PSOpcodes;
import ru.ifmo.gpo.shader.ps_1_1.instructions.PixelShaderInstruction;
import ru.ifmo.gpo.shader.ps_1_1.state.DestinationWriteMask;
import ru.ifmo.gpo.shader.ps_1_1.state.PSRegister;
import ru.ifmo.gpo.shader.ps_1_1.state.PSStateSchema;
import ru.ifmo.gpo.shader.ps_1_1.state.RegisterType;

/**
 * User: e_smirnov
 * Date: 10.08.2011
 * Time: 17:47:43
 */
public class PSCodeExecutorTest {

    private PixelShaderCodeGenerator codegen = new PixelShaderCodeGenerator();

    @Test
    public void testProfileSimple() {
        InstructionSequence seq = new InstructionSequence();
        seq.add(new PixelShaderInstruction(PSOpcodes.DEF, new PSRegister(RegisterType.CONSTANT, 0), new NumericConstant(0), new NumericConstant(1), new NumericConstant(2), new NumericConstant(3)));
        seq.add(new PixelShaderInstruction(PSOpcodes.MOV, new PSRegister(RegisterType.COLOR.OUTPUT_COLOR, 0), new PSRegister(RegisterType.CONSTANT, 0)));
        ICodeGenerationResult rez = codegen.generateProfileCode(seq, new PSStateSchema(), null); // schema does not matter in profiling, as code is not actually executed
        PixelShaderCodeExecutor executor = new PixelShaderCodeExecutor();
        long rz = executor.profileCode(rez, new PixelShaderCodeExecutionEnvironment(new PSStateSchema()));

        Assert.assertTrue(rz > 0);
    }

    @Test
    public void testExecutionResultZeroDifference() {
        InstructionSequence seq = new InstructionSequence();
        // seq.add(new PixelShaderInstruction(PSOpcodes.TEX, new PSRegister(RegisterType.TEXTURE, 1)));
        // seq.add(new PixelShaderInstruction(PSOpcodes.TEX, new PSRegister(RegisterType.TEXTURE, 2)));
        seq.add(new PixelShaderInstruction(PSOpcodes.ADD, new PSRegister(RegisterType.TEMP, 0), new PSRegister(RegisterType.TEXTURE, 1), new PSRegister(RegisterType.TEXTURE, 2)));

        ICodeGenerationResult rez = codegen.generateSandboxedCode(seq, ShaderEmulator.buildSchema(seq), null);
        PixelShaderCodeExecutor executor = new PixelShaderCodeExecutor();
        ICodeExecutionEnvironment env = rez.createExecutionEnvironment();
        env.setRandomValues();
        PixelShaderExecutionResult execRez1 = (PixelShaderExecutionResult) executor.executeCode(rez, env);
        PixelShaderExecutionResult execRez2 = (PixelShaderExecutionResult) executor.executeCode(rez, env);
        Assert.assertTrue(execRez1.computeDifference(execRez2).intValue() == 0);
    }

    @Test
    public void testTextureSample() {
        InstructionSequence seq = new InstructionSequence();
        seq.add(new PixelShaderInstruction(PSOpcodes.DCL_2D, new PSRegister(RegisterType.SAMPLER, 0)));
        seq.add(new PixelShaderInstruction(PSOpcodes.DCL, new PSRegister(new PSRegister(RegisterType.TEXTURE, 0), DestinationWriteMask.XY)));
        seq.add(new PixelShaderInstruction(PSOpcodes.TEXLD, new PSRegister(RegisterType.TEMP, 0), new PSRegister(RegisterType.TEXTURE, 0), new PSRegister(RegisterType.SAMPLER, 0)));

        ICodeGenerationResult res = codegen.generateSandboxedCode(seq, ShaderEmulator.buildSchema(seq), null);
        PixelShaderCodeExecutor executor = new PixelShaderCodeExecutor();
        PixelShaderCodeExecutionEnvironment env = (PixelShaderCodeExecutionEnvironment) res.createExecutionEnvironment();
        env.setRandomValues();
        double u = 0;
        double v = 0;
        for (GenericOperand op : env.getValues()) {
            if (op.getId().equals("t0")) {
                u = ((PSRegister) op).getChannel(0).read();
                v = ((PSRegister) op).getChannel(1).read();
                break;
            }
        }
        PixelShaderExecutionResult execRez = (PixelShaderExecutionResult) executor.executeCode(res, env);
        float[] data = env.getTextureStages()[0].sample((float) u, (float) v);
        Assert.assertTrue(Math.abs(data[0] - execRez.getValues().get("r0.x")) < 0.001);
        Assert.assertTrue(Math.abs(data[1] - execRez.getValues().get("r0.y")) < 0.001);
        Assert.assertTrue(Math.abs(data[2] - execRez.getValues().get("r0.z")) < 0.001);
        Assert.assertTrue(Math.abs(data[3] - execRez.getValues().get("r0.w")) < 0.001);

    }
}
