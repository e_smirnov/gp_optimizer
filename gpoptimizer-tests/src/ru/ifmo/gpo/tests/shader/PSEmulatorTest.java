package ru.ifmo.gpo.tests.shader;

import org.junit.Assert;
import org.junit.Test;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.shader.ps_1_1.PixelShaderTargetArchitecture;
import ru.ifmo.gpo.shader.ps_1_1.emulator.ShaderEmulator;
import ru.ifmo.gpo.shader.ps_1_1.instructions.PSOpcodes;
import ru.ifmo.gpo.shader.ps_1_1.instructions.PixelShaderInstruction;
import ru.ifmo.gpo.shader.ps_1_1.parser.ShaderParseException;
import ru.ifmo.gpo.shader.ps_1_1.parser.ShaderParser;
import ru.ifmo.gpo.shader.ps_1_1.state.*;

/**
 * User: e_smirnov
 * Date: 03.08.2011
 * Time: 19:10:23
 */
public class PSEmulatorTest {

    private static PSRegister createRegister(RegisterType t, int idx, double... values) {
        PSRegister rz = new PSRegister(t, idx);
        for (int i = 0; i < values.length; ++i) {
            rz.getChannel(i).write(values[i]);
        }
        return rz;
    }

    @Test
    public void testAddResult() {
        PixelShaderInstruction insn = new PixelShaderInstruction(PSOpcodes.ADD, new PSRegister(RegisterType.TEMP, 0), new PSRegister(RegisterType.TEMP, 1), new PSRegister(RegisterType.COLOR, 0));
        PSState state = new PSState();
        PSRegister r0 = new PSRegister(RegisterType.TEMP, 0);
        r0.setX(0);
        r0.setY(0);
        r0.setZ(0);
        r0.setW(0);
        PSRegister r1 = new PSRegister(RegisterType.TEMP, 1);
        r1.setX(2);
        r1.setY(2);
        r1.setZ(2);
        r1.setW(2);
        PSRegister v0 = new PSRegister(RegisterType.COLOR, 0);
        v0.setX(1);
        v0.setY(2);
        v0.setZ(3);
        v0.setW(4);
        state.getTempRegisters().put("r0", r0);
        state.getTempRegisters().put("r1", r1);
        state.getColorRegisters().put("v0", v0);

        PSState rz = ShaderEmulator.emulateInstruction(state, insn);

        PSRegister r0result = rz.getValue(r0);
        Assert.assertTrue(Math.abs(r0result.x().read() - 3.0) < 0.0001);
        Assert.assertTrue(Math.abs(r0result.y().read() - 4.0) < 0.0001);
        Assert.assertTrue(Math.abs(r0result.z().read() - 5.0) < 0.0001);
        Assert.assertTrue(Math.abs(r0result.w().read() - 6.0) < 0.0001);
    }

    @Test
    public void testMultiplyResult() {
        PixelShaderInstruction insn = new PixelShaderInstruction(PSOpcodes.MUL, new PSRegister(RegisterType.TEMP, 0), new PSRegister(RegisterType.TEMP, 1), new PSRegister(RegisterType.COLOR, 0));
        PSState state = new PSState();
        PSRegister r0 = new PSRegister(RegisterType.TEMP, 0);
        r0.setX(0);
        r0.setY(0);
        r0.setZ(0);
        r0.setW(0);
        PSRegister r1 = new PSRegister(RegisterType.TEMP, 1);
        r1.setX(2);
        r1.setY(2);
        r1.setZ(2);
        r1.setW(2);
        PSRegister v0 = new PSRegister(RegisterType.COLOR, 0);
        v0.setX(1);
        v0.setY(2);
        v0.setZ(3);
        v0.setW(4);
        state.getTempRegisters().put("r0", r0);
        state.getTempRegisters().put("r1", r1);
        state.getColorRegisters().put("v0", v0);

        PSState rz = ShaderEmulator.emulateInstruction(state, insn);

        PSRegister r0result = rz.getValue(r0);
        Assert.assertTrue(Math.abs(r0result.x().read() - 2.0) < 0.0001);
        Assert.assertTrue(Math.abs(r0result.y().read() - 4.0) < 0.0001);
        Assert.assertTrue(Math.abs(r0result.z().read() - 6.0) < 0.0001);
        Assert.assertTrue(Math.abs(r0result.w().read() - 8.0) < 0.0001);
    }

    @Test
    public void testBuildSchemaSimple() {
        InstructionSequence seq = new InstructionSequence();

        seq.add(new PixelShaderInstruction(PSOpcodes.MOV, new PSRegister(RegisterType.TEXTURE, 2), new PSRegister(RegisterType.CONSTANT, 0)));
        seq.add(new PixelShaderInstruction(PSOpcodes.ADD, new PSRegister(RegisterType.TEMP, 0), new PSRegister(RegisterType.TEXTURE, 1), new PSRegister(RegisterType.TEXTURE, 2)));

        PSStateSchema schema = ShaderEmulator.buildSchema(seq);

        Assert.assertEquals(4, schema.getOperands().size());
        Assert.assertNotNull(schema.getOperands().get(PSRegister.getRegisterId(RegisterType.TEXTURE, 1)));
        Assert.assertNotNull(schema.getOperands().get(PSRegister.getRegisterId(RegisterType.TEXTURE, 2)));
        Assert.assertNotNull(schema.getOperands().get(PSRegister.getRegisterId(RegisterType.TEMP, 0)));

        Assert.assertTrue(schema.getRequiredContents().contains("c0"));
        Assert.assertTrue(schema.getRequiredContents().contains("t1"));
        Assert.assertFalse(schema.getRequiredContents().contains("t2"));

        Assert.assertTrue(schema.getOperands().get("t2").isWriteable());
        Assert.assertTrue(schema.getOperands().get("r0").isWriteable());
    }

    @Test
    public void testDP3Instruction() {
        PixelShaderInstruction insn = new PixelShaderInstruction(PSOpcodes.DP3, new PSRegister(RegisterType.TEMP, 0), new PSRegister(RegisterType.TEMP, 1), new PSRegister(RegisterType.COLOR, 0));
        PSState state = new PSState();
        PSRegister r0 = new PSRegister(RegisterType.TEMP, 0);
        r0.setX(0);
        r0.setY(0);
        r0.setZ(0);
        r0.setW(0);
        PSRegister r1 = new PSRegister(RegisterType.TEMP, 1);
        r1.setX(2);
        r1.setY(2);
        r1.setZ(2);
        r1.setW(2);
        PSRegister v0 = new PSRegister(RegisterType.COLOR, 0);
        v0.setX(1);
        v0.setY(2);
        v0.setZ(3);
        v0.setW(4);
        state.getTempRegisters().put("r0", r0);
        state.getTempRegisters().put("r1", r1);
        state.getColorRegisters().put("v0", v0);

        PSState rz = ShaderEmulator.emulateInstruction(state, insn);

        PSRegister r0result = rz.getValue(r0);
        Assert.assertTrue(Math.abs(r0result.x().read() - 10.0) < 0.0001);
        Assert.assertTrue(Math.abs(r0result.y().read() - 10.0) < 0.0001);
        Assert.assertTrue(Math.abs(r0result.z().read() - 10.0) < 0.0001);
        Assert.assertTrue(Math.abs(r0result.w().read() - 10.0) < 0.0001);
    }

    @Test
    public void testMADInstruction() {
        PixelShaderInstruction insn = new PixelShaderInstruction(PSOpcodes.MAD, new PSRegister(RegisterType.TEMP, 0), new PSRegister(RegisterType.TEMP, 1), new PSRegister(RegisterType.TEXTURE, 0), new PSRegister(RegisterType.COLOR, 0));
        PSState state = new PSState();
        PSRegister r0 = new PSRegister(RegisterType.TEMP, 0);
        PSRegister r1 = createRegister(RegisterType.TEMP, 1, 1, 2, 3, 4);
        PSRegister t0 = createRegister(RegisterType.TEXTURE, 0, 2, 4, 6, 8);
        PSRegister v0 = createRegister(RegisterType.COLOR, 0, 1, 1, 1, 1);
        state.getTempRegisters().put("r0", r0);
        state.getTempRegisters().put("r1", r1);
        state.getTextureRegisters().put("t0", t0);
        state.getColorRegisters().put("v0", v0);

        PSState rz = ShaderEmulator.emulateInstruction(state, insn);

        PSRegister r0result = rz.getValue(r0);
        Assert.assertTrue(Math.abs(r0result.x().read() - 3.0) < 0.0001);
        Assert.assertTrue(Math.abs(r0result.y().read() - 9.0) < 0.0001);
        Assert.assertTrue(Math.abs(r0result.z().read() - 19.0) < 0.0001);
        Assert.assertTrue(Math.abs(r0result.w().read() - 33.0) < 0.0001);

    }

    @Test
    public void testMADWithSelfInstruction() {
        PixelShaderInstruction insn = new PixelShaderInstruction(PSOpcodes.MAD, new PSRegister(RegisterType.TEMP, 0), new PSRegister(RegisterType.TEMP, 1), new PSRegister(RegisterType.TEXTURE, 0), new PSRegister(RegisterType.TEMP, 0));
        PSState state = new PSState();
        PSRegister r0 = createRegister(RegisterType.TEMP, 0, 5, 10, 15, 20);
        PSRegister r1 = createRegister(RegisterType.TEMP, 1, 1, 2, 3, 4);
        PSRegister t0 = createRegister(RegisterType.TEXTURE, 0, 2, 4, 6, 8);
        state.getTempRegisters().put("r0", r0);
        state.getTempRegisters().put("r1", r1);
        state.getTextureRegisters().put("t0", t0);

        PSState rz = ShaderEmulator.emulateInstruction(state, insn);

        PSRegister r0result = rz.getValue(r0);
        Assert.assertTrue(Math.abs(r0result.x().read() - 7.0) < 0.0001);
        Assert.assertTrue(Math.abs(r0result.y().read() - 18.0) < 0.0001);
        Assert.assertTrue(Math.abs(r0result.z().read() - 33.0) < 0.0001);
        Assert.assertTrue(Math.abs(r0result.w().read() - 52.0) < 0.0001);

    }

    @Test
    public void testLRPInstruction() {
        PixelShaderInstruction insn = new PixelShaderInstruction(PSOpcodes.LRP, new PSRegister(RegisterType.TEMP, 0), new PSRegister(RegisterType.TEMP, 1), new PSRegister(RegisterType.COLOR, 0), new PSRegister(RegisterType.COLOR, 1));
        PSState state = new PSState();
        PSRegister r0 = new PSRegister(RegisterType.TEMP, 0);
        r0.setX(0);
        r0.setY(0);
        r0.setZ(0);
        r0.setW(0);
        PSRegister r1 = new PSRegister(RegisterType.TEMP, 1);
        r1.setX(2);
        r1.setY(2);
        r1.setZ(2);
        r1.setW(2);
        PSRegister v0 = new PSRegister(RegisterType.COLOR, 0);
        v0.setX(1);
        v0.setY(2);
        v0.setZ(3);
        v0.setW(4);

        PSRegister v1 = new PSRegister(RegisterType.COLOR, 1);
        v1.setX(1);
        v1.setY(2);
        v1.setZ(3);
        v1.setW(1);

        state.getTempRegisters().put("r0", r0);
        state.getTempRegisters().put("r1", r1);
        state.getColorRegisters().put("v0", v0);
        state.getColorRegisters().put("v1", v1);

        PSState rz = ShaderEmulator.emulateInstruction(state, insn);

        PSRegister r0result = rz.getValue(r0);
        Assert.assertTrue(Math.abs(r0result.x().read() - 1.0) < 0.0001);
        Assert.assertTrue(Math.abs(r0result.y().read() - 2.0) < 0.0001);
        Assert.assertTrue(Math.abs(r0result.z().read() - 3.0) < 0.0001);
        Assert.assertTrue(Math.abs(r0result.w().read() - 7.0) < 0.0001);
    }

    @Test
    public void testDestinationRegisterWriteMask() {
        PixelShaderInstruction insn = new PixelShaderInstruction(PSOpcodes.ADD, new PSRegister(new PSRegister(RegisterType.TEMP, 0), DestinationWriteMask.W), new PSRegister(RegisterType.TEMP, 1), new PSRegister(RegisterType.COLOR, 0));
        PSState state = new PSState();
        PSRegister r0 = new PSRegister(RegisterType.TEMP, 0);
        r0.setX(0);
        r0.setY(0);
        r0.setZ(0);
        r0.setW(0);
        PSRegister r1 = new PSRegister(RegisterType.TEMP, 1);
        r1.setX(2);
        r1.setY(2);
        r1.setZ(2);
        r1.setW(2);
        PSRegister v0 = new PSRegister(RegisterType.COLOR, 0);
        v0.setX(1);
        v0.setY(2);
        v0.setZ(3);
        v0.setW(4);
        state.getTempRegisters().put("r0", r0);
        state.getTempRegisters().put("r1", r1);
        state.getColorRegisters().put("v0", v0);

        PSState rz = ShaderEmulator.emulateInstruction(state, insn);

        PSRegister r0result = rz.getValue(r0);

        Assert.assertTrue(Math.abs(r0result.x().read()) < 0.0001);
        Assert.assertTrue(Math.abs(r0result.y().read()) < 0.0001);
        Assert.assertTrue(Math.abs(r0result.z().read()) < 0.0001);
        Assert.assertTrue(Math.abs(r0result.w().read() - 6.0) < 0.0001);
    }

    @Test
    public void testTextureSampling() {
        PSRegister t0 = new PSRegister(RegisterType.TEXTURE, 0);
        PSRegister s0 = new PSRegister(RegisterType.SAMPLER, 0);
        PSRegister r0 = new PSRegister(RegisterType.TEMP, 0);
        PixelShaderInstruction insn = new PixelShaderInstruction(PSOpcodes.TEXLD, new PSRegister(RegisterType.TEMP, 0), t0, s0);
        PSState state = new PSState();
        state.getTextureRegisters().put("t0", t0);
        state.getSamplerRegisters().put("s0", s0);
        state.getTempRegisters().put("r0", r0);
        state.getTextureStages()[0].setToFF();
        PSState rz = ShaderEmulator.emulateInstruction(state, insn);
        PSRegister r0Result = rz.getValue(r0);

        Assert.assertTrue(Math.abs(r0Result.x().read() - 1.0) < 0.0001);
        Assert.assertTrue(Math.abs(r0Result.y().read() - 1.0) < 0.0001);
        Assert.assertTrue(Math.abs(r0Result.z().read() - 1.0) < 0.0001);
        Assert.assertTrue(Math.abs(r0Result.w().read() - 1.0) < 0.0001);
    }


    @Test
    public void testConstantCollection() throws ShaderParseException {
        InstructionSequence seq = new ShaderParser().parse("def c0, 1.123,-0.5,23.1,0.111");
        PSStateSchema schema = (PSStateSchema) ShaderEmulator.buildSchema(seq);
        Assert.assertEquals(8, schema.getConstants().getFloatConstantNumber());
        Assert.assertTrue(schema.getConstants().getFloatConstants().contains(-0.5f));
    }

    @Test
    public void testSourceRegisterModifier() {
        PixelShaderInstruction insn = new PixelShaderInstruction(PSOpcodes.MUL, new PSRegister(RegisterType.TEMP, 0), new PSRegister(new PSRegister(RegisterType.TEMP, 1), SourceRegisterModifier.NEGATE), new PSRegister(RegisterType.COLOR, 0));
        PSState state = new PSState();
        PSRegister r0 = new PSRegister(RegisterType.TEMP, 0);
        r0.setX(0);
        r0.setY(0);
        r0.setZ(0);
        r0.setW(0);
        PSRegister r1 = new PSRegister(RegisterType.TEMP, 1);
        r1.setX(2);
        r1.setY(2);
        r1.setZ(2);
        r1.setW(2);
        PSRegister v0 = new PSRegister(RegisterType.COLOR, 0);
        v0.setX(1);
        v0.setY(2);
        v0.setZ(3);
        v0.setW(4);
        state.getTempRegisters().put("r0", r0);
        state.getTempRegisters().put("r1", r1);
        state.getColorRegisters().put("v0", v0);

        PSState rz = ShaderEmulator.emulateInstruction(state, insn);

        PSRegister r0result = rz.getValue(r0);
        Assert.assertTrue(Math.abs(r0result.x().read() + 2.0) < 0.0001);
        Assert.assertTrue(Math.abs(r0result.y().read() + 4.0) < 0.0001);
        Assert.assertTrue(Math.abs(r0result.z().read() + 6.0) < 0.0001);
        Assert.assertTrue(Math.abs(r0result.w().read() + 8.0) < 0.0001);
    }

    @Test
    public void testMultipleSourceRegModifiers() {

        // mul r0, -r1.wwww, v0
        PixelShaderInstruction insn = new PixelShaderInstruction(PSOpcodes.MUL, new PSRegister(RegisterType.TEMP, 0), new PSRegister(new PSRegister(RegisterType.TEMP, 1), SourceRegisterModifier.NEGATE, SourceRegisterModifier.SWIZZLE_WWWW), new PSRegister(RegisterType.COLOR, 0));
        PSState state = new PSState();
        PSRegister r0 = createRegister(RegisterType.TEMP, 0, 0, 0, 0, 0);
        PSRegister r1 = createRegister(RegisterType.TEMP, 1, 4, 5, 6, 7);
        PSRegister v0 = createRegister(RegisterType.COLOR, 0, 1, 2, 3, 4);
        state.getTempRegisters().put("r0", r0);
        state.getTempRegisters().put("r1", r1);
        state.getColorRegisters().put("v0", v0);

        PSState rz = ShaderEmulator.emulateInstruction(state, insn);

        PSRegister r0result = rz.getValue(r0);
        Assert.assertTrue(Math.abs(r0result.x().read() + 7.0) < 0.0001);
        Assert.assertTrue(Math.abs(r0result.y().read() + 14.0) < 0.0001);
        Assert.assertTrue(Math.abs(r0result.z().read() + 21.0) < 0.0001);
        Assert.assertTrue(Math.abs(r0result.w().read() + 28.0) < 0.0001);
    }

    @Test
    public void testStripSetupInstructions() {
        InstructionSequence seq = new InstructionSequence();
        seq.add(new PixelShaderInstruction(PSOpcodes.DCL_2D, new PSRegister(RegisterType.SAMPLER, 0)));
        seq.add(new PixelShaderInstruction(PSOpcodes.DCL, new PSRegister(new PSRegister(RegisterType.TEXTURE, 0), DestinationWriteMask.XY)));
        seq.add(new PixelShaderInstruction(PSOpcodes.TEXLD, new PSRegister(RegisterType.TEMP, 0), new PSRegister(RegisterType.TEXTURE, 0), new PSRegister(RegisterType.SAMPLER, 0)));

        PixelShaderTargetArchitecture arch = new PixelShaderTargetArchitecture();
        InstructionSequence stripped = arch.preprocessSource(seq);

        Assert.assertEquals(1, stripped.size());
    }

    @Test
    public void testDCLMakesAvailableOnlyValidParts() {
        InstructionSequence seq = new InstructionSequence();
        seq.add(new PixelShaderInstruction(PSOpcodes.DCL, new PSRegister(new PSRegister(RegisterType.TEXTURE, 0), DestinationWriteMask.XY)));

        PSStateSchema schema = ShaderEmulator.buildSchema(seq);

        PSState state = new PSState(schema);
        PSRegister reg = state.getTextureRegisters().get("t0");
        Assert.assertTrue(reg.hasChannel(0));
        Assert.assertTrue(reg.hasChannel(1));
        Assert.assertFalse(reg.hasChannel(2));
        Assert.assertFalse(reg.hasChannel(3));

    }

    @Test
    public void miminumInvalidTexopTest() throws ShaderParseException {
        InstructionSequence seq = new ShaderParser().parse(
                "ps_2_0 \n" +
                        "dcl t1\n" +
                        "dcl_2d s0\n" +
                        "texld r1, t1, s0\n" +
                        "texld r1, t1, s0\n" +
                        "texld r1, t1, s0\n" +
                        "dp3 r1, c1, t1\n" +
                        "texld r1, t1, s0\n" +
                        "texld r1, t1, s0\n" +
                        "mov oC0, r1");

        PSStateSchema schema = ShaderEmulator.buildSchema(seq);
        PSInstructionSequenceAnalyzer analyzer = new PSInstructionSequenceAnalyzer(seq, schema);
        Assert.assertEquals(4, analyzer.getStateAfter().getTempRegisters().get("r1").getDependentTexOpRead());
    }

    @Test
    public void testBuggedTexop() throws ShaderParseException {
        InstructionSequence seq = new ShaderParser().parse(
                "ps_2_0 \n" +
                        "dcl t0\n" +
                        "dcl t1\n" +
                        "dcl t2\n" +
                        "dcl t3\n" +
                        "dcl t4\n" +
                        "dcl t5\n" +
                        "dcl t6\n" +
                        "dcl t7\n" +
                        "dcl_2d s0\n" +
                        "texld r0, t0, s0\n" +
                        "texld r1, t1, s0\n" +
                        "mul r0, r0, c0\n" +
                        "mad r0, r1, c1, r0\n" +
                        "texld r1, t2, s0\n" +
                        "texld r2, t3, s0\n" +
                        "mad r0, r1, c2, r0\n" +
                        "mad r0, r2, c3, r0\n" +
                        "dp3 r1, c1, t5\n" +
                        "texld r1, t4, s0\n" +
                        "texld r1, t4, s0\n" +
                        "texld r2, t5, s0\n" +
                        "mad r0, r1, c4, r0\n" +
                        "mad r0, r2, c5, r0\n" +
                        "texld r1, t6, s0\n" +
                        "texld r2, t7, s0\n" +
                        "mad r0, r1, c6, r0\n" +
                        "mad r0, r2, c7, r0\n" +
                        "mov oC0, r0"
        );

        PSStateSchema schema = ShaderEmulator.buildSchema(seq);
        PSInstructionSequenceAnalyzer analyzer = new PSInstructionSequenceAnalyzer(seq, schema);
        Assert.assertEquals(4, analyzer.getStateAfter().getTempRegisters().get("r1").getDependentTexOpRead());
    }

    @Test
    public void testBuildStateFromSchemaAccessRights() throws ShaderParseException {
        InstructionSequence seq = new ShaderParser().parse(
                "texld r1, t1, s0\n" +
                        "mul r0, r0, c0\n" +
                        "mad r0, r1, c1, r0\n"
        );

        PSStateSchema schema = ShaderEmulator.buildSchema(seq);
        PSState state = new PSState(schema);

        PSRegister r1 = state.getTempRegisters().get("r1");
        PSRegister r0 = state.getTempRegisters().get("r0");
        PSRegister t1 = state.getTextureRegisters().get("t1");

        Assert.assertEquals(GenericOperand.AccessType.WRITE, r1.getAccessType());
        Assert.assertEquals(GenericOperand.AccessType.ANY, r0.getAccessType());
        Assert.assertEquals(GenericOperand.AccessType.READ, t1.getAccessType());
    }


}
