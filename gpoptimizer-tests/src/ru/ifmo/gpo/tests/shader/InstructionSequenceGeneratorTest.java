package ru.ifmo.gpo.tests.shader;

import org.junit.Assert;
import org.junit.Test;
import ru.ifmo.gpo.core.ITargetArchitecture;
import ru.ifmo.gpo.core.instructions.SequenceGenerationFailedException;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.vmstate.NoSuitableOperandFoundException;
import ru.ifmo.gpo.shader.ps_1_1.PixelShaderTargetArchitecture;
import ru.ifmo.gpo.shader.ps_1_1.instructions.PSOpcodes;
import ru.ifmo.gpo.shader.ps_1_1.state.PSRegister;
import ru.ifmo.gpo.shader.ps_1_1.state.PSState;
import ru.ifmo.gpo.shader.ps_1_1.state.RegisterType;

import java.util.List;

/**
 * User: e_smirnov
 * Date: 20.09.2011
 * Time: 12:40:05
 */
public class InstructionSequenceGeneratorTest {

    private ITargetArchitecture arch = new PixelShaderTargetArchitecture();

    @Test
    public void testGenerateWriteToR0() throws SequenceGenerationFailedException {
        /*   InstructionSequence seq = new InstructionSequence();
        seq.add(new PixelShaderInstruction(PSOpcodes.TEX, new PSRegister(RegisterType.TEXTURE, 1)));
        seq.add(new PixelShaderInstruction(PSOpcodes.TEX, new PSRegister(RegisterType.TEXTURE, 2)));
        seq.add(new PixelShaderInstruction(PSOpcodes.ADD, new PSRegister(RegisterType.TEMP, 0), new PSRegister(RegisterType.TEXTURE, 1), new PSRegister(RegisterType.TEXTURE, 2)));
        PSStateSchema schema = (PSStateSchema) ShaderEmulator.buildSchema(seq);

        PSInstructionSequenceAnalyzer analyzer = new PSInstructionSequenceAnalyzer(seq, schema);

        InstructionSequence rz = arch.getInstructionSequenceGenerator().generateInstructionSequence(analyzer.getStateBefore(), analyzer.getStateChange(), 3);
        PSInstructionSequenceAnalyzer rzAnalyzer = new PSInstructionSequenceAnalyzer(rz, schema);

        Assert.assertTrue(rzAnalyzer.checkStateEqual(analyzer));
        Assert.assertTrue(analyzer.checkStateEqual(rzAnalyzer));*/
    }

    @Test(expected = NoSuitableOperandFoundException.class)
    public void testCantSelectOperandsWithoutBreakingReadPortLimit() throws NoSuitableOperandFoundException {
        PSState state = new PSState();

        PSRegister writeReg = new PSRegister(RegisterType.TEMP, 0);
        writeReg.setAccessType(GenericOperand.AccessType.WRITE);

        state.getTempRegisters().put(writeReg.getId(), writeReg);

        for (int i = 0; i < 3; ++i) {
            PSRegister readonlyRegister = new PSRegister(RegisterType.CONSTANT, i);
            readonlyRegister.setAccessType(GenericOperand.AccessType.READ);
            state.getConstantRegisters().put(readonlyRegister.getId(), readonlyRegister);
        }

        // will throw exception, as requires 3 source operands, only available registers are 3 constant registers, but they have read port limit of 2
        PSOpcodes.MAD.getOperandSelector().selectOperandsRandom(PSOpcodes.MAD, state);
    }

    @Test
    public void testInsnWith3SourcesOperandSelection() throws NoSuitableOperandFoundException {
        PSState state = new PSState();

        PSRegister writeReg = new PSRegister(RegisterType.TEMP, 0);
        writeReg.setAccessType(GenericOperand.AccessType.WRITE);

        state.getTempRegisters().put(writeReg.getId(), writeReg);

        for (int i = 0; i < 2; ++i) {
            PSRegister readonlyRegister = new PSRegister(RegisterType.CONSTANT, i);
            readonlyRegister.setAccessType(GenericOperand.AccessType.READ);
            state.getConstantRegisters().put(readonlyRegister.getId(), readonlyRegister);
        }

        PSRegister anotherReadonly = new PSRegister(RegisterType.TEXTURE, 0);
        anotherReadonly.setAccessType(GenericOperand.AccessType.READ);
        state.getTextureRegisters().put(anotherReadonly.getId(), anotherReadonly);

        // will throw exception, as requires 3 source operands, only available registers are 3 constant registers, but they have read port limit of 2
        List<GenericOperand> rz = PSOpcodes.MAD.getOperandSelector().selectOperandsRandom(PSOpcodes.MAD, state);

        Assert.assertEquals(4, rz.size());
        PSRegister dst = ((PSRegister) rz.get(0));
        for (int i = 0; i < dst.getChannelCount(); ++i) {
            if (dst.hasChannel(i)) {
                PSRegister dstRegister = dst.getChannel(i).getOwner();
                if (dstRegister.getRegisterType() == RegisterType.TEMP && dstRegister.getIndex() == 0) {
                    return;
                }
            }
        }
        Assert.assertFalse(false);
    }
}
