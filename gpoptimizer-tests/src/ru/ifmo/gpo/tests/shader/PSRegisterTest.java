package ru.ifmo.gpo.tests.shader;

import org.junit.Assert;
import org.junit.Test;
import ru.ifmo.gpo.shader.ps_1_1.state.*;

/**
 * User: jedi-philosopher
 * Date: 22.12.11
 * Time: 22:09
 */
public class PSRegisterTest {
    @Test
    public void testAddAndGetChannel() {
        PSRegister cg = new PSRegister(RegisterType.TEMP, 0, new Channel(new PSRegister(RegisterType.TEMP, 0), Channel.Name.y));

        Assert.assertTrue(cg.hasChannel(Channel.Name.y.getChannelIdx()));
        Assert.assertFalse(cg.hasChannel(Channel.Name.x.getChannelIdx()));
    }

    @Test
    public void testSatisfiesMask() {
        PSRegister cg = new PSRegister(RegisterType.TEMP, 0, new Channel(new PSRegister("r0"), Channel.Name.x), new Channel(new PSRegister("r0"), Channel.Name.w));

        Assert.assertTrue(cg.satisfiesMask(DestinationWriteMask.W));
        Assert.assertFalse(cg.satisfiesMask(DestinationWriteMask.XYZW));
    }

    @Test
    public void testSatisfiesMaskStrict() {
        PSRegister cg1 = new PSRegister(RegisterType.TEMP, 0, new Channel(new PSRegister("r0"), Channel.Name.x));
        PSRegister cg2 = new PSRegister(RegisterType.TEMP, 0, new Channel(new PSRegister("r0"), Channel.Name.x), new Channel(new PSRegister("r0"), Channel.Name.w));
        Assert.assertTrue(cg1.satisfiesMaskStrict(DestinationWriteMask.X));

        Assert.assertFalse(cg2.satisfiesMaskStrict(DestinationWriteMask.X));
    }

    @Test
    public void testCreateFromMask() {
        PSRegister reg = new PSRegister(RegisterType.TEMP, 1);
        PSRegister cg = new PSRegister(reg, DestinationWriteMask.XYZ);

        Assert.assertTrue(cg.satisfiesMask(DestinationWriteMask.XYZ));
        Assert.assertTrue(cg.satisfiesMaskStrict(DestinationWriteMask.XYZ));
        Assert.assertEquals(3, cg.getChannelCount());
        Assert.assertFalse(cg.hasChannel(Channel.Name.w.getChannelIdx()));
    }

    @Test
    public void testToStringWithSourceModifiers() {
        PSRegister reg = new PSRegister(RegisterType.TEMP, 1);
        PSRegister cg = new PSRegister(reg, SourceRegisterModifier.SWIZZLE_WWWW, SourceRegisterModifier.NEGATE);
        Assert.assertEquals("-r1.wwww", cg.getStringRepresentation());
    }

    @Test
    public void testIterator() {
        PSRegister reg = new PSRegister(RegisterType.TEMP, 1);
        PSRegister cg = new PSRegister(reg, DestinationWriteMask.XY);
        int count = 0;
        for (Channel c : cg) {
            Assert.assertTrue(c.getName() == Channel.Name.x || c.getName() == Channel.Name.y);
            ++count;
        }
        Assert.assertEquals(2, count);
    }

}
