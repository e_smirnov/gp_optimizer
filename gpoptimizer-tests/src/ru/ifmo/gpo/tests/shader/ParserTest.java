package ru.ifmo.gpo.tests.shader;

import org.junit.Assert;
import org.junit.Test;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.NumericConstant;
import ru.ifmo.gpo.shader.ps_1_1.instructions.PSOpcodes;
import ru.ifmo.gpo.shader.ps_1_1.parser.ShaderParseException;
import ru.ifmo.gpo.shader.ps_1_1.parser.ShaderParser;
import ru.ifmo.gpo.shader.ps_1_1.state.DestinationWriteMask;
import ru.ifmo.gpo.shader.ps_1_1.state.PSRegister;
import ru.ifmo.gpo.shader.ps_1_1.state.SourceRegisterModifier;

/**
 * User: jedi-philosopher
 * Date: 14.01.12
 * Time: 21:14
 */
public class ParserTest {

    @Test
    public void testSingleNoArg() throws ShaderParseException {
        String shader = "nop";
        ShaderParser p = new ShaderParser();
        InstructionSequence seq = p.parse(shader);

        Assert.assertEquals(1, seq.size());
        Assert.assertEquals(PSOpcodes.NOP.getOpcode(), seq.get(0).getOpcode());
    }

    @Test
    public void testSingleWithArg() throws ShaderParseException {
        String shader = "mov r0,r1";
        ShaderParser p = new ShaderParser();
        InstructionSequence seq = p.parse(shader);

        Assert.assertEquals(1, seq.size());
        Assert.assertEquals(PSOpcodes.MOV.getOpcode(), seq.get(0).getOpcode());
        Assert.assertEquals(2, seq.get(0).getParameters().size());
        Assert.assertEquals("r0", seq.get(0).getParameters().get(0).getId());
        Assert.assertEquals("r1", seq.get(0).getParameters().get(1).getId());
    }

    @Test
    public void testSingleDestWriteMask() throws ShaderParseException {
        String shader = "mul r0.xyz, r1, t0";
        ShaderParser p = new ShaderParser();
        InstructionSequence seq = p.parse(shader);

        Assert.assertEquals(1, seq.size());
        Assert.assertTrue(seq.get(0).getParameters().get(0) instanceof PSRegister);
        Assert.assertTrue(((PSRegister) seq.get(0).getParameters().get(0)).satisfiesMaskStrict(DestinationWriteMask.XYZ));
    }

    @Test
    public void testMultiple() throws ShaderParseException {
        String shader = "ps_2_0 \n def c0, 1, 1, 1, 1\n mov oC0,c0";
        ShaderParser p = new ShaderParser();
        InstructionSequence seq = p.parse(shader);

        Assert.assertEquals(3, seq.size());
        Assert.assertEquals(PSOpcodes.PS_2_0.getOpcode(), seq.get(0).getOpcode());
        Assert.assertEquals(PSOpcodes.DEF.getOpcode(), seq.get(1).getOpcode());
        Assert.assertEquals(PSOpcodes.MOV.getOpcode(), seq.get(2).getOpcode());
    }

    @Test
    public void testImmediateOperand() throws ShaderParseException {
        String shader = "def c1, 1.5, 0.0, 0.0, -0.5";
        ShaderParser p = new ShaderParser();
        InstructionSequence seq = p.parse(shader);

        Assert.assertEquals(1, seq.size());
        Assert.assertEquals(PSOpcodes.DEF.getOpcode(), seq.get(0).getOpcode());
        Assert.assertEquals(5, seq.get(0).getParameters().size());
        Assert.assertEquals("c1", seq.get(0).getParameters().get(0).getId());
        Assert.assertTrue(seq.get(0).getParameters().get(1) instanceof NumericConstant);
        Assert.assertTrue(Math.abs(((NumericConstant) seq.get(0).getParameters().get(1)).doubleValue() - 1.5) < 0.001);
    }

    @Test
    public void testSourceRegModifier() throws ShaderParseException {
        String shader = "sub r0,r1,-t0";
        ShaderParser p = new ShaderParser();
        InstructionSequence seq = p.parse(shader);

        Assert.assertEquals(1, seq.size());
        Assert.assertEquals(PSOpcodes.SUB.getOpcode(), seq.get(0).getOpcode());

        Assert.assertEquals("r1", seq.get(0).getParameters().get(1).getId());
        Assert.assertEquals(SourceRegisterModifier.NEGATE, ((PSRegister) seq.get(0).getParameters().get(2)).getModifiers()[0]);
        Assert.assertEquals("t0", seq.get(0).getParameters().get(2).getId());
    }

    @Test
    public void testMultipleSourceRegModifier() throws ShaderParseException {
        String shader = "sub r0, r1,-t0.wwww";
        ShaderParser p = new ShaderParser();
        InstructionSequence seq = p.parse(shader);

        Assert.assertEquals(1, seq.size());
        Assert.assertEquals(PSOpcodes.SUB.getOpcode(), seq.get(0).getOpcode());

        Assert.assertEquals("r1", seq.get(0).getParameters().get(1).getId());
        Assert.assertEquals(SourceRegisterModifier.SWIZZLE_WWWW, ((PSRegister) seq.get(0).getParameters().get(2)).getModifiers()[0]);
        Assert.assertEquals(SourceRegisterModifier.NEGATE, ((PSRegister) seq.get(0).getParameters().get(2)).getModifiers()[1]);
        Assert.assertEquals("t0", seq.get(0).getParameters().get(2).getId());
    }

    @Test
    public void testSimplePS2_0() throws ShaderParseException {
        final String shader = "ps_2_0\n" +
                "dcl_2d s0\n" +
                "dcl t0.xy\n" +
                "texld r1, t0, s0\n" +
                "mov oC0, r1";

        ShaderParser p = new ShaderParser();
        InstructionSequence seq = p.parse(shader);

        Assert.assertEquals(5, seq.size());

    }

}
