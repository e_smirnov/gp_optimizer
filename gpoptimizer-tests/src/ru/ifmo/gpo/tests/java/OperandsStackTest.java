package ru.ifmo.gpo.tests.java;

import org.junit.Assert;
import org.junit.Test;
import org.objectweb.asm.Type;
import ru.ifmo.gpo.java.instructions.JavaOpcodes;
import ru.ifmo.gpo.java.vmstate.VerifyableOperandStack;
import ru.ifmo.gpo.java.vmstate.VerifyableOperandStackItem;
import ru.ifmo.gpo.verify.UnknownValueNode;

/**
 * User: jedi-philosopher
 * Date: 05.09.2010
 * Time: 21:28:29
 */
public class OperandsStackTest {

    @Test
    public void testPush() {
        VerifyableOperandStack stack = new VerifyableOperandStack();
        stack.applyPush(new VerifyableOperandStackItem(Type.INT_TYPE, new UnknownValueNode()));

        assert stack.size() == 1;
    }

    @Test
    public void testPopWithUnderflows() {
        VerifyableOperandStack stack = new VerifyableOperandStack();
        assert stack.canPop(Type.LONG_TYPE);
    }

    @Test
    public void testPopWithoutUnderflows() {
        VerifyableOperandStack stack = new VerifyableOperandStack(false);
        assert !stack.canPop(Type.LONG_TYPE);
    }

    @Test
    public void testPop() {
        VerifyableOperandStack stack = new VerifyableOperandStack();
        stack.applyPush(new VerifyableOperandStackItem(Type.INT_TYPE, new UnknownValueNode()));

        assert stack.size() == 1;

        stack.applyPop(Type.INT_TYPE);

        assert stack.isEmpty();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIncorrectPop() {
        VerifyableOperandStack stack = new VerifyableOperandStack();
        stack.applyPush(new VerifyableOperandStackItem(Type.INT_TYPE, new UnknownValueNode()));
        stack.applyPush(new VerifyableOperandStackItem(Type.LONG_TYPE, new UnknownValueNode()));

        assert stack.size() == 2;

        stack.applyPop(Type.INT_TYPE, Type.DOUBLE_TYPE);  // should cause an exception

        assert false; // should never get here
    }

    @Test
    public void testUnderflow() {
        VerifyableOperandStack stack = new VerifyableOperandStack();
        stack.applyPush(new VerifyableOperandStackItem(Type.INT_TYPE, new UnknownValueNode()));
        assert stack.size() == 1;
        stack.applyPop(Type.INT_TYPE, Type.INT_TYPE);
        assert stack.isEmpty();
        assert stack.getUnderflowStack().size() == 1;
    }

    @Test
    public void testComplex() {
        VerifyableOperandStack stack = new VerifyableOperandStack();
        stack.applyPush(new VerifyableOperandStackItem(Type.INT_TYPE, new UnknownValueNode()));
        stack.applyPush(new VerifyableOperandStackItem(Type.DOUBLE_TYPE, new UnknownValueNode()));
        stack.applyPush(new VerifyableOperandStackItem(Type.LONG_TYPE, new UnknownValueNode()));

        stack.applyPop(Type.LONG_TYPE, Type.DOUBLE_TYPE);
        stack.applyPop(Type.INT_TYPE, Type.INT_TYPE);

        stack.applyPush(new VerifyableOperandStackItem(Type.FLOAT_TYPE, new UnknownValueNode()));

        assert stack.size() == 1;
        assert stack.peek().getType() == Type.FLOAT_TYPE;
        assert stack.getUnderflowStack().size() == 1;
        assert stack.getUnderflowStack().get(0).getType() == Type.INT_TYPE;
    }

    @Test
    public void testEquals() {
        VerifyableOperandStack first = new VerifyableOperandStack();
        first.applyPush(new VerifyableOperandStackItem(Type.INT_TYPE, new UnknownValueNode()));
        first.applyPush(new VerifyableOperandStackItem(Type.DOUBLE_TYPE, new UnknownValueNode()));

        VerifyableOperandStack second = new VerifyableOperandStack();
        second.applyPush(new VerifyableOperandStackItem(Type.INT_TYPE, new UnknownValueNode()));
        second.applyPush(new VerifyableOperandStackItem(Type.DOUBLE_TYPE, new UnknownValueNode()));
        second.applyPush(new VerifyableOperandStackItem(Type.DOUBLE_TYPE, new UnknownValueNode()));
        second.applyPop(Type.DOUBLE_TYPE);

        assert first.equals(second);
    }

    @Test
    public void testEqualsWithUnderflow() {
        VerifyableOperandStack first = new VerifyableOperandStack();
        first.applyPop(Type.LONG_TYPE);
        first.applyPush(new VerifyableOperandStackItem(Type.INT_TYPE, new UnknownValueNode()));

        VerifyableOperandStack second = new VerifyableOperandStack();
        second.applyPush(new VerifyableOperandStackItem(Type.DOUBLE_TYPE, new UnknownValueNode()));
        second.applyPop(Type.DOUBLE_TYPE);
        second.applyPop(Type.LONG_TYPE);
        second.applyPush(new VerifyableOperandStackItem(Type.INT_TYPE, new UnknownValueNode()));

        Assert.assertTrue(first.equals(second));
    }

    @Test
    public void testNotEqualsUnderflowDifferent() {
        VerifyableOperandStack first = new VerifyableOperandStack();
        first.applyPop(Type.LONG_TYPE);
        first.applyPush(new VerifyableOperandStackItem(Type.INT_TYPE, new UnknownValueNode()));

        VerifyableOperandStack second = new VerifyableOperandStack();
        second.applyPop(Type.INT_TYPE);
        first.applyPush(new VerifyableOperandStackItem(Type.INT_TYPE, new UnknownValueNode()));

        Assert.assertFalse(first.equals(second));
    }

    @Test
    public void testCanPopAll() {
        VerifyableOperandStack stack = new VerifyableOperandStack();
        stack.applyPush(new VerifyableOperandStackItem(Type.INT_TYPE, new UnknownValueNode()), new VerifyableOperandStackItem(Type.INT_TYPE, new UnknownValueNode()));

        Assert.assertTrue(stack.canPopAll(JavaOpcodes.IADD.getExpectedStackTypes()));
    }

    @Test
    public void testCanPopAllFailed() {
        VerifyableOperandStack stack = new VerifyableOperandStack();
        stack.applyPush(new VerifyableOperandStackItem(Type.INT_TYPE, new UnknownValueNode()), new VerifyableOperandStackItem(Type.FLOAT_TYPE, new UnknownValueNode()));

        Assert.assertFalse(stack.canPopAll(JavaOpcodes.ISHR.getExpectedStackTypes()));
        Assert.assertTrue(stack.canPopAll(JavaOpcodes.F2I.getExpectedStackTypes()));
    }

    @Test
    public void testCanPopAllFailed2Types() {
        VerifyableOperandStack stack = new VerifyableOperandStack();
        stack.applyPush(new VerifyableOperandStackItem(Type.INT_TYPE, new UnknownValueNode()), new VerifyableOperandStackItem(Type.LONG_TYPE, new UnknownValueNode()));

        Assert.assertFalse(stack.canPopAll(JavaOpcodes.LSHR.getExpectedStackTypes()));
    }

    @Test
    public void testCanPopAllVoid() {
        VerifyableOperandStack stack = new VerifyableOperandStack();
        stack.applyPush(new VerifyableOperandStackItem(Type.INT_TYPE, new UnknownValueNode()), new VerifyableOperandStackItem(Type.INT_TYPE, new UnknownValueNode()));

        Assert.assertTrue(stack.canPopAll(JavaOpcodes.POP.getExpectedStackTypes()));
    }

    @Test
    public void testPeekLast()
    {
        VerifyableOperandStack stack = new VerifyableOperandStack();
        stack.setAllowUnderflows(true);
        stack.applyPop(Type.INT_TYPE);
        stack.applyPop(Type.DOUBLE_TYPE);
        stack.applyPush(new VerifyableOperandStackItem(Type.FLOAT_TYPE, null));
        stack.applyPush(new VerifyableOperandStackItem(Type.LONG_TYPE, null));

        Assert.assertTrue(stack.peek(0).getType().equals(Type.LONG_TYPE));
        Assert.assertTrue(stack.peek(1).getType().equals(Type.FLOAT_TYPE));
        Assert.assertTrue(stack.peek(2).getType().equals(Type.INT_TYPE));
        Assert.assertTrue(stack.peek(3).getType().equals(Type.DOUBLE_TYPE));
        Assert.assertNull(stack.peek(4));
    }
}
