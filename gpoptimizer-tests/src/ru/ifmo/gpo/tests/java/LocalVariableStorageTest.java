package ru.ifmo.gpo.tests.java;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.java.instructions.JVMInstruction;
import ru.ifmo.gpo.java.instructions.JavaOpcodes;
import ru.ifmo.gpo.java.vmstate.JVMStateSchema;
import ru.ifmo.gpo.java.vmstate.LocalVariableSlot;
import ru.ifmo.gpo.java.vmstate.LocalVariableStorage;
import ru.ifmo.gpo.verify.IArithmeticTreeNode;
import ru.ifmo.gpo.verify.IntegerConstantTreeNode;
import ru.ifmo.gpo.verify.NumericConstantTreeNode;

/**
 * User: jedi-philosopher
 * Date: 04.11.2010
 * Time: 22:33:13
 */
public class LocalVariableStorageTest {

    private JVMStateSchema schema;

    @Before
    public void setUp() {
        schema = new JVMStateSchema();
    }

    @Test
    public void testAddAndGet() {
        schema.addOrUpdateSlot(0, Type.INT_TYPE, GenericOperand.AccessType.ANY);

        LocalVariableStorage storage = new LocalVariableStorage(schema);
        storage.setValue(0, Type.INT_TYPE, new IntegerConstantTreeNode(5));
        IArithmeticTreeNode node = storage.getValue(0, Type.INT_TYPE);

        assert node instanceof IntegerConstantTreeNode;
    }

    @Test
    public void testOverwrite() {

        schema.addOrUpdateSlot(0, Type.INT_TYPE, GenericOperand.AccessType.ANY);

        LocalVariableStorage storage = new LocalVariableStorage(schema);

        storage.setValue(0, Type.INT_TYPE, new IntegerConstantTreeNode(5));
        storage.setValue(0, Type.INT_TYPE, new IntegerConstantTreeNode(10));

        assert ((IntegerConstantTreeNode) storage.getValue(0)).getValue() == 10;
    }

    @Test
    public void testSchemaWith2SlotLocalVar() {
        InstructionSequence seq = new InstructionSequence();
        seq.add(new JVMInstruction(JavaOpcodes.LLOAD, new LocalVariableSlot(0)));
        schema = new JVMStateSchema(seq);

        Assert.assertEquals(Type.LONG_TYPE, schema.getSlot(0).getType());
        Assert.assertEquals(Type.VOID_TYPE, schema.getSlot(1).getType());
        Assert.assertNull(schema.getSlot(2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFailedWriteLongToSingleSlot() {
        InstructionSequence seq = new InstructionSequence();
        seq.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(0)));
        schema = new JVMStateSchema(seq);

        LocalVariableStorage vars = new LocalVariableStorage(schema);
        vars.setValue(0, Type.LONG_TYPE, new NumericConstantTreeNode(Type.LONG_TYPE, 0L)); // should crash here as saving long requires slot 1 to be writeable
    }

    @Test
    public void testLongOverwritesInt() {
        InstructionSequence seq = new InstructionSequence();
        seq.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(0)));
        seq.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(1)));
        schema = new JVMStateSchema(seq);
        LocalVariableStorage vars = new LocalVariableStorage(schema);
        vars.setValue(0, Type.LONG_TYPE, new NumericConstantTreeNode(Type.LONG_TYPE, 0L));

        Assert.assertTrue(vars.getLocalVariable(0).hasValue());
        Assert.assertTrue(vars.getLocalVariable(0).getType().equals(Type.LONG_TYPE));
        Assert.assertFalse(vars.getLocalVariable(1).hasValue());

    }

    @Test
    public void intInvalidatesLong() {
        InstructionSequence seq = new InstructionSequence();
        seq.add(new JVMInstruction(JavaOpcodes.LSTORE, new LocalVariableSlot(0)));
        schema = new JVMStateSchema(seq);

        LocalVariableStorage vars = new LocalVariableStorage(schema);
        vars.setValue(1, Type.INT_TYPE, new NumericConstantTreeNode(Type.INT_TYPE, 0));

        Assert.assertNull(vars.getLocalVariable(0));
        Assert.assertTrue(vars.getLocalVariable(1).hasValue());
        Assert.assertTrue(vars.getLocalVariable(1).getType().equals(Type.INT_TYPE));
    }


}
