package ru.ifmo.gpo.tests.java;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.Settings;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.SequenceGenerationFailedException;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.instructions.generic.IGenericInstruction;
import ru.ifmo.gpo.java.JavaBytecodeAnalyzer;
import ru.ifmo.gpo.java.JavaTargetArchitecture;
import ru.ifmo.gpo.java.instructions.JVMInstruction;
import ru.ifmo.gpo.java.instructions.JavaOpcodes;
import ru.ifmo.gpo.java.vmstate.*;
import ru.ifmo.gpo.verify.UnknownValueNode;

/**
 * User: e_smirnov
 * Date: 14.12.2010
 * Time: 15:52:16
 */
public class InstructionSequenceGeneratorTest {

    private JVMStateSchema schema;

    @Before
    public void setUp() {
        schema = new JVMStateSchema();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSingleIntLoad() throws SequenceGenerationFailedException {
        schema.addOrUpdateSlot(0, Type.INT_TYPE, GenericOperand.AccessType.ANY);

        VMState source = new VMState(schema);
        VMState target = new VMState(schema);
        source.getLocalVars().setValue(0, Type.INT_TYPE, new UnknownValueNode());
        target.getStackState().applyPush(new VerifyableOperandStackItem(Type.INT_TYPE, new UnknownValueNode()));

        InstructionSequence seq = null;
        int retryCount = 0;
        JavaTargetArchitecture arch = new JavaTargetArchitecture();
        do {
            try {
                seq = arch.getInstructionSequenceGenerator().generateInstructionSequence(source, target, 1);
            } catch (SequenceGenerationFailedException ex) {
                if (++retryCount > Settings.getRetryCount()) {
                    throw ex;
                }
            }
        } while (seq == null);

        JavaBytecodeAnalyzer analyzer = new JavaBytecodeAnalyzer(seq, schema);
        VMStateDelta delta = VMStateDelta.create(analyzer.getStateAfter(), target);
        assert delta.getDelta() == 0;
    }

    @Test
    public void testIntFloatLoad() throws SequenceGenerationFailedException {
        schema.addOrUpdateSlot(0, Type.INT_TYPE, GenericOperand.AccessType.ANY);

        VMState source = new VMState(schema);
        VMState target = new VMState(schema);
        source.getLocalVars().setValue(0, Type.INT_TYPE, new UnknownValueNode());
        target.getStackState().applyPush(new VerifyableOperandStackItem(Type.INT_TYPE, new UnknownValueNode()));
        target.getStackState().applyPush(new VerifyableOperandStackItem(Type.FLOAT_TYPE, new UnknownValueNode()));

        JavaTargetArchitecture arch = new JavaTargetArchitecture();

        InstructionSequence seq = null;
        int retryCount = 0;

        do {
            try {
                seq = arch.getInstructionSequenceGenerator().generateInstructionSequence(source, target, 1);
            } catch (SequenceGenerationFailedException ex) {
                if (++retryCount > Settings.getRetryCount()) {
                    throw ex;
                }
            }
        } while (seq == null);
        JavaBytecodeAnalyzer analyzer = new JavaBytecodeAnalyzer(seq, schema);
        VMStateDelta delta = VMStateDelta.create(analyzer.getStateAfter(), target);
        assert delta.getDelta() == 0;
    }

    @Test
    public void testLocalVarWrite() throws SequenceGenerationFailedException {
        schema.addOrUpdateSlot(0, Type.INT_TYPE, GenericOperand.AccessType.ANY);

        VMState source = new VMState(schema);
        VMState target = new VMState(schema);
        source.getLocalVars().setValue(0, Type.INT_TYPE, new UnknownValueNode());
        target.getLocalVars().setValue(0, Type.FLOAT_TYPE, new UnknownValueNode());

        InstructionSequence seq = null;
        JavaTargetArchitecture arch = new JavaTargetArchitecture();
        int retryCount = 0;

        do {
            try {
                seq = arch.getInstructionSequenceGenerator().generateInstructionSequence(source, target, 1);
            } catch (SequenceGenerationFailedException ex) {
                if (++retryCount > Settings.getRetryCount()) {
                    throw ex;
                }
            }
        } while (seq == null);
        JavaBytecodeAnalyzer analyzer = new JavaBytecodeAnalyzer(seq, schema);
        VMStateDelta delta = VMStateDelta.create(analyzer.getStateAfter(), target);
        assert delta.getDelta() == 0;

        for (IGenericInstruction instr : seq) {
            JVMInstruction jvmInstr = (JVMInstruction) instr;
            if (jvmInstr.getOpcodeInternal().equals(JavaOpcodes.FSTORE) && (((LocalVariableSlot) jvmInstr.getParameter(0)).getIdx() == 0)) {
                return;
            }
        }
        assert false;
    }
}
