package ru.ifmo.gpo.tests.java;

import org.junit.Assert;
import org.junit.Test;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import ru.ifmo.gpo.java.codegen.*;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;

/**
 * User: e_smirnov
 * Date: 17.11.2010
 * Time: 16:44:53
 */
public class CodeFragmentsTest extends ClassLoader {

    public static void dumpCode(byte[] code) {
        try {
            OutputStream os = new FileOutputStream("Code.class");
            os.write(code);
            os.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static class TestCodeFragment implements ICodeFragment {
        @Override
        public void appendCode(MethodVisitor mw) {
            // empty
        }

        @Override
        public void collectMethodParameterDescriptions(MethodDescriptor methodDescriptor) {
            methodDescriptor.addParameter(0, Type.INT_TYPE.getDescriptor());
            methodDescriptor.addParameter(1, Type.getType(LinkedList.class).getDescriptor());
            methodDescriptor.setReturnValue(Type.VOID_TYPE.getDescriptor());
        }
    }

    private static class ConstantReturningFragment implements ICodeFragment {

        @Override
        public void appendCode(MethodVisitor mw) {
            mw.visitInsn(Opcodes.ICONST_1);
            mw.visitInsn(Opcodes.IRETURN);
        }

        @Override
        public void collectMethodParameterDescriptions(MethodDescriptor methodDescriptor) {
            methodDescriptor.setReturnValue(Type.INT_TYPE.getDescriptor());
        }
    }

    private static class ParameterIncFragment implements ICodeFragment {
        private int varIdx;

        private ParameterIncFragment(int varIdx) {
            this.varIdx = varIdx;
        }

        @Override
        public void appendCode(MethodVisitor mw) {
            mw.visitIincInsn(varIdx, 1);
        }

        @Override
        public void collectMethodParameterDescriptions(MethodDescriptor methodDescriptor) {
            methodDescriptor.addParameter(varIdx, Type.INT_TYPE.getDescriptor());
        }
    }

    @Test
    public void testMethodDescriptor() {
        PreparedMethod method = new PreparedMethod("run");
        method.addCodeFragment(new TestCodeFragment());

        Assert.assertEquals(Type.getMethodDescriptor(Type.VOID_TYPE, new Type[]{Type.INT_TYPE, Type.getType(LinkedList.class)}), method.getMethodDescriptor());
    }

    @Test
    public void testPreparedClass() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        PreparedClass preparedClass = new PreparedClass("TestPreparedClass1");
        PreparedMethod preparedMethod = new PreparedMethod("run");
        preparedMethod.addCodeFragment(new ConstantReturningFragment());
        preparedClass.addMethod(preparedMethod);

        byte[] code = preparedClass.getCode();
        Class testClass = defineClass(preparedClass.getName(), code, 0, code.length);

        Assert.assertEquals(testClass.getMethod("run").invoke(null), 1);
    }

    @Test(expected = IllegalStateException.class)
    public void testMethodDescriptionDifferentReturnValues() {
        ReturnVariableCodeFragment first = new ReturnVariableCodeFragment(Type.INT_TYPE, 1);
        ReturnVariableCodeFragment second = new ReturnVariableCodeFragment(Type.LONG_TYPE, 1);

        MethodDescriptor methodDescriptor = new MethodDescriptor();
        first.collectMethodParameterDescriptions(methodDescriptor);
        second.collectMethodParameterDescriptions(methodDescriptor); // here will be illegal state exception, as both fragments will try to set return value
    }

    @Test
    public void testGetCurrentTime() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        PreparedClass preparedClass = new PreparedClass("TimeGetter");
        PreparedMethod method = preparedClass.createMethod("getTime");
        method.addCodeFragment(new GetTimeCodeFragment(0));
        method.addCodeFragment(new ReturnVariableCodeFragment(Type.LONG_TYPE, 0));

        byte[] code = preparedClass.getCode();
        Class testClass = defineClass(preparedClass.getName(), code, 0, code.length);

        Long time = (Long) testClass.getMethod("getTime").invoke(null);
        Assert.assertTrue(Math.abs(System.nanoTime() - time) < 5000000000L); // let's assume  5 seconds will be enough precision
    }

    @Test
    public void testMultipleLoops() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        PreparedClass preparedClass = new PreparedClass("MultipleLoopTest");
        PreparedMethod method = preparedClass.createMethod("run");
        method.addCodeFragment(new LoopCodeFragment(new ParameterIncFragment(0), 10, 1));
        method.addCodeFragment(new LoopCodeFragment(new ParameterIncFragment(0), 10, 1));
        method.addCodeFragment(new ReturnVariableCodeFragment(Type.INT_TYPE, 0));

        byte[] code = preparedClass.getCode();
        Class testClass = defineClass(preparedClass.getName(), code, 0, code.length);

        Integer rz = (Integer) testClass.getMethod("run", int.class).invoke(null, 0);

        Assert.assertEquals(Integer.valueOf(20), rz);
    }


}
