package ru.ifmo.gpo.tests.java;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.GenericOperand;
import ru.ifmo.gpo.core.vmstate.NoSuitableOperandFoundException;
import ru.ifmo.gpo.java.JavaBytecodeAnalyzer;
import ru.ifmo.gpo.java.instructions.JVMInstruction;
import ru.ifmo.gpo.java.instructions.JavaOpcodes;
import ru.ifmo.gpo.java.vmstate.JVMStateSchema;
import ru.ifmo.gpo.java.vmstate.LocalVariableSlot;
import ru.ifmo.gpo.java.vmstate.VMState;
import ru.ifmo.gpo.java.vmstate.VerifyableOperandStackItem;
import ru.ifmo.gpo.verify.UnknownValueNode;

import java.util.List;

/**
 * User: e_smirnov
 * Date: 16.12.2010
 * Time: 12:40:38
 */
public class OperandSelectorTest {

    private JVMStateSchema schema;

    @Before
    public void setUp() {
        schema = new JVMStateSchema();
    }

    @Test
    public void testSelectReadVariable() throws NoSuitableOperandFoundException {
        schema.addOrUpdateSlot(0, Type.INT_TYPE, GenericOperand.AccessType.READ);
        VMState source = new VMState(schema);
        source.getLocalVars().fillWithDefaultValues();

        VMState target = new VMState(schema);
        target.getStackState().applyPush(new VerifyableOperandStackItem(Type.INT_TYPE, new UnknownValueNode()));

        Object operand = JavaOpcodes.ILOAD.getOperandSelector().selectOperandsRandom(JavaOpcodes.ILOAD, source).get(0);
        assert operand.equals(new LocalVariableSlot(0));
    }

    @Test
    public void testSelectWriteVariable() throws NoSuitableOperandFoundException {
        schema.addOrUpdateSlot(1, Type.INT_TYPE, GenericOperand.AccessType.WRITE);

        VMState source = new VMState(schema);
        source.getLocalVars().fillWithDefaultValues();
        source.getStackState().applyPush(new VerifyableOperandStackItem(Type.INT_TYPE, new UnknownValueNode()));

        Object operand = JavaOpcodes.ISTORE.getOperandSelector().selectOperandsRandom(JavaOpcodes.ISTORE, source).get(0);
        Assert.assertEquals(1, ((LocalVariableSlot) operand).getIdx());
    }

    @Test(expected = NoSuitableOperandFoundException.class)
    public void testSelectWriteVariableFailed() throws NoSuitableOperandFoundException {
        VMState source = new VMState(schema);
        source.getLocalVars().fillWithDefaultValues();
        source.getStackState().applyPush(new VerifyableOperandStackItem(Type.FLOAT_TYPE, new UnknownValueNode()));

        Object operand = JavaOpcodes.FSTORE.getOperandSelector().selectOperandsRandom(JavaOpcodes.FSTORE, source);
    }

    @Test(expected = NoSuitableOperandFoundException.class)
    public void testAllowedReadButDisallowedWrite() throws NoSuitableOperandFoundException {
        schema.addOrUpdateSlot(0, Type.INT_TYPE, GenericOperand.AccessType.READ);

        VMState source = new VMState(schema);
        source.getLocalVars().fillWithDefaultValues();

        source.getStackState().applyPush(new VerifyableOperandStackItem(Type.FLOAT_TYPE, new UnknownValueNode()));

        Object operand = JavaOpcodes.FSTORE.getOperandSelector().selectOperandsRandom(JavaOpcodes.FSTORE, source);
    }

    @Test
    public void testCodeFromGeneratorTest() throws NoSuitableOperandFoundException {
        InstructionSequence ref = new InstructionSequence();
        ref.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        ref.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(1)));
        ref.add(new JVMInstruction(JavaOpcodes.FCONST_2));
        ref.add(new JVMInstruction(JavaOpcodes.FDIV));
        ref.add(new JVMInstruction(JavaOpcodes.F2I));
        ref.add(new JVMInstruction(JavaOpcodes.IADD));
        ref.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(2)));
        schema = new JVMStateSchema(ref);
        JavaBytecodeAnalyzer analyzer = new JavaBytecodeAnalyzer(ref, schema);

        Object operand = JavaOpcodes.ISTORE.getOperandSelector().selectOparandsSatisfyRestrictions(JavaOpcodes.ISTORE, analyzer.getStateBefore(), analyzer.getStateAfter(), analyzer.getStateChange()).get(0);
        assert operand.equals(new LocalVariableSlot(2));
    }

    @Test(expected = NoSuitableOperandFoundException.class)
    public void testNoLongAvailableForReadingAfterPartialOverwrite() throws NoSuitableOperandFoundException {
        InstructionSequence ref = new InstructionSequence();
        ref.add(new JVMInstruction(JavaOpcodes.LSTORE, new LocalVariableSlot(0)));
        ref.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(1)));
        schema = new JVMStateSchema(ref);
        JavaBytecodeAnalyzer analyzer = new JavaBytecodeAnalyzer(ref, schema);
        Object operand = JavaOpcodes.LLOAD.getOperandSelector().selectOperandsRandom(JavaOpcodes.LLOAD, analyzer.getStateAfter());
    }

    @Test
    public void testIINCSelector() throws NoSuitableOperandFoundException {
        InstructionSequence ref = new InstructionSequence();
        ref.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        ref.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(1)));
        ref.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(0)));
        schema = new JVMStateSchema(ref);
        JavaBytecodeAnalyzer analyzer = new JavaBytecodeAnalyzer(ref, schema);
        List<GenericOperand> operands = JavaOpcodes.IINC.getOperandSelector().selectOperandsRandom(JavaOpcodes.IINC, analyzer.getStateAfter());
        Assert.assertEquals(0, ((LocalVariableSlot) operands.get(0)).getIdx());
    }

    @Test(expected = NoSuitableOperandFoundException.class)
    public void testExceptionOnPOPofItemOfSecondType() throws NoSuitableOperandFoundException
    {
        InstructionSequence ref = new InstructionSequence();
        ref.add(new JVMInstruction(JavaOpcodes.LLOAD, new LocalVariableSlot(0)));
        schema = new JVMStateSchema(ref);
        JavaBytecodeAnalyzer analyzer = new JavaBytecodeAnalyzer(ref, schema);
        JavaOpcodes.POP.getOperandSelector().selectOperandsRandom(JavaOpcodes.POP, analyzer.getStateAfter());
    }
}
