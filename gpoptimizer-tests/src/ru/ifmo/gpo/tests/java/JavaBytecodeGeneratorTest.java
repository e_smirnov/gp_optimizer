package ru.ifmo.gpo.tests.java;

import org.jgap.Configuration;
import org.jgap.InvalidConfigurationException;
import org.jgap.UnsupportedRepresentationException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.ifmo.gpo.core.GPConfiguration;
import ru.ifmo.gpo.core.ICodeGenerationResult;
import ru.ifmo.gpo.core.ICodeGenerator;
import ru.ifmo.gpo.core.MainIslandGPConfiguration;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.instructions.generic.NumericConstant;
import ru.ifmo.gpo.java.JavaBytecodeAnalyzer;
import ru.ifmo.gpo.java.JavaBytecodeGenerator;
import ru.ifmo.gpo.java.JavaCodeGenerationResult;
import ru.ifmo.gpo.java.JavaTargetArchitecture;
import ru.ifmo.gpo.java.instructions.JVMInstruction;
import ru.ifmo.gpo.java.instructions.JavaOpcodes;
import ru.ifmo.gpo.java.vmstate.JVMStateSchema;
import ru.ifmo.gpo.java.vmstate.LocalVariableSlot;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;

/**
 * User: jedi-philosopher
 * Date: 16.01.2010
 * Time: 16:32:20
 */
public class JavaBytecodeGeneratorTest extends ClassLoader {

    static GPConfiguration conf;

    @Before
    public void setConf() throws InvalidConfigurationException {
        Configuration.reset();
        conf = new MainIslandGPConfiguration(new JavaTargetArchitecture());
        JavaBytecodeGenerator.cleanCache();
    }

    @After
    public void resetConf() {
        conf = null;
    }

    @SuppressWarnings("unused")
    private void dumpCode(JavaCodeGenerationResult res) {
        try {
            OutputStream os = new FileOutputStream("Code.class");
            os.write(res.getCode());
            os.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSandboxedBytecodeGeneration() throws InvalidConfigurationException, UnsupportedRepresentationException, InvocationTargetException, IllegalAccessException {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.IMUL));

        JVMStateSchema schema = new JVMStateSchema(sequence);

        ICodeGenerator gen = conf.getTargetArchitecture().getCodeGenerator();
        ICodeGenerationResult res = gen.generateSandboxedCode(sequence, schema, null);

        Class testClass = defineClass(res.getName(), ((JavaCodeGenerationResult) res).getCode(), 0, ((JavaCodeGenerationResult) res).getCode().length);
        LinkedList<Object> operandStack = new LinkedList<Object>();
        testClass.getMethods()[0].invoke(null, 5, operandStack);

        assert operandStack.size() == 1;
        assert operandStack.get(0).equals(25);

    }

    @SuppressWarnings({"ShiftOutOfRange"})
    @Test
    public void testLargeSandboxedCode() throws InvocationTargetException, IllegalAccessException {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_M1));
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_M1));
        sequence.add(new JVMInstruction(JavaOpcodes.ISHL));
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_M1));
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_M1));
        sequence.add(new JVMInstruction(JavaOpcodes.ISHL));
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_M1));
        sequence.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.INEG));
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_M1));
        sequence.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.INEG));

        JVMStateSchema schema = new JVMStateSchema(sequence);

        ICodeGenerationResult res = conf.getTargetArchitecture().getCodeGenerator().generateSandboxedCode(sequence, schema, null);
        Class testClass = defineClass(res.getName(), ((JavaCodeGenerationResult) res).getCode(), 0, ((JavaCodeGenerationResult) res).getCode().length);

        LinkedList<Object> operandStack = new LinkedList<Object>();
        testClass.getMethods()[0].invoke(null, 5, operandStack);

        assert operandStack.size() == 3; //2 is for stack and 1 for local variable
        assert operandStack.get(0).equals(-1 << -1);
        assert operandStack.get(1).equals(-1 << -1);
        assert operandStack.get(2).equals(-1);

    }

    @Test
    public void testSandboxedCodegenWithUnderflow() throws InvalidConfigurationException, UnsupportedRepresentationException, InvocationTargetException, IllegalAccessException {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.IMUL));
        JVMStateSchema schema = new JVMStateSchema(sequence);

        ICodeGenerator gen = conf.getTargetArchitecture().getCodeGenerator();
        ICodeGenerationResult res = gen.generateSandboxedCode(sequence, schema, null);

        Class testClass = defineClass(res.getName(), ((JavaCodeGenerationResult) res).getCode(), 0, ((JavaCodeGenerationResult) res).getCode().length);
        LinkedList<Object> operandStack = new LinkedList<Object>();
        testClass.getMethods()[0].invoke(null, 6, 5, operandStack);

        assert operandStack.size() == 1;
        assert operandStack.get(0).equals(30);
    }

    @Test
    public void testSandboxedSimpleIntAndFloat() throws InvocationTargetException, IllegalAccessException {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.I2F));
        sequence.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(1)));
        sequence.add(new JVMInstruction(JavaOpcodes.FDIV));

        JVMStateSchema schema = new JVMStateSchema(sequence);

        ICodeGenerator gen = conf.getTargetArchitecture().getCodeGenerator();
        ICodeGenerationResult res = gen.generateSandboxedCode(sequence, schema, null);

        Class testClass = defineClass(res.getName(), ((JavaCodeGenerationResult) res).getCode(), 0, ((JavaCodeGenerationResult) res).getCode().length);
        LinkedList<Object> operandStack = new LinkedList<Object>();
        testClass.getMethods()[0].invoke(null, 5, 2.5f, operandStack);

        assert operandStack.size() == 1;
        assert Math.abs((Float) operandStack.get(0) - 2.0f) < 0.001f;
    }

    @Test
    public void testFloatLocalVarReturn() throws InvocationTargetException, IllegalAccessException {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.FCONST_2));
        sequence.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.FMUL));
        sequence.add(new JVMInstruction(JavaOpcodes.FSTORE, new LocalVariableSlot(0)));

        JVMStateSchema schema = new JVMStateSchema(sequence);

        ICodeGenerator gen = conf.getTargetArchitecture().getCodeGenerator();
        ICodeGenerationResult res = gen.generateSandboxedCode(sequence, schema, null);

        Class testClass = defineClass(res.getName(), ((JavaCodeGenerationResult) res).getCode(), 0, ((JavaCodeGenerationResult) res).getCode().length);
        LinkedList<Object> operandStack = new LinkedList<Object>();
        testClass.getMethods()[0].invoke(null, 12.5f, operandStack);

        assert operandStack.size() == 1;
        assert Math.abs((Float) operandStack.get(0) - 25.0f) < 0.001f;
    }

    @Test
    public void testProfileCodegen() throws InvocationTargetException, IllegalAccessException, UnsupportedRepresentationException, InvalidConfigurationException {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.IMUL));

        JVMStateSchema schema = new JVMStateSchema(sequence);

        ICodeGenerator gen = conf.getTargetArchitecture().getCodeGenerator();
        ICodeGenerationResult res = gen.generateProfileCode(sequence, schema, null);

        Class testClass = defineClass(res.getName(), ((JavaCodeGenerationResult) res).getCode(), 0, ((JavaCodeGenerationResult) res).getCode().length);
        LinkedList<Object> operandStack = new LinkedList<Object>();
        Long profileTime = (Long) testClass.getMethods()[0].invoke(null, 5, operandStack);
        assert profileTime > 0L;
    }

    @Test
    public void testProfileCodegenTwoTypes() throws InvocationTargetException, IllegalAccessException {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.IMUL));
        sequence.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(1)));
        sequence.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(2)));
        sequence.add(new JVMInstruction(JavaOpcodes.FMUL));
        sequence.add(new JVMInstruction(JavaOpcodes.F2I));

        JVMStateSchema schema = new JVMStateSchema(sequence);

        ICodeGenerator gen = conf.getTargetArchitecture().getCodeGenerator();
        ICodeGenerationResult res = gen.generateProfileCode(sequence, schema, null);

        Class testClass = defineClass(res.getName(), ((JavaCodeGenerationResult) res).getCode(), 0, ((JavaCodeGenerationResult) res).getCode().length);
        LinkedList<Object> operandStack = new LinkedList<Object>();
        Long profileTime = (Long) testClass.getMethods()[0].invoke(null, 5, 1.0f, -123.543f, operandStack);
        assert profileTime > 0L;
    }

    @Test
    public void testProfileCodegenComplex() throws InvocationTargetException, IllegalAccessException {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        sequence.add(new JVMInstruction(JavaOpcodes.ISHR));
        sequence.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(3)));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(3)));
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        sequence.add(new JVMInstruction(JavaOpcodes.ISHR));
        sequence.add(new JVMInstruction(JavaOpcodes.IXOR));
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        sequence.add(new JVMInstruction(JavaOpcodes.IAND));

        JVMStateSchema schema = new JVMStateSchema(sequence);

        ICodeGenerator gen = conf.getTargetArchitecture().getCodeGenerator();
        ICodeGenerationResult res = gen.generateProfileCode(sequence, schema, null);
        Class testClass = defineClass(res.getName(), ((JavaCodeGenerationResult) res).getCode(), 0, ((JavaCodeGenerationResult) res).getCode().length);
        LinkedList<Object> operandStack = new LinkedList<Object>();
        Long profileTime = (Long) testClass.getMethods()[0].invoke(null, 10, 0, 0, 5, operandStack);
        assert profileTime > 0L;

    }


    @Test
    public void testConsecutiveWritesToSameRegisterDifferentTypesWithSourceAnalyzer() throws InvocationTargetException, IllegalAccessException {
        InstructionSequence source = new InstructionSequence();
        source.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        source.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(1)));
        source.add(new JVMInstruction(JavaOpcodes.IADD));
        source.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(2)));

        JVMStateSchema schema = new JVMStateSchema(source);

        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(1)));
        sequence.add(new JVMInstruction(JavaOpcodes.IMUL));
        sequence.add(new JVMInstruction(JavaOpcodes.FCONST_0));
        sequence.add(new JVMInstruction(JavaOpcodes.FSTORE, new LocalVariableSlot(2)));
        sequence.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(2)));

        ICodeGenerator gen = conf.getTargetArchitecture().getCodeGenerator();
        ICodeGenerationResult res = gen.generateSandboxedCode(sequence, schema, new JavaBytecodeAnalyzer(source, schema));

        Class testClass = defineClass(res.getName(), ((JavaCodeGenerationResult) res).getCode(), 0, ((JavaCodeGenerationResult) res).getCode().length);
        LinkedList<Object> operandStack = new LinkedList<Object>();
        testClass.getMethods()[0].invoke(null, 1, 5, 0, operandStack);

        Assert.assertEquals(1, operandStack.size());
        Assert.assertEquals(5, operandStack.get(0));
    }

    @Test
    public void testUntypedFirstInstruction() throws IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException {
        InstructionSequence source = new InstructionSequence();
        source.add(new JVMInstruction(JavaOpcodes.DUP));
        source.add(new JVMInstruction(JavaOpcodes.IMUL));

        JVMStateSchema schema = new JVMStateSchema(source);

        ICodeGenerator gen = conf.getTargetArchitecture().getCodeGenerator();
        ICodeGenerationResult res = gen.generateSandboxedCode(source, schema, new JavaBytecodeAnalyzer(source, schema));

        Class testClass = defineClass(res.getName(), ((JavaCodeGenerationResult) res).getCode(), 0, ((JavaCodeGenerationResult) res).getCode().length);
        LinkedList<Object> operandStack = new LinkedList<Object>();
        testClass.getMethods()[0].invoke(null, 2, operandStack);

        Assert.assertEquals(1, operandStack.size());
        Assert.assertEquals(4, operandStack.get(0));
    }

    @Test
    public void testBIPUSH() throws IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException {
        InstructionSequence source = new InstructionSequence();
        source.add(new JVMInstruction(JavaOpcodes.BIPUSH, new NumericConstant(31)));

        JVMStateSchema schema = new JVMStateSchema(source);

        ICodeGenerator gen = conf.getTargetArchitecture().getCodeGenerator();
        ICodeGenerationResult res = gen.generateSandboxedCode(source, schema, new JavaBytecodeAnalyzer(source, schema));

        Class testClass = defineClass(res.getName(), ((JavaCodeGenerationResult) res).getCode(), 0, ((JavaCodeGenerationResult) res).getCode().length);
        LinkedList<Object> operandStack = new LinkedList<Object>();
        testClass.getMethods()[0].invoke(null, operandStack);

        Assert.assertEquals(1, operandStack.size());
        Assert.assertEquals(31, operandStack.get(0));
    }

    @Test
    public void testIINC() throws InvocationTargetException, IllegalAccessException {
        InstructionSequence source = new InstructionSequence();
        source.add(new JVMInstruction(JavaOpcodes.IINC, new LocalVariableSlot(0), new NumericConstant(2)));

        JVMStateSchema schema = new JVMStateSchema(source);

        ICodeGenerator gen = conf.getTargetArchitecture().getCodeGenerator();
        ICodeGenerationResult res = gen.generateSandboxedCode(source, schema, new JavaBytecodeAnalyzer(source, schema));

        Class testClass = defineClass(res.getName(), ((JavaCodeGenerationResult) res).getCode(), 0, ((JavaCodeGenerationResult) res).getCode().length);
        LinkedList<Object> operandStack = new LinkedList<Object>();
        testClass.getMethods()[0].invoke(null, 1, operandStack);

        Assert.assertEquals(1, operandStack.size());
        Assert.assertEquals(3, operandStack.get(0));
    }

    @Test
    public void testCorrectMethodParamsWith2SlotType() throws InvocationTargetException, IllegalAccessException {
        InstructionSequence source = new InstructionSequence();
        source.add(new JVMInstruction(JavaOpcodes.LSTORE, new LocalVariableSlot(3)));
        source.add(new JVMInstruction(JavaOpcodes.LLOAD, new LocalVariableSlot(3)));
        source.add(new JVMInstruction(JavaOpcodes.LCONST_0));

        JVMStateSchema schema = new JVMStateSchema(source);

        ICodeGenerator gen = conf.getTargetArchitecture().getCodeGenerator();
        ICodeGenerationResult res = gen.generateSandboxedCode(source, schema, new JavaBytecodeAnalyzer(source, schema));

        Class testClass = defineClass(res.getName(), ((JavaCodeGenerationResult) res).getCode(), 0, ((JavaCodeGenerationResult) res).getCode().length);
        LinkedList<Object> operandStack = new LinkedList<Object>();
        testClass.getMethods()[0].invoke(null, 0, 1, 2, 3L, 4L, operandStack);

        Assert.assertEquals(3, operandStack.size());
    }
}
