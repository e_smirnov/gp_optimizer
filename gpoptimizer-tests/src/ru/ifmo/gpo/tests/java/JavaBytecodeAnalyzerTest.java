package ru.ifmo.gpo.tests.java;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.core.vmstate.IMachineState;
import ru.ifmo.gpo.java.JavaBytecodeAnalyzer;
import ru.ifmo.gpo.java.instructions.JVMInstruction;
import ru.ifmo.gpo.java.instructions.JavaOpcodes;
import ru.ifmo.gpo.java.vmstate.*;
import ru.ifmo.gpo.verify.*;

import java.util.List;
import java.util.Stack;

/**
 * User: jedi-philosopher
 * Date: 03.09.2010
 * Time: 0:06:49
 */
public class JavaBytecodeAnalyzerTest {

    public InstructionSequence generateSimpleInstructionSequence() {
        // x^2
        InstructionSequence testSequence = new InstructionSequence();
        testSequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        testSequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        testSequence.add(new JVMInstruction(JavaOpcodes.IMUL));
        return testSequence;
    }

    public InstructionSequence generateComplexInstructionSequence() {
        // 3x^2 - 1
        InstructionSequence testSequence = new InstructionSequence();
        testSequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        testSequence.add(new JVMInstruction(JavaOpcodes.DUP));
        testSequence.add(new JVMInstruction(JavaOpcodes.ICONST_3));
        testSequence.add(new JVMInstruction(JavaOpcodes.IMUL));
        testSequence.add(new JVMInstruction(JavaOpcodes.IMUL));
        testSequence.add(new JVMInstruction(JavaOpcodes.ICONST_M1));
        testSequence.add(new JVMInstruction(JavaOpcodes.IADD));
        return testSequence;
    }

    @Before
    public void setUp() {
        // check that assertions are enabled
        boolean assertionsEnabled = false;
        assert (assertionsEnabled = true);
        Assert.assertTrue("Assertions must be enabled (-ea JVM command-line option)", assertionsEnabled);
    }

    @Test
    public void testDoesNotCrashOnEmptySequence() {
        InstructionSequence seq = new InstructionSequence();
        JavaBytecodeAnalyzer analyzer = new JavaBytecodeAnalyzer(seq, new JVMStateSchema(seq));
        assert analyzer.getNeededStackBefore().size() == 0;
        assert analyzer.getStackAfter().size() == 0;
        assert analyzer.getStateChange().getDelta() == 0;
    }

    @Test
    public void test_analyzeSimple() {
        InstructionSequence seq = generateSimpleInstructionSequence();
        JavaBytecodeAnalyzer analyzer = new JavaBytecodeAnalyzer(seq, new JVMStateSchema(seq));

        assert analyzer.getNeededStackBefore().size() == 0;
        assert analyzer.getStackAfter().size() == 1;
        assert analyzer.getStackAfter().get(0).getType() == Type.INT_TYPE;
        assert analyzer.getStackAfter().get(0).getExpression() instanceof BinaryOperatorNode;
    }

    @Test
    public void test_stackUnderflow() {
        InstructionSequence testSequence = new InstructionSequence();
        testSequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        testSequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        testSequence.add(new JVMInstruction(JavaOpcodes.IMUL));
        testSequence.add(new JVMInstruction(JavaOpcodes.ICONST_0));
        testSequence.add(new JVMInstruction(JavaOpcodes.POP));
        testSequence.add(new JVMInstruction(JavaOpcodes.IADD));

        JavaBytecodeAnalyzer analyzer = new JavaBytecodeAnalyzer(testSequence, new JVMStateSchema(testSequence));

        assert analyzer.getNeededStackBefore().size() == 1;
        assert analyzer.getNeededStackBefore().get(0).getType() == Type.INT_TYPE;
        assert analyzer.getStackAfter().size() == 1;
        assert analyzer.getStackAfter().get(0).getType().equals(Type.INT_TYPE);
        assert analyzer.getStateAfter().getLocalVars().getLocalVariable(0).getType() == Type.INT_TYPE;
    }

    @Test
    public void test_treeConstruction() {
        // testing equation x*x + y
        InstructionSequence testSequence = new InstructionSequence();
        testSequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        testSequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        testSequence.add(new JVMInstruction(JavaOpcodes.IMUL));
        testSequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(1)));
        testSequence.add(new JVMInstruction(JavaOpcodes.IADD));

        JavaBytecodeAnalyzer analyzer = new JavaBytecodeAnalyzer(testSequence, new JVMStateSchema(testSequence));
        assert analyzer.getStackAfter().size() == 1;

        IArithmeticTreeNode root = analyzer.getStackAfter().get(0).getExpression();
        assert root instanceof BinaryOperatorNode;

        BinaryOperatorNode rootNode = (BinaryOperatorNode) root;
        assert rootNode.getLeft() instanceof BinaryOperatorNode;
        assert rootNode.getOperator().equals(GenericOperator.PLUS);
        assert rootNode.getRight() instanceof VariableNode;
        assert ((VariableNode) rootNode.getRight()).getName().equals(VariableNameGenerator.getInstance().getLocalVarName(1));

        assert analyzer.getStateAfter().getLocalVars().getSchema().getSlot(0).getType() == Type.INT_TYPE;
        assert analyzer.getStateAfter().getLocalVars().getSchema().getSlot(1).getType() == Type.INT_TYPE;
    }

    @Test
    public void test_analyzeComplex() {
        InstructionSequence seq = generateComplexInstructionSequence();
        JavaBytecodeAnalyzer analyzer = new JavaBytecodeAnalyzer(seq, new JVMStateSchema(seq));

        assert analyzer.getNeededStackBefore().size() == 0;
        assert analyzer.getStackAfter().size() == 1;
        assert analyzer.getStackAfter().get(0).getType().equals(Type.INT_TYPE);
        VMState state = analyzer.getStateAfter();
        assert state.getLocalVars().getSchema().getSlot(0).getType().equals(Type.INT_TYPE);
    }

    @Test
    public void test_intermediateStackStates() {
        InstructionSequence seq = generateComplexInstructionSequence();
        JavaBytecodeAnalyzer analyzer = new JavaBytecodeAnalyzer(seq, new JVMStateSchema(seq));
        List<? extends IMachineState> intermediateStates = analyzer.getStates();

        assert ((VMState) intermediateStates.get(0)).getStackState().size() == 1;
        assert ((VMState) intermediateStates.get(1)).getStackState().size() == 2;
        assert ((VMState) intermediateStates.get(2)).getStackState().size() == 3;
        for (VerifyableOperandStackItem i : ((VMState) intermediateStates.get(2)).getStackState()) {
            assert i.getType().equals(Type.INT_TYPE);
        }

        assert ((VMState) intermediateStates.get(3)).getStackState().size() == 2;
        assert ((VMState) intermediateStates.get(4)).getStackState().size() == 1;
        assert ((VMState) intermediateStates.get(5)).getStackState().size() == 2;
        assert ((VMState) intermediateStates.get(6)).getStackState().size() == 1;
    }

    @Test
    public void test_differentOpcodes() {
        InstructionSequence testSequence = new InstructionSequence();
        testSequence.add(new JVMInstruction(JavaOpcodes.ICONST_3));
        testSequence.add(new JVMInstruction(JavaOpcodes.NOP));
        testSequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        testSequence.add(new JVMInstruction(JavaOpcodes.DUP));
        testSequence.add(new JVMInstruction(JavaOpcodes.SWAP));
        testSequence.add(new JVMInstruction(JavaOpcodes.ISHL));
        testSequence.add(new JVMInstruction(JavaOpcodes.POP));

        JavaBytecodeAnalyzer analyzer = new JavaBytecodeAnalyzer(testSequence, new JVMStateSchema(testSequence));

        assert analyzer.getNeededStackBefore().size() == 0;
        assert analyzer.getStackAfter().size() == 1;
        assert analyzer.getStackAfter().get(0).getType().equals(Type.INT_TYPE);
    }

    @Test
    public void test_desiredStackOperationsArithmetics() {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ISHL));

        JavaBytecodeAnalyzer analyzer = new JavaBytecodeAnalyzer(sequence, new JVMStateSchema(sequence));

        Assert.assertEquals(1, analyzer.getStateChange().getStackOperations().size());
        Assert.assertEquals(StackOperation.Operation.PUSH, analyzer.getStateChange().getStackOperations().get(0).op);
        Assert.assertEquals(Type.INT_TYPE, analyzer.getStateChange().getStackOperations().get(0).type);
    }

    @Test
    public void test_desiredStackOperationsTwoTypes() {
        InstructionSequence testSequence = new InstructionSequence();
        testSequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        testSequence.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(1)));

        JavaBytecodeAnalyzer analyzer = new JavaBytecodeAnalyzer(testSequence, new JVMStateSchema(testSequence));

        Stack<StackOperation> stackOperations = analyzer.getStateChange().getStackOperations();
        Assert.assertTrue(stackOperations.size() == 2);
        StackOperation top = stackOperations.pop();
        Assert.assertEquals(StackOperation.Operation.PUSH, top.op);
        Assert.assertEquals(Type.INT_TYPE, top.type);

        StackOperation bottom = stackOperations.pop();
        Assert.assertEquals(StackOperation.Operation.PUSH, bottom.op);
        Assert.assertEquals(Type.FLOAT_TYPE, bottom.type);
    }

    @Test
    public void test_analyzeNOP() {
        InstructionSequence testSequence = new InstructionSequence();
        testSequence.add(new JVMInstruction(JavaOpcodes.NOP));
        JavaBytecodeAnalyzer analyzer = new JavaBytecodeAnalyzer(testSequence, new JVMStateSchema(testSequence));

        Assert.assertTrue(analyzer.getNeededStackBefore().isEmpty());
        Assert.assertTrue(analyzer.getStackAfter().isEmpty());
        Assert.assertTrue(analyzer.getStateChange().getStackOperations().isEmpty());
    }

    @Test
    public void test_desiredLocalVariables() {
        InstructionSequence testSequence = new InstructionSequence();
        testSequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        testSequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(1)));
        testSequence.add(new JVMInstruction(JavaOpcodes.IMUL));
        testSequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(3)));

        JavaBytecodeAnalyzer analyzer = new JavaBytecodeAnalyzer(testSequence, new JVMStateSchema(testSequence));
        LocalVariableStorage vars = analyzer.getStateAfter().getLocalVars();

        Assert.assertEquals(vars.getSchema().getSlot(0).getType(), Type.INT_TYPE);
        Assert.assertEquals(vars.getSchema().getSlot(1).getType(), Type.INT_TYPE);
        Assert.assertNull(vars.getSchema().getSlot(2));
        Assert.assertEquals(vars.getSchema().getSlot(3).getType(), Type.INT_TYPE);
    }

    @Test
    public void test_analyzeFloatSimple() {
        InstructionSequence testSequence = new InstructionSequence();
        testSequence.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(0)));
        testSequence.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(0)));
        testSequence.add(new JVMInstruction(JavaOpcodes.FMUL));

        JavaBytecodeAnalyzer analyzer = new JavaBytecodeAnalyzer(testSequence, new JVMStateSchema(testSequence));

        assert analyzer.getNeededStackBefore().size() == 0;
        assert analyzer.getStackAfter().size() == 1;
        assert analyzer.getStackAfter().get(0).getType() == Type.FLOAT_TYPE;
        assert analyzer.getStackAfter().get(0).getExpression() instanceof BinaryOperatorNode;
        assert analyzer.getStateAfter().getLocalVars().getSchema().getSlot(0).getType() == Type.FLOAT_TYPE;
    }

    @Test
    public void testIntToFloatConversion() {
        InstructionSequence testSequence = new InstructionSequence();
        testSequence.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(0)));
        testSequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(1)));
        testSequence.add(new JVMInstruction(JavaOpcodes.I2F));
        testSequence.add(new JVMInstruction(JavaOpcodes.FADD));
        JavaBytecodeAnalyzer analyzer = new JavaBytecodeAnalyzer(testSequence, new JVMStateSchema(testSequence));

        assert analyzer.getNeededStackBefore().size() == 0;
        assert analyzer.getStackAfter().size() == 1;
        assert analyzer.getStackAfter().get(0).getType() == Type.FLOAT_TYPE;
        assert analyzer.getStackAfter().get(0).getExpression() instanceof BinaryOperatorNode;
        assert analyzer.getStateAfter().getLocalVars().getSchema().getSlot(0).getType() == Type.FLOAT_TYPE;
        assert analyzer.getStateAfter().getLocalVars().getSchema().getSlot(1).getType() == Type.INT_TYPE;
    }

    @Test
    public void testInitialState() {
        InstructionSequence ref = new InstructionSequence();
        ref.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        ref.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(1)));
        ref.add(new JVMInstruction(JavaOpcodes.FCONST_2));
        ref.add(new JVMInstruction(JavaOpcodes.FDIV));
        ref.add(new JVMInstruction(JavaOpcodes.F2I));
        ref.add(new JVMInstruction(JavaOpcodes.IADD));
        ref.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(0)));

        JavaBytecodeAnalyzer analyzer = new JavaBytecodeAnalyzer(ref, new JVMStateSchema(ref));
        VMState initialState = analyzer.getStateBefore();

        assert initialState.getStackState().isEmpty();
        assert initialState.getStackState().getUnderflowStack().isEmpty();
        assert initialState.getLocalVars().getSchema().getSlot(0).getType().equals(Type.INT_TYPE);
        assert initialState.getLocalVars().getSchema().getSlot(1).getType().equals(Type.FLOAT_TYPE);
    }

    @Test
    public void testLongSimple() {
        InstructionSequence testSequence = new InstructionSequence();
        testSequence.add(new JVMInstruction(JavaOpcodes.LLOAD, new LocalVariableSlot(0)));
        testSequence.add(new JVMInstruction(JavaOpcodes.LLOAD, new LocalVariableSlot(0)));
        testSequence.add(new JVMInstruction(JavaOpcodes.LMUL));

        JavaBytecodeAnalyzer analyzer = new JavaBytecodeAnalyzer(testSequence, new JVMStateSchema(testSequence));
        final VMState state = analyzer.getStateAfter();

        Assert.assertEquals(1, state.getStack().size());
        Assert.assertEquals(Type.LONG_TYPE, state.getStackState().getFirst().getType());
    }

    @Test
    public void testLongAfterOverwriteOfOnePartIsNotReadible() {
        InstructionSequence testSequence = new InstructionSequence();
        testSequence.add(new JVMInstruction(JavaOpcodes.LSTORE, new LocalVariableSlot(0)));
        testSequence.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(1)));

        JavaBytecodeAnalyzer analyzer = new JavaBytecodeAnalyzer(testSequence, new JVMStateSchema(testSequence));
        final VMState state = analyzer.getStateAfter();

        Assert.assertNull(state.getLocalVars().getLocalVariable(0));
    }

    @Test
    public void testDeltaIsZero() {
        InstructionSequence testSequence = new InstructionSequence();
        testSequence.add(new JVMInstruction(JavaOpcodes.NOP));
        JavaBytecodeAnalyzer analyzer = new JavaBytecodeAnalyzer(testSequence, new JVMStateSchema(testSequence));

        Assert.assertTrue(VMStateDelta.statesEqual(analyzer.getStateBefore(), analyzer.getStateAfter()));
        Assert.assertTrue(VMStateDelta.statesEqual(analyzer.getStateAfter(), analyzer.getStateBefore()));
    }

    @Test
    public void testStackIsEmpty() {
        // example from bugfix
        InstructionSequence testSequence = new InstructionSequence();
        testSequence.add(new JVMInstruction(JavaOpcodes.POP));
        testSequence.add(new JVMInstruction(JavaOpcodes.ICONST_1));
        testSequence.add(new JVMInstruction(JavaOpcodes.ICONST_2));
        testSequence.add(new JVMInstruction(JavaOpcodes.IOR));
        testSequence.add(new JVMInstruction(JavaOpcodes.I2F));
        testSequence.add(new JVMInstruction(JavaOpcodes.POP));
        JavaBytecodeAnalyzer analyzer = new JavaBytecodeAnalyzer(testSequence, new JVMStateSchema());

        Assert.assertEquals(1, analyzer.getNeededStackBefore().size());
        Assert.assertTrue(analyzer.getStateAfter().getStackState().isEmpty());
    }

}
