package ru.ifmo.gpo.tests.java;

import org.junit.Test;
import ru.ifmo.gpo.client.PatternCollector;
import ru.ifmo.gpo.java.instructions.JavaSimpleOpcodeFactory;
import ru.ifmo.gpo.protocol.PatternItem;

import java.io.IOException;
import java.util.List;

/**
 * User: jedi-philosopher
 * Date: 27.03.2010
 * Time: 17:44:38
 */
public class PatternCollectorTest {
    @SuppressWarnings("unused")
    public static class TestClass {
        public static int testMethod(int i, float y) {
            int rz1 = i * i * i * i;
            float rz2 = y * 0.5f;
            return rz1 * (rz1 + 1);
        }
    }

    @Test
    public void test_patternCollectorBasic() {
        PatternCollector collector = new PatternCollector(JavaSimpleOpcodeFactory.getSupportedOperations(), 2, 10);
        try {
            collector.collectPatternsFromClass("ru.ifmo.gpo.tests.java.PatternCollectorTest$TestClass");
        } catch (Exception ex) {
            assert false;
        }

        List<PatternItem> collectedItems = collector.getPatterns();
        assert collectedItems.size() == 2;
    }

    @Test
    public void test_patternCollectorTestFromJIdeaClass() throws IOException {
        PatternCollector collector = new PatternCollector(JavaSimpleOpcodeFactory.getSupportedOperations(), 5, 10);

        collector.collectPatternsFromClass("java.math.BigDecimal");
        List<PatternItem> collectedItems = collector.getPatterns();
        assert !collectedItems.isEmpty();
    }
}
