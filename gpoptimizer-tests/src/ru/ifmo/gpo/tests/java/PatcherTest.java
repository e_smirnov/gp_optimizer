package ru.ifmo.gpo.tests.java;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ru.ifmo.gpo.client.Patcher;
import ru.ifmo.gpo.client.PatternItemStorage;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.java.instructions.JVMInstruction;
import ru.ifmo.gpo.java.instructions.JavaOpcodes;
import ru.ifmo.gpo.java.vmstate.LocalVariableSlot;
import ru.ifmo.gpo.protocol.PatternItem;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * User: e_smirnov
 * Date: 30.08.2010
 * Time: 14:59:00
 */
public class PatcherTest extends ClassLoader {
    public static class TestClassForPatcher {
        // iload
        // iload
        // iadd
        // iload
        // iadd
        // iret
        public static int f(int x) {
            return x + x + x;
        }
    }

    @Before
    public void setUp() {
        PatternItemStorage.init();
    }

    @After
    public void tearDown() {
        PatternItemStorage.term();
    }

    @Test
    public void test_patchClass() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, IOException {

        InstructionSequence newSequence = new InstructionSequence();
        newSequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        newSequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        newSequence.add(new JVMInstruction(JavaOpcodes.IMUL));
        newSequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        newSequence.add(new JVMInstruction(JavaOpcodes.IMUL));

        String input = "./out/production/gpoptimizer-tests/ru/ifmo/gpo/tests/java/PatcherTest$TestClassForPatcher.class";
        String outputDir = "./out/test/gpoptimizer-tests/";
        String outputFile = "./out/test/gpoptimizer-tests/ru/ifmo/gpo/tests/java/PatcherTest$TestClassForPatcher.class";
        PatternItem item = new PatternItem(new PatternItem.SourceFileInfo("ru/ifmo/gpo/tests/java/PatcherTest$TestClassForPatcher", "f", "(I)I", 0), newSequence);
        item.setOptimized(newSequence);
        PatternItemStorage.instance().addItem(item);

        Patcher patcher = new Patcher();
        patcher.patchClass(input, outputDir);

        BufferedInputStream iStream = new BufferedInputStream(new FileInputStream(outputFile));
        byte[] b = new byte[iStream.available()];
        int read = iStream.read(b);
        iStream.close();
        Class patchedClass = defineClass("ru.ifmo.gpo.tests.java.PatcherTest$TestClassForPatcher", b, 0, read);
        Method method = patchedClass.getMethod("f", int.class);
        int rz = (Integer) method.invoke(null, 5);
        assert rz == 125;

        File f = new File(outputFile);
        f.deleteOnExit();
    }
}
