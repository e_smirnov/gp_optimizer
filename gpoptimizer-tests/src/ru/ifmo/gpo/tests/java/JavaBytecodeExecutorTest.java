package ru.ifmo.gpo.tests.java;

import org.jgap.Configuration;
import org.jgap.InvalidConfigurationException;
import org.jgap.UnsupportedRepresentationException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.objectweb.asm.Type;
import ru.ifmo.gpo.core.*;
import ru.ifmo.gpo.core.instructions.InstructionSequence;
import ru.ifmo.gpo.java.CodeExecutionEnvironment;
import ru.ifmo.gpo.java.JavaExecutionResult;
import ru.ifmo.gpo.java.JavaTargetArchitecture;
import ru.ifmo.gpo.java.instructions.JVMInstruction;
import ru.ifmo.gpo.java.instructions.JavaOpcodes;
import ru.ifmo.gpo.java.vmstate.JVMStateSchema;
import ru.ifmo.gpo.java.vmstate.LocalVariableSlot;

/**
 * Created by IntelliJ IDEA.
 * User: jedi-philosopher
 * Date: 17.01.2010
 * Time: 17:48:18
 */
public class JavaBytecodeExecutorTest {


    static GPConfiguration conf;

    @Before
    public void setConf() throws InvalidConfigurationException {
        Configuration.reset();
        conf = new MainIslandGPConfiguration(new JavaTargetArchitecture());
    }

    @After
    public void resetConf() {
        conf = null;
    }

    private boolean almostEqual(float expected, float actual) {
        return Math.abs(expected - actual) < 0.0001f;
    }

    @Test
    public void testCodeExecute() throws InvalidConfigurationException, UnsupportedRepresentationException {

        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.IMUL));
        JVMStateSchema schema = new JVMStateSchema(sequence);

        ICodeGenerator gen = conf.getTargetArchitecture().getCodeGenerator();
        ICodeGenerationResult code = gen.generateSandboxedCode(sequence, schema, null);

        ICodeExecutor executor = conf.getTargetArchitecture().getCodeExecutor();
        ICodeExecutionEnvironment env = code.createExecutionEnvironment();
        JavaExecutionResult res = (JavaExecutionResult) executor.executeCode(code, env);

        Assert.assertEquals(1, res.getOperandsStack().size());
        Assert.assertEquals(Type.INT, res.getOperandsStack().get(0).type);
        Assert.assertEquals(res.getOperandsStack().get(0).item, (Integer) ((CodeExecutionEnvironment) env).getArgumentValues()[0] * (Integer) ((CodeExecutionEnvironment) env).getArgumentValues()[0]);

        res.dispose();
    }

    @Test
    public void testCodeExecuteWithLocalVariableWrite() {
        //x + 4, 1 -> 2
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.ICONST_2));
        sequence.add(new JVMInstruction(JavaOpcodes.DUP));
        sequence.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(1)));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(1)));
        sequence.add(new JVMInstruction(JavaOpcodes.IMUL));
        sequence.add(new JVMInstruction(JavaOpcodes.IADD));
        JVMStateSchema schema = new JVMStateSchema(sequence);

        ICodeGenerator gen = conf.getTargetArchitecture().getCodeGenerator();
        ICodeGenerationResult code = gen.generateSandboxedCode(sequence, schema, null);

        ICodeExecutor executor = conf.getTargetArchitecture().getCodeExecutor();
        ICodeExecutionEnvironment env = code.createExecutionEnvironment();
        JavaExecutionResult res = (JavaExecutionResult) executor.executeCode(code, env);

        assert res.getOperandsStack().size() == 2; //1 var, 1 stack
        int param = (Integer) ((CodeExecutionEnvironment) env).getArgumentValues()[0];
        assert res.getOperandsStack().get(0).item.equals(param + 4);
        assert res.getOperandsStack().get(1).item.equals(2);

        res.dispose();
    }

    @Test
    public void testCodeExecuteWith2TypesAndLocalWrite() {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
        sequence.add(new JVMInstruction(JavaOpcodes.I2F));
        sequence.add(new JVMInstruction(JavaOpcodes.FLOAD, new LocalVariableSlot(1)));
        sequence.add(new JVMInstruction(JavaOpcodes.FDIV));
        sequence.add(new JVMInstruction(JavaOpcodes.DUP));
        sequence.add(new JVMInstruction(JavaOpcodes.FSTORE, new LocalVariableSlot(1)));
        JVMStateSchema schema = new JVMStateSchema(sequence);

        ICodeGenerator gen = conf.getTargetArchitecture().getCodeGenerator();
        ICodeGenerationResult code = gen.generateSandboxedCode(sequence, schema, null);

        ICodeExecutor executor = conf.getTargetArchitecture().getCodeExecutor();
        ICodeExecutionEnvironment env = code.createExecutionEnvironment();
        JavaExecutionResult res = (JavaExecutionResult) executor.executeCode(code, env);

        try {
            int intParam = (Integer) ((CodeExecutionEnvironment) env).getArgumentValues()[0];
            float floatParam = (Float) ((CodeExecutionEnvironment) env).getArgumentValues()[1];
            assert res.getOperandsStack().size() == 2; //1var, 1stack
            assert almostEqual((float) intParam / floatParam, (Float) res.getOperandsStack().get(0).item);
            assert almostEqual((float) intParam / floatParam, (Float) res.getOperandsStack().get(1).item);
        } finally {
            res.dispose();
        }
    }

    @Test
    public void testExecuteSampleFromBigDecimal() {
        InstructionSequence sequence = new InstructionSequence();
        sequence.add(new JVMInstruction(JavaOpcodes.IREM));
        sequence.add(new JVMInstruction(JavaOpcodes.ISTORE, new LocalVariableSlot(8)));
        sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(8)));

        JVMStateSchema schema = new JVMStateSchema(sequence);

        ICodeGenerator gen = conf.getTargetArchitecture().getCodeGenerator();
        ICodeGenerationResult code = gen.generateSandboxedCode(sequence, schema, null);

        ICodeExecutor executor = conf.getTargetArchitecture().getCodeExecutor();
        ICodeExecutionEnvironment env = code.createExecutionEnvironment();
        JavaExecutionResult res = (JavaExecutionResult) executor.executeCode(code, env); // should not crash here

        Assert.assertNotNull(res);

        res.dispose();
    }

    @Test
    public void testCodeProfile() throws InvalidConfigurationException, UnsupportedRepresentationException {
        InstructionSequence sequence = new InstructionSequence();

        for (int i = 0; i < 2; ++i) {
            sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
            sequence.add(new JVMInstruction(JavaOpcodes.ILOAD, new LocalVariableSlot(0)));
            sequence.add(new JVMInstruction(JavaOpcodes.IMUL));
        }
        sequence.add(new JVMInstruction(JavaOpcodes.POP));

        JVMStateSchema schema = new JVMStateSchema(sequence);
        ICodeGenerator gen = conf.getTargetArchitecture().getCodeGenerator();
        ICodeGenerationResult code = gen.generateProfileCode(sequence, schema, null);

        ICodeExecutor executor = conf.getTargetArchitecture().getCodeExecutor();
        ICodeExecutionEnvironment env = code.createExecutionEnvironment();

        long res = executor.profileCode(code, env);

        assert res > 0;

    }
}
